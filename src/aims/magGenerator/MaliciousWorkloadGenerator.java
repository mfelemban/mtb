/**
 * Created by: Muhamad Felemban 
 * Jun 28, 2017
 */
package aims.magGenerator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;

/**
 * @author mfelemban
 *
 */
public class MaliciousWorkloadGenerator {

	//	private static final Logger LOG = Logger.getLogger(BetterMal.class);
	public static void main(String[] args) throws IOException{
		String file = args[0];
		int mal = Integer.parseInt(args[1]);
		int burstSize = Integer.parseInt(args[2]);
		random(file,mal);
		bursty(file,mal,burstSize);

	}


	public static void random(String file, int numMal) throws IOException{
		ArrayList<TxEntry> workload = solFreeWorkload2(new File(file),numMal);
		
		String malFile = file.substring(0,file.lastIndexOf(".")) + "_m"+numMal+"_uniform.logm"; 
		printToFile(malFile,workload);
	}
	
	public static void bursty(String file, int numMal,int burstSize) throws IOException{
		ArrayList<TxEntry> workload = burstyMalWorkload(new File(file),numMal,burstSize);
		
//		String malFile = file.substring(0,file.lastIndexOf(".")) + "_m"+numMal+"_bursty_" +burstSize+ ".logm";
		String malFile = file.substring(0,file.lastIndexOf(".")) + "_m"+numMal+"_bursty.logm";
		printToFile(malFile,workload);
	}

	public static void printToFile(String file,ArrayList<TxEntry> transactions) throws IOException{
		System.out.println("File " + file + " created");
		BufferedWriter w= new BufferedWriter(new FileWriter(file));
		int counter = 0;
		for(TxEntry t:transactions){
//			t.setID(counter++);
			if(t.type==101 || t.type == 102){
				w.write(t.toString()+";\n");
				//				w.write("101," + t.txID + "," + new Timestamp(System.currentTimeMillis())+ ";\n");
			}else{
				if(t.dest.length!=0 || t.src.length!=0)
					w.write(t.toString()+";\n");	
			}
		}
		w.close();
	}

	public static ArrayList<TxEntry> readTransactions(File file){
		ArrayList<TxEntry> transactions = new ArrayList<TxEntry>();
		try{
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line = "";
			while((line=br.readLine())!=null){
				transactions.add(new TxEntry(line));
			}

			br.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return transactions;
	}

	public static ArrayList<TxEntry> solFreeWorkload(File file,int mal){
		ArrayList<TxEntry> workload = readTransactions(file);
		int gap= (int)((double) workload.size()/(double)mal);
		ArrayList<TxEntry> newWorkload = new ArrayList<TxEntry>();
		for(int i=0;i<mal;i++){
			ArrayList<TxEntry> temp = new ArrayList<TxEntry>();
			temp.add(workload.get(i*gap+0));
			TxEntry malTx = workload.get(i*gap+0);
			malTx.setMalicious();
			for(int j=1;j<gap;j++){
				temp.add(workload.get(i*gap+j));
			}
			newWorkload.addAll(temp);
		}
		return newWorkload;
	}
	
	/*
	 * Generate malicious workload as the following:
	 * 1- Picks a type 3 (distribute transaction) as malicious, 
	 * change its source id to 0 (to prevent the malicious from being affected 
	 * by other malicious transactions). and remove it from list 
	 * 3- Then, iterates over all remaining transactions, and picks the transactions
	 * that their src interect with dest ids of the malicious transaction (followers)
	 * 4- if the number of followers less than the gap (i.e. Number of Malicious Tx/Number of total Tx)
	 * , then randomly pick transactions to fill the gap
	 * 5- Repeat until you pick m number of malicious transactions
	 * 
	 */
	public static ArrayList<TxEntry> solFreeWorkload2(File file,int mal){
		ArrayList<TxEntry> workload = readTransactions(file);
		int gap= (int)((double) workload.size()/(double)mal);
		ArrayList<TxEntry> newWorkload = new ArrayList<TxEntry>();
		for(int i=0;i<mal;i++){
			Iterator<TxEntry> iter = workload.iterator();
			if(!iter.hasNext())
				break;
			TxEntry nextMal = iter.next();
			while(nextMal.type!=3 && iter.hasNext())
				nextMal = iter.next();
//			System.out.println("Was " + nextMal.toString());
			long[] malWrite= ArrayUtils.toPrimitive(nextMal.dest);
			nextMal.setMalicious();
			nextMal.src = new Long[]{(long)0};
			nextMal.type = 3;
			if(nextMal.dest.length<2){
				nextMal.addDest(0);
			}
//			System.out.println("Now " + nextMal.toString());
			newWorkload.add(nextMal);
			iter.remove();
			int gapCounter = 0;
			Set<Long> used = new HashSet<Long>();
			iter = workload.iterator();
			while(iter.hasNext()){
				TxEntry temp = iter.next();
				if(used.contains(temp.txID))
					continue;
				long[] tempRead = ArrayUtils.toPrimitive(temp.src);
				long[] inter = intersection(tempRead,malWrite);
				if(inter.length>0){
					newWorkload.add(temp);
					used.add(temp.txID);
					iter.remove();
					gapCounter++;
				}
				if(gapCounter>=gap)
					break;
			}
			iter = workload.iterator();
			while(gapCounter<gap && iter.hasNext()){
				TxEntry temp = iter.next();
				iter.remove();
				newWorkload.add(temp);
				gapCounter++;
			}
		}
		return newWorkload;
	}
	
	public static ArrayList<TxEntry> burstyMalWorkload(File file,int mal,int burstSize){
		ArrayList<TxEntry> workload = readTransactions(file);
		int gap= (int)((double) workload.size()/(double)mal)*burstSize;
		ArrayList<TxEntry> newWorkload = new ArrayList<TxEntry>();

		for(int i=0;i<mal;i=i+burstSize){
			Iterator<TxEntry> iter = workload.iterator();
			if(!iter.hasNext())
				break;
			TxEntry nextMal = iter.next();
			int counter = 0;
			int gapCounter = 0;
			long[] malWrite = null;
			while(iter.hasNext() && counter<burstSize){
				nextMal = iter.next();
				malWrite=ArrayUtils.addAll(malWrite, ArrayUtils.toPrimitive(nextMal.dest));
				nextMal.setMalicious();
				nextMal.src = new Long[]{(long)0};
				nextMal.type = 3;
				if(nextMal.dest.length<2){
					nextMal.addDest(0);
				}
				newWorkload.add(nextMal);
				iter.remove();
				counter++;
			}
//			System.out.println("Was " + nextMal.toString());
			Set<Long> used = new HashSet<Long>();
			iter = workload.iterator();
			while(iter.hasNext()){
				TxEntry temp = iter.next();
				if(used.contains(temp.txID))
					continue;
				long[] tempRead = ArrayUtils.toPrimitive(temp.src);
				long[] inter = intersection(tempRead,malWrite);
				if(inter.length>0){
					newWorkload.add(temp);
					used.add(temp.txID);
					iter.remove();
					gapCounter++;
				}
				if(gapCounter>=gap)
					break;
			}
			iter = workload.iterator();
			while(gapCounter<gap && iter.hasNext()){
				TxEntry temp = iter.next();
				iter.remove();
				newWorkload.add(temp);
				gapCounter++;
			}
		}
		return newWorkload;
	}
	
	public static long[] intersection(long[] a, long[] b) {
	    return Arrays.stream(a)
	            .filter(x -> Arrays.stream(b)
	                    .anyMatch(y -> y == x)
	            )
	            .toArray();
	}
}