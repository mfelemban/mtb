/**
 * Created by: Muhamad Felemban 
 * Apr 12, 2017
 */
package aims.magGenerator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 * @author mfelemban
 *
 */
public class PoissonArrivals {
	public static void main(String[] args) throws InterruptedException, IOException{
		try{
			double[] lambdas = {0.03};
//			double[] lambdas = {0.06};
			for(double lambda:lambdas){
				BufferedWriter bw = new BufferedWriter(new FileWriter("workload/uni_l" + (lambda*1000) + ".txt"));
				for(int i=0;i<200000;i++){
					// 0.1, 0.5,0.01,0.02,0.04 
					// 10ms , 50ms, 100ms, 200ms, 400ms
					// 0.01 0.05 0.1 0.2 0.4
					// 100 20 10 5 2.5
					double next =genExp(lambda);
					bw.write(next+"\n");
				}


				bw.close();
			}
		}catch(Exception e){

		}

		// 0.02,0.04,0.05,0.08
		// 20,40,60,80

//		double[] lambda = {0.01,0.02,0.03,0.04,0.06,0.08};
//		int mal = 50;
//		for(double d:lambda)
//			generateCompounded(d,10,mal);
	}

	public static double genExp(double lambda){
		Random rand = new Random();
		return  Math.log(1-rand.nextDouble())/(-lambda);
	}



}
