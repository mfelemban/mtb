/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.magGenerator;

import java.sql.Timestamp;

public class TxEntry {

	int type;
	public long txID;
	Long[] src,dest;
	double alpha;
	public Timestamp timestamp;
	int malicious=0;
	int worker;
	
	public TxEntry(String line){
		super();
		String[] tokens = line.split(",");
		this.type = Integer.parseInt(tokens[0]);
		this.txID = Long.parseLong(tokens[1]);
		this.src = parseArray(tokens[2]);
		this.dest = parseArray(tokens[3]);
		this.alpha = Double.parseDouble(tokens[4]);
		this.timestamp = new Timestamp(System.currentTimeMillis());
	}
	

	public Long[] parseArray(String v){
		String[] vs = v.split(":");
		Long[] dmn = new Long[vs.length];
		int i=0;
		for(String s:vs){
			dmn[i++] = Long.parseLong(s);
		}
		return dmn;
	}
	
	public TxEntry(int type,long txID,Timestamp timestamp){
		this.type=type;
		this.txID = txID;
		this.timestamp=timestamp;
	}
	
	public TxEntry(int type, long txID, Long[] src, Long[] dest,
			double alpha,Timestamp timestamp) {
		super();
		this.type = type;
		this.txID = txID;
		this.src = src;
		this.dest = dest;
		this.alpha = alpha;
		this.timestamp = timestamp;
	}

	public String toString(){
		if(type!=102)
			return type+","+txID+","+printArray(src)+","+printArray(dest)+","+alpha+","+timestamp.toString()+","+malicious;
//			return txID+"";
		else 
			return type+","+txID+"," + timestamp.toString();
//		return txID+" ";
		
	}
	
	public void addDest(long a){
		Long[] newDest = new Long[dest.length+1];
		int i=0;
		for(i=0;i<dest.length;i++){
			newDest[i] = dest[i];
		}
		newDest[i] = a;
		this.dest = newDest;
	}
	
	public void setMalicious(){
		malicious = 1;
	}
	
	public void setID(long txid){
		this.txID = txid;
	}

	public String printArray(Long[] a){
		String output = "";
		for(int i=0;i<a.length;i++){
			if(i<a.length-1)
				output += a[i]+":";
			else
				output += a[i];
		}
		return output;	
	}


}



