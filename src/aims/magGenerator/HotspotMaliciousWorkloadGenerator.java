/**
 * Created by: Muhamad Felemban 
 * Jun 28, 2017
 */
package aims.magGenerator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;

/**
 * @author mfelemban
 *
 */
public class HotspotMaliciousWorkloadGenerator {

	//	private static final Logger LOG = Logger.getLogger(BetterMal.class);
	public static void main(String[] args) throws IOException{
		String file = args[0];
		int mal = Integer.parseInt(args[1]);
		random(file,mal);

	}


	public static void random(String file, int numMal) throws IOException{
		ArrayList<TxEntry> workload = hotspotWorkload(new File(file),numMal);
		
		String malFile = file.substring(0,file.lastIndexOf(".")) + "_m"+numMal+".logm"; 
		printToFile(malFile,workload);
	}

	public static void printToFile(String file,ArrayList<TxEntry> transactions) throws IOException{
		System.out.println("File " + file + " created");
		BufferedWriter w= new BufferedWriter(new FileWriter(file));
		int counter = 0;
		for(TxEntry t:transactions){
			t.setID(counter++);
			if(t.type==101 || t.type == 102){
				w.write(t.toString()+";\n");
				//				w.write("101," + t.txID + "," + new Timestamp(System.currentTimeMillis())+ ";\n");
			}else{
				if(t.dest.length!=0 || t.src.length!=0)
					w.write(t.toString()+";\n");	
			}
		}
		w.close();
	}

	public static ArrayList<TxEntry> readTransactions(File file){
		ArrayList<TxEntry> transactions = new ArrayList<TxEntry>();
		try{
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line = "";
			while((line=br.readLine())!=null){
				transactions.add(new TxEntry(line));
			}

			br.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return transactions;
	}

	public static ArrayList<TxEntry> hotspotWorkload(File file,int mal){
		Map<Long,Integer> txCost = new HashMap<Long,Integer>();
		try {
			BufferedReader br = new  BufferedReader(new FileReader("workload/hotspot/tx1000/0.5/workload_0.5_1.0.txhot"));
			String line;
			while((line=br.readLine())!=null){
				String[] tkns = line.split(";");
				long txid = Long.parseLong(tkns[0].split(":")[1].trim());
				int cost = Integer.parseInt(tkns[2].split(":")[1].trim());
				txCost.put(txid, cost);
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(txCost.toString());
		ArrayList<TxEntry> workload = readTransactions(file);
		int gap= (int)((double) workload.size()/(double)mal);
		ArrayList<TxEntry> newWorkload = new ArrayList<TxEntry>();
		for(int i=0;i<mal;i++){
			Iterator<TxEntry> iter = workload.iterator();
			TxEntry nextMal = iter.next();
			long[] malWrite= ArrayUtils.toPrimitive(nextMal.dest);
			nextMal.setMalicious();
			nextMal.src = new Long[]{(long)0};
			nextMal.type = 3;
			nextMal.addDest(0);
			if(nextMal.dest.length<2){
				
			}
			newWorkload.add(nextMal);
			iter.remove();
			int gapCounter = 0;
			Set<Long> used = new HashSet<Long>();
			while(iter.hasNext()){
				TxEntry temp = iter.next();
				if(used.contains(temp.txID))
					continue;
				long[] tempRead = ArrayUtils.toPrimitive(temp.src);
				long[] inter = intersection(tempRead,malWrite);
				if(inter.length>0){
					newWorkload.add(temp);
					used.add(temp.txID);
					iter.remove();
					gapCounter++;
				}
				
				if(gapCounter>=gap)
					break;
			}
			
		}
		return newWorkload;
	}
	
	public static long[] intersection(long[] a, long[] b) {
	    return Arrays.stream(a)
	            .filter(x -> Arrays.stream(b)
	                    .anyMatch(y -> y == x)
	            )
	            .toArray();
	}

}
