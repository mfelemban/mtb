/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.magGenerator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class StatisticsGenerator {

	public static void main(String[] args){
//		StatisticsGenerator.phase1Stat("workload/pool_tiny/sol/s1_1.5_s2_1.0_2_efa.sol","workload/pool_tiny/txsol/s1_1.5_s2_1.0_2_efa.txsol","workload/pool_tiny/s1_1.5_s2_1.0_5.logm","workload/pool_tiny/s1_1.5_s2_1.0_5.winfo");
		StatisticsGenerator.phase1Stat(args[0],args[1],args[2],args[3],Integer.parseInt(args[4]));
	}
	public static void phase1Stat(String solFile, String txSolFile, String script, String outputFile,int k){
		try{
			BufferedReader brSol = new BufferedReader(new FileReader(solFile));
			BufferedReader brTxSol = new BufferedReader(new FileReader(txSolFile));
			BufferedReader brScript = new BufferedReader(new FileReader(script));

			Map<Integer,Set<Integer>> sol = new HashMap<Integer,Set<Integer>>();
			Map<Integer,Set<Integer>> txSol = new HashMap<Integer,Set<Integer>>();

			String line;
			// read sol file
			while((line = brSol.readLine())!=null){
				String[] tkns = line.split(",");
				Integer tpl = Integer.parseInt(tkns[0]);
				Integer ib = Integer.parseInt(tkns[2]);
				Set<Integer> temp;
				if(sol.containsKey(tpl)){
					temp = sol.get(tpl);
					temp.add(ib);
				}else{
					temp = new HashSet<Integer>();
					temp.add(ib);
				}
				sol.put(tpl, temp);
			}

			while((line = brTxSol.readLine())!=null){
				String[] tkns = line.split(",");
				Integer tpl = Integer.parseInt(tkns[0]);
				Integer ib = Integer.parseInt(tkns[1]);
				Set<Integer> temp;
				if(txSol.containsKey(tpl)){
					temp = sol.get(tpl);
					temp.add(ib);
				}else{
					temp = new HashSet<Integer>();
					temp.add(ib);
				}
				txSol.put(tpl, temp);
			}

			Map<Integer,Set<Integer>> scSol = new HashMap<Integer,Set<Integer>>();
			Map<Integer,Set<Integer>> scTxSol = new HashMap<Integer,Set<Integer>>();
			while((line = brScript.readLine())!=null){
				String[] tkns = line.split(",");
				int type = Integer.parseInt(tkns[0]);
				if(type ==101)
					continue;
				int txid = Integer.parseInt(tkns[1]);
				if(txSol.containsKey(txid))
					scTxSol.put(txid, txSol.get(txid));

				String[] src = tkns[2].split(":");
				for(String val:src){
					int tVal = Integer.parseInt(val);
					if(sol.containsKey(tVal)){
						scSol.put(tVal, sol.get(tVal));
					}

				}

				String[] dest = tkns[3].split(":");
				for(String val:dest){
					int tVal = Integer.parseInt(val);
					if(sol.containsKey(tVal)){
						scSol.put(tVal, sol.get(tVal));
					}
				}
			}

//			System.out.println(scSol.toString());
//			System.out.println(scTxSol.toString());

			brSol.close();
			brTxSol.close();
			brScript.close();

			int[] solHist = extractHist(scSol,k,1);
			int[] txSolHist = extractHist(scTxSol,k,1);
			int[] boHist = extractBoHist(scSol,k,1);
			
//			System.out.println(Arrays.toString(solHist));
//			System.out.println(Arrays.toString(txSolHist));
			

			BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
			bw.write("Transaction Distribution:\n " + Arrays.toString(txSolHist)+"\n");
			bw.write("Tuples Distribution:\n " + Arrays.toString(solHist)+"\n");
			bw.write("Boundary Distribution:\n " + Arrays.toString(boHist)+"\n");

			bw.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public static int[] convertToArray(Map<Integer,Integer> hist,int k,int mode){
		int[] h = new int[k];
		for(Integer t:hist.keySet()){
			if(mode ==1)
				h[t-1] = hist.get(t);
			else
				h[t] = hist.get(t);
		}

		return h;
	}

	public static int[] extractHist(Map<Integer,Set<Integer>> sol,int k,int mode){
		Map<Integer,Integer> hist = new HashMap<Integer,Integer>();
		for(Map.Entry<Integer, Set<Integer>> entry:sol.entrySet()){
			Set<Integer> values = entry.getValue();
			for(Integer t:values){
				if(hist.containsKey(t)){
					hist.put(t, hist.get(t)+1);
				}else{
					hist.put(t, 1);
				}
			}
		}
		return convertToArray(hist,k,mode);
	}

	public static int[] extractBoHist(Map<Integer,Set<Integer>> sol,int k,int mode){
		Map<Integer,Integer> hist = new HashMap<Integer,Integer>();
		for(Map.Entry<Integer, Set<Integer>> entry:sol.entrySet()){
			Set<Integer> values = entry.getValue();
			if(values.size()>1){
				for(Integer t:values){
					if(hist.containsKey(t)){
						hist.put(t, hist.get(t)+1);
					}else{
						hist.put(t, 1);
					}
				}
			}
		}
		return convertToArray(hist,k,mode);
	}
}
