/**
 * Created by: Muhamad Felemban 
 * Nov 16, 2017
 */
package aims.magGenerator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import aims.heurestics.Metrics;

/**
 * @author mfelemban
 *
 */
public class TxVariance {
	public static void main(String[] args) throws IOException{
		String file = "workload/crazy/tx5000/0.75/0.5.sql";
		BufferedReader br = new BufferedReader(new FileReader(file));
		
		String line = null;
		ArrayList<Integer> sizes = new ArrayList<Integer>();
		while((line=br.readLine())!=null){
			String[] tokens = line.split(",");
			int srcSize = tokens[2].split(":").length;
			int destSize = tokens[3].split(":").length;
			sizes.add(srcSize + destSize);
		}
		
		double var = Metrics.computeSampleVariance(sizes);
		System.out.println("Variance " + var);
	}
}
