/**
 * Created by: Muhamad Felemban 
 * Jul 12, 2017
 */
package aims.magGenerator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Stack;



/**
 * @author mfelemban
 *
 */
public class GenerateTDGwithDistribution {
	Stack<Integer> stack = new Stack<Integer>();
	int n;
	ArrayList<TempTx> all = new ArrayList<TempTx>();
	Map<Integer,Integer> allTx = new HashMap<Integer,Integer>();
	Random rand = new Random();
	int id = 0;
	long tupleID=1;
	int p;
	int segSize =0;
	int max;


	public GenerateTDGwithDistribution(int n,int max){
		this.n = n;
		for(int i=0;i<n;i++){
			int size = rand.nextInt(max);
			allTx.put(i, size);
			TempTx next =new TempTx(i,size);
			tupleID = next.setSize(max,tupleID);
			all.add(next);
		}
	}


	public double generateTransactions(double depFactor){
		for(int j=0;j<all.size();j++){
			TempTx currTx = all.get(j);
//			System.out.print(currTx.id + " to " );
			int toto = j;
			for(int i=0;i<currTx.writeTuples.size();i++){
				if(rand.nextDouble()<depFactor){
					int nextTx = toto+1;
					if(nextTx >= all.size())
						continue;
					TempTx detTx = all.get(nextTx);
//					System.out.print(detTx.id + " " );
					if(currTx.noMore()){
						long tempTuple = new ArrayList<Long>(currTx.writeTuples).get(rand.nextInt(currTx.writeTuples.size()));
						detTx.changeRead(tempTuple);
					}else{
						long tempTuple = new ArrayList<Long>(detTx.readTuples).get(rand.nextInt(detTx.readTuples.size()));
						currTx.changeWrite(tempTuple);
						
					}
				}
			}
			System.out.println();
		}
		return 0;


	}

	public void printToFile(String file) throws IOException{
		ArrayList<TxEntry> txList = new ArrayList<TxEntry>();


		for(TempTx tx:all){
			Long[] src,dest;
			src = new Long[tx.readTuples.size()];
			dest = new Long[tx.writeTuples.size()];
			src = (Long[]) tx.readTuples.toArray(src);
			dest = (Long[]) tx.writeTuples.toArray(dest);
			//					LOG.info(Arrays.toString(src));
			//					LOG.info(Arrays.toString(dest));
			txList.add(new TxEntry(tx.type,tx.id,src,dest,Math.random()*0.001,new Timestamp(System.currentTimeMillis())));

		}

		//		Collections.shuffle(txList);
		System.out.println("File " + file + " created");
		BufferedWriter w= new BufferedWriter(new FileWriter(file));
		for(TxEntry t:txList){
			if(t.type==101){
				w.write(t.toString()+";\n");
				//				w.write("101," + t.txID + "," + new Timestamp(System.currentTimeMillis())+ ";\n");
			}else{
				if(t.dest.length!=0 || t.src.length!=0)
					w.write(t.toString()+";\n");	
			}
		}
		w.close();
	}

	

	public static void main(String[] args) throws IOException{
		File dir=null ;
		String path = "";
		int numTransaction=0;
		double depFactor =0;
		int max = 0;
		boolean noP = true,noL = true, noM = true;
		int txSizeMargin = 1;	// determines the upper bound of tx Size. The larger the marging, the higher the variance of tx sizes
		for(int i=0;i<args.length;i++){
			String arg = args[i];
			i++;
			char option = arg.charAt(1);
			switch(option){
			case 'd':{
				//				dir = new File(args[i]);
				path = args[i];
				break;
			}
			case 'c':{
				depFactor = Double.parseDouble(args[i]);
				break;
			}
			case 't':{
				numTransaction = Integer.parseInt(args[i]);
				break;
			}
			case 'm':{
				max = Integer.parseInt(args[i]);
				noM = false;
				break;
			}
			default: {
				//				System.err.println("Error in arguments");
				//				System.exit(0);
			}
			}
		}


		path  += "/tx" + numTransaction;
		dir = new File(path);

		if (!dir.exists()) {
			System.out.println("creating directory: " + path);
			boolean result = false;

			try{
				dir.mkdirs();
				result = true;
			} 
			catch(SecurityException se){
				//handle it
			}        
			if(result) {    
				System.out.println("DIR created");  
			}
		}

		GenerateTDGwithDistribution tdg = new GenerateTDGwithDistribution(numTransaction,max);			// Number of transactions in each segments (connected components)


		double IBInt = tdg.generateTransactions(depFactor);
//		for(TempTx next: tdg.all)
//			System.out.println(next.toString());
		double totalSize = 0;



		

//		for(TempTx t:tdg.all){
//			t.setSize(txSizeMargin);
//			totalSize += t.size;
//			while(!t.full()){
//				t.addTuple(tdg.tupleID++);
//			}
//			//			System.out.println(t.toString());
//		}

		System.out.println("Total number of tuples:"+(tdg.tupleID-1));
		System.out.println("Average tx size: " + totalSize/(double) numTransaction);
		tdg.printToFile(dir+"/"+ depFactor +"_" + max+ ".sql");
		//		tdg.printRepeatedToFile(dir+"/workload_"+ localPerc + ".sql");
		try{
			BufferedWriter bw = new BufferedWriter(new FileWriter(dir+"/README_workload_"+  depFactor ));
			bw.write("-c " + depFactor + " -m " + max + " -t " + numTransaction  + " -d " + dir 
					+ " -g " + txSizeMargin );
			bw.write("NumTransactions: " +numTransaction+"\n");
			bw.write("DepFactor: " + depFactor+"\n");
			bw.write("segSize:" + tdg.segSize+"\n");
			bw.write("txSizeMargin:" + txSizeMargin+"\n");
			bw.write("AvgTxSize:" + (totalSize/(double) numTransaction)+"\n");
			bw.write("AvgIBIntensity:" + IBInt+"\n");
			bw.write("Total number of tuples: " + tdg.tupleID);
			bw.close();
		}catch(Exception e){

		}

	}



}
