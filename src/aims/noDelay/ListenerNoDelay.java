/**
 * Created by: Muhamad Felemban 
 * Jun 30, 2017
 */
package aims.noDelay;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author mfelemban
 *
 */
//class Listener extends Thread {
class ListenerNoDelay extends Thread{

	protected Connection conn;
	protected org.postgresql.PGConnection pgconn;

	ListenerNoDelay(Connection conn, String message) throws SQLException {
		this.conn = conn;
		this.pgconn = (org.postgresql.PGConnection)conn;
		Statement stmt = conn.createStatement();
		System.out.println("LISTEN "+ message);
		stmt.execute("LISTEN "+ message);
		stmt.close();
	}
	
	public void run(){
		
	}

}
