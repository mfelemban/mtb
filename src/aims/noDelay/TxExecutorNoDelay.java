/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
package aims.noDelay;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;

import aims.util.QueriesStatements;
import aims.util.Util;

import com.oltpbenchmark.api.BenchmarkModule;
import com.oltpbenchmark.benchmarks.smallworldbank.procedures.Collect;
import com.oltpbenchmark.benchmarks.smallworldbank.procedures.Distribute;
import com.oltpbenchmark.benchmarks.smallworldbank.procedures.MNTransfer;

public class TxExecutorNoDelay implements Runnable {
	private static final Logger LOG = Logger.getLogger(TxExecutorNoDelay.class);
	BenchmarkModule benchmarkModule;
	Map<Integer,Integer> txidMap;
	ActiveTransactionNoDelay at;
	Boolean[] ibTxStatus;
	//	Integer runningTx;
	AtomicInteger runningTx;
	AtomicBoolean waiting,idAlert;
	AtomicInteger succCounter;
	Connection newConn;
	//	long malPrev;
	public TxExecutorNoDelay(ActiveTransactionNoDelay at, BenchmarkModule benchmarkModule,Map<Integer,Integer> txidMap,
			Boolean[] ibTxStatus,AtomicInteger runningTx,AtomicBoolean waiting, AtomicInteger succCounter,
			Connection newConn,AtomicBoolean idAlert){
		this.at = at;
		this.benchmarkModule= benchmarkModule;
		this.txidMap = txidMap;
		this.ibTxStatus = ibTxStatus;
		this.runningTx = runningTx;
		this.waiting = waiting;
		this.succCounter = succCounter;
		this.newConn = newConn;
		this.idAlert = idAlert;
	}

	@SuppressWarnings("deprecation")
	public void run(){
		Timestamp startTimestamp = new Timestamp(System.currentTimeMillis());
		int numberRetried = 0;

		Integer txid = -1;


		ArrayList<QueryNoDelay> tx = at.getTx();
		QueryNoDelay q = tx.get(0);
		Integer currentTxID = tx.get(0).getTxid();
		long[] src = q.getSrc();
		long[] dest = q.getDest();
		double amount = q.getAmount();
		long[] all = Util.mergeArrays(src,dest);
		long[] allOids = new long[all.length];
		long[] srcOids = new long[src.length];
		long[] destOids = new long[dest.length];


		ResultSet rs = null;

		//		at.start();
		int IB = 0;

		try{
			Connection conn = benchmarkModule.makeConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(Connection.TRANSACTION_REPEATABLE_READ);


			IB = at.getIB();
			//			LOG.info("Starting " + currentTxID  + " IB " + IB);

			Statement getOids = conn.createStatement();
			int i=0;

			rs = getOids.executeQuery(QueriesStatements.createGetOIDsStatement(src));
			i=0;
			while(rs.next()){
				srcOids[i++] = rs.getLong(1);
			}
			rs.close();

			getOids = conn.createStatement();
			rs = getOids.executeQuery(QueriesStatements.createGetOIDsStatement(dest));
			i=0;
			while(rs.next()){
				destOids[i++] = rs.getLong(1);
			}
			rs.close();
			allOids = (long[]) ArrayUtils.addAll(srcOids, destOids);
			getOids.close();
			conn.commit();

			at.start();


			numberRetried++;
			int numberOfBlocked =0;
			int numberOfRepairSet=0;
			//			LOG.info("Before ");
			//			printBlockedBO();

			

			if(at.isBoBlocked() )
				at.endBOBlocking(System.currentTimeMillis());	
			//			LOG.info("After ");
			//			printBlockedBO();


			synchronized(ibTxStatus){
				while(ibTxStatus[IB-1]){
					if(!at.isRSet()){
						at.setRSet();
						at.startRsetBlocking(System.currentTimeMillis());
					}
					synchronized(waiting){
						if(waiting.get()){
							try{
								runningTx.notifyAll();
							}catch(Exception e){

							}
						}
					}
					//					LOG.info(currentTxID + " Some recovery is running, going to sleep " );
					ibTxStatus.wait();
				}
				synchronized(runningTx){
					runningTx.incrementAndGet();
				}
			}
			//			LOG.info(currentTxID + " ready to go " );


			Statement checkBlockedTuples = conn.createStatement();
			rs = checkBlockedTuples.executeQuery(QueriesStatements.createCheckBlockedTuples(srcOids)); 
			rs.next();
			numberOfBlocked = rs.getInt(1);
			rs.close();


			Statement checkRepairTable = conn.createStatement();
			ResultSet rs1 = checkRepairTable.executeQuery(QueriesStatements.createRepairTable(srcOids));
			rs1.next();
			numberOfRepairSet = rs1.getInt(1);
			//			LOG.info(currentTxID + " r " + numberOfRepairSet);


			Statement checkTotalBlockedTuples = conn.createStatement();
			rs = checkTotalBlockedTuples.executeQuery(QueriesStatements.createTotalBlockedTuples()); 
			rs.next();
			int totalBlocked = rs.getInt(1);

			Statement checkTotalRepairTuples = conn.createStatement();
			rs = checkTotalRepairTuples.executeQuery(QueriesStatements.getTotalRepairTuples()); 
			rs.next();
			int totalRepair = rs.getInt(1);
			totalBlocked += totalRepair;

			boolean bsetFirst = true,rsetFirst = true;
			boolean bset = false;
			boolean rset = false;
			//			boolean corBlocked = false;

			//				LOG.info(currentTxID + " drty " + numberOfDirtyBoundary + " blkd " + numberOfBlocked + " rpr " + numberOfRepairSet);
			if(numberOfBlocked>0 || numberOfRepairSet > 0){
				if(numberOfRepairSet>0){
					if(!rset){
						//						LOG.info(currentTxID + " have to wait for repair set [" + numberOfBlocked + "," + numberOfRepairSet + "]");
						rset =true;
					}
					if(rsetFirst){
						at.setRSet();
						at.startRsetBlocking(System.currentTimeMillis());
						at.setTuplesToRepair(numberOfRepairSet);
						at.setTotalTuplesBlocked(totalBlocked);
						rsetFirst = false;
					}
				}
				if(numberOfBlocked>0){
					if(!bset){
						//						LOG.info(currentTxID + " have to wait for blocked set [" + numberOfBlocked + "," + numberOfRepairSet + "]");
						bset =true;
					}
					if(bsetFirst){
						at.setBSet();
						at.startBsetBlocking(System.currentTimeMillis());
						at.setTuplesBlocked(numberOfBlocked);
						at.setTotalTuplesBlocked(totalBlocked);
						bsetFirst = false;
					}
				}
				synchronized(runningTx){
					runningTx.decrementAndGet();
					//						LOG.info("Decrement " + currentTxID);
					//						LOG.info("counter " + runningTx.get());
					synchronized(waiting){
						if(waiting.get())
							runningTx.notifyAll();
					}
				}
				Connection lConn = benchmarkModule.makeConnection();
				BRSetListenerNoDelay lstn = new BRSetListenerNoDelay(lConn,srcOids,currentTxID);
				//					BRSetListener lstn = new BRSetListener(conn,srcOids);
				lstn.start();

				lstn.join();
				lConn.close();
				synchronized(ibTxStatus){
					while(ibTxStatus[IB-1]){
						ibTxStatus.wait();
					}
					synchronized(runningTx){
						runningTx.incrementAndGet();
						//							LOG.info("increment " + currentTxID +" "+ runningTx.get());
					}
				}
			}


			conn.commit();
			if(at.isBSet() )
				at.endBsetBlocking(System.currentTimeMillis());		// end blockage time computation
			if(at.isRSet() )
				at.endRsetBlocking(System.currentTimeMillis());		// end blockage time computation


			synchronized(idAlert){
				while(idAlert.get()){
					idAlert.wait();
				}
			}
			//			LOG.info("Executing " + currentTxID + " at IB " + IB + " malicious:" + ((q.isMalicious())? "yes":"no"));


			switch(q.getType()){
			case 2:{
				break;
			}
			case 3: { 
				while(txid==-1){
					txid = new Distribute().isRun(conn, src[0], dest, amount);
				}
				break;
			}
			case 4: {
				while(txid==-1){
					txid = new Collect().isRun(conn, src, dest[0], amount);
				}
				break;
			}
			case 5: {
				while(txid==-1){
					txid= new MNTransfer().isRun(conn, src, dest,amount);
				}
				break;
			}
			}
			at.end();
			if(txid!=-1){
				Statement addMap = conn.createStatement();
				addMap.executeUpdate("insert into transaction_id_map values ("+ txid +"," + currentTxID+ ", clock_timestamp());");
				conn.commit();
				//				LOG.info(currentTxID + " done");

				synchronized(txidMap){
					txidMap.put(q.getTxid(), txid);
					txidMap.notifyAll();
				}
				//					conn.close();
			}else{
				at.endBsetBlocking(System.currentTimeMillis());
				at.endRsetBlocking(System.currentTimeMillis());
				conn.rollback();
				LOG.info(currentTxID + " is rolledback");
				//				for(long oid:allOids){
				
			}

			synchronized(runningTx){
				runningTx.decrementAndGet();
				synchronized(waiting){
					if(waiting.get()){
						runningTx.notifyAll();
					}
				}
			}

			synchronized(succCounter){
				succCounter.incrementAndGet();
				succCounter.notify();

			}




			Timestamp endTimestamp = new Timestamp(System.currentTimeMillis());
			startTimestamp.setHours(startTimestamp.getHours()+4);
			endTimestamp.setHours(endTimestamp.getHours()+4);

			PreparedStatement addTimeInfo = conn.prepareStatement("insert into availability_info values (" + currentTxID + "," + txid + "," + ((q.isMalicious())? 1:0) + ",'" + startTimestamp +"','" 
					+ endTimestamp+ "', " + at.getRunTime()+"," + at.getTotalRunTime() +","+ at.getBsetBlockageTime() +","+ at.getRsetBlockageTime() 
					+ "," +  at.getBOBlockageTime()  + "," +((at.boBlocked)? 1:0 ) +","+((at.isBSet())? 1:0 )
					+","+((at.isRSet())? 1:0 ) + ", (select count( distinct object_id) from log_table where transaction_id="+txid+ "),"
					+at.getTuplesBlocked() + "," +at.getTuplesToRepair()+"," +at.getTotalBlockedTuples()+","+IB+","+numberRetried+");");
			addTimeInfo.executeUpdate();
			//			LOG.info(txid + " retried " + numberRetried);


			ArrayList<Long> toBeAdded = new ArrayList<Long>();
			

			String stmt = "insert into touched_boundary_objects values ";
			for( i=0;i<allOids.length;i++){
				stmt += "(" + currentTxID  + "," + allOids[i] +"),";
			}
			stmt = stmt.substring(0,stmt.length()-1) + ";";
			PreparedStatement ps = conn.prepareStatement(stmt);
			ps.executeUpdate();


			conn.commit();
			conn.close();

			//			LOG.info("Finishing " + currentTxID  + " IB " + IB);
			BRSetNorifierNoDelay notifier = new BRSetNorifierNoDelay(newConn);
			notifier.start();

		}
		catch(Exception e){
			e.printStackTrace();
		}


	}

	

	public  boolean containBlocked(Long[] src, List<Long> blockedTuples){
		boolean contain  = false;
		if(blockedTuples.isEmpty())
			return contain;
		for(Long id:src){
			if(blockedTuples.contains(id))
				return true;
		}
		return contain;
	}

	/* (non-Javadoc)
	 * @see java.util.concurrent.Callable#call()
	 */

}