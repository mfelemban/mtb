/**
 * Created by: Muhamad Felemban 
 * Jun 30, 2017
 */
package aims.noDelay;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author mfelemban
 *
 */
public class BRSetNorifierNoDelay extends Thread{
	Connection conn;
	public BRSetNorifierNoDelay(Connection conn){
		this.conn=conn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			if(!conn.isClosed()){
				Statement stmt = conn.createStatement();
				stmt.execute("NOTIFY BRSet");
				stmt.close();
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		} 
	}

}
