/**
 * Created by: Muhamad Felemban 
 * Jun 30, 2017
 */
package aims.noDelay;

import java.sql.Connection;

/**
 * @author mfelemban
 *
 */
class NotifierNoDelay {

	protected Connection conn;

	public NotifierNoDelay(Connection conn) {
		this.conn = conn;
	}

}
