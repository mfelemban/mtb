/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
package aims.noDelay;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import aims.util.FairLock;

import com.oltpbenchmark.api.BenchmarkModule;


public class RecoverNoDelay implements Callable<Boolean>{
	private static final Logger LOG = Logger.getLogger(aims.syncooo.Recover.class);
	long txid;
	private boolean success=false;
	private Timestamp ts;
	BenchmarkModule benchmarkModule;
	Boolean[] ibTxStatus;
	AtomicInteger runningTx;
	AtomicBoolean waiting;
	FairLock fl;
	Connection newConn;
	AtomicBoolean recRunning;
	int ib;
	public RecoverNoDelay(long txid,BenchmarkModule benchmarkModule,Connection newConn,Timestamp ts,
			Boolean[] ibTxStatus, AtomicInteger runningTx,AtomicBoolean waiting,FairLock fl,AtomicBoolean recRunning,int ib)
	{
		this.fl = fl;
		this.txid = txid;
		this.benchmarkModule = benchmarkModule;
		this.ts = ts;
		this.ibTxStatus = ibTxStatus;
		this.runningTx = runningTx;
		this.waiting = waiting;
		this.newConn = newConn;
		this.recRunning = recRunning;
		this.ib= ib;
	}



	public Boolean call() {
		//		boolean locked = getLock();

		try{
			Connection conn = benchmarkModule.makeConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			Integer[] ibs = getIBs(conn,txid);

			fl.lock();

//			LOG.info("Started recovering " + txid);


			ResultSet rs;

			synchronized(ibTxStatus){
				for(int i=0;i<ibs.length;i++)
					ibTxStatus[ibs[i]-1] = true;
				synchronized(recRunning){
					recRunning.set(true);
				}
			}

//			LOG.info("Locked IBs " + txid + "  " + Arrays.toString(ibs));
			
			synchronized(runningTx){
				while(runningTx.intValue()>0){
					synchronized(waiting){
						waiting.set(true);;
					}
					//					LOG.info("Going to sleep counter " + runningTx.intValue() + " waiting is " + waiting);
					runningTx.wait();
					//					LOG.info("Woke up and counter " + runningTx.intValue());
				}

				synchronized(waiting){
					waiting.set(false);;
				}
			}
//			LOG.info("No running Tx. for " + txid );

			

			PreparedStatement ps1 = conn.prepareStatement("select alertMTxn(?,?,?);");
			ps1.setLong(1, txid);
			ps1.setTimestamp(2, ts);
			ps1.setInt(3,ib);
			ps1.execute();

			conn.commit();

			synchronized(ibTxStatus){
				for(int i=0;i<ibs.length;i++)
					ibTxStatus[ibs[i]-1] = false;
				ibTxStatus.notifyAll();
			}

//			LOG.info("OPened IBs " + txid + "  " + Arrays.toString(ibs));

			PreparedStatement time = conn.prepareStatement("select current_timestamp + interval '5 hours';");
			ResultSet rsTime = time.executeQuery();
			rsTime.next();
			Timestamp ts1 = rsTime.getTimestamp(1);

			PreparedStatement txID = conn.prepareStatement("select txid_current();");
			ResultSet txRS = txID.executeQuery();
			txRS.next();
			long tx = txRS.getLong(1);
			PreparedStatement releaseStatement = conn.prepareStatement("update blocked_tuples_table set (recovery_timestamp,recovering_transaction) = (?,?) "
					+ "where malicious_transaction = ? and transaction_id not in (select transaction_id from corrupted_transactions_table where malicious_transaction = ? )");
			releaseStatement.setTimestamp(1, ts1);
			releaseStatement.setLong(2, tx);
			releaseStatement.setLong(3, txid);
			releaseStatement.setLong(4, txid);
			releaseStatement.executeUpdate();

			releaseStatement = conn.prepareStatement("select update_status_table(?)");
			releaseStatement.setLong(1,txid);
			releaseStatement.executeQuery();

			conn.commit();


			// undo all affected except the malicious transaction 
			boolean good = false;
			while(!good){
				try{
					txID = conn.prepareStatement("select txid_current();");
					txRS = txID.executeQuery();
					txRS.next();
					tx = txRS.getLong(1);
					//					LOG.info("Doing undo " + tx);

					ps1 = conn.prepareStatement("with temp as (select distinct on (object_id) transaction_id, object_id "
							+ "from temp_log_table where transaction_id = ? and reference_txn = ? and object_id not in ("
							+ "select tuple_id from hotspot_backup)"
							+ " and operation = 3 order by object_id,time_stamp) "
							+ "update checking as ch set balance = cc.column_a from "
							+ "(select c.old_balance, c.tuple_id from checking_backup c, temp t where t.object_id = c.tuple_id and c.mod_transaction = t.transaction_id) as cc(column_a, column_b) "
							+ "where cc.column_b = ch.oid");
					ps1.setLong(1, txid);
					ps1.setLong(2, txid);
					ps1.execute();

					ps1 = conn.prepareStatement("with temp as (select distinct on (object_id) transaction_id, object_id "
							+ "from temp_log_table where transaction_id in (select transaction_id from corrupted_transactions_table "
							+ "where malicious_transaction =? and status = 'affected') and object_id not in ("
							+ "select object_id from temp_log_table where transaction_id =? and reference_txn = ?"
							+ ") and operation = 3 order by object_id,time_stamp) "
							+ "update checking as ch set balance = cc.column_a from "
							+ "(select c.old_balance, c.tuple_id from checking_backup c, temp t where t.object_id = c.tuple_id and c.mod_transaction = t.transaction_id) as cc(column_a, column_b) "
							+ "where cc.column_b = ch.oid");


					ps1.setLong(1, txid);
					ps1.setLong(2, txid);
					ps1.setLong(3, txid);
					ps1.execute();
					good = true;
				}catch(Exception e){
					LOG.info("Something wrong in Undo " + txid);
					e.printStackTrace();
					conn.rollback();
				}
			}



			ps1 = conn.prepareStatement("update blocked_tuples_table set (recovery_timestamp,recovering_transaction) = (clock_timestamp() + interval '5 hours',txid_current()) "
					+ "where transaction_id = ?;");
			ps1.setLong(1, txid);
			ps1.execute();



//						LOG.info(txid + "undo complete");

			conn.commit();


			ps1 = conn.prepareStatement("select transaction_id from corrupted_transactions_table "
					+ "where status = 'affected' and malicious_transaction = ? order by transaction_id asc");
			ps1.setLong(1, txid);
			rs = ps1.executeQuery();
			ArrayList<Long> affectedTx = new ArrayList<Long>();
			while(rs.next()){
				affectedTx.add(rs.getLong(1));
			}
			conn.commit();
			for(Long aTx:affectedTx){
				good = false;
				txID = conn.prepareStatement("select txid_current();");
				txRS = txID.executeQuery();
				txRS.next();
				tx = txRS.getLong(1);
//					LOG.info("## Redoing Tx " + aTx + " by " + tx);
				while(!good){
					try{
						ps1 = conn.prepareStatement("select redo_proc(?)");
						ps1.setLong(1, aTx);
						ps1.executeQuery();
						//																		LOG.info("## Redone Tx " + aTx);
						conn.commit();												
						good = true;
					}catch(SQLException e){	
						e.printStackTrace();
						conn.rollback();
						LOG.info("************* Deadlock at " + txid + " recovery ");
					}
				}
				BRSetNorifierNoDelay notifier = new BRSetNorifierNoDelay(newConn);
				notifier.start();

			}
//			LOG.info("Redo " + txid  + " completed");
			Thread.sleep(1000);
			ps1 = conn.prepareStatement("select mal_trg_ends(?);");
			ps1.setLong(1, txid);
			ps1.executeQuery();
			success = true;


			if(success){

				conn.commit();
//				LOG.info("--- Tx " + txid + " Recovered ");
				fl.unlock();
			}else{
				conn.rollback();
			}

			synchronized(recRunning){
				recRunning.set(false);
				recRunning.notifyAll();
			}

			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		BRSetNorifierNoDelay notifier = new BRSetNorifierNoDelay(newConn);
		notifier.start();
		
//		synchronized(BO){
//			BO.notifyAll();
//		}

		return success;
	}

	public Integer[] getIBs(Connection conn,long currentTxId){
		Integer[] tempIB=null;
		try{
			Statement getIBs = conn.createStatement();
			//			ResultSet rs = getIBs.executeQuery("select ib from transactions_ib where transaction_id=" + currentTxId);
			ResultSet rs = getIBs.executeQuery("select distinct i.ib from log_table l,ibd i where transaction_id ="+ currentTxId +" and i.object_id = l.object_id;");
			ArrayList<Integer> ibs = new ArrayList<Integer>();
			while(rs.next()){
				ibs.add(rs.getInt(1));
			}
			tempIB = ibs.toArray(new Integer[ibs.size()]);
			conn.commit();
		}
		catch(Exception e){
			e.printStackTrace();
			//			LOG.info(e.toString());
		} 

		return tempIB;
	}


	boolean checkIBTxStatus(Integer[] ibs){
		boolean go = false;
		for(int i=0;i<ibs.length;i++){
			if(ibTxStatus[ibs[i]-1])
				return true;
		}
		return go;
	}
}	