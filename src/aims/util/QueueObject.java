/**
 * Created by: Muhamad Felemban 
 * Jul 3, 2017
 */
package aims.util;

/**
 * @author mfelemban
 *
 */
public class QueueObject {

	  private boolean isNotified = false;

	  public synchronized void doWait() throws InterruptedException {
	    while(!isNotified){
	        this.wait();
	    }
	    this.isNotified = false;
	  }

	  public synchronized void doNotify() {
	    this.isNotified = true;
	    this.notify();
	  }

	  public boolean equals(Object o) {
	    return this == o;
	  }
	}
