/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.util;

import java.util.Map;

import com.oltpbenchmark.benchmarks.smallworldbank.SWBankConstants;

public class QueriesStatements {
	
	public static  String checkBlocked(long[] oids){
		String stmt = "SELECT * FROM blocked_tuples_table where blocked_tuples in (";
		for(int i=0;i<oids.length;i++){
			stmt += oids[i] +",";
		}
		stmt = stmt.substring(0,stmt.length()-1) + ") ;";
		return stmt;
	}
	
	public static  String createCheckBlockedTuples(long[] oids){
		String stmt = "SELECT count(distinct blocked_tuples) FROM blocked_tuples_table where blocked_tuples in (";
		for(int i=0;i<oids.length;i++){
			stmt += oids[i] +",";
		}
		stmt = stmt.substring(0,stmt.length()-1) + ") and recovery_timestamp IS NULL;";
		return stmt;
	}
	
	public static  String createCheckBlockedTuples(long[] oids,int ib){
		String stmt = "SELECT count(distinct blocked_tuples) FROM blocked_tuples_table_ib_"+ib+" where blocked_tuples in (";
		for(int i=0;i<oids.length;i++){
			stmt += oids[i] +",";
		}
		stmt = stmt.substring(0,stmt.length()-1) + ") and recovery_timestamp IS NULL;";
		return stmt;
	}
	
	public static  String createTotalBlockedTuples(){
		String stmt = "SELECT count(distinct blocked_tuples) FROM blocked_tuples_table where recovery_timestamp IS NULL;";
		return stmt;
	}
	
	public static  String createTotalBlockedTuples(int ib){
		String stmt = "SELECT count(distinct blocked_tuples) FROM blocked_tuples_table_ib_"+ib+" where recovery_timestamp IS NULL;";
		return stmt;
	}
	
	public static  String createCheckDirtyBoundary(long[] oids,long currentTxID){
		String stmt = "SELECT count(distinct object_id) FROM boundary_objects where object_id in (";
		for(int i=0;i<oids.length;i++){
			stmt += oids[i] +",";
		}
		stmt = stmt.substring(0,stmt.length()-1) + ") and on_hold = 1 and transaction_id != " +  currentTxID + ";";
		return stmt;
	}
	
	public static  String createCheckDirtyBoundary(long[] oids){
		String stmt = "SELECT count(distinct object_id) FROM boundary_objects where object_id in (";
		for(int i=0;i<oids.length;i++){
			stmt += oids[i] +",";
		}
		stmt = stmt.substring(0,stmt.length()-1) + ") and on_hold = 1;";
		return stmt;
	}


//	public static  String createCheckBoundary(long[] oids){
//		String stmt = "SELECT count(distinct object_id) FROM boundary_objects where object_id in (";
//		for(int i=0;i<oids.length;i++){
//			stmt += oids[i] +",";
//		}
//		stmt = stmt.substring(0,stmt.length()-1) + ");";
//		return stmt;
//	}
	
	public static  String holdBoundaries(long[] oids){
		String stmt = "SELECT object_id FROM boundary_objects where object_id in (";
		for(int i=0;i<oids.length;i++){
			stmt += oids[i] +",";
		}
		stmt = stmt.substring(0,stmt.length()-1) + ") for update;";
		return stmt;
	}
	
	
	public static  String createRepairTable(long[] oids){
		String stmt = "SELECT count(distinct object_id) FROM repair_table where object_id in (";
		for(int i=0;i<oids.length;i++){
			stmt += oids[i] +",";
		}
		stmt = stmt.substring(0,stmt.length()-1) + ") and recovery_timestamp is null;";
		return stmt;
	}
	
	public static  String getTotalRepairTuples(){
		String stmt = "SELECT count(distinct object_id) FROM repair_table where recovery_timestamp is null;";
		return stmt;
	}

	public static  String createGetOIDsStatement(long[] src){
		String stmt =  "SELECT oid"
				+ "  FROM " + SWBankConstants.TABLENAME_CHECKING 
				+ " WHERE CHK_ID IN (" ;
		for(int i=0;i<src.length;i++)
			stmt += src[i] + ",";
		stmt = stmt.substring(0,stmt.length()-1) + ");";
		return stmt;
	}

	public static  String createGetIB(long txid){
		String stmt = "Select ib from transactions_ib where transaction_id = " + txid + ";";
		return stmt;
	}
	
	public static String insertIntoBackup(Map<Long, Double> ids){
		String stmt = "insert into checking_backup (select txid_current(), current_timestamp, c.oid, c.chk_id,c.balance, null from checking c where chk_id in (";
		for(Long id:ids.keySet()){
			stmt += id+",";
		}
		
		stmt = stmt.substring(0,stmt.length()-1)+"))";
		return stmt;
	}
	
	public static String updateBackup(Map<Long, Double> ids, long txid){
		String stmt = "UPDATE checking_backup SET ";
		stmt += "old_BALANCE = new_BALANCE + c.column_a" + " from ( values ";
		for (Map.Entry<Long, Double> entry : ids.entrySet()) {
			stmt += "(" + entry.getValue() + "," + entry.getKey() + "),";
		}
		stmt = stmt.substring(0, stmt.length() - 1)
				+ ") as c(column_a, id) where c.id = chk_id and mod_transaction=" + txid;
		return stmt;
	}
}