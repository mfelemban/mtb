/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.util;

import java.util.HashSet;
import java.util.Set;

public class AIMSTransaction {
	Set<Long> readSet;
	Set<Long> writeSet;
	Transaction t;

	AIMSTransaction(int id){
		this.t = new Transaction(id);
		readSet = new HashSet<Long>();
		writeSet = new HashSet<Long>();
	}

	void addRead(Long read){
		readSet.add(read);
	}

	void addObject(long id){
		t.addInternalObject(id);
	}

	void addWrite(Long write){
		writeSet.add(write);
	}

	boolean read(Long o){
		return readSet.contains(o);
	}

	boolean write(Long o){
		return writeSet.contains(o);
	}

	boolean isWriteEmpty(){
		return writeSet.isEmpty();
	}

	boolean isReadEmpty(){
		return readSet.isEmpty();
	}

//	public String toString(){
//		return t.print() + " Read: " + readSet.toString() + "\nWrite: " + writeSet +"\n";
//	}

	Transaction getTx(){
		return t;
	}
}
