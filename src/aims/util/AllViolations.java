/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class AllViolations {
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(args[0]));
		String line= br.readLine();
		Set<Long> shared = new HashSet<Long>();
		int sum = 0;
		while((line=br.readLine())!=null){
			String[] a = line.split(" ");
			for(String tt:a){
				shared.add(Long.parseLong(tt));
			}
			if(a.length>1)
				sum+= a.length-1;
		}
		System.out.println(shared.size());
		System.out.println("All possible edges " + sum);
	}
}
