/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Transaction  {
	private int id;
	private Set<Long> internalObjects = new LinkedHashSet<Long>();
	private Set<Long> sharedObjects = new LinkedHashSet<Long>();
	private List<AIMSObject> objects = new ArrayList<AIMSObject>();
	public Transaction(int id){
		this.id = id;
	}
	
	public int getId(){
		return id;
	}
	
	public void addObject(AIMSObject o){
		objects.add(o);
	}
	
	public void addInternalObject(Long o){
		internalObjects.add(o);
	}
	
	public List<AIMSObject> getObjects(){
		return objects;
	}
	
	public void addSharedObject(Long o){
		sharedObjects.add(o);
	}
	
	/* Shared to internal objects ratio
	 * [0-1) if the shared is less than tuples (i.e. non-overlapping transaction)
	 * [1-inf) more shared than internal (i.e. highly overlapped transaction)
	 */
	
	public double internalRatio(){
		double sharedSize = sharedObjects.size();
		double internalSize = internalObjects.size();
		return internalSize/(internalSize+sharedSize);
	}
	
	public int size(){
		return internalObjects.size() + sharedObjects.size();
	}
	
	public Set<Long> getSharedObjects(){
		return sharedObjects;
	}
	
	public Set<Long> getInternalObjects(){
		return internalObjects;
	}
	
	public double getOutGoingEdges(Map<Long,Set<Long>> E){
		int sum=0;
		for(Long shared:sharedObjects){
			Set<Long> otherEnd = E.get(shared);
			sum += otherEnd.size();
		}
		return sum;
	}
	
	public Set<Integer> getNeighbors(Map<Long, AIMSObject> gO){
		Set<Integer> nbrs = new HashSet<Integer>();
		for(Long s:sharedObjects){
			AIMSObject o = gO.get(s);
			nbrs.addAll(o.getTxSet());
			nbrs.remove(id);
		}
		return nbrs;
	}
		
	public String toString(){
		return "Id: " + id +" shared " + sharedObjects.toString() + " internal " + internalObjects.toString();
	}
	
	
}
