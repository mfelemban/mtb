/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;

public class ComputeAvailibility {
	public static void main(String[] args) throws IOException{
		String fileName = args[0];
		//		String mode= "/aims/";
		//		int[] nums = {11,12};
		//		for(int i=0;i<nums.length;i++)
		run(fileName);
		//		mode = "/dtqr/";
		//		for(int i=0;i<nums.length;i++)
		//			run(fileName+mode+nums[i]);

	}

	public static void run(String fileName) throws IOException{
		BufferedReader timeReader = new BufferedReader(new FileReader(fileName+".t"));
		BufferedReader trReader = new BufferedReader(new FileReader(fileName+".tr"));
		BufferedReader statReader = new BufferedReader(new FileReader(fileName+".stat"));
		BufferedReader tranReader = new BufferedReader(new FileReader(fileName+".n"));

		double totalTime = extractNumber(timeReader);
		timeReader.close();

		double n =extractNumber(tranReader);
		tranReader.close();
		
		double T =extractNumber(trReader);
		tranReader.close();

		String line;
		ArrayList<StatusEntry> sList = new ArrayList<StatusEntry>();
		while((line=statReader.readLine())!=null){
			StatusEntry s = new StatusEntry(Timestamp.valueOf(line.split(",")[1]),Integer.parseInt(line.split(",")[0]));
			sList.add(s);
		}
		statReader.close();


		double totATRxr=0;
		double sumRecTotal =0;
		double totalBlkd = 0;


		for(int i=0;i<sList.size()-1;i++){
			StatusEntry curr = sList.get(i);
			StatusEntry next = sList.get(i+1);
			int blckTuples =curr.getNum(); 
			if(blckTuples!=0){
				double recTime = (next.getTs().getTime() - curr.getTs().getTime())/1000.0;
				sumRecTotal += recTime;
				totATRxr += recTime * ((n-blckTuples)/n);
				totalBlkd += blckTuples;
				//				System.out.println((next.getTs().getTime() - curr.getTs().getTime())/1000.0);
				//				System.out.println(curr.getNum());
			}
		}
		System.out.println("Number of touched tuples " + n);
		System.out.println("Number of transactions " + T);
		System.out.println("Number of blocked tuples " + totalBlkd);
		System.out.println("Total exp time " + totalTime);
		System.out.println("Sum of all recovery time " + sumRecTotal);
		System.out.println("Sum of ATR " + totATRxr);
		if(sumRecTotal > 0)
			System.out.println("Availability " + (totATRxr)/sumRecTotal*100);
		else
			System.out.println("Availability " + 100);

	}	

	public static double extractNumber(BufferedReader br) throws IOException{
		br.readLine();
		br.readLine();
		String line = br.readLine();
		System.out.println(line);
		return Double.parseDouble(line);
	}
}

class StatusEntry{
	Timestamp ts;
	int num = 0;

	StatusEntry(Timestamp ts,int num){
		this.num = num;
		this.ts = ts;
	}

	public Timestamp getTs() {
		return ts;
	}

	public void setTs(Timestamp ts) {
		this.ts = ts;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}


}
