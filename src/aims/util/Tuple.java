/**
 * Created by: Muhamad Felemban 
 * Apr 15, 2017
 */
package aims.util;

/**
 * @author mfelemban
 *
 */
public class Tuple {
	Character c;
	Double d;
	
	public Tuple(Character c,Double d){
		this.d = d;
		this.c = c;
	}
	
	public Character getLeft(){
		return c;
	}
	
	public Double getRight(){
		return d;
	}
	
	public String toString(){
		return c+":"+d;
	}
}
