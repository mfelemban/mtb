/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

public class Util {
	
	private static final Logger LOG = Logger.getLogger(Util.class);
	public static Map<Integer,Integer> createHistogram(Map<Integer,Integer> tupleCap ){
		Map<Integer,Integer> histogram = new HashMap<Integer,Integer>();
		for(Map.Entry<Integer,Integer> entry:tupleCap.entrySet()){
			Integer value = entry.getValue();
			if(histogram.containsKey(value)){
				histogram.put(value,histogram.get(value)+1);
			}else{
				histogram.put(value,1);
			}
		}
		histogram = sortByComparator(histogram,false);
		//		print(histogram.toString());
		return histogram;
	}
	
//	public static int[] createHistogram(ArrayList<Double> data, double intervals){
//		System.out.println("Date " + data.toString());
//		if(data.size()==0)
//			return new int[0];
//		double maxValue = Double.MIN_VALUE;
//		for(Double v:data){
//			if(v>maxValue){
//				maxValue = v;
//			}
//		}
//		int size = (int) Math.ceil(maxValue/intervals);
//		System.out.println("Intervals " + intervals + " size " + size);
//		int[] hist = new int[size];
//		
//		for(Double v:data){
//			int index = (int) Math.floor(v/intervals); 
//			System.out.println("data point " + v + " index " + index);
//			hist[index] ++;
//		}
//		return hist;
//	}
	
	
	public static <T> String printArray(T[] a){
		String output = "";
		for(int i=0;i<a.length;i++){
			if(i<a.length-1)
				output += a[i]+":";
			else
				output += a[i];
		}
		return output;	
	}
	
	public static String printArray(long[] a){
		String output = "";
		for(int i=0;i<a.length;i++){
			if(i<a.length-1)
				output += a[i]+":";
			else
				output += a[i];
		}
		return output;	
	}
	

	public static int getBinomial(int n, double p) {
		double log_q = Math.log(1.0 - p);
		int x = 0;
		double sum = 0;
		for(;;) {
			sum += Math.log(Math.random()) / (n - x);
			if(sum < log_q) {
				return x;
			}
			x++;
		}
	}
	
	public static Map<Integer, Integer> sortByComparator(Map<Integer, Integer> unsortMap, final boolean order)
	{

		List<Entry<Integer, Integer>> list = new LinkedList<Entry<Integer, Integer>>(unsortMap.entrySet());

		// Sorting the list based on values
		Collections.sort(list, new Comparator<Entry<Integer, Integer>>()
				{
			public int compare(Entry<Integer, Integer> o1,
					Entry<Integer, Integer> o2)
			{
				if (order)
				{
					return o1.getValue().compareTo(o2.getValue());
				}
				else
				{
					return o2.getValue().compareTo(o1.getValue());

				}
			}
				});

		// Maintaining insertion order with the help of LinkedList
		Map<Integer, Integer> sortedMap = new LinkedHashMap<Integer, Integer>();
		for (Entry<Integer, Integer> entry : list)
		{
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}

	public static void printMap(Map<Integer, Integer> map)
	{
		for (Entry<Integer,Integer> entry : map.entrySet())
		{
			LOG.info("Key : " + entry.getKey() + " Value : "+ entry.getValue());
		}
	}
	
	public static void printHistogram(Map<Integer, Integer> H) {
		for(Map.Entry<Integer, Integer> entry : H.entrySet()){
			String label = entry.getKey()+ " : ";
			LOG.info(label + convertToStars(entry.getValue()));
		}
	}

	private static String convertToStars(int num) {
		StringBuilder builder = new StringBuilder();
		for (int j = 0; j < num; j++) {
			builder.append('*');
		}
		return builder.toString();
	}
	
	public static long[] mergeArrays(long[] a,long[]b){
		long[] results = new long[a.length + b.length];
		for(int i=0;i<a.length;i++){
			results[i] = a[i];
		}
		for(int i=0;i<b.length;i++){
			results[i+a.length] = b[i];
		}
		return results;
	}

}
