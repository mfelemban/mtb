/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.util;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class WorkloadTransaction {
	int type;
	public Integer[] src;
	public Integer[] dest;
	public int id;
	double alpha;
	public int capacity;
	ArrayList<Integer> shared;
	ArrayList<Integer> nonShared;

	public WorkloadTransaction(int id, int size){
		this.id=id;
		this.capacity = size;
		shared = new ArrayList<Integer>();
		nonShared = new ArrayList<Integer>();
	}
	
	public WorkloadTransaction(String tx){
		String[] tkns = tx.split(",");
		this.type = Integer.parseInt(tkns[0]);
		this.id = Integer.parseInt(tkns[1]);
		int counter = 0;
		src = new Integer[tkns[2].split(":").length];
		for(String id:tkns[2].split(":"))
			src[counter++] = Integer.parseInt(id);
		dest = new Integer[tkns[3].split(":").length];
		counter = 0;
		for(String id:tkns[3].split(":"))
			dest[counter++] = Integer.parseInt(id);
		this.alpha = Double.parseDouble(tkns[4]);			
	}

	public void addShared(Integer tuple){
		shared.add(tuple);
	}

	public void addNonShared(Integer tuple){
		nonShared.add(tuple);
	}

	public int getSize(){
		return shared.size() + nonShared.size();
	}

	public boolean full(){
		return getSize() >= capacity;	
	}

	public void generateTx(Set<Integer> readTuples){
		ArrayList<Integer> allTuples = new ArrayList<Integer>(shared);
		allTuples.addAll(nonShared);
		Random rand = new Random();
		int srcSize = rand.nextInt(allTuples.size()-1);
		int destSize = allTuples.size() - srcSize;
		this.src = new Integer[srcSize];
		this.dest = new Integer[destSize];
		this.alpha = Math.random()*0.01;
		Collections.shuffle(allTuples);
		getType();
		for(int i=0;i<srcSize;i++)
			src[i] = allTuples.get(i);
		for(int i=0;i<destSize;i++)
			dest[i] = allTuples.get(srcSize + i);
		readTuples.addAll(new HashSet<Integer>(Arrays.asList(src)));
	}
	
	public int getType(){
		if(src.length==1 && dest.length==1)
			this.type = 2;
		else if(src.length==1 && dest.length>1)
			this.type = 3;
		else if(src.length> 1 && dest.length==1)
			this.type = 4;
		else 
			this.type = 5;
		return this.type;
	}

	//	public void setType(int type){
	//		this.type = type;
	//		// Send Payment
	//		if(type==2){
	//			this.src = new long[1];
	//			this.dest= new long[1];
	//		}
	//		// Distribute
	//		else if(type==3){
	//			src = new long[1];
	//			dest = new long[size-1];
	//		}
	//		// Collect
	//		else if(type==4){
	//			dest = new long[1];
	//			src = new long[size-1];
	//		
	//		}
	//		// Many-Many
	//		else if(type==5){
	//			int t = size/2;
	//			src = new long[t];
	//			dest = new long[size -t];
	//		}
	//	}

	public String getString(){
		return type +"," + id+ "," + Util.printArray(src) + "," + Util.printArray(dest) + "," + alpha + "," + new Timestamp(new Date().getTime()).toString();
	}

	public String toString(){
		return "ID:"+ id + ",shared:" + shared.size() + ",nonshared:" + nonShared.size()+",cap:" + capacity; 
	}

}
