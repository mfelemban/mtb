/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SwingObject {
	Map<Integer,Integer> ibs = new HashMap<Integer,Integer>();
	
	public SwingObject(){
		
	}
	
	public void addIB(Integer tx,Integer ib){
		ibs.put(tx,ib);
	}
	
	
	public boolean isBoundary(){
		Set<Integer> assignedIBs = new HashSet<Integer>(ibs.values());
		return assignedIBs.size()>1;
	}
	
	public void removeIB(Integer tx,Integer ib){
		if(!ibs.keySet().contains(tx))
			return;
		else{
			ibs.remove(tx);
		}
		
	}
	
	public Set<Integer> getIBS(){
		return new HashSet<Integer>(ibs.values());
	}
	
	public boolean inIB(int ib){
		return ibs.values().contains(ib);
	}
	
	public boolean assignedToIB(int ib){
		Set<Integer> assTo = new HashSet<Integer>(ibs.values());
		if(assTo.contains(ibs))
			return true;
		else
			return false;
	}
	
	public String toString(){
		return ibs.toString();
	}
}
