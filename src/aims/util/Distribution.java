/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.util;

import java.util.Random;

import org.apache.commons.math3.util.CombinatoricsUtils;

public class Distribution {
	
	public static long choose(int n, int k){
		return CombinatoricsUtils.binomialCoefficient(n, k);
	}

	public static double zeta(long n, double theta, double lastZeta,long start){
		double sum=lastZeta;
		for (long i=start; i<n; i++)
		{
			sum+=1/(Math.pow(i+1,theta));
		}
		return sum;
	}
	
	
	public static long zipf(long n,double zetan, double theta)
	{
		double alpha = 1 / (1 - theta);
		double eta = (1 - Math.pow(2.0 / n, 1 - theta)) /(1 - zeta(2,theta,0,0) / zetan);
		double u = new Random().nextDouble();
		double uz = u * zetan;
		if (uz < 1)
			return 1;
		if (uz < 1 + Math.pow(0.5, theta))
			return 2;
		return 1 + (int)(n * Math.pow(eta*u - eta + 1, alpha));
	}
}
