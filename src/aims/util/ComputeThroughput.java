/**
 * Created by: Muhamad Felemban 
 * Nov 17, 2017
 */
package aims.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author mfelemban
 *
 */
//3977,257073173,0,2017-11-10 03:33:12.135,2017-11-10 03:33:12.158,9,9,0,0,0,0,0,0,4,0,0,0,8,1
public class ComputeThroughput {
	public static void main(String[] args) throws IOException, ParseException{
		String file = args[0];
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line = "";
		Date expStart=null, expEnd=null;
		boolean first = true;
		double numTx = 0;
		while((line=br.readLine())!= null){
			String[] tokens = line.split(",");
			String start = tokens[3];
			java.util.Date startTime= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(start);
			String end = tokens[4];
			java.util.Date endTime= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(end);
			if(first){
				expStart = startTime;
				expEnd = endTime;
			}else{
				if(startTime.before(expStart))
					expStart = startTime;
				if(endTime.after(expEnd))
					expEnd = endTime;
			}
			first =false;
			numTx++;
			//		}

		}
		System.out.println(expEnd.getTime());
		System.out.println(expStart.getTime());
		double throughput = numTx / (expEnd.getTime() - expStart.getTime())*1000.0;
		System.out.println(throughput);
	}
}
