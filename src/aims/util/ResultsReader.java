/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ResultsReader {
	public static void main(String[] args) throws IOException{
		ArrayList<File> files = new ArrayList<File>();
		Set<Double> s1 = new HashSet<Double>();
		Set<Double> s2 = new HashSet<Double>();
		Set<String> alg = new HashSet<String>();
		for(int i=0;i<args.length;i++){
			String fileName = args[i];
			System.out.println("Looking at " + fileName);
			Pattern p = Pattern.compile("\\d+[.]\\d+");
			Matcher m = p.matcher(fileName);
			ArrayList<Double> matchedNumbers = new ArrayList<Double>();
			while(m.find()){
				matchedNumbers.add(Double.parseDouble(m.group()));
			}
			s1.add(matchedNumbers.get(0));
			s2.add(matchedNumbers.get(1)); 
			String a = fileName.substring(fileName.lastIndexOf("/")+1,fileName.lastIndexOf("/")+4);
			alg.add(a);
			files.add(new File(args[i]));
		}
		
		if(alg.size()>1){
			String output = "s1_" + s1.toArray()[0] + "s2_" + s2.toArray()[0];
			algBased(files,output);
		}
		
		
		
		
		
	}
	
	
	public static void algBased(ArrayList<File> files,String outputFile) throws IOException{
		BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
		BufferedReader br;
		String line;
		ArrayList<String> o = new ArrayList<String>();
		for(File f:files){
			
			br = new BufferedReader(new FileReader(f));
			line = br.readLine();
			while(line!=null){
				if(!line.contains("=")){
					line = br.readLine();
					continue;
				}
				line = br.readLine();
			}
		}
	}
}
