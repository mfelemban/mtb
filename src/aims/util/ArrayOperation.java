/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.util;


public class  ArrayOperation {
	public static void print2dMatrix(int[][] matrix,String name){
		System.out.println("------- Printing " + name + " Array ---------");
		int unass = 0;
		int sum = 0;
		for(int i=0;i<matrix.length;i++){
			String output = "";
			boolean no =true;
			for(int j=0;j<matrix[i].length;j++){
				if(matrix[i][j]>0)
					no = false;
				sum += matrix[i][j];
				output += matrix[i][j] + ",";
			}
			if(no)
				unass++;
//			output +=  + "\n";
			System.out.println(output.substring(0,output.length()-1));
		}
		System.out.println("sum = " +sum + " unassigned items " + unass);
		System.out.println("----------------------------");
	}
	
//	public static void printCondProb(double[][] matrix,String name){
//		System.out.println("------- Printing " + name + " Array ---------");
//		double sum = 0;
//		for(int i=0;i<matrix.length;i++){
//			sum = 0;
//			for(int j=0;j<matrix[i].length;j++){
//				System.out.print(matrix[i][j] + ",");
//				sum += matrix[i][j];
//			}
//			System.out.println("  " +sum);
//		}
//	}
	
	public static void printPMF(double[][] matrix,String name){
		System.out.println("------- Printing " + name + " PMF ---------");
		double sum = 0;
		for(int i=0;i<matrix.length;i++){
			for(int j=0;j<matrix[i].length;j++){
				System.out.print(matrix[i][j] + ",");
				sum += matrix[i][j];
			}
			System.out.println();
		}
		System.out.println(sum);
	}

//	public static void printArray(double[] da,String name){
//		System.out.print(name + ": \n{ ");
//
//		for(int i=0;i<da.length;i++){
//			System.out.print( da[i] + " ");
//		}
//		System.out.println("}");
//	}
//	
//	static double truncate(double number, int precision)
//	{
//		double prec = Math.pow(10, precision);
//		int integerPart = (int) number;
//		double fractionalPart = number - integerPart;
//		fractionalPart *= prec;
//		int fractPart = (int) fractionalPart;
//		fractionalPart = (double) (integerPart) + (double) (fractPart)/prec;
//		return fractionalPart;
//	}
//	public static double round(double value, int places) {
//	    if (places < 0) throw new IllegalArgumentException();
//
//	    long factor = (long) Math.pow(10, places);
//	    value = value * factor;
//	    long tmp = Math.round(value);
//	    return (double) tmp / factor;
//	}
//	
//	public static void print3dMatrix(double[][][] matrix,String name){
//		for(int i=0;i<matrix.length;i++){
//			System.out.print("i="+i+"\n[\n");
//			for(int j=0;j<matrix[i].length;j++){
//				for(int k=0;k<matrix[i][j].length;k++){
//					System.out.print(matrix[i][j][k]+"  ");
//				}
//				System.out.println();
//			}
//			System.out.print("]\n");
//		}
//	}

}
