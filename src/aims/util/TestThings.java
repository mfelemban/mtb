/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.util;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

import org.apache.commons.math3.distribution.ZipfDistribution;

import com.oltpbenchmark.distributions.ZipFianDistribution;

public class TestThings {
	public static void main(String[] args) throws IOException{
		DDG ddg = new DDG();
		ddg.load("workload/large/s1_3.5_s2_4.0.sql");
		Map<Long,Set<Long>> E = ddg.getE();
		int total = 0;
		for(Set<Long> edges: E.values()){
			total+= edges.size();
		}
		System.out.println("Total " + total);
	}
}
