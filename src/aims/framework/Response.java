/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
package aims.framework;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

import org.apache.log4j.Logger;

import com.oltpbenchmark.api.BenchmarkModule;


public class Response implements Callable<Boolean>{
	private static final Logger LOG = Logger.getLogger(Response.class);
	BenchmarkModule benchmarkModule;
	CountDownLatch latch;
	long txid;
	private boolean success = false;


	public Response(BenchmarkModule benchmarkModule, long txid)
	{
		this.benchmarkModule = benchmarkModule;
		//		this.latch = latch;
		this.txid = txid;
	}
	@Override
	public Boolean call() throws Exception {
		try {
			Connection conn = benchmarkModule.makeConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(benchmarkModule.getWorkloadConfiguration().getIsolationMode());
			PreparedStatement time = conn.prepareStatement("select current_timestamp;");
			ResultSet rsTime = time.executeQuery();
			rsTime.next();
			Timestamp ts = rsTime.getTimestamp(1);
			PreparedStatement ps1 = conn.prepareStatement("select blockTxn(?,?);");
			ps1.setLong(1, txid);
			ps1.setTimestamp(2, ts);
			ps1.execute();
			this.success = true;
			if(success){
				conn.commit();
				LOG.info("Responded " + txid  + "--> B set approximated");
			}else{
				conn.rollback();
			}
		} catch (SQLException ex) {
		}

		//		LOG.info("Took " + (end-start)/1000 + " seconds to response " + txid);

		return success;
	}

}
