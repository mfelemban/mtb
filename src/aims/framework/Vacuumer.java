/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.framework;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.apache.log4j.Logger;

import com.oltpbenchmark.api.BenchmarkModule;

public class Vacuumer implements Runnable{
	private static final Logger LOG = Logger.getLogger(Vacuumer.class);

	BenchmarkModule benchmarksModule;
	boolean running = true;
	Connection conn;
	public Vacuumer(Connection conn,BenchmarkModule benchmarksModule){
		this.conn = conn;
		this.benchmarksModule = benchmarksModule;
	}
	
	public void terminate(){
		LOG.info("Termination vaccumer");
		this.running = false;
	}
	
	@Override
	public void run() {	
		// TODO Auto-generated method stub
		PreparedStatement ps;
		while(running){
			try {
				Thread.sleep(5000);
				ps = conn.prepareStatement("delete from checking_backup where mod_time < clock_timestamp() - interval '5 minutes'");
				ps.executeUpdate();
//				LOG.info("Deleted from backup table " + d);
				ps = conn.prepareStatement("delete from log_table where time_stamp < clock_timestamp() - interval '2 minutes'");
				ps.executeUpdate();
//				LOG.info("Deleted from log table " + d);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
	}

}
