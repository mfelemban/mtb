/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
package aims.framework;

import java.util.ArrayList;

import com.oltpbenchmark.util.Query;

public class ActiveTransaction {
	ArrayList<Query> tx;
	long startTime;
	long runTime;
	long totalRuntime;
	long blockageTime=0;
	long submissionTime;
	long blockedSince;
	public boolean boundary;
	public boolean boBlocked,corBlocked;
	ArrayList<Long> boundaries;
	boolean currentlyBlocked = false;
	int tuplesBlocked=0;
	int tuplesToRepair=0;
	int totalTuplesBlocked=0;
	int IB;

	public ActiveTransaction(ArrayList<Query> tx,long submissionTime,boolean boundary){
		this.tx = tx;
		this.submissionTime = submissionTime;
		this.boundary = boundary;
		this.boundaries = new ArrayList<Long>();
		this.boBlocked = false;
		this.corBlocked = false;
	}

	public ActiveTransaction(ArrayList<Query> tx,int IB,long submissionTime){
		this.tx = tx;
		this.submissionTime = submissionTime;
		this.boundaries = new ArrayList<Long>();
		this.boBlocked = false;
		this.corBlocked = false;
	}
	
	public ActiveTransaction(ArrayList<Query> tx,long submissionTime){
		this.tx = tx;
		this.submissionTime = submissionTime;;
	}
	
	public int getIB(){
		return IB;
	}

	public int getTuplesBlocked(){
		return tuplesBlocked;
	}

	public int getTuplesToRepair(){
		return tuplesToRepair;
	}

	public int getTotalBlockedTuples(){
		return totalTuplesBlocked;
	}

	public void setBoBlocked(){
		this.boBlocked = true;
	}
	
	public void setCorBlocked(){
		this.corBlocked = true;
	}

	public ArrayList<Query> getTx(){
		return tx;
	}

	public void resetBlocking(){
		this.blockageTime=0;
	}

	public void setBoundaries(ArrayList<Long> b){
		this.boundaries =b;
	}

	public boolean isBoBlocked(){
		return boBlocked;
	}
	
	public boolean isCorBlocked(){
		return corBlocked;
	}

	public ArrayList<Long> getBoundaries(){
		return boundaries;
	}

	public boolean isBoundary(){
		return boundary;
	}

	public long getSubmissionTime(){
		return submissionTime;
	}

	public void start(){
		this.startTime = System.currentTimeMillis();
	}

	public void end(){
		this.runTime = System.currentTimeMillis() - startTime;
		this.totalRuntime += runTime;
	}

	public long getRunTime(){
		return this.runTime;
	}
	
	public long getTotalRunTime(){
		return totalRuntime;
	}

	//	public void setBlockageTime(long blockage){
	//		this.blockageTime = blockage;
	//	}
	public long getBlockageTime(){
		return blockageTime;
	}

	public void startBlocking(long ts){
		if(!currentlyBlocked){
			this.blockedSince = ts;
			currentlyBlocked = true;
		}
	}

	public void setTuplesBlocked(int n){
		this.tuplesBlocked = n;
	}

	public void setTuplesToRepair(int n){
		this.tuplesToRepair = n;
	}

	public void setTotalTuplesBlocked(int n){
		this.totalTuplesBlocked = n;
	}

	public void endBlocking(long current){
		if(currentlyBlocked){
			this.blockageTime += current - blockedSince;
			this.currentlyBlocked = false;
		}
	}

	public String toString(){
		return tx.get(0).getTxid()+":"+tx.get(0).getType()+":"+tx.get(0).getThreadId();
	}

}
