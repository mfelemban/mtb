/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.framework;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

import com.oltpbenchmark.api.BenchmarkModule;
import com.oltpbenchmark.util.AIMSLogger;

public class IDMessageSender implements Runnable {

	private static final Logger LOG = Logger.getLogger(IDMessageSender.class);

	private long txid;
	private Timestamp ts;
	private long delay;
	BenchmarkModule benchmarkModule;
	Map<Long,Long> txidMap;
	Boolean[] recoveryStatus,ibTxStatus;
	Integer runningTx;
	Boolean waiting;

	public IDMessageSender(long txid,BenchmarkModule benchmarkModule,Map<Long,Long> txidMap,
			Boolean[] recoveryStatus,Boolean [] ibTxStatus,Integer runningTx,Boolean waiting) throws SQLException {
		super();
		this.txid = txid;
		this.delay = benchmarkModule.getWorkloadConfiguration().getXmlConfig().getLong("mddelay");;
		this.benchmarkModule = benchmarkModule;
		this.txidMap = txidMap;
		this.recoveryStatus = recoveryStatus;
		this.ibTxStatus = ibTxStatus;
		this.runningTx = runningTx;
		this.waiting = waiting;
	}

	public IDMessageSender(long txid,long delay,BenchmarkModule benchmarkModule) throws SQLException {
		super();
		this.txid = txid;
		this.delay = delay;
		this.benchmarkModule = benchmarkModule;
	}




	public void run() {
		try{
			long malTxid = -1;
//			LOG.info("Potenetial mal. " + txid);
			if(txidMap != null){
				// Need to be synchornized
				synchronized(txidMap){
					while(!txidMap.containsKey(txid)){
						txidMap.wait();
					}
					malTxid = txidMap.get(txid);
				}

				txid = malTxid;
			}

			
			Thread.sleep(delay);
			LOG.info("Malicious tx " + txid );
			ts = new Timestamp(System.currentTimeMillis());
			
			AIMSLogger.logTransactionSpecs(101, String.format("%d,%s;", txid,new Timestamp(System.currentTimeMillis())));
			ExecutorService pool = Executors.newSingleThreadScheduledExecutor();
			Callable<Boolean> aims = new Response(benchmarkModule,txid);
			Future<Boolean> resFuture = pool.submit(aims);

			boolean resSucess= resFuture.get();

			while(!resSucess){
				LOG.info("Re executing response " + txid);
				aims = new Response(benchmarkModule,txid);
				resFuture = pool.submit(aims);
				resSucess= resFuture.get();				
			}



			AIMSLogger.logTransactionSpecs(101, String.format("%d,%s;", txid,new Timestamp(System.currentTimeMillis())));
			aims = new Recovery(txid,benchmarkModule,ts,recoveryStatus,ibTxStatus,runningTx,waiting);
			Future<Boolean> recFuture = pool.submit(aims);

			boolean recSucess= recFuture.get();
			while(!recSucess){
				LOG.info("Re executing recovery " + txid);
				aims = new Recovery(txid,benchmarkModule,ts,recoveryStatus,ibTxStatus,runningTx,waiting);
				recFuture = pool.submit(aims);
				recSucess= recFuture.get();				
			}
			pool.shutdown();

		}catch(Exception e){
			e.printStackTrace();
		}

		//        LOG.info(String.format("Transaction # %d is malicious",txid));
	}

}