/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.framework;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import com.oltpbenchmark.api.BenchmarkModule;


public class Recovery implements Callable<Boolean>{
	private static final Logger LOG = Logger.getLogger(Recovery.class);
	long txid;
	private boolean success=false;
	private Timestamp ts;
	BenchmarkModule benchmarkModule;
	Boolean[] recoveryStatus,ibTxStatus;
	Integer runningTx;
	Boolean waiting;
	
	public Recovery(long txid,BenchmarkModule benchmarkModule,Timestamp ts,Boolean[] recoveryStatus,Boolean[] ibTxStatus,Integer runningTx,Boolean waiting)
	{
		this.txid = txid;
		this.benchmarkModule = benchmarkModule;
		this.ts = ts;
		this.recoveryStatus = recoveryStatus;
		this.ibTxStatus = ibTxStatus;
		this.waiting = waiting;
		this.runningTx = runningTx;
	}



	@SuppressWarnings("static-access")
	@Override
	public Boolean call() {
		//		boolean locked = getLock();
		long start;
		try{
			Connection conn = benchmarkModule.makeConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(benchmarkModule.getWorkloadConfiguration().getIsolationMode());

			Connection temp = benchmarkModule.makeConnection();
			temp.setAutoCommit(false);
			temp.setTransactionIsolation(benchmarkModule.getWorkloadConfiguration().getIsolationMode());
			PreparedStatement ps = conn.prepareStatement("select txid_current()");
			ResultSet rr = ps.executeQuery();
			rr.next();
			long mappedTx = rr.getLong(1);
			rr.close();
			ps = conn.prepareStatement("select wl_txid from transaction_id_map where pg_transaction_id = " + txid);
			rr = ps.executeQuery();
			rr.next();
			long currentTxId = rr.getLong(1);


			Integer[] ibs = getIBs(conn);
//			LOG.info(Arrays.toString(ibs));

			start = System.currentTimeMillis();
			synchronized(recoveryStatus){
				while(checkRecoveryStatus(ibs)){
					recoveryStatus.wait();
				}
				for(int i=0;i<ibs.length;i++)
					recoveryStatus[ibs[i]-1] = true;
			}
			LOG.info("Start Recovering " + currentTxId + ":" + txid + " with " + mappedTx);
//			LOG.info("Rec Lock " + (System.currentTimeMillis()-start));
			ResultSet rs;

			start = System.currentTimeMillis();

//			synchronized(ibTxStatus){
//				while(checkIBTxStatus(ibs)){
//					ibTxStatus.wait();
//				}
//				for(int i=0;i<ibs.length;i++)
//					ibTxStatus[ibs[i]-1] = true;
//			}
			LOG.info("Recovery " + currentTxId + " trying to lock ");
			synchronized(ibTxStatus){
				for(int i=0;i<ibs.length;i++)
					ibTxStatus[ibs[i]-1] = true;
				LOG.info("Recovery " + currentTxId + "-->Gates closed");
			}
			LOG.info("Recovery " + currentTxId + " obtained lock ");
			
			
			
			synchronized(runningTx){
				while(runningTx>0){
					synchronized(waiting){
						waiting = true;
					}
					runningTx.wait();
				}
				synchronized(waiting){
					waiting = false;
				}
			}
			
			LOG.info("Recovery " + currentTxId + " all running transactions term.");
			

			/*
			 * Wait for all active transactions to commit
			 */
//			LOG.info("Adv Lock " + (System.currentTimeMillis()-start));
			
			/*
			 * Use a counter 
			 */


			start = System.currentTimeMillis();
			PreparedStatement ps1 = temp.prepareStatement("select alertMTxn(?,?);");
			ps1.setLong(1, txid);
			ps1.setTimestamp(2, ts);
			ps1.execute();

			temp.commit();
			temp.close();
			
			LOG.info("Recovery " + currentTxId +  " --> AlertMTxn:Affected tx known ");
//			synchronized(ibTxStatus){
//				for(int i=0;i<ibs.length;i++)
//					ibTxStatus[ibs[i]-1] = false;
//				ibTxStatus.notifyAll();
//			}
			
			synchronized(ibTxStatus){
				for(int i=0;i<ibs.length;i++)
					ibTxStatus[ibs[i]-1] = false;
				LOG.info("Gates open");
				ibTxStatus.notifyAll();
			}


			Connection recConn = benchmarkModule.makeConnection();
			recConn.setAutoCommit(false);
			recConn.setTransactionIsolation(benchmarkModule.getWorkloadConfiguration().getIsolationMode());
			start = System.currentTimeMillis();
			PreparedStatement time = recConn.prepareStatement("select current_timestamp + interval '5 hours';");
			ResultSet rsTime = time.executeQuery();
			rsTime.next();
			Timestamp ts1 = rsTime.getTimestamp(1);

			PreparedStatement txID = recConn.prepareStatement("select txid_current();");
			ResultSet txRS = txID.executeQuery();
			txRS.next();
			long tx = txRS.getLong(1);
			PreparedStatement releaseStatement = recConn.prepareStatement("update blocked_tuples_table set (recovery_timestamp,recovering_transaction) = (?,?) "
					+ "where malicious_transaction = ? and transaction_id not in (select transaction_id from corrupted_transactions_table where malicious_transaction = ? )");
			releaseStatement.setTimestamp(1, ts1);
			releaseStatement.setLong(2, tx);
			releaseStatement.setLong(3, txid);
			releaseStatement.setLong(4, txid);
			releaseStatement.executeUpdate();

			releaseStatement = recConn.prepareStatement("select update_status_table(?)");
			releaseStatement.setLong(1,txid);
			releaseStatement.executeQuery();

			recConn.commit();
			recConn.close();

			LOG.info("Recovery " + currentTxId +  " --> benign tupled released");

			Connection undoConn = benchmarkModule.makeConnection();
			undoConn.setAutoCommit(false);
			undoConn.setTransactionIsolation(benchmarkModule.getWorkloadConfiguration().getIsolationMode());
			start = System.currentTimeMillis();

			ps1 = undoConn.prepareStatement("select transaction_id,status from corrupted_transactions_table "
					+ "where malicious_transaction = ? order by transaction_id asc");
			ps1.setLong(1, txid);
			rs = ps1.executeQuery();
			PreparedStatement undo;
			boolean good;
			while(rs.next()){
				//				LOG.info("Tx " + rs.getLong(1) + " undone");
				undo = undoConn.prepareStatement("select undo_proc(?,?)");
				undo.setLong(1, rs.getLong(1));
				undo.setString(2, rs.getString(2));
				good = false;
				while(!good){
					try{
						undo.executeQuery();
						good = true;
					}catch(SQLException e){
//						if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
							LOG.info("************* Deadlock at " + txid + " recovery ");
//						}
					}
				}
			}

			undoConn.commit();
			undoConn.close();

			LOG.info("Recovery " + currentTxId +  " --> transactions undone");

			start = System.currentTimeMillis();

			ps1 = conn.prepareStatement("select transaction_id from corrupted_transactions_table "
					+ "where status = 'affected' and malicious_transaction = ? order by transaction_id asc");
			ps1.setLong(1, txid);
			rs = ps1.executeQuery();
			ArrayList<Long> affectedTx = new ArrayList<Long>();
			while(rs.next()){
				affectedTx.add(rs.getLong(1));
			}
			good = false;
			for(Long aTx:affectedTx){
				Connection c = benchmarkModule.makeConnection();
				c.setAutoCommit(false);
				c.setTransactionIsolation(benchmarkModule.getWorkloadConfiguration().getIsolationMode());
				ps1 = c.prepareStatement("select redo_proc(?)");
				ps1.setLong(1, aTx);
				try{
					ps1.executeQuery();
					good = true;
				}catch(SQLException e){
					if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
						LOG.info("************* Deadlock at " + txid + " recovery ");
					}
				}
				c.commit();
				c.close();
				LOG.info("Recovery " + currentTxId +  " --> transactions redone");
			}




			ps1 = conn.prepareStatement("select mal_trg_ends(?);");
			ps1.setLong(1, txid);
			ps1.executeQuery();
			success = true;



			if(success){
				conn.commit();
				conn.close();
				synchronized(recoveryStatus){
					for(int i=0;i<ibs.length;i++)
						recoveryStatus[ibs[i]-1] = false;
					recoveryStatus.notify();
				}

				LOG.info("--- Tx " + txid + " Recovered");
			}else{
				conn.rollback();
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		return success;
	}

	public Integer[] getIBs(Connection conn){
		Integer[] tempIB=null;
		try{
			Statement getIBs = conn.createStatement();
			ResultSet rs = getIBs.executeQuery("select distinct ib from temp_tuples_table_AIMS where malicious_transaction= " + txid);
			ArrayList<Integer> ibs = new ArrayList<Integer>();
			while(rs.next()){
				ibs.add(rs.getInt(1));
			}
			tempIB = ibs.toArray(new Integer[ibs.size()]);
		}
		catch(Exception e){
			e.printStackTrace();
			LOG.info(e.toString());
		} 
		return tempIB;
	}


	boolean checkRecoveryStatus(Integer[] ibs){
		boolean go = false;
		for(int i=0;i<ibs.length;i++){
			if(recoveryStatus[ibs[i]-1])
				return true;
		}
		return go;
	}

	boolean checkIBTxStatus(Integer[] ibs){
		boolean go = false;
		for(int i=0;i<ibs.length;i++){
			if(ibTxStatus[ibs[i]-1])
				return true;
		}
		return go;
	}

	//	


}
