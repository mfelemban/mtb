/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
package aims.framework;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.ConcurrentMap;

import org.apache.log4j.Logger;

import com.oltpbenchmark.api.BenchmarkModule;

public class BoundaryReleaser implements Runnable {

	private static final Logger LOG = Logger.getLogger(BoundaryReleaser.class);
	long[] oids;
	BenchmarkModule benchmarkModule;
	boolean afterRecvoery;
	long txid;
	Boolean blkdBO;
	Connection newConn;
	long id;
	ConcurrentMap<Long,Long> BO;
	public BoundaryReleaser(long txid,BenchmarkModule benchmarkModule, Connection newConn, long[] oids ,boolean afterRecvoery,Boolean blkdBO,long id,ConcurrentMap<Long,Long> BO){
		this.oids = oids;
		this.benchmarkModule = benchmarkModule;
		this.afterRecvoery = afterRecvoery;
		this.txid = txid;
		this.blkdBO = blkdBO;
		this.newConn = newConn;
		this.id = id;
		this.BO = BO;
	}
	@Override
	public void run() {

		long delay = benchmarkModule.getWorkloadConfiguration().getXmlConfig().getLong("mddelay");
		delay = (long) ((long) delay *1);
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//		synchronized(BO){
		int counter = 0;
		if(!afterRecvoery)
			for(long oid:oids){
				if(BO.containsKey(oid)){
					if(BO.get(oid)==id){
						counter++;
						BO.replace(oid, (long) -1);
					}
				}
			}
		else
			for(Long bo:BO.keySet()){
				if(BO.get(bo)==id){
					counter++;
					BO.replace(bo, (long) -1);
				}

			}
//		LOG.info(id + "  " + counter + " BO tuples released");
		//			LOG.info(id + " " + Arrays.toString(oids));
		//			printBlockedBO();
		//			BO.notifyAll();



		// TODO Auto-generated method stub
		boolean success = false;
		//		Connection conn = null;
		//		try {
		//			conn = benchmarkModule.makeConnection();
		//			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		//		} catch (SQLException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}
		//		while(!success){
		//
		//			try{
		//
		//				long delay = benchmarkModule.getWorkloadConfiguration().getXmlConfig().getLong("mddelay");
		//				delay = (long) ((long) delay *1.5);
		//				try {
		//					Thread.sleep(delay);
		//				} catch (InterruptedException e) {
		//					// TODO Auto-generated catch block
		//					e.printStackTrace();
		//				}
		//				if(!afterRecvoery){
		//
		//
		//					boolean mal = false;
		//
		//					PreparedStatement ps = conn.prepareStatement(checkMalicious(txid));
		//					ResultSet rs = ps.executeQuery();
		//					rs.next();
		//					if(rs.getInt(1)>0){
		//						mal = true;
		//					}
		//
		//					if(!mal){
		//						ps = conn.prepareStatement(releaseBoundaryObject());
		//						int releasedBO = ps.executeUpdate();
		//						ps.toString();
		//						LOG.info("Released " +  releasedBO + " BO of " + txid);
		//					}
		//				}else{
		//
		//					PreparedStatement ps = conn.prepareStatement(releaseBoundaryObject());
		//					ps.executeUpdate();
		//					ps.toString();
		//				}
		//
		//
		//				BONotifier notifier = new BONotifier(newConn);
		//				notifier.start();
		//
		//				success = true;
		//			}catch(Exception e){
		//				e.printStackTrace();
		//				LOG.info("Problem in releasing boundary object");
		//			}
		//
		//		}
		//		try {
		//			conn.close();
		//		} catch (SQLException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
		
		synchronized(BO){
			for(long oid:oids)
				BO.get(oid).notifyAll();
		}


	}

	public void printBlockedBO(){
		String bos= "Blocked BO [";
		for(Long oid:BO.keySet()){
			if(BO.get(oid)!=-1){
				bos += oid + ":" + BO.get(oid) + ",";
			}
		}
		LOG.info(bos+"]");
	}

	//	public  String releaseBoundaryObject(long[] oids) throws SQLException{
	public  String releaseBoundaryObject() throws SQLException{
		String stmt = "update boundary_objects set on_hold = 0,transaction_id=-1 where transaction_id = " + id + ";";
		return stmt;
	}

	public String checkMalicious(long txid){
		String stmt = "select count(*) from IDS where malicious_transaction = "  +txid+ "";

		return stmt;
	}

}