/**
 * Created by: Muhamad Felemban 
 * Jun 30, 2017
 */
package aims.syncooo;

import java.sql.Connection;

/**
 * @author mfelemban
 *
 */
class Notifier {

	protected Connection conn;

	public Notifier(Connection conn) {
		this.conn = conn;
	}

}
