/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
package aims.syncooo;

import java.util.Arrays;
import java.util.concurrent.ConcurrentMap;

import org.apache.log4j.Logger;

import com.oltpbenchmark.api.BenchmarkModule;

public class BoundaryReleaser implements Runnable {

	private static final Logger LOG = Logger.getLogger(aims.syncooo.BoundaryReleaser.class);
	long[] oids;
	BenchmarkModule benchmarkModule;
	boolean afterRecvoery;
	long txid;;
	long id;
	ConcurrentMap<Long,Integer> BO;
	public BoundaryReleaser(long txid,BenchmarkModule benchmarkModule, long[] oids ,boolean afterRecvoery,
			long id,ConcurrentMap<Long,Integer> BO){
		this.oids = oids;
		this.benchmarkModule = benchmarkModule;
		this.afterRecvoery = afterRecvoery;
		this.txid = txid;
		this.id = id;
		this.BO = BO;
	}
	@Override
	public void run() {

		long delay = benchmarkModule.getWorkloadConfiguration().getXmlConfig().getLong("delay");
		delay = (long) ((long) delay *1.5);
		try {
			Thread.sleep(delay);


			//		synchronized(BO){
			if(!afterRecvoery)
				for(long oid:oids){
					if(BO.containsKey(oid)){
						if(BO.get(oid)==id){
							BO.replace(oid, -1);
						}
					}
				}
			else
				for(Long bo:BO.keySet()){
					if(BO.get(bo)==id){
						BO.replace(bo,  -1);
					}

				}
//			LOG.info("Tx " + txid + " released " + Arrays.toString(oids)) ;
//			LOG.info(txid  + " notifying ");
			synchronized(BO){
				BO.notifyAll();
			}

//			LOG.info(printBlockedBO());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			//			e.printStackTrace();
		}


	}

	public String printBlockedBO(){
		String bos= "Blocked BO [";
		for(Long oid:BO.keySet()){
			if(BO.get(oid)!=-1){
				bos += oid + ":" + BO.get(oid) + ",";
			}
		}
		return bos+"]";
	}


}