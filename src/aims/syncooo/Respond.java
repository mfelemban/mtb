/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
package aims.syncooo;




import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CountDownLatch;

import org.apache.log4j.Logger;

import com.oltpbenchmark.api.BenchmarkModule;


public class Respond implements Callable<Boolean>{
	private static final Logger LOG = Logger.getLogger(aims.syncooo.Respond.class);
	BenchmarkModule benchmarkModule;
	CountDownLatch latch;
	long txid;
	private boolean success = false;
	long malID;
	Timestamp ts;
	ConcurrentMap<Long,Integer> BO;
	int ib;
	public Respond(BenchmarkModule benchmarkModule, long txid,long malID,ConcurrentMap<Long,Integer> BO,int ib
			,Timestamp ts)
	{
		this.benchmarkModule = benchmarkModule;
		//		this.latch = latch;
		this.txid = txid;
		this.malID = malID;
		this.BO = BO;
		this.ib = ib;
		this.ts = ts;
	}
	@Override
	public Boolean call() throws Exception {
		try {
//			LOG.info("Response for " + txid + ":" + malID + " start");
			Connection conn = benchmarkModule.makeConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
//			PreparedStatement time = conn.prepareStatement("select current_timestamp;");
//			ResultSet rsTime = time.executeQuery();
//			rsTime.next();
//			Timestamp ts = rsTime.getTimestamp(1);
			PreparedStatement ps1 = conn.prepareStatement("select blockTxn(?,?,?);");
			ps1.setLong(1, txid);
			ps1.setTimestamp(2, ts);
			ps1.setInt(3, ib);
			ps1.execute();
			conn.commit();
			conn.close();
//			LOG.info("Response for " + txid + ":" + malID + " done");
			this.success = true;
			if(success){
				//				LOG.info("Releasing boundary objects");
				Thread th = new Thread(new BoundaryReleaser(txid,benchmarkModule,null,true,malID,BO));
				th.start();
			}else{
				conn.rollback();
			}
			
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return success;
	}

}