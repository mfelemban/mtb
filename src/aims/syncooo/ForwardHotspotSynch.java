/**
 * Created by: Muhamad Felemban 
 * Sep 27, 2017
 */
package aims.syncooo;

import java.sql.Connection;
import java.sql.PreparedStatement;

import com.oltpbenchmark.api.BenchmarkModule;

/**
 * @author mfelemban
 *
 */
public class ForwardHotspotSynch implements Runnable{
	Integer txid;
	BenchmarkModule benchmark;
	long[] allOids;
	public ForwardHotspotSynch(Integer txid,BenchmarkModule benchmark,long[] allOids){
		this.txid = txid;
		this.allOids = allOids;
		this.benchmark = benchmark;
	}
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		long delay = benchmark.getWorkloadConfiguration().getXmlConfig().getLong("delay");
		delay = (long) ((long) delay *1);
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try{
			Connection conn = benchmark.makeConnection();
			conn.setAutoCommit(false);
			
			String query = "with temp as (select balance,chk_id from hotspot_backup where tuple_id in (";
			for(long oid:allOids){
				query+= oid+",";
			}
			query = query.substring(0,query.length()-1) + ")) update checking set balance= cc.balance from(select balance,chk_id from temp) as cc where checking.chk_id = cc.chk_id;";
			PreparedStatement ps = conn.prepareCall(query);
			ps.executeUpdate();

			conn.commit();
		}catch(Exception e){
			e.printStackTrace();
		}

	}


}
