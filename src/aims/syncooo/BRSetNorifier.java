/**
 * Created by: Muhamad Felemban 
 * Jun 30, 2017
 */
package aims.syncooo;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author mfelemban
 *
 */
public class BRSetNorifier extends Thread{
	Connection conn;
	public BRSetNorifier(Connection conn){
		this.conn=conn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			if(!conn.isClosed()){
				Statement stmt = conn.createStatement();
				stmt.execute("NOTIFY BRSet");
				stmt.close();
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		} 
	}

}
