package aims.syncooo;

import java.sql.Timestamp;
import java.util.Arrays;

public class Query{
	private int type;
	private Integer txid;
	private long[] src;
	private long[] dest;
	private double amount;
	private int thrdId;
	private int mal;
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setTxid(Integer txid) {
		this.txid = txid;
	}

	private Timestamp ts;

	public Query(int type,Integer txid,long[] src, long[] dest, double amount, Timestamp ts,int mal){
		this.type = type;
		this.txid = txid;
		this.src =src;
		this.dest = dest;
		this.amount = amount;
		this.ts= ts;
		this.mal = mal;
	}
	
	public Query(int type,Integer txid,long[] src, long[] dest, double amount, Timestamp ts){
		this.type = type;
		this.txid = txid;
		this.src =src;
		this.dest = dest;
		this.amount = amount;
		this.ts= ts;
		this.mal = mal;
	}
	
	public Query(int type,Integer txid,Timestamp ts){
		this.type = type;
		this.txid = txid;
		this.ts= ts;
	}
	
	public Query(int thrdId,int type,Integer txid,long[] src, long[] dest, double amount, Timestamp ts){
		this.thrdId = thrdId;
		this.type = type;
		this.txid = txid;
		this.src =src;
		this.dest = dest;
		this.amount = amount;
		this.ts= ts;
	}
	
	public Query(int type){
		this.type = type;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public long[] getSrc() {
		return src;
	}

	public void setSrc(long[] src) {
		this.src = src;
	}

	public long[] getDest() {
		return dest;
	}
	
	public boolean isMalicious(){
		if(mal==1)
			return true;
		else
			return false;
	}

	public void setDest(long[] dest) {
		this.dest = dest;
	}
	
	public int getThreadId(){
		return thrdId;
	}


	public Timestamp getTs() {
		return ts;
	}

	public void setTs(Timestamp ts) {
		this.ts = ts;
	}

	public Integer getTxid(){
		return txid;
	}

	public String toString(){
		return type + "," + txid +","+ Arrays.toString(src) + "," + Arrays.toString(dest) + "," + amount +"," + ts.toString(); 
	}


}
