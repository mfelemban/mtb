/**
 * Created by: Muhamad Felemban 
 * Sep 27, 2017
 */
package aims.syncooo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.oltpbenchmark.api.BenchmarkModule;

/**
 * @author mfelemban
 *
 */
public class BackwardHotspotSynch implements Runnable{
	Integer txid;
	Integer pgtxid;
	BenchmarkModule benchmark;
	long[] allOids;
	public BackwardHotspotSynch(Integer txid,Integer pgtxid, BenchmarkModule benchmark,long[] allOids){
		this.txid = txid;
		this.allOids = allOids;
		this.benchmark = benchmark;
		this.pgtxid = pgtxid;
	}
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		long delay = benchmark.getWorkloadConfiguration().getXmlConfig().getLong("delay");
		delay = (long) ((long) delay *1);
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean retry = true;
		while(retry)
			try{

				Connection conn = benchmark.makeConnection();
				conn.setAutoCommit(false);

				String checkAffected = "select * from corrupted_transactions_table where transaction_id = " + pgtxid + ";";
				PreparedStatement ps = conn.prepareStatement(checkAffected);
				ResultSet rs = ps.executeQuery();
				if(rs.getMetaData().getColumnCount()==0){

					String query = "with temp as (select balance,chk_id from checking where oid in (";
					for(long oid:allOids){
						query+= oid+",";
					}
					query = query.substring(0,query.length()-1) + ")) update hotspot_backup set mod_transaction =" +  txid + ", mod_time = clock_timestamp() + interval '5 hours',balance = cc.balance from(select balance,chk_id from temp) as cc where hotspot_backup.chk_id = cc.chk_id;";
					ps = conn.prepareCall(query);
					ps.executeUpdate();
				}
				conn.commit();
				retry = false;
			}catch(Exception e){
				e.printStackTrace();
			}


	}


}
