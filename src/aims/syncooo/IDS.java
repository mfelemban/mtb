/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.syncooo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import aims.util.FairLock;

import com.oltpbenchmark.api.BenchmarkModule;

public class IDS implements Runnable {

	private static final Logger LOG = Logger.getLogger(aims.syncooo.IDS.class);

	private Integer txid;
	private long delay;
	BenchmarkModule benchmarkModule;
	Map<Integer,Integer> txidMap;
	Boolean[] ibTxStatus;
	AtomicInteger runningTx;
	AtomicBoolean waiting;
	FairLock fl;
	Connection newConn;
	Integer malID;
	AtomicBoolean recRunning,idAlert;
	ConcurrentMap<Long,Integer> BO;
	int ib;
	public IDS(Integer txid,BenchmarkModule benchmarkModule,Connection newConn,
			Boolean [] ibTxStatus,AtomicInteger runningTx,AtomicBoolean waiting,FairLock fl,
			Integer malID,AtomicBoolean recRunning,Map<Integer,Integer> txidMap,ConcurrentMap<Long,Integer> BO,
			int ib,AtomicBoolean idAlert) throws SQLException {
		super();
		this.malID = malID;
		this.fl = fl;
		this.txid = txid;
		this.delay = benchmarkModule.getWorkloadConfiguration().getXmlConfig().getLong("delay");;
		this.benchmarkModule = benchmarkModule;
		this.ibTxStatus = ibTxStatus;
		this.runningTx = runningTx;
		this.waiting = waiting;
		this.newConn = newConn;
		this.recRunning = recRunning;
		this.txidMap=txidMap;
		this.BO = BO;
		this.ib = ib;
		this.idAlert=idAlert;
	}



	public void run() {
		try{
			long malTxid = 0;
			synchronized(txidMap){
				while(!txidMap.containsKey(txid)){
					txidMap.wait();
				}
			}
			Connection conn = benchmarkModule.makeConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			PreparedStatement time = conn.prepareStatement("select current_timestamp;");
			ResultSet rsTime = time.executeQuery();
			rsTime.next();
			Timestamp ts = rsTime.getTimestamp(1);
			
			malTxid = txidMap.get(txid);
//			LOG.info(txid +  "  in IDS");
			Thread.sleep(delay);
//			LOG.info(txid +  "  detecting " + malTxid);
		
			synchronized(idAlert){
				idAlert.set(true);
			}
			ExecutorService pool = Executors.newSingleThreadScheduledExecutor();
			Callable<Boolean> aims = new aims.syncooo.Respond(benchmarkModule,malTxid,malID,BO,ib,ts);
			Future<Boolean> resFuture = pool.submit(aims);

			boolean resSucess= resFuture.get();

			while(!resSucess){
				LOG.info("");
				aims = new aims.syncooo.Respond(benchmarkModule,malTxid,malID,BO,ib,ts);
				resFuture = pool.submit(aims);
				resSucess= resFuture.get();				
			}
			
			synchronized(idAlert){
				idAlert.set(false);
				idAlert.notifyAll();
			}
			
			
//			LOG.info("to recover");
			aims = new aims.syncooo.Recover(malTxid,benchmarkModule,newConn,ts,ibTxStatus,runningTx,waiting,fl,recRunning,ib);
			Future<Boolean> recFuture = pool.submit(aims);

			boolean recSucess= recFuture.get();
			while(!recSucess){
				aims = new aims.syncooo.Recover(malTxid,benchmarkModule,newConn,ts,ibTxStatus,runningTx,waiting,fl,recRunning,ib);
				recFuture = pool.submit(aims);
				recSucess= recFuture.get();				
			}
			pool.shutdown();
			conn.close();
			

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

}