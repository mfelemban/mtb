/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.heurestics;


/*
 * New name, BalancedAssignment
 */
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import aims.util.AIMSObject;
import aims.util.DDG;
import aims.util.Transaction;

public class FirstFitAssignment extends Assignment{

	public FirstFitAssignment(DDG ddg,int k){
		super(ddg,k);
	}
	
	public IBSolution solve2(int k) throws IOException{

		long start = System.currentTimeMillis();
		super.solution = new IBSolution(k,ddg);
		ArrayList<Transaction> transactions = new ArrayList<Transaction>(ddg.getT());

		Set<Integer> assignedIBs = new HashSet<Integer>();
		Iterator<Transaction> iter = transactions.iterator();
		while(iter.hasNext()){
			Transaction currTx = iter.next();
			Set<Integer> overlapingIBs = overlappingIBs(currTx);
			if(overlapingIBs.size()!=0 && assignedIBs.size()>=k){
				int ib = solution.getSmallestIB(overlapingIBs);
				solution.assignTransaction(currTx, ib);
			}
			else{
				int ib = solution.getSmallestIB();
				assignedIBs.add(ib);
				solution.assignTransaction(currTx, ib);
			}
		}
		solution.update();
		System.out.println("Time:" + (System.currentTimeMillis()-start));
		return solution;
	}



	public Set<Integer> neighboringTx(Transaction t){
		Set<Integer> neighbors = new HashSet<Integer>();
		for(Long o:t.getSharedObjects()){
			AIMSObject tempO = ddg.getGlobalObjects().get(o);
			//			neighbors.addAll(tempO.getTxSet());ove
			for(Integer overlapTx:tempO.getTxSet()){
				neighbors.add(overlapTx);
			}
		}
		neighbors.remove(new Integer(t.getId()));
		return neighbors;
	}

	// O(Tk)
	public Set<Integer> overlappingIBs(Transaction t){
		Set<Integer> overlappingTx = neighboringTx(t);
		Set<Integer> ovrlabIBs = new HashSet<Integer>();
		for(int i=0;i<k;i++){
			if(CollectionUtils.containsAny(solution.ibTxSet.get(i), overlappingTx))
				ovrlabIBs.add(i);
		}
		return ovrlabIBs;
	}



	public static void main(String[] args) throws IOException{
		String logFile = args[0];
		String tFile = logFile.substring(0, logFile.lastIndexOf(".")) + "_T" + logFile.substring(logFile.lastIndexOf("."), logFile.length());
		File tempDir = new File(tFile);
		DDG ddg = new DDG();
		if(tempDir.exists())
			ddg.load(logFile);
		else
			ddg.generate(logFile);
		String[] a = args[1].split(",");
		int[] K = new int[a.length];
		int i=0;
		for(String s:a){
			K[i] = Integer.parseInt(s);
			i++;
		}
		for(int k:K){

			FirstFitAssignment ffa = new FirstFitAssignment(ddg,k);
			if(ffa.T.size()<k){
				print("K must be less than number of transactions!");
				System.exit(0);
			}
//			ffa.solution = ffa.solve(k);
			ffa.solution = ffa.solve2(k);
//			ffa.printResults(logFile, k,"ffa");
			
		}

	}
}
