/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.heurestics;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import aims.util.DDG;
import aims.util.Transaction;

public class RandomAssignment extends Assignment{
	public RandomAssignment(DDG ddg,int k){
		super(ddg,k);
	}


	public IBSolution solve(int k) throws IOException{
		long start = System.currentTimeMillis();
		
		super.solution = new IBSolution(k,ddg);
		// T log T

		ArrayList<Transaction> transactions = ddg.getT();


		Random rand = new Random();
		int targetIB;
		for(Transaction t:transactions){
			if(solution.emptyIBs()>0)
				targetIB = solution.getEmptyIB();
			else
				targetIB = rand.nextInt(k);
			solution.assignTransaction(t,targetIB);
		}
		solution.update();
		System.out.println("Time:" + (System.currentTimeMillis()-start));
		return solution;
	}

	public static void main(String[] args) throws IOException{
		String logFile = args[0];
		String tFile = logFile.substring(0, logFile.lastIndexOf(".")) + "_T" + logFile.substring(logFile.lastIndexOf("."), logFile.length());
		File tempDir = new File(tFile);
		DDG ddg = new DDG();
		if(tempDir.exists())
			ddg.load(logFile);
		else
			ddg.generate(logFile);


		String[] a = args[1].split(",");
		int[] K = new int[a.length];
		int i=0;
		for(String s:a){
			K[i] = Integer.parseInt(s);
			i++;
		}

		for(int k:K){
			RandomAssignment rfa = new RandomAssignment(ddg,k);
			if(rfa.T.size()<k){
				print("K must be less than number of transactions!");
				System.exit(0);
			}
			rfa.solution = rfa.solve(k);
//			rfa.printResults(logFile, k, "rfa");
		}

	}
}
