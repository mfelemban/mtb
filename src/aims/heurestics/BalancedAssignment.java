/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.heurestics;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import aims.util.AIMSObject;
import aims.util.DDG;
import aims.util.Transaction;

public class BalancedAssignment extends Assignment{

	public BalancedAssignment(DDG ddg,int k){
		super(ddg,k);
	}

	public IBSolution solve(int k) throws IOException{
		
		long start = System.currentTimeMillis();
		super.solution = new IBSolution(k,ddg);
		ArrayList<Transaction> transactions = new ArrayList<Transaction>(ddg.getT());
		Collections.sort(transactions, new Comparator<Transaction>() {
			@Override
			public int compare(Transaction t1, Transaction t2)
			{
				// descedning
				return  Double.compare(t2.internalRatio() , t1.internalRatio());
			}
		});
		
		Iterator<Transaction> iter = transactions.iterator();
		while(iter.hasNext()){
			solution.assignTransaction(iter.next(), solution.getSmallestIB());
		}
		solution.update();
		
//		print(solution.toString());
		System.out.println("Time:" + (System.currentTimeMillis()-start));
		return solution;
	}
	
	// O(nT)
	public Map<Integer,Integer> neighboringTx(Transaction t){
		Map<Integer,Integer> neighbors = new HashMap<Integer,Integer>();
		for(Long o:t.getSharedObjects()){
			AIMSObject tempO = ddg.getGlobalObjects().get(o);
//			neighbors.addAll(tempO.getTxSet());
			for(Integer overlapTx:tempO.getTxSet()){
				if(neighbors.containsKey(overlapTx)){
					neighbors.put(overlapTx,neighbors.get(overlapTx)+1);
				}else{
					neighbors.put(overlapTx,1);
				}
			}
		}
		neighbors.remove(new Integer(t.getId()));
		return neighbors;
	}
	
	// O(Tk)
	public Map<Integer,Integer> overlappingIBs(Map<Integer,Integer> overlappingTx){
		Map<Integer,Integer> ovrlabIBs = new HashMap<Integer,Integer>();
		for(int i=0;i<k;i++){
			for(Integer tx:overlappingTx.keySet()){
				if(solution.ibTxSet.get(i).contains(tx)){
					if(ovrlabIBs.containsKey(i)){
						ovrlabIBs.put(i, ovrlabIBs.get(i)+overlappingTx.get(tx));
					}else{
						ovrlabIBs.put(i, overlappingTx.get(tx));
					}
				}
			}
		}
		return ovrlabIBs;
	}



	public static void main(String[] args) throws IOException{
		String logFile = args[0];
		String tFile = logFile.substring(0, logFile.lastIndexOf(".")) + "_T" + logFile.substring(logFile.lastIndexOf("."), logFile.length());
		File tempDir = new File(tFile);
		DDG ddg = new DDG();
		if(tempDir.exists())
			ddg.load(logFile);
		else
			ddg.generate(logFile);
		String[] a = args[1].split(",");
		int[] K = new int[a.length];
		int i=0;
		for(String s:a){
			K[i] = Integer.parseInt(s);
			i++;
		}
		for(int k:K){

			BalancedAssignment ffa = new BalancedAssignment(ddg,k);
			if(ffa.T.size()<k){
				print("K must be less than number of transactions!");
				System.exit(0);
			}
			ffa.solution = ffa.solve(k);
//			ffa.printResults(logFile, k,"ffa");
		}

	}
}
