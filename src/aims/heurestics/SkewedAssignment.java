/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.heurestics;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import aims.util.DDG;
import aims.util.Transaction;

public class SkewedAssignment extends Assignment{
	public SkewedAssignment(DDG ddg,int k){
		super(ddg,k);
	}


	public IBSolution solve(int k) throws IOException{
		long start = System.currentTimeMillis();
		
		super.solution = new IBSolution(k,ddg);
		// T log T

		ArrayList<Transaction> transactions = ddg.getT();
		
		int k80 = (int) (k *0.9);
		int k20 = k - k80;
		
		int t80 = (int) (transactions.size()*0.9);
		int t20 = transactions.size() - t80;
		
		List<Transaction> large = transactions.subList(0, t80);
		List<Transaction> small = transactions.subList(t80, transactions.size()); 
		
		System.out.println("Transaction " + transactions.size() + " " + t80 + " " + t20);
		System.out.println("k " + k + " " + k80 + " " + k20);

		Random rand = new Random();
		int targetIB;
		for(Transaction t:large){
			if(solution.emptyIBs()>0)
				targetIB = solution.getEmptyIB();
			else
				targetIB = rand.nextInt(k20);
			solution.assignTransaction(t,targetIB);
		}
		
		for(Transaction t:small){
			if(solution.emptyIBs()>0)
				targetIB = solution.getEmptyIB();
			else
				targetIB = rand.nextInt(k-k20) + k20;
			solution.assignTransaction(t,targetIB);
		}
		
		
		solution.update();
		System.out.println("Time:" + (System.currentTimeMillis()-start));
		return solution;
	}

	public static void main(String[] args) throws IOException{
		String logFile = "workload/crazy/tx5000/0.75/0.5.sql";
		String tFile = logFile.substring(0, logFile.lastIndexOf(".")) + "_T" + logFile.substring(logFile.lastIndexOf("."), logFile.length());
		File tempDir = new File(tFile);
		DDG ddg = new DDG();
		if(tempDir.exists())
			ddg.load(logFile);
		else
			ddg.generate(logFile);


		String[] a = new String[]{"5","10","15","20"};
		int[] K = new int[a.length];
		int i=0;
		for(String s:a){
			K[i] = Integer.parseInt(s);
			i++;
		}

		for(int k:K){
			SkewedAssignment rfa = new SkewedAssignment(ddg,k);
			if(rfa.T.size()<k){
				print("K must be less than number of transactions!");
				System.exit(0);
			}
			rfa.solution = rfa.solve(k);
//			rfa.printResults(logFile, k, "sa");
		}

	}
}
