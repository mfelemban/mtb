/**
 * Created by: Muhamad Felemban 
 * Aug 28, 2017
 */
package aims.heurestics;

import java.io.IOException;

import aims.util.DDG;


/**
 * @author mfelemban
 *
 */
public class Solve {
	public static void main(String[] args) throws IOException{
		String logFile = args[0];
		String temp = logFile.substring(logFile.lastIndexOf("/")+1, logFile.lastIndexOf("."));
//		double l = Double.parseDouble(temp);
		double l = Double.parseDouble(temp.split("_")[0]);
		
		
		DDG ddg = new DDG();		
		ddg.generate(logFile);
		String[] a = args[1].split(",");
		int[] K = new int[a.length];
		int i=0;
		for(String s:a){
			K[i] = Integer.parseInt(s);
			i++;
		}
		for(int k:K){

			RandomAssignment rfa = new RandomAssignment(ddg,k);
		
			rfa.solution = rfa.solve(k);
			//			efa.solution = efa.solve2(k);
			rfa.printResults(logFile, k,"rfa",l);
			IBStatisticsGenerator.boIntRatio(rfa.solution);
		}
//		for(int k:K){
//
//			FirstFitAssignment ffa = new FirstFitAssignment(ddg,k);
//		
//			ffa.solution = ffa.solve2(k);
//			//			efa.solution = efa.solve2(k);
//			ffa.printResults(logFile, k,"ffa",l);
//			IBStatisticsGenerator.boIntRatio(ffa.solution);
//		}
//		for(int k:K){
//
//			EdgeFitAssignment efa = new EdgeFitAssignment(ddg,k);
//		
//			efa.solution = efa.solve(k);
//			//			efa.solution = efa.solve2(k);
//			efa.printResults(logFile, k,"efa",l);
//			IBStatisticsGenerator.boIntRatio(efa.solution);
//		}
//		for(int k:K){
//
//			CrazyAssignment cfa = new CrazyAssignment(ddg,k);
//		
//			cfa.solution = cfa.solve(k);
//			//			efa.solution = efa.solve2(k);
//			cfa.printResults(logFile, k,"cfa",l);
//			IBStatisticsGenerator.boIntRatio(cfa.solution);
//		}
//		for(int k:K){
//
//			SkewedAssignment cfa = new SkewedAssignment(ddg,k);
//		
//			cfa.solution = cfa.solve(k);
//			//			efa.solution = efa.solve2(k);
//			cfa.printResults(logFile, k,"sa",l);
//			IBStatisticsGenerator.boIntRatio(cfa.solution);
//		}
		
		
	}
}	
