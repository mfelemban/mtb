/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.heurestics;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import aims.util.AIMSObject;
import aims.util.DDG;
import aims.util.Transaction;

public class Assignment {
	
	private static final Logger LOG = Logger.getLogger(Assignment.class);
	Map<Long,Set<Long>> E;
	Map<Integer,Transaction> T;
	IBSolution solution;
	DDG ddg;
	int k;
	public Assignment( DDG ddg,int k){
		this.ddg = ddg;
		this.T = new HashMap<Integer,Transaction>();
		for(Transaction t:ddg.getT()){
			this.T.put(t.getId(),t);
		}
		this.E = ddg.getE();
		this.k = k;
	}
	
	public static void print(String message,BufferedWriter bw){
//		LOG.info(message);
		try {
			bw.append(message+"\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void print(String message){
//		System.out.println(message);
	}
	
	public void printResults(String logFile, int k, String type,double l) throws IOException{
		File theDir = new File(logFile.substring(0, logFile.lastIndexOf("/"))+"/output/");
		if (!theDir.exists()) {
			try{
				theDir.mkdir();
			} 
			catch(SecurityException se){
				//handle it
			}        
		}
		File file = new File(logFile.substring(0, logFile.lastIndexOf("/"))+"/output/"+"k"+k+"_"+type+"_"  + l + ".output");
		BufferedWriter bw = new BufferedWriter(new FileWriter(file));
		print("*********************" + type + "******************************************",bw);
		print("File " + logFile ,bw);
		print("K " + k ,bw);
		print(Arrays.toString(solution.getTxVector()),bw);
		double[] ratios = IBStatisticsGenerator.boIntRatio(solution);
		if(solution!=null){
			print("# of shared objects " + ddg.getSharedObjects().size(),bw);
			double obj = solution.evaluateObjFunction();
			print("# BO=" + solution.getBoundaryObjects().size(),bw);
			print("F1=" + solution.F1,bw);
			print("F2=" + solution.F2,bw);
			print("F=" + obj,bw);
			print("IFairness=" + Metrics.intSizeFairness(solution),bw);
			print("TFairness=" + Metrics.txSizeFairness(solution),bw);
			print("Variance="+Metrics.sizeVariance(solution),bw);
			print("BST=" + Metrics.boundaryObjectRatio(solution),bw);
			print("non-shared to totol ratios=" + average(ratios), bw);
			printSolution(logFile,solution,type);
		}else{
			print("No feasible solution",bw);
		}
		
		
		solution.getIBCharcterstics();
		print("***************************************************************",bw);
//		System.out.println(solution.toString());
		bw.close();
	}
	
	public double average(double[] ratios){
		double total = 0;
		for(int i=0;i<ratios.length;i++){
			total += ratios[i];
		}
		return total/(double) ratios.length;
	}


	public void printSolution(String file,IBSolution solution,String type) throws IOException{
				String tFile,txFile;
				File theDir = new File(file.substring(0, file.lastIndexOf("/"))+"/sol");
				if (!theDir.exists()) {
					try{
						theDir.mkdir();
					} 
					catch(SecurityException se){
						//handle it
					}        
				}
				theDir = new File(file.substring(0, file.lastIndexOf("/"))+"/txsol");
				if (!theDir.exists()) {
//					System.out.println("creating directory: " + theDir.getName());
					try{
						theDir.mkdir();
					} 
					catch(SecurityException se){
						//handle it
					}        
				}
				if(type.equals("rfa")){
					tFile = file.substring(0, file.lastIndexOf(".")) + "_" + solution.getK()  + "_rfa.sol";
					tFile = tFile.substring(0,tFile.lastIndexOf("/")) + "/sol/" + tFile.substring(tFile.lastIndexOf("/")+1,tFile.length());
					txFile = file.substring(0, file.lastIndexOf(".")) + "_" + solution.getK() + "_rfa.txsol";
					txFile = txFile.substring(0,txFile.lastIndexOf("/")) + "/txsol/" + txFile.substring(txFile.lastIndexOf("/")+1,txFile.length());
				}else if(type.equals("ffa")){
					tFile = file.substring(0, file.lastIndexOf(".")) + "_" + solution.getK()  + "_ffa.sol";
					tFile = tFile.substring(0,tFile.lastIndexOf("/")) + "/sol/" + tFile.substring(tFile.lastIndexOf("/")+1,tFile.length());
					txFile = file.substring(0, file.lastIndexOf(".")) + "_" + solution.getK() + "_ffa.txsol";
					txFile = txFile.substring(0,txFile.lastIndexOf("/")) + "/txsol/" + txFile.substring(txFile.lastIndexOf("/")+1,txFile.length());
				}else if(type.equals("efa")){
					tFile = file.substring(0, file.lastIndexOf(".")) + "_" + solution.getK() + "_efa.sol";
					tFile = tFile.substring(0,tFile.lastIndexOf("/")) + "/sol/" + tFile.substring(tFile.lastIndexOf("/")+1,tFile.length());
					txFile = file.substring(0, file.lastIndexOf(".")) + "_" + solution.getK() + "_efa.txsol";
					txFile = txFile.substring(0,txFile.lastIndexOf("/")) + "/txsol/" + txFile.substring(txFile.lastIndexOf("/")+1,txFile.length());
				}else{
					tFile = file.substring(0, file.lastIndexOf(".")) + "_" + solution.getK() + "_sa.sol";
					tFile = tFile.substring(0,tFile.lastIndexOf("/")) + "/sol/" + tFile.substring(tFile.lastIndexOf("/")+1,tFile.length());
					txFile = file.substring(0, file.lastIndexOf(".")) + "_" + solution.getK() + "_sa.txsol";
					txFile = txFile.substring(0,txFile.lastIndexOf("/")) + "/txsol/" + txFile.substring(txFile.lastIndexOf("/")+1,txFile.length());
				}
				System.out.println("File " + tFile + " created");
				BufferedWriter bw = new BufferedWriter(new FileWriter(tFile));
				System.out.println("File " + txFile + " created");
				BufferedWriter bw2 = new BufferedWriter(new FileWriter(txFile));
//				for(int i=0;i<solution.getK();i++){
//					for(Long obj:solution.ibInternalObjects.get(i)){
//						bw.write(obj+","+(i+1)+"\n");
//					}
//				}
//				for(int i=0;i<solution.getK();i++){
//					for(Long obj:solution.ibBoundaryObjects.get(i)){
//						bw.write(obj+","+(i+1)+"\n");
//					}
//				}
				for(int i=0;i<solution.getK();i++){
					ArrayList<Integer> tempSorted = new ArrayList<Integer>(solution.ibTxSet.get(i));
					Collections.sort(tempSorted);
					for(Integer obj:tempSorted){
						bw2.write(obj+","+(i+1)+"\n");
						for(AIMSObject a:T.get(obj).getObjects())
							bw.write(a.getID() + ","+ obj +"," + (i+1)+"\n");
					}
				}
				bw2.close();
				bw.close();
	}
}
