/**
 * Created by: Muhamad Felemban 
 * Jun 29, 2017
 */
package aims.heurestics;

import org.apache.log4j.Logger;

/**
 * @author mfelemban
 *
 */
public class IBStatisticsGenerator {
	private static final Logger LOG = Logger.getLogger(IBStatisticsGenerator.class);
	public static double[] boIntRatio(IBSolution sol){
		double total=0;;
		double[] ratios = new double[sol.k];
		for(int i=0;i<sol.k;i++){
			double ratio = ((double)sol.ibInternalObjects.get(i).size() / (double)(sol.ibInternalObjects.get(i).size() + sol.ibBoundaryObjects.get(i).size()) );
			total += ratio;
			ratios[i] = ratio;
		}
		return ratios;
	}
}
