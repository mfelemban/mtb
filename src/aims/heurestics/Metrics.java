/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.heurestics;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import aims.util.Transaction;


public class Metrics {

	public static double intSizeFairness(IBSolution solution){
		int k = solution.k;
		double[] sizes = new double[k];
		int i =0;
		for(i=0;i<k;i++){ 
//			solution.ibBoundaryObjects
			sizes[i] = solution.ibBoundaryObjects.get(i).size();
		}
		return JainFairness(sizes);
	}

	public static double txSizeFairness(IBSolution solution){
		int k = solution.k;
		double[] sizes = new double[k];
		int i =0;
		for(i=0;i<k;i++){
			sizes[i] = solution.ibTxSet.get(i).size();
		}
		return JainFairness(sizes);
	}
	
	public static double sizeVariance(IBSolution solution){
		int k = solution.k;
		ArrayList<Integer> sizes = new ArrayList<Integer>();
		for(int i=0;i<k;i++){
			sizes.add(solution.ibTxSet.get(i).size());
		}
		return computeSampleVariance(sizes);
		
	}
	
	public static double computeSampleVariance(ArrayList<Integer> ss){
		// Compute average
		double[] sample = new double[ss.size()];
		int i=0;
		for(Integer d:ss)
			sample[i++] = d;
		double sampleSum = 0;
		for(i=0;i<sample.length;i++){
			sampleSum += sample[i];
		}
		double sampleAvg = sampleSum/(double)sample.length;
		double sampleVar = 0;
		for(i=0;i<sample.length;i++){
			sampleVar += Math.pow((sampleAvg-sample[i]), 2);
		}
		sampleVar = sampleVar/(double)sample.length;
		return Math.sqrt(sampleVar);
	}

	public static double computeAverage(ArrayList<Integer> ss){
		// Compute average
		double[] sample = new double[ss.size()];
		int i=0;
		for(Integer d:ss)
			sample[i++] = d;
		double sampleSum = 0;
		for(i=0;i<sample.length;i++){
			sampleSum += sample[i];
		}
		double sampleAvg = sampleSum/(double)sample.length;
		return sampleAvg;
	}

	public static double JainFairness(double[] x){
		double J=0;
		double sum = 0;
		double squareSum = 0;
		double size = x.length;
		for(int i=0;i<x.length;i++){
			sum += x[i];
			squareSum += Math.pow(x[i], 2);
		}
		J = (Math.pow(sum, 2))/(size * squareSum);
		return J;
	}

	public static double boundaryObjectRatio(IBSolution solution){
		Set<Long> sObjects = new HashSet<Long>();
		Set<Long> bObjects = solution.getBoundaryObjects();
		Transaction tt = null;
		int k = solution.k;
		int i =0;
		for(i=0;i<k;i++){
			for(Integer t:solution.ibTxSet.get(i)){
				tt = solution.getDDG().getT().get(t);
				sObjects.addAll(tt.getSharedObjects());
			}
		}
		double sSize = sObjects.size();
		double bSize = bObjects.size();
		return bSize/sSize;
	}
	
	
/*
 * Violating edge are the edges going out from a boundary object to another boundary object 
 */
	public static double averageEdgeViolation(IBSolution solution){
		Set<Long> boundaries = solution.getBoundaryObjects();
		List<Long> b = new ArrayList<Long>(boundaries);
		Map<Long,Set<Long>> E = solution.ddg.getE();
		double violation = 0;
		for(int i=0;i<b.size();i++){
			Set<Long> iNeibor = new HashSet<Long>(E.get(b.get(i)));
			iNeibor.retainAll(boundaries);
			violation+= iNeibor.size();
		}
		return violation;
	}
	
	public static double averageCrossingEdge(IBSolution solution){
		Set<Long> boundaries = solution.getBoundaryObjects();
		List<Long> b = new ArrayList<Long>(boundaries);
		Map<Long,Set<Long>> E = solution.ddg.getE();
		double violation = 0;
		for(int i=0;i<b.size();i++){
			Set<Long> iNeibor = new HashSet<Long>(E.get(b.get(i)));
			iNeibor.removeAll(boundaries);
			violation+= iNeibor.size();
		}
		return violation;
	}
}
