package aims.hotspot;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * 
 * TupleFrequency count the frequency of usage for each tuple in the DB according to transactions activities
 * 
 * @author Anas Daghistani <anas@purdue.edu>
 *
 */

public class HotspotFinderNormalized {
	private Hashtable<Integer, TupleInfo> tupleInfoHT;
	private Hashtable<Integer, TransactionInfoNormalized> transactionInfoHT;
	public ArrayList<TupleInfo> tupleInfoArray;
	public ArrayList<TransactionInfoNormalized> transactionInfoArray;
	private Comparator<TupleInfo> tupleInfoComparator = new TupleInfoComparator();
	private Comparator<TransactionInfoNormalized> transactionInfoComparator = new TransactionInfoComparator();


	public HotspotFinderNormalized(int tupleInitialCapacity,int transactionInitialCapacity) {
		super();
		this.tupleInfoHT = new Hashtable<Integer, TupleInfo>(tupleInitialCapacity);
		this.transactionInfoHT = new Hashtable<Integer, TransactionInfoNormalized>(transactionInitialCapacity);
	}

	public HotspotFinderNormalized() {
		super();
		this.tupleInfoHT = new Hashtable<Integer, TupleInfo>();
		this.transactionInfoHT = new Hashtable<Integer, TransactionInfoNormalized>();
	}

	public void calculateAndSortTransactionsBasedOnCost(){
		Enumeration<TransactionInfoNormalized> Tx = transactionInfoHT.elements();
		transactionInfoArray = new ArrayList<TransactionInfoNormalized>(transactionInfoHT.size());
		TransactionInfoNormalized currentTx;
		while(Tx.hasMoreElements()){
			currentTx = Tx.nextElement();
			currentTx.calculateCost(tupleInfoHT);
			transactionInfoArray.add(currentTx);
		}
		Collections.sort(transactionInfoArray,transactionInfoComparator);//largest to lowest
	}

	public TransactionInfoNormalized peekNextHotspotTransaction(){
		return transactionInfoArray.get(0);
	}

	public TransactionInfoNormalized getNextHotspotTransaction(){
		TransactionInfoNormalized tx = transactionInfoArray.remove(0);
		//System.out.println("Number of tuples accesed by the current hotspot: "+tx.accessedTupleId.size());
		for(Integer tupleId : tx.accessedTupleId){
			//System.out.println(" -------tx.accessedTupleId.get(i): "+tupleId);
			for(Integer accessedTx : tupleInfoHT.get(tupleId).accessedByTransactionId){
				if(accessedTx != tx.transactionId){
					//System.out.println("sssssssssssss transactionInfoHT.get(accessedTx).transactionId: "+transactionInfoHT.get(accessedTx).transactionId);
					transactionInfoHT.get(accessedTx).updateCost(tupleInfoHT.get(tupleId).frequency, tupleId);
				}
			}
		}
		Collections.sort(transactionInfoArray,transactionInfoComparator);//largest to lowest
		return tx;
	}

	public void sortTuplesBasedOnFrequency(){
		Collection<TupleInfo> f = tupleInfoHT.values();
		tupleInfoArray = new ArrayList<TupleInfo>(tupleInfoHT.size());
		/*
		Enumeration<TupleInfo> allTupleInfo = tupleInfoHT.elements();
		while(allTupleInfo.hasMoreElements()){
			tupleInfoArray.add(allTupleInfo.nextElement());
		}
		 */		
		tupleInfoArray.addAll(f);
		Collections.sort(tupleInfoArray,tupleInfoComparator);//largest to lowest
	}

	public void countNewTransactionTuples(TransactionActivity tx){
		//Add the Transaction if this is the first time to be received or update its repetition
		if(!transactionInfoHT.containsKey(tx.tx_id)){
			transactionInfoHT.put(tx.tx_id, new TransactionInfoNormalized(tx.tx_id,tx.src_id,tx.dest_id));
		}
		else{
			transactionInfoHT.get(tx.tx_id).repetition++;
		}

		for(int i=0; i<tx.src_id.length; i++){
			if(tupleInfoHT.containsKey(tx.src_id[i])){
				tupleInfoHT.get(tx.src_id[i]).frequency++;
				if(!tupleInfoHT.get(tx.src_id[i]).accessedByTransactionId.contains(tx.tx_id)){
					tupleInfoHT.get(tx.src_id[i]).accessedByTransactionId.add(tx.tx_id);
				}
			}
			else{
				tupleInfoHT.put(tx.src_id[i], new TupleInfo(tx.tx_id, tx.src_id[i]));
			}
		}
		for(int i=0; i<tx.dest_id.length; i++){
			if(tupleInfoHT.containsKey(tx.dest_id[i])){
				tupleInfoHT.get(tx.dest_id[i]).frequency++;
				if(!tupleInfoHT.get(tx.dest_id[i]).accessedByTransactionId.contains(tx.tx_id)){
					tupleInfoHT.get(tx.dest_id[i]).accessedByTransactionId.add(tx.tx_id);
				}
			}
			else{
				tupleInfoHT.put(tx.dest_id[i], new TupleInfo(tx.tx_id, tx.dest_id[i]));
			}
		}
	}

	public void cleartupleInfoHT(){
		tupleInfoHT.clear();
	}

	/**
	 * 
	 * Comparator for TupleInfo based on frequency from larger to smaller 
	 *
	 */
	private class TupleInfoComparator implements Comparator<TupleInfo>{

		@Override
		public int compare(TupleInfo x, TupleInfo y) {
			if (x.frequency > y.frequency)
				return -1;

			if (x.frequency < y.frequency)			
				return 1;

			return 0;
		}
	}

	/**
	 * 
	 * Comparator for TransactionInfoNormalized based on cost from larger to smaller 
	 *
	 */
	private class TransactionInfoComparator implements Comparator<TransactionInfoNormalized>{

		@Override
		public int compare(TransactionInfoNormalized x, TransactionInfoNormalized y) {
			if (x.cost > y.cost)
				return -1;

			if (x.cost < y.cost)			
				return 1;

			return 0;
		}
	}
}
