package aims.hotspot;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * 
 * TransactionInfo hold information about received transaction 
 * 
 * @author Anas Daghistani <anas@purdue.edu>
 *
 */
public class TransactionInfo {
	public int transactionId;
	public ArrayList<Integer> accessedTupleId;
	public int originalAccessedTupleCount;
	public int cost;
	public int repetition;
	
	public TransactionInfo(int tupleId, int[] src_id, int[] dest_id) {
		super();
		this.accessedTupleId = new ArrayList<Integer>();
		this.transactionId = tupleId;
		this.cost = 0;
		this.repetition = 1;
		for(int s : src_id){
			this.accessedTupleId.add(s);
		}
		for(int d : dest_id){
			if(!this.accessedTupleId.contains(d)){
				this.accessedTupleId.add(d);
			}
		}
		this.originalAccessedTupleCount = this.accessedTupleId.size();
	}
	
	public void calculateCost(Hashtable<Integer, TupleInfo> tupleInfoHT){
		for(int tupleid : accessedTupleId){
			cost += tupleInfoHT.get(tupleid).frequency;
		}
	}
	
	public void updateCost(int tupleFrequency, Integer tupleId){
		cost -= tupleFrequency;
		if(!accessedTupleId.remove(tupleId)){
			System.err.println("XXXXXXXXXXXXXXX error didn't found tuple: transactionId: "+transactionId+" remove tupleId: "+tupleId+" that have frequency: "+tupleFrequency+" cost before removing tuple: "+cost);
			System.exit(0);
		}
	}
}
