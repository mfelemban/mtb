package aims.hotspot;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * 
 * TransactionInfoNormalized hold information about received transaction and uses the normalized cost equations
 * 
 * @author Anas Daghistani <anas@purdue.edu>
 *
 */
public class TransactionInfoNormalized {
	public int transactionId;
	public ArrayList<Integer> accessedTupleId;
	public int originalAccessedTupleCount;
	public double cost;
	public int repetition;
	
	public TransactionInfoNormalized(int tupleId, int[] src_id, int[] dest_id) {
		super();
		this.accessedTupleId = new ArrayList<Integer>();
		this.transactionId = tupleId;
		this.cost = 0;
		this.repetition = 1;
		for(int s : src_id){
			this.accessedTupleId.add(s);
		}
		for(int d : dest_id){
			if(!this.accessedTupleId.contains(d)){
				this.accessedTupleId.add(d);
			}
		}
		this.originalAccessedTupleCount = this.accessedTupleId.size();
	}
	
	public void calculateCost(Hashtable<Integer, TupleInfo> tupleInfoHT){
		for(int tupleid : accessedTupleId){
			cost += tupleInfoHT.get(tupleid).frequency;
		}
		cost /= accessedTupleId.size();
	}
	
	public void updateCost(int tupleFrequency, Integer tupleId){
		cost *= accessedTupleId.size();
		cost -= tupleFrequency;
		if(!accessedTupleId.remove(tupleId)){
			System.err.println("XXXXXXXXXXXXXXX error didn't found tuple: transactionId: "+transactionId+" remove tupleId: "+tupleId+" that have frequency: "+tupleFrequency+" cost before removing tuple: "+cost);
			System.exit(0);
		}
		cost /= accessedTupleId.size();
	}
	
	public String toString(){
		return transactionId+":"+repetition;
	}
}
