package aims.hotspot;

/**
 * 
 * TransactionActivity class to hold information about transaction activity
 * 
 * @author Anas Daghistani <anas@purdue.edu>
 *
 */

public class TransactionActivity {
	public int tx_type;
	public int tx_id;
	public int[] src_id;
	public int[] dest_id;
	public String amount;
	public String timestamp;
	
	public TransactionActivity(int tx_type, int tx_id, int[] src_id,
			int[] dest_id, String amount, String timestamp) {
		super();
		this.tx_type = tx_type;
		this.tx_id = tx_id;
		this.src_id = src_id;
		this.dest_id = dest_id;
		this.amount = amount;
		this.timestamp = timestamp;
	}

	public TransactionActivity(int tx_type, int tx_id, String timestamp) {
		super();
		this.tx_type = tx_type;
		this.tx_id = tx_id;
		this.timestamp = timestamp;
	}

	public TransactionActivity() {
		super();
	}
	
	
}
