package aims.hotspot;
import java.util.ArrayList;

/**
 * 
 * TupleInfo hold information about accessed tuples
 * 
 * @author Anas Daghistani <anas@purdue.edu>
 *
 */

public class TupleInfo {
	public int tupleId;
	public int frequency;
	public ArrayList<Integer> accessedByTransactionId;
	
	public TupleInfo(int transactionId, int tupleId) {
		super();
		this.accessedByTransactionId = new ArrayList<Integer>();
		this.tupleId = tupleId;
		this.frequency = 1;
		this.accessedByTransactionId.add(transactionId);
	}
	
}
