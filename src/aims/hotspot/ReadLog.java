package aims.hotspot;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * 
 * ReadLog extract information about transactions activities from log files
 * 
 * @author Anas Daghistani <anas@purdue.edu>
 *
 */

public class ReadLog {
	private BufferedReader reader;

	public ReadLog(String file) {
		super();
		readFile(file);
	}

	public void readFile(String file){
		try {
			this.reader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			System.err.println("File not found");
			e.printStackTrace();
		}
	}
	
	public TransactionActivity readLine(){
		TransactionActivity tx = new TransactionActivity();
		try {
			if (reader.ready()) {
				String[] data = reader.readLine().split(",");
				if(data.length==3){ //we don't have malicious transactions represented like this now so this will not work in new workloads 
					tx.tx_type  = 101;
					tx.tx_id = Integer.parseInt(data[1]);
					tx.timestamp = data[2];
				}
				else{//we have 6 for old workloads and 7 from new one
					tx.tx_type  = Integer.parseInt(data[0]);
					tx.tx_id = Integer.parseInt(data[1]);

					String[] src_id = data[2].split(":");
					tx.src_id = new int[src_id.length];
					for(int i=0; i<src_id.length; i++){
						tx.src_id[i] = Integer.parseInt(src_id[i]);
					}

					String[] dest_id = data[3].split(":");
					tx.dest_id = new int[dest_id.length];
					for(int i=0; i<dest_id.length; i++){
						tx.dest_id[i] = Integer.parseInt(dest_id[i]);
					}
					tx.amount = data[4];
					tx.timestamp = data[5];
				}
				return tx;
			}
		} catch (NumberFormatException e) {
			System.err.println("NumberFormatException in readLine");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("IOException in readLine");
			e.printStackTrace();
		}
		
		try {
			reader.close();
		} catch (IOException e) {
			System.err.println("IOException: could not close the BufferedReader in readLine");
			e.printStackTrace();
		}
		return null;
	}

}
