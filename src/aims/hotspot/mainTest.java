package aims.hotspot;
/**
 * 
 * Extract information about tuples usage from transactions activities in log files
 * 
 * @author Anas Daghistani <anas@purdue.edu>
 *
 */

public class mainTest {
	public static void main(String[] args){
		//String fileName = "s1_1.5_s2_1.0_100u_h.logm";
		//String fileName = "s1_1.5_s2_10.0.sql";
		//String fileName = "s1_1.5_m_0.1.sql";
		//String fileName = "s1_1.5_m_0.01.sql";
		//String fileName = "s1_3.5_m_0.1.sql";
		//String fileName = "new_workload_0.5.sql";
		//String fileName = "workload_0.25_0.5.sql";
		//String fileName = "workload_0.25_1.0.sql";
		//String fileName = "workload_0.5_0.5.sql";
		//String fileName = "workload_0.5_1.0.sql";
		String fileName = "workload_0.5.sql";
		//String fileName = "workload_0.75_0.5.sql";
		//String fileName = "workload_0.75_1.0.sql";
		
		int tupleInitialCapacity = 2000;
		int transactionInitialCapacity = 1000;
		int nubmerOfPrintedTopTransaction = 5;
		int numberOfHotspotTransactions = 50;

		TransactionInfo currentHotspotTx;
		HotspotFinder hotspotFinder = new HotspotFinder(tupleInitialCapacity, transactionInitialCapacity);
		ReadLog readLog = new ReadLog(fileName);
		TransactionActivity tx = readLog.readLine();
		System.out.println("Start reading the file: "+fileName);
		while(tx != null){
			if(tx.tx_type != 101){
				hotspotFinder.countNewTransactionTuples(tx);
			}
			tx = readLog.readLine();
		}
		System.out.println("Finished reading the file: "+fileName);
		System.out.println("Start sorting...");
		//tupleFrequency.sortTuplesBasedOnFrequency();
		//tupleFrequency.cleartupleInfoHT();
		hotspotFinder.calculateAndSortTransactionsBasedOnCost();
		System.out.println("Finished sorting...");
		//printTupleFrequencyArrayTop(topTuples,tupleFrequency);
		printTransactionInfoArrayTop(nubmerOfPrintedTopTransaction, hotspotFinder);

		System.out.println("----------------------------------------------------------------------------------");
		System.out.println("\nStart finding ("+numberOfHotspotTransactions+") hotspot transactions...");
		for(int i=1; i<=numberOfHotspotTransactions; i++){
			currentHotspotTx = hotspotFinder.getNextHotspotTransaction();
			printHostspotTransaction(i,currentHotspotTx);
			printTransactionInfoArrayTop(nubmerOfPrintedTopTransaction, hotspotFinder);
		}
	}

	private static void printHostspotTransaction(int i, TransactionInfo tx) {
		System.out.print("Hostspot Transaction number ( "+i+" ) :::: TxID: "+tx.transactionId+"; Cost: "+tx.cost+"; Repetition: "+tx.repetition+"; originalAccessedTupleCount: "+tx.originalAccessedTupleCount+"; currentTupleCount: "+tx.accessedTupleId.size()+"; Acceassed_TupleIDs: ");
		for(int tuple : tx.accessedTupleId){
			System.out.print(tuple+", ");
		}
		System.out.println("");
	}

	private static void printTransactionInfoArrayTop(int topTransaction, HotspotFinder hotspotFinder) {
		System.out.println("Top (("+topTransaction+")) Accessed Transaction out of (("+hotspotFinder.transactionInfoArray.size()+")):");
		System.out.println("i \t:: TxID\t\t:: Cost\t\t:: Repetition\t:: originalTupleCount\t:: currentTupleCount\t:: Acceassed_TupleIDs");
		for(int i=0; i<topTransaction; i++){
			System.out.printf("%-7d :: %-12d :: %-12d :: %-12d :: %-20d :: %-20d :: ",(i+1), hotspotFinder.transactionInfoArray.get(i).transactionId,hotspotFinder.transactionInfoArray.get(i).cost, hotspotFinder.transactionInfoArray.get(i).repetition, hotspotFinder.transactionInfoArray.get(i).originalAccessedTupleCount, hotspotFinder.transactionInfoArray.get(i).accessedTupleId.size());
			for(int tuple : hotspotFinder.transactionInfoArray.get(i).accessedTupleId){
				System.out.print(tuple+", ");
			}
			System.out.println("");
		}
		System.out.println("\n----------------------------------------------------------------------------------\n");
	}

	private static void printTupleFrequencyArrayTop(int topTuples, HotspotFinder tupleFrequency) {
		System.out.println("Top (("+topTuples+")) Accessed Tuples out of (("+tupleFrequency.tupleInfoArray.size()+"))");
		System.out.println("i \t:: TupleId \t:: Frequency");
		for(int i=0; i<topTuples; i++){
			System.out.println((i+1)+"\t:: "+tupleFrequency.tupleInfoArray.get(i).tupleId+"\t:: "+tupleFrequency.tupleInfoArray.get(i).frequency);
		}

	}

	private void printFileWhileReading(TransactionActivity tx, ReadLog readLog){
		while(tx != null){
			if(tx.tx_type != 101){
				System.out.print("tx_type:"+tx.tx_type+" tx_id:"+tx.tx_id+" amount:"+tx.amount+" timestamp:"+tx.timestamp+" src_id(s):");
				for(int j=0; j<tx.src_id.length; j++){
					System.out.print(tx.src_id[j]+",");
				}
				System.out.print(" dest_id(s): ");
				for(int j=0; j<tx.dest_id.length; j++){
					System.out.print(tx.dest_id[j]+",");
				}
				System.out.println("");
			}
			else{
				System.out.println("tx_type:"+tx.tx_type+" tx_id:"+tx.tx_id+" timestamp:"+tx.timestamp);
			}
			tx = readLog.readLine();
		}
	}
}
