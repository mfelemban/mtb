/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
package aims.DTQR;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;

import aims.util.FairLock;

import com.oltpbenchmark.api.BenchmarkModule;

public class IDSDTQR implements Runnable {

	private static final Logger LOG = Logger.getLogger(IDSDTQR.class);

	private Integer txid;
	private long delay;
	BenchmarkModule benchmarkModule;
	Map<Integer,Integer> txidMap;
	FairLock fl;
	Connection newConn;
	Integer malID;
	IDSAlert idAlert;
	int ib;
	public IDSDTQR(Integer txid,BenchmarkModule benchmarkModule,Connection newConn,
			FairLock fl,
			Integer malID,Map<Integer,Integer> txidMap,
			int ib,IDSAlert idAlert) throws SQLException {
		super();
		this.malID = malID;
		this.fl = fl;
		this.txid = txid;
		this.delay = benchmarkModule.getWorkloadConfiguration().getXmlConfig().getLong("delay");;
		this.benchmarkModule = benchmarkModule;
		this.newConn = newConn;
		this.txidMap=txidMap;
		this.ib = ib;
		this.idAlert=idAlert;
	}



	public void run() {
		try{
			long malTxid = 0;
			synchronized(txidMap){
				while(!txidMap.containsKey(txid)){
					txidMap.wait();
				}
			}
			Connection conn = benchmarkModule.makeConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			PreparedStatement time = conn.prepareStatement("select current_timestamp;");
			ResultSet rsTime = time.executeQuery();
			rsTime.next();
			Timestamp ts = rsTime.getTimestamp(1);

			malTxid = txidMap.get(txid);
//						LOG.info(malTxid +  "  in IDS");
			Thread.sleep(delay);

			boolean success = false;
			while(!success){
				try {
					PreparedStatement ps1 = conn.prepareStatement("select blockTxn(?,?,?);");
					ps1.setLong(1, malTxid);
					ps1.setTimestamp(2, ts);
					ps1.setInt(3, ib);
					ps1.execute();
					conn.commit();
					success = true;
					if(success){
					}else{
						conn.rollback();
					}

				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}

			


			PreparedStatement ps = conn.prepareStatement("select f.commit_time, s.detection_time_stamp from"
					+ " blocked_transactions_table s , transactions_commit_time f where s.transaction_id = ? "
					+ "and f.transaction_id = ?;");
			ps.setLong(1,malTxid);
			ps.setLong(2,malTxid);
			ResultSet rs = ps.executeQuery();
			//			LOG.info(ps.toString());
			rs.next();
			idAlert.setStart(rs.getTimestamp(1));
			idAlert.setEnd(rs.getTimestamp(2));
			idAlert.setActive();
			
//			LOG.info("Response for " + malTxid + " is done [" + idAlert.getStart().toString() + "," + idAlert.getEnd().toString() + "]");


			ExecutorService pool = Executors.newSingleThreadScheduledExecutor();
			Callable<Boolean>aims = new RecoverDTQR(malTxid,benchmarkModule,newConn,ts,fl,ib,idAlert);
			Future<Boolean> recFuture = pool.submit(aims);

			boolean recSucess= recFuture.get();
			while(!recSucess){
				aims = new RecoverDTQR(malTxid,benchmarkModule,newConn,ts,fl,ib,idAlert);
				recFuture = pool.submit(aims);
				recSucess= recFuture.get();				
			}
			pool.shutdown();
			conn.close();
			
			idAlert.setPassive();


		}catch(Exception e){
			e.printStackTrace();
		}
	}


}