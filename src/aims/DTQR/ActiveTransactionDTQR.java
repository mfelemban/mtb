/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
package aims.DTQR;

import java.util.ArrayList;


public class ActiveTransactionDTQR {
	ArrayList<QueryDTQR> tx;
	long startTime;
	long runTime;
	long totalRuntime;
	long bsetBlockageTime=0,rsetBlockageTime=0,BOBlockageTime=0;
	long submissionTime;
	long bsetBlockedSince,rsetBlockedSince,BOBlockedSince;
	public boolean boundary;
	public boolean boBlocked,bset,rset;
	ArrayList<Long> boundaries;
	boolean currentlyBsetBlocked = false,currentlyRsetBlocked = false,currentlyBOBlocked=false;
	int tuplesBlocked=0;
	int tuplesToRepair=0;
	int totalTuplesBlocked=0;
	int IB;


	public ActiveTransactionDTQR(ArrayList<QueryDTQR> tx,int IB,long submissionTime){
		this.tx = tx;
		this.submissionTime = submissionTime;
		this.boundaries = new ArrayList<Long>();
		this.boBlocked = false;
		this.bset = false;
		this.rset = false;
		this.IB = IB;
	}
	

	public int getIB(){
		return IB;
	}

	public int getTuplesBlocked(){
		return tuplesBlocked;
	}

	public int getTuplesToRepair(){
		return tuplesToRepair;
	}

	public int getTotalBlockedTuples(){
		return totalTuplesBlocked;
	}

	public void setBoBlocked(){
		this.boBlocked = true;
	}
	
	public void setBSet(){
		this.bset = true;
	}
	
	public void setRSet(){
		this.rset = true;
	}

	public ArrayList<QueryDTQR> getTx(){
		return tx;
	}

	public void resetBsetBlocking(){
		this.bsetBlockageTime=0;
	}
	
	public void resetRsetBlocking(){
		this.rsetBlockageTime=0;
	}

	public void setBoundaries(ArrayList<Long> b){
		this.boundaries =b;
	}

	public boolean isBoBlocked(){
		return boBlocked;
	}
	
	public boolean isBSet(){
		return bset;
	}
	
	public boolean isRSet(){
		return rset;
	}

	public ArrayList<Long> getBoundaries(){
		return boundaries;
	}

	public boolean isBoundary(){
		return boundary;
	}

	public long getSubmissionTime(){
		return submissionTime;
	}

	public void start(){
		this.startTime = System.currentTimeMillis();
	}

	public void end(){
		this.runTime = System.currentTimeMillis() - startTime;
		this.totalRuntime += runTime;
	}

	public long getRunTime(){
		return this.runTime;
	}
	
	public long getTotalRunTime(){
		return totalRuntime;
	}

	public long getBsetBlockageTime(){
		return bsetBlockageTime;
	}
	
	public long getBOBlockageTime(){
		return BOBlockageTime;
	}
	
	public long getRsetBlockageTime(){
		return rsetBlockageTime;
	}

	public void startBsetBlocking(long ts){
		if(!currentlyBsetBlocked){
			this.bsetBlockedSince = ts;
			currentlyBsetBlocked = true;
		}
	}
	
	public void startRsetBlocking(long ts){
		if(!currentlyRsetBlocked){
			this.rsetBlockedSince = ts;
			currentlyRsetBlocked = true;
		}
	}
	
	public void setTuplesBlocked(int n){
		this.tuplesBlocked = n;
	}

	public void setTuplesToRepair(int n){
		this.tuplesToRepair = n;
	}

	public void setTotalTuplesBlocked(int n){
		this.totalTuplesBlocked = n;
	}

	public void endBsetBlocking(long current){
		if(currentlyBsetBlocked){
			this.bsetBlockageTime += current - bsetBlockedSince;
			this.currentlyBsetBlocked = false;
		}
	}
	
	public void endRsetBlocking(long current){
		if(currentlyRsetBlocked){
			this.rsetBlockageTime += current - rsetBlockedSince;
			this.currentlyRsetBlocked = false;
		}
	}
	
	public void startBOBlocking(long ts){
		if(!currentlyBOBlocked){
			this.BOBlockedSince = ts;
			currentlyBOBlocked = true;
		}
	}
	
	public void endBOBlocking(long current){
		if(currentlyBOBlocked){
			this.BOBlockageTime += current - BOBlockedSince;
			this.currentlyBOBlocked = false;
		}
	}

	public String toString(){
		return tx.get(0).getTxid()+":"+tx.get(0).getType()+":"+tx.get(0).getThreadId();
	}

}
