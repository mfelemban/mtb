/**
 * Created by: Muhamad Felemban 
 * Nov 9, 2017
 */
package aims.DTQR;

import java.sql.Timestamp;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author mfelemban
 *
 */
public class IDSAlert {
	AtomicBoolean malActive;
	Timestamp start,end;
	public IDSAlert(){
		this.malActive = new AtomicBoolean();
		this.start = new Timestamp(-1);
		this.end = new Timestamp(-1);
	}
	
	public synchronized void setActive(){
		malActive.set(true);
	}
	
	public boolean isActive(){
		return malActive.get();
	}
	
	public synchronized void setPassive(){
		malActive.set(false);
		// Default values for inactive malicious transactions
		start = new Timestamp(-1);
		end = new Timestamp(-1);
	}
	
	public synchronized void setStart(Timestamp start){
		this.start = start;
	}
	
	public synchronized void setEnd(Timestamp end){
		this.end = end;
	}
	
	public Timestamp getStart(){
		return start;
	}
	
	public Timestamp getEnd(){
		return end;
	}
	
}
