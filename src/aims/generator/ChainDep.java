/**
 * Created by: Muhamad Felemban 
 * Jul 16, 2017
 */
package aims.generator;

import java.util.ArrayList;
import java.util.Random;

/**
 * @author mfelemban
 *
 */
public class ChainDep {
	public static void generateTDG(){
		int tupleID = 0;
		int numTx = 100;
		int min = 5,max=10;
		double depProb = 0.4;
		TuplePool pool = new TuplePool(100);
		for(int i=0;i<numTx;i++){
			int txSize = (new Random().nextInt((max - min)) + 1) + min;
			int txType = new Random().nextInt((5 - 3) + 1) + 3;
			Long[] src,dest;
			int srcSize=0,destSize=0;
			if(txType == 5){
				srcSize = txSize / 2;
				destSize = txSize - srcSize;
			}
			else if(txType == 4){
				srcSize = txSize - 1;
				destSize = 1;
			}
			else if(txType == 3){
				srcSize = 1;
				destSize = txSize -1;
			}
			src = new Long[srcSize];
			dest = new Long[destSize];
			if(i!=0 && Math.random()>depProb){
				ArrayList<Integer> destination;
				if(pool.size()>=destSize){
					destination = pool.pickN(destSize);
				}else{
					
				}
			}
			
			
			
		}
	}
}

class TuplePool{
	ArrayList<Integer> pool = new ArrayList<Integer>();
	int size;
	TuplePool(int size){
		this.size = size;
	}
	
	void add(ArrayList<Integer> tuples){
		int numTuples = tuples.size();
		for(int i=0;i<numTuples;i++){
			pool.remove(pool.size() - 1 -i);
		}
		
		pool.addAll(tuples);
	}
	
	int size(){
		return pool.size();
	}
	
	ArrayList<Integer> pickN(int n){
		ArrayList<Integer> picked = new ArrayList<Integer>();
		for(int i=0;i<n;i++){
			int c = pool.get(new Random().nextInt(pool.size()));
			if(picked.contains(c))
				c = pool.get(new Random().nextInt(pool.size()));
			picked.add(c);
		}
		return picked;
	}
}
