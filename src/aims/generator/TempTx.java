/**
 * Created by: Muhamad Felemban 
 * Jun 27, 2017
 */
package aims.generator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author mfelemban
 *
 */
public class TempTx {
	ArrayList<Long> tuples = new ArrayList<Long>();
	ArrayList<Long> readTuples = new ArrayList<Long>();
	ArrayList<Long> writeTuples = new ArrayList<Long>();
	Map<Integer,Integer> neighbors = new HashMap<Integer,Integer>();
	int size;
	int id;
	int type,srcSize, destSize;
	public TempTx(int id,int size){
		this.id = id;
		this.size = size;
	}

	public TempTx(int id){
		this.id = id;
	}

	public void setSize(int buffer){
		 
		this.size = new Random().nextInt(Math.max(writeTuples.size(), readTuples.size())+buffer) +writeTuples.size()+readTuples.size() + 2;
		if(size<=2)
			size++;
		if(readTuples.size()>=2){
			if(Math.random()>0.5){
				type = 4;
			}else{
				type = 5;
			}
		}else{
			if(Math.random()<=0.3333){
				type = 3;
			}else if(Math.random()>0.3333 && Math.random()<=0.6666){
				type = 4;
			}else {
				type = 5;
			}
		}
		
		if(type == 5){
			srcSize = size / 2;
			destSize = size - srcSize;
		}
		else if(type == 4){
			srcSize = size - 1;
			destSize = 1;
		}
		else if(type == 3){
			srcSize = 1;
			destSize = size -1;
		}
	}

	public void addTuple(Long tuple){
		tuples.add(tuple);
		if(readTuples.size()<srcSize){
			readTuples.add(tuple);
		}else{
			writeTuples.add(tuple);
		}
	}

	public void addReadTuple(Long tuple){
		tuples.add(tuple);
		readTuples.add(tuple);
	}

	public void addWriteTuple(Long tuple){
		tuples.add(tuple);
		writeTuples.add(tuple);
	}

	public ArrayList<Long> getTuples(){
		return tuples;
	}

	public boolean full(){
		return tuples.size() == size;
	}

	public int getSharingDensity(){
		int sd = 0;
		for(Map.Entry<Integer,Integer> entry:neighbors.entrySet()){
			Integer key = entry.getKey();
			if(key>id)
				sd ++;
		}
		return sd;
	}
	public String toString(){
		return id+ ":"+ type + ":" +size+":"+tuples.size()+":" + readTuples.toString() + ":"+ writeTuples.toString() +"\n";
	}
}
