/**
 * Created by: Muhamad Felemban 
 * Jul 12, 2017
 */
package aims.generator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Stack;



/**
 * @author mfelemban
 *
 */
public class GenerateTDG {
	Stack<Integer> stack = new Stack<Integer>();
	int n;
	ArrayList<ArrayList<TempTx>> locals = new ArrayList<ArrayList<TempTx>>();
	ArrayList<TempTx> globals = new ArrayList<TempTx>();
	Map<Integer,Integer> allTx = new HashMap<Integer,Integer>();
	Random rand = new Random();
	double localPerc;
	int id = 0;
	long tupleID=1;
	int p;
	int segSize =0;
	int max;
//	int[][] adjMatrix;
//	ArrayList<ArrayList<Integer>> adjMatrix;
	int localTx;
	public GenerateTDG(int n,int partitions,double localPerc,int max){
		this.n = n;
		for(int i=0;i<n;i++)
			allTx.put(i, 0);
		this.p = partitions;
		this.localPerc = localPerc;
		this.localTx = (int) (n*localPerc);
		int globalTx = n-localTx;
		int partitionSize = localTx/partitions;
		this.max = max;
		this.segSize = max/2;
		for(int i=0;i<localTx;i++)
			locals.add(new ArrayList<TempTx>());
		System.out.println("Number of transactions : " + n);
		System.out.println("Number of partitions : " + p);
		System.out.println("Number of local transactions : " + localTx);
		System.out.println("Number of global transactions : " + globalTx);
		System.out.println("Size of local partitions : " + partitionSize);
		System.out.println("Maximum number of sharing tx: " + max);
		System.out.println("Segment size: " + segSize);

		int partCounter =-1;
		for(int i=0;i<localTx;i++){
			if(i%partitionSize==0)
				partCounter++;
			locals.get(partCounter).add(new TempTx(partCounter*partitionSize + (i%partitionSize)));
		}
		for(int i=0;i<globalTx;i++)
			globals.add(new TempTx(i+localTx));

//		adjMatrix = new int[n][n];
//		for(int i=0;i<n;i++)
//			adjMatrix.add(new ArrayList<Integer>());
	}


	public double generateTransactions(double depFactor){
		TempTx currTx;
		Map<Long,Integer> sharability = new HashMap<Long,Integer>();
		Random rand= new Random();
		int partitionSize = locals.get(0).size();
		int localTx = (int) (n*localPerc);

		int index =0;
		for(int i=0;i<locals.size();i++){
			ArrayList<TempTx> currLocal = locals.get(i);
			index = i*partitionSize;
			for(int j=0;j<currLocal.size();j++){
				currTx = currLocal.get(j);
				Set<Integer> visited = new HashSet<Integer>();
				currTx.addWriteTuple(tupleID);
				sharability.put(tupleID, 1);
				int next = 0;
				int k =0;
				allTx.put(index+j, allTx.get(index+j)+1);
				while(k<segSize){
					next = rand.nextInt(currLocal.size());
					while(visited.contains(next) || next == j || allTx.get(index + next)>=max){
						next = rand.nextInt(currLocal.size());
					}
					allTx.put(index + next, allTx.get(index + next)+1);
//					adjMatrix[index + j][index + next] = 1;
//					adjMatrix.get(index+j).add(index+next);
					visited.add(next);
					if(Math.random()<depFactor){
						sharability.put(tupleID, sharability.get(tupleID)+1);
						currLocal.get(next).addReadTuple(tupleID);
					}
					k++;
				}
				tupleID++;
			}
		}

		for(int i=0;i<globals.size();i++){
			//			System.out.println("Global " + i);
			currTx = globals.get(i);
			Set<Integer> visited = new HashSet<Integer>();
			currTx.addWriteTuple(tupleID);
			sharability.put(tupleID, 1);
			int next = 0;
			int k =0;
			allTx.put(localTx+i, allTx.get(localTx+i)+1);
			while(k<segSize){
				next = rand.nextInt(n);
				index = next;
				while(visited.contains(next) || allTx.get(index)>=max){
					next = rand.nextInt(n);
					index = next;
				}
				allTx.put(index, allTx.get(index)+1);
//				adjMatrix[localTx+i][index]=1;
//				adjMatrix.get(localTx+i).add(index);
				visited.add(next);
				if(Math.random()<depFactor){
					if(next>=localTx){
						int lindex = next%localTx;
						globals.get(lindex ).addReadTuple(tupleID);
						//						System.out.println("global next " + next);
						//						System.out.println("global index  is " + index);
					}else {
						int partition = next/partitionSize;
						int lindex  = next%partitionSize;
						locals.get(partition ).get(lindex ).addReadTuple(tupleID);
						//						System.out.println("next " + next);
						//						System.out.println("local parition is " + partition);
						//						System.out.println("local index  is " + index);
					}


					sharability.put(tupleID, sharability.get(tupleID)+1);
				}
				k++;
			}
			tupleID++;

		}


		double total = 0,shared =0;
		for(Integer s:sharability.values()){
			if(s>0){
				total += s;
				shared++;
			}
		}
		System.out.println("Average overlapping TX intensity per tuple: " + total/shared);	
		System.out.println("Number of shared Tuples: " + shared);
		return total/shared;


	}

	public void printToFile(String file) throws IOException{
		ArrayList<TxEntry> txList = new ArrayList<TxEntry>();
		for(ArrayList<TempTx> l:locals)
			for(TempTx tx:l){
				Long[] src,dest;
				src = new Long[tx.readTuples.size()];
				dest = new Long[tx.writeTuples.size()];
				src = (Long[]) tx.readTuples.toArray(src);
				dest = (Long[]) tx.writeTuples.toArray(dest);
				//					LOG.info(Arrays.toString(src));
				//					LOG.info(Arrays.toString(dest));
				txList.add(new TxEntry(tx.type,tx.id,src,dest,Math.random()*0.001,new Timestamp(System.currentTimeMillis())));

			}

		for(TempTx tx:globals){
			Long[] src,dest;
			src = new Long[tx.readTuples.size()];
			dest = new Long[tx.writeTuples.size()];
			src = (Long[]) tx.readTuples.toArray(src);
			dest = (Long[]) tx.writeTuples.toArray(dest);
			//					LOG.info(Arrays.toString(src));
			//					LOG.info(Arrays.toString(dest));
			txList.add(new TxEntry(tx.type,tx.id,src,dest,Math.random()*0.001,new Timestamp(System.currentTimeMillis())));

		}

		//		Collections.shuffle(txList);
		System.out.println("File " + file + " created");
		BufferedWriter w= new BufferedWriter(new FileWriter(file));
		for(TxEntry t:txList){
			if(t.type==101){
				w.write(t.toString()+";\n");
				//				w.write("101," + t.txID + "," + new Timestamp(System.currentTimeMillis())+ ";\n");
			}else{
				if(t.dest.length!=0 || t.src.length!=0)
					w.write(t.toString()+";\n");	
			}
		}
		w.close();
	}

	public void printRepeatedToFile(String file) throws IOException{
		ArrayList<TxEntry> txList = new ArrayList<TxEntry>();
		Random rand = new Random();
		int rptd;
		for(ArrayList<TempTx> l:locals)
			for(TempTx tx:l){
				Long[] src,dest;
				src = new Long[tx.readTuples.size()];
				dest = new Long[tx.writeTuples.size()];
				src = (Long[]) tx.readTuples.toArray(src);
				dest = (Long[]) tx.writeTuples.toArray(dest);
				rptd  = rand.nextInt(10);
				for(int i=0;i<rptd;i++)
					txList.add(new TxEntry(tx.type,tx.id,src,dest,Math.random()*0.001,new Timestamp(System.currentTimeMillis())));
			}

		for(TempTx tx:globals){
			Long[] src,dest;
			src = new Long[tx.readTuples.size()];
			dest = new Long[tx.writeTuples.size()];
			src = (Long[]) tx.readTuples.toArray(src);
			dest = (Long[]) tx.writeTuples.toArray(dest);
			rptd  = rand.nextInt(10);
			for(int i=0;i<rptd;i++)
				txList.add(new TxEntry(tx.type,tx.id,src,dest,Math.random()*0.001,new Timestamp(System.currentTimeMillis())));

		}

		Collections.shuffle(txList);
		System.out.println("File " + file + " created");
		BufferedWriter w= new BufferedWriter(new FileWriter(file));
		for(TxEntry t:txList){
			if(t.dest.length!=0 || t.src.length!=0)
				w.write(t.toString()+";\n");	

		}
		w.close();
	}





	public static void main(String[] args) throws IOException{
		File dir=null ;
		String path = "";
		int numTransaction=0;
		int numPartitions = 0 ;
		double localPerc=0 ;
		double depFactor =0;
		int max = 0;
		boolean noP = true,noL = true, noM = true;
		int txSizeMargin = 1;	// determines the upper bound of tx Size. The larger the marging, the higher the variance of tx sizes
		for(int i=0;i<args.length;i++){
			String arg = args[i];
			i++;
			char option = arg.charAt(1);
			switch(option){
			case 'd':{
				//				dir = new File(args[i]);
				path = args[i];
				break;
			}
			case 'p':{
				numPartitions = Integer.parseInt(args[i]);
				noP = false;
				break;
			}
			case 'l':{
				localPerc = Double.parseDouble(args[i]);
				noL = false;
				break;
			}
			case 'c':{
				depFactor = Double.parseDouble(args[i]);
				break;
			}
			case 't':{
				numTransaction = Integer.parseInt(args[i]);
				break;
			}
			case 'm':{
				max = Integer.parseInt(args[i]);
				noM = false;
				break;
			}
			case 'g':{
				txSizeMargin = Integer.parseInt(args[i]);
				break;
			}
			default: {
				//				System.err.println("Error in arguments");
				//				System.exit(0);
			}
			}
		}

		if(noP){
			numPartitions = (int) (numTransaction*0.1);
		}
		if(noM){
			max = (int)((numTransaction/numPartitions) * 0.1);
		}
		if(noL){
			localPerc = 0.9;
		}


		path  += "/tx" + numTransaction;
		if(depFactor!=0){
			path += "/" + depFactor;
		}
		dir = new File(path);

		if (!dir.exists()) {
			System.out.println("creating directory: " + path);
			boolean result = false;

			try{
				dir.mkdirs();
				result = true;
			} 
			catch(SecurityException se){
				//handle it
			}        
			if(result) {    
				System.out.println("DIR created");  
			}
		}

		GenerateTDG tdg = new GenerateTDG(numTransaction,numPartitions, localPerc,max);			// Number of transactions in each segments (connected components)
		

		double IBInt = tdg.generateTransactions(depFactor);
		double totalSize = 0;



		for(ArrayList<TempTx> l:tdg.locals){
			for(TempTx t:l){
				t.setSize(txSizeMargin);
				totalSize += t.size;
				while(!t.full()){
					t.addTuple(tdg.tupleID++);
				}
				//			System.out.println(t.toString());
			}
		}

		for(TempTx t:tdg.globals){
			t.setSize(txSizeMargin);
			totalSize += t.size;
			while(!t.full()){
				t.addTuple(tdg.tupleID++);
			}
			//			System.out.println(t.toString());
		}

		System.out.println("Total number of tuples:"+(tdg.tupleID-1));
		System.out.println("Average tx size: " + totalSize/(double) numTransaction);
				tdg.printToFile(dir+"/"+ localPerc + ".sql");
//		tdg.printRepeatedToFile(dir+"/workload_"+ localPerc + ".sql");
		try{
			System.out.println("File" + dir+"/README_"+  localPerc  + " created");
			BufferedWriter bw = new BufferedWriter(new FileWriter(dir+"/README_workload_"+  localPerc ));
			bw.write("-c " + depFactor + " -l " + localPerc + " -m " + max + " -t " + numTransaction + " -p " + numPartitions + " -d " + dir 
					+ " -g " + txSizeMargin );
			bw.write("NumTransactions: " +numTransaction+"\n");
			bw.write("DepFactor: " + depFactor+"\n");
			bw.write("segSize:" + tdg.segSize+"\n");
			bw.write("txSizeMargin:" + txSizeMargin+"\n");
			bw.write("AvgTxSize:" + (totalSize/(double) numTransaction)+"\n");
			bw.write("AvgIBIntensity:" + IBInt+"\n");
			bw.write("Total number of tuples: " + tdg.tupleID);
			bw.close();
		}catch(Exception e){

		}

	}



}
