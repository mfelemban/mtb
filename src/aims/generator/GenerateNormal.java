/**
 * Created by: Muhamad Felemban 
 * Oct 4, 2017
 */
package aims.generator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 * @author mfelemban
 *
 */
public class GenerateNormal {
	public static void main(String[] args) throws IOException{
		int ds[] = {2};
		int sizes[] = {500};
		double meane = 50;
		double variance = 10;
		Random rand = new Random();
		String line = "";
		for(int s:sizes)
			for(int d:ds){
				String output = "workload/rmp/normal_" +d + ".txt";
				BufferedWriter bw = new BufferedWriter(new FileWriter(output));
				for(int j=0;j<s;j++){
					for(int i=0;i<d;i++){
						line += (int)Math.round(rand.nextGaussian()*10+50) + ",";
					}
					line=line.substring(0,line.length()-1);
					bw.write(line+"\n");
					line="";
				}
				bw.close();

			}
	}
}
