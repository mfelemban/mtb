package aims.tests;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HistoFixer {
	public static void main(String[] args){
		String file = args[0];
		try{
			BufferedReader br = new BufferedReader(new FileReader(file + ".ghist"));
			BufferedWriter bw = new BufferedWriter(new FileWriter(file + ".ghist2"));
			String line;
			int size = 0;
			int i=1;
			while((line = br.readLine())!=null){
				System.out.println(line);
				if(line.contains("#")){
					bw.write(line+"\n");
					size = Integer.parseInt(args[i++]);
				}else {
					Map<Integer,Integer> hist = new HashMap<Integer,Integer>();
					String t[] = line.split(",");
					int bin;
					for(String tkn: t){
						Double value = Double.parseDouble(tkn);
						bin = (int) (value/size);
						if(!hist.containsKey(bin)){
							hist.put(bin, 1);
						}else{
							hist.put(bin, hist.get(bin)+1);
						}
					}
					bw.write(print(hist) + "\n");
				}

			}
			bw.close();br.close();


			br = new BufferedReader(new FileReader(file + ".ibhist"));
			bw = new BufferedWriter(new FileWriter(file + ".ibhist2"));
			size = 0;
			i=0;
			while((line = br.readLine())!=null){
				System.out.println(line);
				if(line.contains("#")){
					System.out.println(line);
					bw.write(line+"\n");
					size = Integer.parseInt(args[i++]);
				}else if(line.contains("$")){
					System.out.println("IB " + line.split("$")[0] );
					bw.write("IB " + line.split(":")[0]+ ":");
					Map<Integer,Integer> hist = new HashMap<Integer,Integer>();
					if(line.split(":").length>1){
						String t[] = line.split(":")[1].split(",");
						int bin;
						for(String tkn: t){
							System.out.println("Token " + tkn + " size " + size);
							Double value = Double.parseDouble(tkn);
							if(value>3600000)
								value = value.doubleValue()-3600000;
							bin = (int) (value/size);
							System.out.println("Bin " + bin);
							if(!hist.containsKey(bin)){
								hist.put(bin, 1);
							}else{
								hist.put(bin, hist.get(bin)+1);
							}
						}
					}
					bw.write(print(hist) + "\n");
				}

			}
			bw.close();br.close();


		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static String print(Map<Integer,Integer> hist){
		String output = "";
		for(Integer v:hist.values()){
			output += v + "\t";
		}
		return output;
	}


	public static int[] fix(String input,int newInterval,int oldInterval){
		ArrayList<Integer> temp = new ArrayList<Integer>();
		int count = 0;
		int i=0;
		int size = 0;
		if(newInterval>oldInterval)
			size = newInterval/oldInterval;
		else{
			System.out.println("Wrong ");
			System.exit(0);
		}
		System.out.println(size);
		for(String s:input.split(",")){
			count += Integer.parseInt(s.trim());
			if(i>=size){
				temp.add(count);
				i =0;
				count = 0;
			}else{
				i++;
			}
		}
		int[] hist = new int[temp.size()];
		i=0;
		for(Integer t:temp)
			hist[i++] = t;
		return hist;
	}
}
