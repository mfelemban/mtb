//package aims.tests;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.List;
//import java.util.Random;
//
//public class LockTest {
//	/*
//	 * Class to test locking mechanism used to synchornize recovery transactions 
//	 * For a particular set of IBS, a recovery thread shuold wait until all threads performing in any of of IBS terminate (notify) 
//	 */
//
//	public static void main(String[] args){
//
//		int k= 9;
//		Boolean[] status = new Boolean[k];
//		for(int i=0;i<status.length;i++)
//			status[i] = false;
//
//		for(int i=1;i<11;i++){
//			int[] ibs = GenerateRecovery.generate();
//			Recovery rec = new Recovery(status,ibs);
//			Thread th = new Thread(rec);
//			th.start();
//
//
//			try {
//				Thread.sleep(100);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}
//
//
//
//}
//
//class GenerateRecovery{
//	static int[] generate(){
//		Random rand = new Random();
//		int size = rand.nextInt(8)+1;
//		List<Integer> temp = new ArrayList<Integer>();
//		for(int i=0;i<size;i++){
//			temp.add(new Integer(i+1));
//		}
//		Collections.shuffle(temp);
//		int[] ibs = new int[size];
//		for(int i=0;i<size;i++){
//			ibs[i] = temp.get(i);
//		}
//		return ibs;
//	}
//}
//
//class Recovery implements Runnable{
//	Boolean[] status;
//	int[] ibs;
//	Recovery(Boolean[] status, int[] ibs){
//		this.ibs= ibs;
//		this.status = status;
//	}
//	@Override
//	public void run() {
//		System.out.println("Thread id " + Thread.currentThread().getId() + " works on " + Arrays.toString(ibs));
//		// TODO Auto-generated method stub
//		synchronized(status){
//			try{
//				while(goToSleep()){
//					System.out.println("Thread " + Thread.currentThread().getId() + " waiting on " + Arrays.toString(ibs));
//					status.wait();
//				}
//				for(int i=0;i<ibs.length;i++)
//					status[ibs[i]] = true;
//			}catch(Exception e){}
//		}
//		System.out.println("Thread id " + Thread.currentThread().getId() + " Got all of " + Arrays.toString(ibs));
//		try {
//			Thread.sleep(1000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		synchronized(status){
//			for(int i=0;i<ibs.length;i++)
//				status[ibs[i]] = false;
//			System.out.println("Thread id " + Thread.currentThread().getId() + " Released all of " + Arrays.toString(ibs));
//			status.notify();
//		}
//	}
//
//	boolean goToSleep(){
//		boolean go = false;
//		for(int i=0;i<ibs.length;i++){
//			if(status[ibs[i]])
//				return true;
//		}
//		return go;
//	}
//
//}
//
//
