/**
 * Created by: Muhamad Felemban 
 * Sep 12, 2017
 */
package aims.tests;

import java.util.Random;

import java.util.Arrays;

/**
 * @author mfelemban
 *
 */
public class EMD {
	public static void main(String[] args){
		int[] W = new int[5];
		double EMD;
		Random rand = new Random();
		for(int i=0;i<W.length;i++){
			W[i] = rand.nextInt(10)+1;
		}
		for(int j=0;j<1000;j++){
			double[] Pw = getProp(W);
			double EMDW = getEMD(Pw,Pw);


			int[] Q1 = new int[5];
			for(int i=0;i<W.length;i++){
				if(W[i]==0)
					Q1[i]=0;
				else
					Q1[i] = rand.nextInt(W[i])+1;
			}
			double[] Pq1 = getProp(Q1);
			double EMD1 = getEMD(Pq1,Pw);


			int[] Q2 = new int[5];
			for(int i=0;i<W.length;i++){
				if(W[i] - Q1[i]==0)
					Q2[i] =0;
				else
					Q2[i] = rand.nextInt(W[i] - Q1[i]);
			}
			double[] Pq2 = getProp(Q2);
			double EMD2 = getEMD(Pq2,Pw);


			int[] Q12 = new int[5];
			for(int i=0;i<Q12.length;i++)
				Q12[i] = Q1[i] + Q2[i];
			double[] Pq12 = getProp(Q12);
			double EMD12 = getEMD(Pq12,Pw);

			if(EMD12 > Math.max(EMD1, EMD2)){
				System.out.println("W->" + Arrays.toString(W) + ":"+ Arrays.toString(Pw) + ":" + EMDW);
				System.out.println("Q1->" +Arrays.toString(Q1) + ":"+ Arrays.toString(Pq1) + ":" + EMD1);
				System.out.println("Q2->" +Arrays.toString(Q2) + ":"+ Arrays.toString(Pq2) + ":" + EMD2);
				System.out.println("Q12->" +Arrays.toString(Q12) + ":"+ Arrays.toString(Pq12) + ":" + EMD12);
				System.out.println(EMD1 + ":" + EMD2 + "::" + EMD12 + "\n");
			}

		}

	}

	public static double[] getProp(int[] W){
		double sum = 0;
		double[] P = new double[W.length];
		for(int w:W)
			sum+=w;
		for(int i=0;i<P.length;i++)
			P[i] = W[i]/sum;
		return P;
	}


	public static double getEMD(double[] W, double[] P){
		double EMD = 0;
		for(int i=1;i<=W.length-1;i++){
			double pp =0;
			for(int j=1;j<=i;j++){
				//				double p = Math.abs(W[j-1]-P[j-1]);
				double p = W[j-1]-P[j-1];
				pp += p;
			}
			EMD += Math.abs(pp);
		}
		EMD = EMD /(W.length-1);
		return EMD;
	}
	
	public static double getVD(double[] W, double[] P){
		double VD = 0;
		for(int i=1;i<=W.length-1;i++){
			double pp =0;
			for(int j=1;j<=i;j++){
				//				double p = Math.abs(W[j-1]-P[j-1]);
				pp= W[j-1]-P[j-1];
			}
			VD += pp;
		}
		return VD;
	}
}
