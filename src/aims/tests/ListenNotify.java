package aims.tests;

import java.sql.*;

public class ListenNotify {

	public static void main(String args[]) throws Exception {
		Class.forName("org.postgresql.Driver");
		String url = "jdbc:postgresql://128.46.122.119:5432/postgres";

		// Create two distinct connections, one for the notifier
		// and another for the listener to show the communication
		// works across connections although this example would
		// work fine with just one connection.
		Connection lConn = DriverManager.getConnection(url,"postgres","dmsl236#mm");
		Connection nConn = DriverManager.getConnection(url,"postgres","dmsl236#mm");

		// Create two threads, one to issue notifications and
		// the other to receive them.
		Listener listener = new Listener(lConn);
		Notifier notifier = new Notifier(nConn);
		listener.start();
		notifier.start();
		
	}

}

class Listener extends Thread {

	private Connection conn;
	private org.postgresql.PGConnection pgconn;

	Listener(Connection conn) throws SQLException {
		this.conn = conn;
		this.pgconn = (org.postgresql.PGConnection)conn;
		Statement stmt = conn.createStatement();
		stmt.execute("LISTEN dude");
		stmt.close();
		System.out.println("Starting to listen");
	}

	public void run() {
		while (true) {
			try {
				// issue a dummy query to contact the backend
				// and receive any pending notifications.
				

				org.postgresql.PGNotification notifications[] = pgconn.getNotifications();
				System.out.println("Getting it ");
				if (notifications != null) {
					for (int i=0; i<notifications.length; i++) {
						System.out.println("Got notification: " + notifications[i].getName());
					}
					Statement stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery("SELECT count(*) from log_table");
					rs.next();
					System.out.println(rs.getInt(1));
					rs.close();
					stmt.close();
				}

				// wait a while before checking again for new
				// notifications
				Thread.sleep(5000);
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			}
		}
	}

}

class Notifier extends Thread {

	private Connection conn;

	public Notifier(Connection conn) {
		this.conn = conn;
	}

	public void run() {
		while (true) {
			try {
				Statement stmt = conn.createStatement();
				stmt.execute("NOTIFY dude");
				stmt.close();
				Thread.sleep(200);
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			}
		}
	}

}