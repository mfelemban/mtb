/**
 * Created by: Muhamad Felemban 
 * Apr 17, 2017
 */
package aims.tests;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author mfelemban
 *
 */
public class FixAvailability {
	public static void main(String[] args) throws IOException{
		String file = args[0];
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line ="";
		double commResponseTime = 0;
		double commAvail = 0;
		int i=1;
		while((line = br.readLine())!=null){
			String[] tkns = line.split(",");
			int retries = Integer.parseInt(tkns[11]);
			int responseTime= Integer.parseInt(tkns[3]);
			int blockingTime= Integer.parseInt(tkns[4]);
			commResponseTime += (double)responseTime/(double) retries;
			commAvail += (double) blockingTime/((double)responseTime/(double) retries);
			i++;
		}
		System.out.println("Committed responsetime =" +commResponseTime/(int) i );
		System.out.println("Committed availability ="+ (100-commAvail/(int) i)*100 );
	}
}	
