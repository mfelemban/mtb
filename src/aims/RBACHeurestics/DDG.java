/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
package aims.RBACHeurestics;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;

public class DDG {


	//	private Map<DataObject,ArrayList<Integer>> E = new HashMap<DataObject,ArrayList<Integer>>();
	private Map<Long,Set<Long>> E = new HashMap<Long,Set<Long>>();
	private ArrayList<Transaction> T = new ArrayList<Transaction>();
	private Map<Long,AIMSObject> globalObjects = new TreeMap<Long,AIMSObject>();
	private Map<Integer,AIMSTransaction> globalTx = new TreeMap<Integer,AIMSTransaction>();
	private Map<Long,Set<Integer>> sharedObjects = new HashMap<Long,Set<Integer>>();
	private String logFile = "";
	private String eFile = "";
	private String tFile = "";

	public DDG(){
	}

	public Map<Long,Set<Integer>> getSharedObjects(){
		return sharedObjects;
	}

	public void load(String logFile) throws IOException{
//		System.out.println(logFile);
		tFile = logFile.substring(0, logFile.lastIndexOf(".")) + "_T" + logFile.substring(logFile.lastIndexOf("."), logFile.length());
//		System.out.println(tFile);
		eFile = logFile.substring(0, logFile.lastIndexOf(".")) + "_E" + logFile.substring(logFile.lastIndexOf("."), logFile.length());
//		System.out.println(eFile);
		load();
	}

	public void generate(String fileName) throws IOException{
		logFile = fileName;
		this.tFile = logFile.substring(0, logFile.lastIndexOf(".")) + "_T" + logFile.substring(logFile.lastIndexOf("."), logFile.length());
		this.eFile = logFile.substring(0, logFile.lastIndexOf(".")) + "_E" + logFile.substring(logFile.lastIndexOf("."), logFile.length());
		try {
			//			readLogTable(logFile,tFile,eFile);
			readScript(logFile,tFile,eFile);
//			System.out.println("Printing T ");
			printToT(globalTx,globalObjects,tFile);
//			System.out.println("Printing E ");
			printToE(globalObjects,globalTx,eFile);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		load();
	}

	protected void readLogTable(String fileName,String tFile, String eFile) throws Exception{
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		String line = br.readLine();
		Set<Integer> distinctTx = new HashSet<Integer>();
		AIMSObject o ;
		AIMSTransaction t ;
		while((line=br.readLine())!=null){
			String[] a = line.split(",");
			// Parse log table 
			int txNum = Integer.parseInt(a[1]);
			distinctTx.add(txNum);
			long objNum = Long.parseLong(a[3]);
			int operation = Integer.parseInt(a[4]);

			if(globalObjects.containsKey(new Long(objNum))){
				// Weight is the number of transactions read/update object
				globalObjects.get(objNum).increaseWeight();				
				globalObjects.get(objNum).addTx(txNum);
			}else{
				o = new AIMSObject(objNum,1);
				o.addTx(txNum);
				globalObjects.put(objNum,o);
			}

			if(globalTx.containsKey(txNum)){
				t = globalTx.get(txNum);
				t.addObject(globalObjects.get(objNum).getObjID());
				if(operation==1)
					t.addRead(objNum);
				else
					t.addWrite(objNum);
				globalTx.put(txNum, t);
			}else{
				t = new AIMSTransaction(txNum);
				t.addObject(globalObjects.get(objNum).getObjID());
				if(operation==1)
					t.addRead(objNum);
				else
					t.addWrite(objNum);
				globalTx.put(txNum, t);
			}

		}
		br.close();		
	}

	protected void readScript(String fileName,String tFile, String eFile) throws Exception{
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		String line;
		Set<Integer> distinctTx = new HashSet<Integer>();
		AIMSObject o ;
		AIMSTransaction t ;
		Set<Long> readObjects ,writeObjects;
		while((line=br.readLine())!=null){
			String[] a = line.split(",");
			if(Integer.parseInt(a[0])==101)
				continue;
			//			System.out.println(line);
			// Parse log table 
			int txNum = Integer.parseInt(a[1]);
			distinctTx.add(txNum);
			
			readObjects= new HashSet<Long>();
			writeObjects= new HashSet<Long>();

			String rO = a[2];
	
			for(String tt:rO.split(":")){
				readObjects.add(Long.parseLong(tt));
			}


			String wO = a[3];
//			System.out.println(line);
			for(String tt:wO.split(":")){
				writeObjects.add(Long.parseLong(tt));
			}

			for(Long objNum:readObjects){
				if(globalObjects.containsKey(new Long(objNum))){
					// Weight is the number of transactions read/update object
					globalObjects.get(objNum).increaseWeight();				
					globalObjects.get(objNum).addTx(txNum);
					//					globalObject.get(objNum).getObject().addNeighbors(writeObjects);
				}else{
					o = new AIMSObject(objNum,1);
					o.addTx(txNum);
					//					o.getObject().addNeighbors(writeObjects);
					globalObjects.put(objNum,o);
				}
				globalObjects.get(objNum).addWrite(writeObjects);



				if(globalTx.containsKey(txNum)){
					t = globalTx.get(txNum);
					t.addObject(globalObjects.get(objNum).getObjID());
					t.addRead(objNum);
					globalTx.put(txNum, t);
				}else{
					t = new AIMSTransaction(txNum);
					t.addObject(globalObjects.get(objNum).getObjID());
					t.addRead(objNum);
					globalTx.put(txNum, t);
				}
			}

			for(Long objNum:writeObjects){
				if(globalObjects.containsKey(new Long(objNum))){
					// Weight is the number of transactions read/update object
					globalObjects.get(objNum).increaseWeight();				
					globalObjects.get(objNum).addTx(txNum);
				}else{
					o = new AIMSObject(objNum,1);
					o.addTx(txNum);
					globalObjects.put(objNum,o);
				}

				if(globalTx.containsKey(txNum)){
					t = globalTx.get(txNum);
					t.addObject(globalObjects.get(objNum).getObjID());
					t.addWrite(objNum);
					globalTx.put(txNum, t);
				}else{
					t = new AIMSTransaction(txNum);
					t.addObject(globalObjects.get(objNum).getObjID());
					t.addWrite(objNum);
					globalTx.put(txNum, t);
				}
			}
		}

		br.close();		
	}

	protected void printToT(Map<Integer,AIMSTransaction> globalTX, Map<Long,AIMSObject> globalObject,String fileName) throws FileNotFoundException{
		int objSize = globalObject.size();
		//System.out.println(globalObject.size());
		int txSize = globalTX.size();
		PrintWriter pw = new PrintWriter(fileName);


		pw.write("N="+objSize+"\nT="+txSize+"\n");
		ArrayList<Integer> txList = new ArrayList<Integer>(globalTX.keySet());
		for(AIMSObject entry:globalObject.values()){
			pw.write(entry.getObjID() + ":" + entry.getWeight() + " " );
			Set<Integer> objTX = entry.getTxSet();

			for(Integer t:objTX){
				if(globalTX.get(t).readSet.size()!=0){
					pw.write(txList.indexOf(t) + " ");
				}
			}
			pw.write("\n");

		}
		pw.close();

	}

	protected void printToE(Map<Long,AIMSObject> globalObject,Map<Integer,AIMSTransaction> globalTX,String fileName) throws IOException{
		FileOutputStream pw = new FileOutputStream(new File(fileName));

		String output = "N="+globalObject.size()+"\n";
		pw.write(output.getBytes());
		for (AIMSObject entry : globalObject.values()) {
			if(entry.isShared()){
				output = entry.getObjID()+ " ";
				pw.write(output.getBytes());
				for(Long t:entry.getWriteSet()){
					if(globalObject.get(t).isShared()){
						output = t + " ";
						pw.write(output.getBytes());
					}
				}
				pw.write("\n".getBytes());
			}
		}
		pw.close();
	}
// here
	public void load() throws IOException{

		BufferedReader br = new BufferedReader(new FileReader(tFile));
		String line;
		line = br.readLine();
		@SuppressWarnings("unused")
		int objSize = Integer.parseInt(line.substring(line.indexOf("=")+1));
		line = br.readLine();
		int txSize = Integer.parseInt(line.substring(line.indexOf("=")+1));
		for(int i=0;i<txSize;i++){
			Transaction t = new Transaction(i);
			T.add(t);
		}
		while((line =br.readLine())!=null){
			String[] in = line.split(" ");
			Set<Integer> ts= new HashSet<Integer>();
			for(int i=1;i<in.length;i++){
				ts.add(Integer.parseInt(in[i]));
			}
			String[] objID = in[0].split(":");
			long oID = Long.parseLong(objID[0]);
			AIMSObject o = new AIMSObject(oID,Integer.parseInt(in[0].split(":")[1]));
			if(ts.size()>1){
				//				sharedObjects.add(oID);
				Set<Integer> txList;
				if(sharedObjects.containsKey(oID)){
					txList = sharedObjects.get(oID);
					txList.addAll(ts);
				}else{
					txList = new HashSet<Integer>();
					txList.addAll(ts);
				}
				sharedObjects.put(oID, txList);
				for(Integer tTS :ts){
					o.addTx(tTS);
					T.get(tTS).addSharedObject(oID);
					T.get(tTS).addObject(o);
				}
			}else{
				for(Integer tTS :ts){
					T.get(tTS).addInternalObject(oID);
					T.get(tTS).addObject(o);
					o.addTx(tTS);
				}
			}

			globalObjects.put(oID, o);
		}
		br.close();
		br = new BufferedReader(new FileReader(eFile));
		line = br.readLine();
		while((line=br.readLine())!=null){
			String[] a = line.split(" ");
			long objId = Long.parseLong(a[0]); 
			if(sharedObjects.containsKey(objId)){
				E.put(objId,new HashSet<Long>());
				for(int j=1;j<a.length;j++){
					E.get(objId).add(Long.parseLong(a[j]));
				}

			}
		}
		br.close();
	}

	public ArrayList<ArrayList<Integer>> chainedTx(){
		ArrayList<ArrayList<Integer>> indepTx = new ArrayList<ArrayList<Integer>>();
		Set<Integer> visited = new HashSet<Integer>();
		Stack<Integer> activeStack;
		for(Transaction t:T){
			if(visited.contains(t.getId()))
				continue;
			Set<Integer> chain = new HashSet<Integer>();
			activeStack = new Stack<Integer>();
			activeStack.add(t.getId());
			while(!activeStack.isEmpty()){
				int next = activeStack.pop();
				if(chain.contains(next))
					continue;
				chain.add(next);
				Transaction temp = T.get(next);
				for(Long i: temp.getSharedObjects()){
					for(Integer tt: globalObjects.get(i).getTxSet())
						activeStack.add(tt);
				}
			}
			visited.addAll(chain);
			indepTx.add(new ArrayList<Integer>(chain));
		}

		return indepTx;
	}



	public static void print(String message){
		System.out.println(message);
	}

	public int findPotentialViolation(){
		int sharedLinks = 0;
		ArrayList<Long> sharedList = new ArrayList<Long>(sharedObjects.keySet());
		for(int i=0;i<sharedObjects.size();i++){
			for(int j=i+1;j<sharedObjects.size();j++){
				Set<Long> iNeighbors = E.get(sharedList.get(i));
				Set<Long> jNeighbors = E.get(sharedList.get(j));
				if(!iNeighbors.isEmpty())
					if(iNeighbors.contains(sharedList.get(j))){
						sharedLinks++;
					}
				if(!jNeighbors.isEmpty())
					if(jNeighbors.contains(sharedList.get(i))){
						sharedLinks++;
					}
			}
		}
		return sharedLinks;
	}

	public Map<Long, Set<Long>> getE() {
		return E;
	}


	public ArrayList<Transaction> getT() {
		return T;
	}

	public String printT(){
		String output ="[";
		for(Transaction t:T){
			output += "("+t.getId() + "," + t.getInternalObjects().size() + ":" + t.size() +") ";
		}
		return output += "]";
	}

	public static void main(String[] args) throws Exception{
		if(args.length==0){
			System.out.println("*** Usage: \n Arg 1: file path ");
		}
		String logFile = args[0];
		String tFile = logFile.substring(0, logFile.lastIndexOf(".")) + "_T" + logFile.substring(logFile.lastIndexOf("."), logFile.length());
		File tempDir = new File(tFile);
		DDG ddg = new DDG();
		if(tempDir.exists())
			//			ddg.load(logFile);
//			ddg.load(logFile);
			ddg.generate(logFile);
		else
			ddg.generate(logFile);
		ArrayList<ArrayList<Integer>> indepTx = ddg.chainedTx();
		for(ArrayList<Integer> indList:indepTx){
			print(indList.toString());
		}
		
		
		
	}

	public Map<Long, AIMSObject> getGlobalObjects() {
		return globalObjects;
	}

	public void setGlobalObjects(Map<Long, AIMSObject> globalObjects) {
		this.globalObjects = globalObjects;
	}


}
