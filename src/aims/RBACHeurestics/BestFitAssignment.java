/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
package aims.RBACHeurestics;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


public class BestFitAssignment extends Assignment{
	ArrayList<Role> roles;
	Map<Integer,Set<Long>> hotspots;
	public BestFitAssignment(DDG ddg,int k,ArrayList<Role> roles,Map<Integer,Set<Long>> hotspots){
		super(ddg,k,roles,hotspots);
		this.roles = new ArrayList<Role>(roles);
		this.hotspots = hotspots;
	}

	/*
	 * Minimizes teh number of boundary objects 
	 */
	public IBSolution solve(int k) throws IOException{
		super.solution = new IBSolution(k,ddg,roles);
//		ArrayList<Transaction> transactions = new ArrayList<Transaction>(ddg.getT());

		//		Map<Integer,Integer> initialSolution = new HashMap<Integer,Integer>();
		Role currRole ;
		Iterator<Role> iter = roles.iterator();
		int assignedIBs = 0;

//		System.out.println("Non-sorted: " + roles.toString());
		Collections.sort(roles, new Comparator<Role>() {
			@Override
			public int compare(Role r1, Role r2)
			{
				// descedning
				return  Double.compare(r1.getHotspots().size() , r2.getHotspots().size());
			}
		});
//		System.out.println("Sorted: " + roles.toString());
		iter = roles.iterator();	
	
		while(iter.hasNext()){
			currRole = iter.next();
			Map<Integer,Integer> nebors = neighboringTx(currRole);				// O(nT)
			Map<Integer,Integer> ovrlbIBs = overlappingIBs(nebors);				// O(kT)
			if(ovrlbIBs.size()==0){
				solution.assignRole(currRole, solution.getSmallestIB());
				assignedIBs++;
				iter.remove();
			}
			if(assignedIBs==k)
				break;
		}
		solution.update();
		//		iter = transactions.iterator();
		iter = roles.iterator();
		// O(T)
		while(iter.hasNext()){						
			currRole = iter.next();
			Map<Integer,Integer> nebors = neighboringTx(currRole);				// O(nT)
			Map<Integer,Integer> ovrlbIBs = overlappingIBs(nebors);				// O(kT)
			double minOverlapingIb = Double.MAX_VALUE;
			int targetIB = -1;
			//O(k)
			for(Map.Entry<Integer, Integer> entry:ovrlbIBs.entrySet()){

				Integer key = entry.getKey();
				//				Integer value = entry.getValue();
				double value = approximateObjectiveBO(entry);
				//				System.out.println("Tx " + currTx.getId()  + " IB " + key + " new " + value + " old " + solution.evaluateObjFunction()) ;
				if(value<minOverlapingIb){
					minOverlapingIb = value;
					targetIB = key;
				}
				else if(value == minOverlapingIb){
					if(ddg.getT().get(key).internalRatio()<ddg.getT().get(targetIB).internalRatio()){
						targetIB = key;
					}

				}

			}
			if(targetIB==-1){
				targetIB = solution.getSmallestIB();
			}
			// O(n)
			solution.assignRole(currRole, targetIB);
			solution.update();
//			System.out.println(solution.toString());
			//			System.out.println(currTx.getId() + " assigned");
		}
		//				System.out.println("Time:" + (System.currentTimeMillis()-start));
		return solution;
	}

	public double approximateObjectiveBO(Map.Entry<Integer, Integer> overlappingIB){
		double f2 = solution.F2;
//				System.out.println("f2 " + f2);
		f2 += overlappingIB.getValue();
//				System.out.println("f2 " + f2);
		
		double normalizingFactor = ddg.getSharedObjects().size()*k;
//System.out.println(normalizingFactor);
		f2 = (f2/normalizingFactor)*100;
		double f1=0;
		double[] s = new double[k];
		for(int i=0;i<k;i++){
			if(i==overlappingIB.getKey())
				s[i]= solution.ibTxSet.get(i).size()+ 1;
			else
				s[i]= solution.ibTxSet.get(i).size();
		}
		//		System.out.println(Arrays.toString(s));
		double j = Metrics.JainFairness(s);
		//		System.out.println("fairness " +j);
		j = (1-j)*100;
		f1 = j;
//		System.out.println("F1 " + f1 + " F2 " + f2);
		double alpha = 0.75;
		
		double app = alpha*(f1/(f1+f2)) + (1-alpha)*(f2/(f1+f2));
		return app;
	}

	// O(nT)
	/*
	 * Output contains the neighboring transaction id and number of objects common with t
	 */
	public Map<Integer,Integer> neighboringTx(Role r){
		Map<Integer,Integer> neighbors = new HashMap<Integer,Integer>();
		for(Long o:r.getSharedObjects()){
			AIMSObject tempO = ddg.getGlobalObjects().get(o);
			//			neighbors.addAll(tempO.getTxSet());
			for(Integer overlapTx:tempO.getTxSet()){
				if(neighbors.containsKey(overlapTx)){
					neighbors.put(overlapTx,neighbors.get(overlapTx)+1);
				}else{
					neighbors.put(overlapTx,1);
				}
			}
		}
		neighbors.remove(new Integer(r.getId()));
		return neighbors;
	}
	// O(k*T)
	/*
	 * Output return all possbily overlapping IBs along with number of objects overlapping with that IB
	 */
	public Map<Integer,Integer> overlappingIBs(Map<Integer,Integer> overlappingTx){
		Map<Integer,Integer> ovrlabIBs = new HashMap<Integer,Integer>();
		for(int i=0;i<k;i++){
			for(Integer tx:overlappingTx.keySet()){
				if(solution.ibTxSet.get(i).contains(tx)){
					if(ovrlabIBs.containsKey(i)){
						ovrlabIBs.put(i, ovrlabIBs.get(i)+overlappingTx.get(tx));
					}else{
						ovrlabIBs.put(i, overlappingTx.get(tx));
					}
				}
			}
		}
		return ovrlabIBs;
	}



//	public static void main(String[] args) throws IOException{
//		String logFile = args[0];
//		String tFile = logFile.substring(0, logFile.lastIndexOf(".")) + "_T" + logFile.substring(logFile.lastIndexOf("."), logFile.length());
//		File tempDir = new File(tFile);
//		DDG ddg = new DDG();
//		if(tempDir.exists())
//			ddg.load(logFile);
//		else
//			ddg.generate(logFile);
//		String[] a = args[1].split(",");
//		int[] K = new int[a.length];
//		int i=0;
//		for(String s:a){
//			K[i] = Integer.parseInt(s);
//			i++;
//		}
//		for(int k:K){
//
//			BestFitAssignment efa = new BestFitAssignment(ddg,k);
//			if(efa.T.size()<k){
//				print("K must be less than number of transactions!");
//				System.exit(0);
//			}
//			efa.solution = efa.solve(k);
//			//			efa.solution = efa.solve2(k);
//			efa.printResults(logFile, k,"efa");
//			IBStatisticsGenerator.boIntRatio(efa.solution);
//		}
//
//	}
}
