/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.RBACHeurestics;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;



public class Assignment {
	
	private static final Logger LOG = Logger.getLogger(aims.RBACHeurestics.Assignment.class);
	Map<Long,Set<Long>> E;
	Map<Integer,Transaction> T;
	IBSolution solution;
	DDG ddg;
	int k;
	ArrayList<Role> roles;
	Map<Integer,Set<Long>> hotspots;
	public Assignment( DDG ddg,int k,ArrayList<Role> roles,Map<Integer,Set<Long>> hotspots){
		this.ddg = ddg;
		this.T = new HashMap<Integer,Transaction>();
		for(Transaction t:ddg.getT()){
			this.T.put(t.getId(),t);
		}
		this.E = ddg.getE();
		this.k = k;
		this.roles = roles;
		this.hotspots=  hotspots;
	}
	
	public static void print(String message,BufferedWriter bw){
//		LOG.info(message);
		try {
			bw.append(message+"\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void print(String message){
//		System.out.println(message);
	}
	
	public void printResults(String logFile, int k, String type,int r,double h) throws IOException{
		File file = new File(logFile.substring(0,logFile.lastIndexOf("."))+"_k"+k+"_"+type+"_" +h + "_"+ r + ".output");
		BufferedWriter bw = new BufferedWriter(new FileWriter(file));
		print("*********************" + type + "******************************************",bw);
		print("File " + logFile ,bw);
		print("K " + k ,bw);		
		double[] ratios = IBStatisticsGenerator.boIntRatio(solution);
		if(solution!=null){
			print("# of shared objects " + ddg.getSharedObjects().size(),bw);
			double obj = solution.evaluateObjFunction();
			Set<Long> bo = solution.getBoundaryObjects();
			print("BO=" + bo.size(),bw);
			print("HBO=" + findHBO(bo),bw);
			print("F1=" + solution.F1,bw);
			print("F2=" + solution.F2,bw);
			print("F=" + obj,bw);
			print("IFairness=" + Metrics.intSizeFairness(solution),bw);
			print("TFairness=" + Metrics.txSizeFairness(solution),bw);
			print("Variance="+Metrics.sizeVariance(solution),bw);
			print("BST=" + Metrics.boundaryObjectRatio(solution),bw);
			print("non-shared to totol ratios=" + average(ratios), bw);
			printSolution(logFile,solution,type,r,h);
		}else{
			print("No feasible solution",bw);
		}
		
		
		solution.getIBCharcterstics();
		print("***************************************************************",bw);
//		System.out.println(solution.toString());
		bw.close();
	}
	
	public int findHBO(Set<Long> bo){
		Set<Long> allHS = new HashSet<Long>();
		for(Set<Long> temp:hotspots.values()){
			allHS.addAll(temp);
		}
		Set<Long> hbo = new HashSet<Long>();
		for(Long temp:bo){
			if(allHS.contains(temp)){
				hbo.add(temp);
			}
		}
		return hbo.size();
	}
	
	public double average(double[] ratios){
		double total = 0;
		for(int i=0;i<ratios.length;i++){
			total += ratios[i];
		}
		return total/(double) ratios.length;
	}


	public void printSolution(String file,IBSolution solution,String type,int r,double h) throws IOException{
				String tFile,txFile;
				File theDir = new File(file.substring(0, file.lastIndexOf("/"))+"/sol");
				if (!theDir.exists()) {
//					System.out.println("creating directory: " + theDir.getName());
					try{
						theDir.mkdir();
					} 
					catch(SecurityException se){
						//handle it
					}        
				}
				theDir = new File(file.substring(0, file.lastIndexOf("/"))+"/txsol");
				if (!theDir.exists()) {
//					System.out.println("creating directory: " + theDir.getName());
					try{
						theDir.mkdir();
					} 
					catch(SecurityException se){
						//handle it
					}        
				}
				if(type.equals("rfa")){
					tFile = file.substring(0, file.lastIndexOf(".")) + "_" + solution.getK()  + "_rfa_"+ h + "_" +r +".sol";
					tFile = tFile.substring(0,tFile.lastIndexOf("/")) + "/sol/" + tFile.substring(tFile.lastIndexOf("/")+1,tFile.length());
					txFile = file.substring(0, file.lastIndexOf(".")) + "_" + solution.getK() + "_rfa_"+ h + "_" +r +".txsol";
					txFile = txFile.substring(0,txFile.lastIndexOf("/")) + "/txsol/" + txFile.substring(txFile.lastIndexOf("/")+1,txFile.length());
				}else if(type.equals("ffa")){
					tFile = file.substring(0, file.lastIndexOf(".")) + "_" + solution.getK()  + "_ffa_"+ h + "_" +r +".sol";
					tFile = tFile.substring(0,tFile.lastIndexOf("/")) + "/sol/" + tFile.substring(tFile.lastIndexOf("/")+1,tFile.length());
					txFile = file.substring(0, file.lastIndexOf(".")) + "_" + solution.getK() + "_ffa_"+ h + "_" +r +".txsol";
					txFile = txFile.substring(0,txFile.lastIndexOf("/")) + "/txsol/" + txFile.substring(txFile.lastIndexOf("/")+1,txFile.length());
				}else{
					tFile = file.substring(0, file.lastIndexOf(".")) + "_" + solution.getK() + "_efa_"+ h + "_" +r +".sol";
					tFile = tFile.substring(0,tFile.lastIndexOf("/")) + "/sol/" + tFile.substring(tFile.lastIndexOf("/")+1,tFile.length());
					txFile = file.substring(0, file.lastIndexOf(".")) + "_" + solution.getK() + "_efa_"+ h + "_" +r +".txsol";
					txFile = txFile.substring(0,txFile.lastIndexOf("/")) + "/txsol/" + txFile.substring(txFile.lastIndexOf("/")+1,txFile.length());
				}
				System.out.println("File " + tFile + " created");
				BufferedWriter bw = new BufferedWriter(new FileWriter(tFile));
				System.out.println("File " + txFile + " created");
				BufferedWriter bw2 = new BufferedWriter(new FileWriter(txFile));
//				for(int i=0;i<solution.getK();i++){
//					for(Long obj:solution.ibInternalObjects.get(i)){
//						bw.write(obj+","+(i+1)+"\n");
//					}
//				}
//				for(int i=0;i<solution.getK();i++){
//					for(Long obj:solution.ibBoundaryObjects.get(i)){
//						bw.write(obj+","+(i+1)+"\n");
//					}
//				}
				for(int i=0;i<solution.getK();i++){
					ArrayList<Integer> tempRoles = new ArrayList<Integer>(solution.ibTxSet.get(i));
					Collections.sort(tempRoles);
					
//					LOG.info("List of roles at IB " + i + " :" + tempRoles.toString());
//					LOG.info("Number of roles " + roles.size());
//					LOG.info("Number of roles " + roles.toString());
					for(Integer role:tempRoles){
						Set<Integer> txPerRole = roles.get(role).txIDs;
//						LOG.info("Tx of role " + role + ":" + txPerRole);
						for(Integer tx:txPerRole){
							bw2.write(tx+","+(i+1)+"\n");
							for(AIMSObject a:T.get(tx).getObjects())
								bw.write(a.getID() + ","+ tx +"," + (i+1)+"\n");
						}
						
						
					}
					
				}
				bw2.close();
				bw.close();
	}
}
