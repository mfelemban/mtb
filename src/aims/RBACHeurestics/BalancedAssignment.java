/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.RBACHeurestics;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/*
 * Random assignemnt
 */
public class BalancedAssignment extends Assignment{
	ArrayList<Role> roles;
	Map<Integer,Set<Long>> hotspots;
	public BalancedAssignment(DDG ddg,int k,ArrayList<Role> roles,Map<Integer,Set<Long>> hotspots){
		super(ddg,k,roles,hotspots);
		this.roles = roles;
		this.hotspots = hotspots;
	}


	public IBSolution solve(int k) throws IOException{
		long start = System.currentTimeMillis();
		
		super.solution = new IBSolution(k,ddg,roles);
		// T log T

//		ArrayList<Transaction> transactions = ddg.getT();


		Random rand = new Random();
		int targetIB;
		for(Role role:roles){
			if(solution.emptyIBs()>0)
				targetIB = solution.getEmptyIB();
			else
				targetIB = rand.nextInt(k);
			solution.assignRole(role,targetIB);
		}
		solution.update();
		System.out.println("Time:" + (System.currentTimeMillis()-start));
		return solution;
	}

	
}
