/**
 * Created by: Muhamad Felemban 
 * Oct 2, 2017
 */
package aims.RBACHeurestics;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * @author mfelemban
 *
 */
public class Role {
	ArrayList<Transaction> transactions = new ArrayList<Transaction>();
	Set<Integer> txIDs = new HashSet<Integer>();
	Set<Long> hotspots = new HashSet<Long>();
	int id;
	DDG ddg;
	protected int getId() {
		return id;
	}

	protected void setId(int id) {
		this.id = id;
	}

	public Role(int id,DDG ddg){
		this.id = id;
		this.ddg= ddg;
	}
	
	public void addTransaction(Transaction t,Set<Long> h){
		transactions.add(t);
		hotspots.addAll(h);
		txIDs.add(t.getId());
	}
	
	public void addTransaction(Transaction t){
		transactions.add(t);
		txIDs.add(t.getId());
	}
	
	public String toString(){
		return "Role id: " + id + " " + "Tx: " + transactions.size() + " HS: " + hotspots.size();
	}
	
	public Set<Long> getInternalObjects(){
		Set<Long> internalObjects = new HashSet<Long>();
		for(Transaction t:transactions)
			internalObjects.addAll(t.getInternalObjects());
		
		return internalObjects;
	}
	
	public Set<Long> getHotspots(){
		return hotspots;
	}
	
	public Set<Long> getSharedObjects(){
		Set<Long> sharedObjects = new HashSet<Long>();
		for(Transaction t:transactions){
			for(Long so: t.getSharedObjects()){
				Set<Integer> ovlpTx = ddg.getGlobalObjects().get(so).getTxSet();
//				System.out.println("Tuple " + so + " shared by " + ovlpTx.toString());
//				System.out.println("This role txs  " + txIDs.toString());
//				System.out.println("remaingin " + ovlpTx.toString());
				if(!txIDs.containsAll(ovlpTx)){
//					System.out.println(so + " is shared");
					sharedObjects.add(so);
				}
			}
//			sharedObjects.addAl/l(t.getSharedObjects());
		}
		
		return sharedObjects;
	}
	
}	
