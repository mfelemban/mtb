/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.RBACHeurestics;

import java.util.HashSet;
import java.util.Set;

public class AIMSObject {
	private long id;
	private int weight;
	private Set<Long> writeSet; 
	private Set<Integer> txSet;

	AIMSObject(long id, int weight){
		this.id= id;
		this.weight= weight;
		this.writeSet = new HashSet<Long>();
		this.txSet = new HashSet<Integer>();
	}

	public long getID(){
		return id;
	}
	void increaseWeight(){
		weight++;
	}

	long getObjID(){
		return id;
	}

	void addWrite(Long w){
		writeSet.add(w);
	}

	void addWrite(Set<Long> w){
		writeSet.addAll(w);
	}

	int getWeight(){
		return weight;
	}

	void addTx(int t){
		txSet.add(t);
	}

	public Set<Integer> getTxSet(){
		return txSet;
	}

	Set<Long> getWriteSet(){
		return  writeSet;
	}
	
	boolean isShared(){
		return txSet.size()>1;
	}
	public String toString(){
		return "ID: " + id + " weight: " + weight + " Tx: " + txSet.toString();
	}
}
