/**
 * Created by: Muhamad Felemban 
 * Aug 28, 2017
 */
package aims.RBACHeurestics;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;



/**
 * @author mfelemban
 *
 */
public class RBACSolve {
	public static void main(String[] args) throws IOException{
		String logFile = args[0];
		DDG ddg = new DDG();
		ddg.generate(logFile);
		ArrayList<Transaction> T = ddg.getT();

		String[] a = args[1].split(",");
		double[] H = new double[a.length];
		int i=0;
		for(String s:a){
			H[i] = Double.parseDouble(s);
			i++;
		}

		a = args[2].split(",");
		int[] R = new int[a.length];
		i=0;
		for(String s:a){
			R[i] = Integer.parseInt(s);
			i++;
		}

		a = args[3].split(",");
		int[] K = new int[a.length];
		i=0;
		for(String s:a){
			K[i] = Integer.parseInt(s);
			i++;
		}
		for(double h:H){
			String hotspotFile = logFile.substring(0,logFile.lastIndexOf(".")) + "_"+ h +".txhot";
			System.out.println(hotspotFile);
			Map<Integer,Set<Long>> hotspots = readHotspotFile(hotspotFile);
			System.out.println(hotspots.toString());

			for(int r:R){
				ArrayList<Role> roles = assignRoles(T,hotspots,ddg,r);
//				System.out.println(roles.toString());
				for(int k:K){

					BalancedAssignment rfa = new BalancedAssignment(ddg,k,roles, hotspots);

					rfa.solution = rfa.solve(k);
					//			efa.solution = efa.solve2(k);
					rfa.printResults(logFile, k,"rfa",r,h);
					IBStatisticsGenerator.boIntRatio(rfa.solution);

					FirstFitAssignment ffa = new FirstFitAssignment(ddg,k,roles,hotspots);

					ffa.solution = ffa.solve2(k);
					//			efa.solution = efa.solve2(k);
					ffa.printResults(logFile, k,"ffa",r,h);
					IBStatisticsGenerator.boIntRatio(ffa.solution);

					BestFitAssignment efa = new BestFitAssignment(ddg,k,roles,hotspots);
					
					
					efa.solution = efa.solve(k);
					//			efa.solution = efa.solve2(k);
					efa.printResults(logFile, k,"efa",r,h);
					IBStatisticsGenerator.boIntRatio(efa.solution);
				}
			}
		}

	}


	public static ArrayList<Role> assignRoles(ArrayList<Transaction> T,Map<Integer,Set<Long>> hotspots,DDG ddg,int r){
		ArrayList<Role> roles = new ArrayList<Role>();
		for(int i=0;i<r;i++){
			roles.add(new Role(i,ddg));
		}

		Random rand = new Random();
		for(Transaction t:T){
			Set<Long> temp = hotspots.get(t.getId());
			if(hotspots.containsKey(t.getId()))
				roles.get(rand.nextInt(r)).addTransaction(t,temp);
			else
				roles.get(rand.nextInt(r)).addTransaction(t);
		}
		return roles;
	}

	public static Map<Integer,Set<Long>> readHotspotFile(String file) throws IOException{
		Map<Integer,Set<Long>> hotTx = new HashMap<Integer,Set<Long>>();
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line ="";
		while((line=br.readLine())!=null){
			String[] tokens = line.split(";");
			int txid = Integer.parseInt(tokens[0].split(":")[1].trim());
			Set<Long> hotTuples = new HashSet<Long>();
			for(String t: tokens[tokens.length-1].split(":")[1].split(",")){
				if(!t.equals(" "))					
					hotTuples.add(Long.parseLong(t.trim()));
			}
			hotTx.put(txid, hotTuples);
		}
		br.close();
		return hotTx;
	}
}	
