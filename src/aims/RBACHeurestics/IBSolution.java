/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
package aims.RBACHeurestics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;



public class IBSolution {
	Map<Long,SwingObject> allSwings = new HashMap<Long,SwingObject>();
	public ArrayList<Map<Long,SwingObject>> ibSwingObjects = new ArrayList<Map<Long,SwingObject>>();
	public ArrayList<Set<Long>> ibBoundaryObjects = new ArrayList<Set<Long>>();
	public ArrayList<Set<Long>> ibInternalObjects = new ArrayList<Set<Long>>();
	public ArrayList<Set<Integer>> ibTxSet=  new ArrayList<Set<Integer>>();
	public ArrayList<Map<Long,Set<Integer>>> ibHotspotObjects = new ArrayList<Map<Long,Set<Integer>>>();
	ArrayList<Role> roles;
	int k;
	DDG ddg;
	public double F1;
	public double F2;
	public IBSolution(int k,DDG ddg,ArrayList<Role> roles){
		this.k = k;
		this.ddg = ddg;
		this.roles= roles;
		for(int i=0;i<k;i++){
			ibSwingObjects.add(new HashMap<Long,SwingObject>());
			ibTxSet.add(new HashSet<Integer>());
			ibInternalObjects.add(new HashSet<Long>());
			ibBoundaryObjects.add(new HashSet<Long>());
			ibHotspotObjects.add(new HashMap<Long,Set<Integer>>());
		}
		for(Long o:ddg.getSharedObjects().keySet()){
			allSwings.put(o,new SwingObject());
		}
	}

	//  O(n) where n is the number of shared objects in DDG
	public void assignRole(Role role, int ib){
		ibTxSet.get(ib).add(role.getId());
		ibInternalObjects.get(ib).addAll(role.getInternalObjects());
		for(Long hs:role.getHotspots()){
			Set<Integer> hsRoles; 
			if(ibHotspotObjects.get(ib).containsKey(hs)){
				hsRoles = ibHotspotObjects.get(ib).get(hs);
				hsRoles.add(role.id);
			}else{
				hsRoles = new HashSet<Integer>();
				hsRoles.add(role.id);
			}
			ibHotspotObjects.get(ib).put(hs, hsRoles);
		}
		Map<Long,SwingObject> ibSwing = ibSwingObjects.get(ib);
		for(Long o:role.getSharedObjects()){
			SwingObject s = allSwings.get(o);
			s.addIB(role.getId(),ib);
			ibSwing.put(o, s);
		}
	}

	// O(k*n) where n is the number of shared objects in DDG and k is number of ibs
	public void update(){
		for(int i=0;i<k;i++){
			Map<Long,SwingObject> ibSwing = ibSwingObjects.get(i);
			SwingObject s;
			for(Long o:ibSwing.keySet()){
				s = ibSwing.get(o);
				if(s.isBoundary()){
					ibBoundaryObjects.get(i).add(o);
					ibInternalObjects.get(i).remove(o);
				}else{
					ibInternalObjects.get(i).add(o);
					ibBoundaryObjects.get(i).remove(o);
				}

				if(!s.getIBS().contains(i)){
					//					System.out.println("should be removed");
					ibBoundaryObjects.get(i).remove(o);
					ibInternalObjects.get(i).remove(o);
				}else if(s.getIBS().size()==1){
					ibBoundaryObjects.get(i).remove(o);
					ibInternalObjects.get(i).add(o);
				}
			}

		}
		
		Set<Long> globalBoundary = new HashSet<Long>();
		for(int i=0;i<k;i++){
			for(Long o:ibBoundaryObjects.get(i))
				globalBoundary.add(o);
		}

		Map<Long,Integer> boundaries = new HashMap<Long,Integer>();
		for(int i=0;i<k;i++){
			for(Long o:ibBoundaryObjects.get(i)){
				if(boundaries.containsKey(o)){
					boundaries.put(o,boundaries.get(o)+1);
				}else{
					boundaries.put(o,1);
				}
			}
		}
		this.F2 =0;
		for(Long tempKey:boundaries.keySet()){
			int size = boundaries.get(tempKey);
			this.F2 += size-1;
		}
	}

	// O(n) where n is the number of shared objects in DDG
	public void unAssignRole(Role role,int ib){
		ibTxSet.get(ib).remove(new Integer(role.getId()));
		ibInternalObjects.get(ib).removeAll(role.getInternalObjects());
		for(Long hs:role.getHotspots()){
			Set<Integer> tempRoles = ibHotspotObjects.get(ib).get(hs);
			tempRoles.remove(role.id);
			if(tempRoles.size()==0){
				ibHotspotObjects.get(ib).remove(hs);
			}else{
				ibHotspotObjects.get(ib).put(hs, tempRoles);
			}
				
		}
		Map<Long,SwingObject> ibSwing = ibSwingObjects.get(ib);
		for(Long o:role.getSharedObjects()){
			SwingObject s = allSwings.get(o);
			s.removeIB(role.getId(),ib);
			ibSwing.put(o, s);
		}
		//		update();
	}


	public int getK(){
		return k;
	}



	public int getRandomRole(int ib){
		List<Integer> txList = new ArrayList<Integer>(ibTxSet.get(ib));
		Random rand = new Random();
		return txList.get(rand.nextInt(txList.size()));
	}


	public int emptyIBs(){
		int empty =0 ;
		for(int i=0;i<k;i++){
			if(ibTxSet.get(i).size()==0)
				empty++;
		}
		return empty;
	}

	public int getEmptyIB(){
		for(int i=0;i<k;i++){
			if(ibTxSet.get(i).size()==0)
				return i;
		}
		return -1;
	}

	public int getLargestIB(){
		int max = Integer.MIN_VALUE;
		int largest = -1;
		for(int i=0;i<k;i++){
			if(ibInternalObjects.get(i).size()> max){
				max = ibInternalObjects.get(i).size();
				largest = i;
			}
		}
		assert(largest>-1);
		return largest;
	}

	public int getSmallestIB(){
		int min = Integer.MAX_VALUE;
		int smallest = -1;
		for(int i=0;i<k;i++){
			if(ibInternalObjects.get(i).size()< min){
				min = ibInternalObjects.get(i).size();
				smallest = i;
			}
		}
		assert(smallest>-1);
		return smallest;
	}

	
	public int getSmallestHotspotIB(Set<Integer> ibs){
		int min = Integer.MAX_VALUE;
		int smallest = -1;
		for(Integer i:ibs){
			if(ibHotspotObjects.get(i).size()< min){
				min = ibHotspotObjects.get(i).size();
				smallest = i;
			}
		}
		assert(smallest>-1);
		return smallest;
	}

	public double evaluateObjFunction(){
		double f1 = 0;
		double f2 = 0;
		for(int i=0;i<k;i++){
			for(int j=i+1;j<k;j++){
				int iSize = ibHotspotObjects.get(i).size();
				int jSize = ibHotspotObjects.get(j).size();
				f1 += Math.pow(iSize-jSize, 2);
			}
		}
		f1 = Math.sqrt(f1);


		Set<Long> globalBoundary = new HashSet<Long>();
		for(int i=0;i<k;i++){
			for(Long o:ibBoundaryObjects.get(i))
				globalBoundary.add(o);
		}

		Map<Long,Integer> boundaries = new HashMap<Long,Integer>();
		for(int i=0;i<k;i++){
			for(Long o:ibBoundaryObjects.get(i)){
				if(boundaries.containsKey(o)){
					boundaries.put(o,boundaries.get(o)+1);
				}else{
					boundaries.put(o,1);
				}
			}
		}

		for(Long tempKey:boundaries.keySet()){
			int size = boundaries.get(tempKey);
			f2 += size-1;
		}
		this.F1 = f1;
		this.F2 = f2;
		return f1+f2;
	}

	public Set<Long> getBoundaryObjects(){
		Set<Long> boundaries = new HashSet<Long>();
		for(int i=0;i<k;i++)
			boundaries.addAll(ibBoundaryObjects.get(i));
		return boundaries;
	}

	public String toString(){
		Set<Long> boundaries = getBoundaryObjects();
		String output="************** Solution ******************\n";
		output += "Boundaries: " + boundaries.toString() + "\n";
		for(int i=0;i<k;i++){
//			output += "IB: " + i + " Tx: " + ibTxSet.get(i).toString() + ":"+ ibTxSet.get(i).size() + " internal: " + ibInternalObjects.get(i).toString() + " boundary: " + ibBoundaryObjects.get(i).toString() + "\n";
			output += "IB: " + i + " Tx: " + ibTxSet.get(i).size() + " INT: " +  ibInternalObjects.get(i).size() +"\n";
			output += "\t\t\t " + ibTxSet.get(i).toString() + "\n";
		}
		output += "\n********************************";
		return output;
	}

	public DDG getDDG() {
		// TODO Auto-generated method stub
		return ddg;
	}

	public void getIBCharcterstics(){
		for(int i =0;i<ibSwingObjects.size();i++){
			Set<Integer> nbrs = new HashSet<Integer>();
			Map<Long,SwingObject> swingObjects = ibSwingObjects.get(i);
			for(SwingObject so: swingObjects.values()){
				if(so.inIB(i)){
					nbrs.addAll(so.getIBS());
				}
			}
//			System.out.println("IB " + (i+1) + ":" + nbrs.toString() + nbrs.size());
		}
		
		Map<Long,Integer> tupleShared = new HashMap<Long,Integer>();
		for(Map.Entry<Long, SwingObject> entry:allSwings.entrySet()){
			SwingObject so = entry.getValue();
			if(so.isBoundary())
				tupleShared.put(entry.getKey(), so.getIBS().size());
		}
		Map<Integer,Integer> IBOverlapHist = new HashMap<Integer,Integer>();
		for(Map.Entry<Long, Integer> entry: tupleShared.entrySet()){
			int value = entry.getValue();
			if(IBOverlapHist.containsKey(value)){
				IBOverlapHist.put(value, IBOverlapHist.get(value)+1);
			}else{
				IBOverlapHist.put(value, 1);
			}
		}
//		System.out.println("IBOverlapping Histogram");
//		for(Map.Entry<Integer, Integer> entry:IBOverlapHist.entrySet()){
//			System.out.println(entry.getKey() + ":" + entry.getValue());
//		}
		
	}

}
