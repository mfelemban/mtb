/**
 * Created by: Muhamad Felemban 
 * Jun 28, 2017
 */
package aims.ooo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;

import aims.util.FairLock;
import aims.util.QueriesStatements;
import aims.util.Util;

import com.oltpbenchmark.api.BenchmarkModule;
import com.oltpbenchmark.benchmarks.smallworldbank.procedures.MaliciousTx;
import com.oltpbenchmark.util.Query;

/**
 * @author mfelemban
 *
 */
public class OOMaliciousTx implements Runnable{
	private static final Logger LOG = Logger.getLogger(OOMaliciousTx.class);
	BenchmarkModule benchmarkModule;
	Query q;
	Boolean[] ibTxStatus;
	AtomicInteger runningTx;
	AtomicBoolean waiting;
	Boolean blkdBO;
	int k;
	FairLock fl;
	Connection boConn,newConn;
	long malID;
	Map<Long,Long> txidMap;
	long prev;
	AtomicBoolean recRunning;
	public OOMaliciousTx(long malID,int k, BenchmarkModule benchmarkModule,Map<Long,Long> txidMap,long prev,Connection boConn,Connection newConn,Query q,
			Boolean [] ibTxStatus,AtomicInteger runningTx,AtomicBoolean waiting,Boolean blkdBO,FairLock fl,AtomicBoolean recRunning){
		this.malID = malID;
		this.k = k;
		this.fl = fl;
		this.benchmarkModule = benchmarkModule;
		this.q = q;
		this.ibTxStatus = ibTxStatus;
		this.runningTx = runningTx;
		this.waiting = waiting;
		this.blkdBO=blkdBO;
		this.boConn = boConn;
		this.newConn=newConn;
		this.prev = prev;
		this.txidMap = txidMap;
		this.recRunning = recRunning;
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
//			LOG.info("About to kick in");
			long[] src = q.getSrc();
			long[] dest = q.getDest();
			long[] all = Util.mergeArrays(src,dest);
			long[] allOids = new long[all.length];
			long[] srcOids = new long[src.length];
			long[] destOids = new long[dest.length];
			
			synchronized(txidMap){
				while(!txidMap.containsKey(prev) && prev != -1){
					try {
						txidMap.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
			Timestamp startTimestamp = new Timestamp(System.currentTimeMillis());
			
			
			Connection conn = benchmarkModule.makeConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(benchmarkModule.getWorkloadConfiguration().getIsolationMode());

			
			synchronized(recRunning){
				while(recRunning.get()){
					try {
						recRunning.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
//				LOG.info("Ready to go");
				synchronized(runningTx){
//					int counter = runningTx.incrementAndGet();
//					LOG.info("increment count" + malID + " " +  counter);
				}
			}
			
			
			Statement getOids = conn.createStatement();
			int i=0;

			ResultSet rs = getOids.executeQuery(QueriesStatements.createGetOIDsStatement(src));
			i=0;
			while(rs.next()){
				srcOids[i++] = rs.getLong(1);
			}
			rs.close();

			getOids = conn.createStatement();
			rs = getOids.executeQuery(QueriesStatements.createGetOIDsStatement(dest));
			i=0;
			while(rs.next()){
				destOids[i++] = rs.getLong(1);
			}
			rs.close();
			allOids = (long[]) ArrayUtils.addAll(srcOids, destOids);
			getOids.close();
			
			String stmt = "update boundary_objects set on_hold = 1, transaction_id= "  +malID+ " where object_id in (";
			for(i=0;i<allOids.length;i++){
				stmt += allOids[i] +",";
			}
			stmt = stmt.substring(0,stmt.length()-1) + ");";
			PreparedStatement ps = conn.prepareStatement(stmt);
			ps.executeUpdate();
			
			conn.commit();
			
			
			Statement getTx = conn.createStatement();
			rs = getTx.executeQuery("Select txid_current();");
			rs.next();
			long txid = rs.getInt(1);
			
			new MaliciousTx().malRun(malID,conn, q.getSrc()[0], q.getDest(), q.getAmount());
//			LOG.info("Mal " + txid + " executed");
			Statement addMap = conn.createStatement();
			addMap.executeUpdate("insert into transaction_id_map values ("+ txid +",-1, clock_timestamp());");
			conn.commit();
			
			stmt = "insert into touched_boundary_objects select "+malID+ ",object_id from boundary_objects where object_id in (";
			for( i=0;i<allOids.length;i++){
				stmt += allOids[i] +",";
			}
			stmt = stmt.substring(0,stmt.length()-1) + ");";
			ps = conn.prepareStatement(stmt);
			ps.executeUpdate();

			
//			Thread idsM = new Thread(new aims.ooo.IDMessageSender(txid,benchmarkModule,boConn,newConn,ibTxStatus,runningTx,waiting,blkdBO,fl,malID,recRunning));
//			idsM.start();
			
	
			
			synchronized(runningTx){
//				int counter = runningTx.decrementAndGet();
//				LOG.info("deccrement count" + malID + " " +  counter);
				synchronized(waiting){
//					LOG.info("Waiting now is " + waiting);
					if(waiting.get()){
						runningTx.notifyAll();
//						LOG.info("All notified on runningTx");
					}
				}
			}
			
			synchronized(txidMap){
				txidMap.put(q.getTxid(), txid);
				txidMap.notifyAll();
			}
			
			Timestamp endTimestamp = new Timestamp(System.currentTimeMillis());
			startTimestamp.setHours(startTimestamp.getHours()+4);
			endTimestamp.setHours(endTimestamp.getHours()+4);

			
			
			conn.commit();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			LOG.info("Deadlock at " + malID);
		}
	}
	
	public boolean isRecoveryRunning(Boolean[] ibTxStatus){
		boolean running = false;
		for(Boolean b:ibTxStatus){
			if(b)
				return true;
		}
		return running;
	}
}
