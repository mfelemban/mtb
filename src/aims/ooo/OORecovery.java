/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
package aims.ooo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import aims.util.FairLock;

import com.oltpbenchmark.api.BenchmarkModule;


public class OORecovery implements Callable<Boolean>{
	private static final Logger LOG = Logger.getLogger(OORecovery.class);
	long txid;
	private boolean success=false;
	private Timestamp ts;
	BenchmarkModule benchmarkModule;
	Boolean[] recoveryStatus;
	Boolean[] ibTxStatus;
	Integer runningTx;
	Boolean waiting;
	FairLock fl;
	Connection newConn;
	public OORecovery(long txid,BenchmarkModule benchmarkModule,Connection newConn,Timestamp ts,
			Boolean[] recoveryStatus,Boolean[] ibTxStatus, Integer runningTx,Boolean waiting,FairLock fl)
	{
		this.fl = fl;
		this.txid = txid;
		this.benchmarkModule = benchmarkModule;
		this.ts = ts;
		this.recoveryStatus = recoveryStatus;
		this.ibTxStatus = ibTxStatus;
		this.runningTx = runningTx;
		this.waiting = waiting;
		this.newConn = newConn;
	}



	public Boolean call() {
		//		boolean locked = getLock();

		try{
			Connection conn = benchmarkModule.makeConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

//			PreparedStatement ps = conn.prepareStatement("select wl_txid from transaction_id_map where pg_transaction_id = " + txid);
//			ResultSet rr = ps.executeQuery();
//			rr.next();
//			long currentTxId = rr.getLong(1);
			conn.commit();

//			Integer[] ibs = getIBs(conn,currentTxId);
			Integer[] ibs = getIBs(conn,txid);
			
//			LOG.info("Blocking " + Arrays.toString(ibs));

//			synchronized(recoveryStatus){
//				while(checkRecoveryStatus(ibs)){
//					recoveryStatus.wait();
//				}
//				for(int i=0;i<ibs.length;i++)
//					recoveryStatus[ibs[i]-1] = true;
//			}
			
			fl.lock();
			
			LOG.info("Started recovering " + txid);
			
			
			ResultSet rs;

			synchronized(ibTxStatus){
				for(int i=0;i<ibs.length;i++)
					ibTxStatus[ibs[i]-1] = true;
			}

			synchronized(runningTx){
				while(runningTx>0){
					synchronized(waiting){
						waiting = true;
					}
					runningTx.wait();
				}
				synchronized(waiting){
					waiting = false;
				}
			}
			
			

			PreparedStatement ps1 = conn.prepareStatement("select alertMTxn(?,?);");
			ps1.setLong(1, txid);
			ps1.setTimestamp(2, ts);
			ps1.execute();

			conn.commit();

			synchronized(ibTxStatus){
				for(int i=0;i<ibs.length;i++)
					ibTxStatus[ibs[i]-1] = false;
				ibTxStatus.notifyAll();
			}

			PreparedStatement time = conn.prepareStatement("select current_timestamp + interval '4 hours';");
			ResultSet rsTime = time.executeQuery();
			rsTime.next();
			Timestamp ts1 = rsTime.getTimestamp(1);

			PreparedStatement txID = conn.prepareStatement("select txid_current();");
			ResultSet txRS = txID.executeQuery();
			txRS.next();
			long tx = txRS.getLong(1);
			PreparedStatement releaseStatement = conn.prepareStatement("update blocked_tuples_table set (recovery_timestamp,recovering_transaction) = (?,?) "
					+ "where malicious_transaction = ? and transaction_id not in (select transaction_id from corrupted_transactions_table where malicious_transaction = ? )");
			releaseStatement.setTimestamp(1, ts1);
			releaseStatement.setLong(2, tx);
			releaseStatement.setLong(3, txid);
			releaseStatement.setLong(4, txid);
			releaseStatement.executeUpdate();

			releaseStatement = conn.prepareStatement("select update_status_table(?)");
			releaseStatement.setLong(1,txid);
			releaseStatement.executeQuery();

			conn.commit();

			ps1 = conn.prepareStatement("select transaction_id,status from corrupted_transactions_table "
					+ "where malicious_transaction = ? order by transaction_id asc");
			ps1.setLong(1, txid);
			rs = ps1.executeQuery();
			PreparedStatement undo;
			boolean good;
			
			while(rs.next()){
				//				LOG.info("## Undoing Tx " + rs.getLong(1));
				undo = conn.prepareStatement("select undo_proc(?,?)");
				long undoTX = rs.getLong(1);
				undo.setLong(1, undoTX);
				undo.setString(2, rs.getString(2));
				good = false;
				while(!good){
					try{
						undo.executeQuery();
						//						LOG.info("## Undone Tx " + rs.getLong(1));
						good = true;
					}catch(SQLException e){
						//						if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
						good = false;
						conn.rollback();
						LOG.info("************* Deadlock at " + txid + " recovery ");
						//						}
					}
				}
				BRSetNorifier notifier = new BRSetNorifier(newConn);
				notifier.start();
				LOG.info(undoTX + " undone");
			}
			conn.commit();


			ps1 = conn.prepareStatement("select transaction_id from corrupted_transactions_table "
					+ "where status = 'affected' and malicious_transaction = ? order by transaction_id asc");
			ps1.setLong(1, txid);
			rs = ps1.executeQuery();
			ArrayList<Long> affectedTx = new ArrayList<Long>();
			while(rs.next()){
				affectedTx.add(rs.getLong(1));
			}
			conn.commit();
			for(Long aTx:affectedTx){
				good = false;
				//				LOG.info("## Redoing Tx " + aTx);
				ps1 = conn.prepareStatement("select redo_proc(?)");
				ps1.setLong(1, aTx);
				while(!good){
					try{
						ps1.executeQuery();
						//						LOG.info("## Redone Tx " + aTx);
						good = true;
					}catch(SQLException e){	
						e.printStackTrace();
						conn.rollback();
						LOG.info("************* Deadlock at " + txid + " recovery ");
					}
				}
				BRSetNorifier notifier = new BRSetNorifier(newConn);
				notifier.start();
				LOG.info(aTx + " redone");
			}
			ps1 = conn.prepareStatement("select mal_trg_ends(?);");
			ps1.setLong(1, txid);
			ps1.executeQuery();
			success = true;


			if(success){
				
				conn.commit();
				LOG.info("--- Tx " + txid + " Recovered");
				
				
//				synchronized(recoveryStatus){
//					for(int i=0;i<ibs.length;i++)
//						recoveryStatus[ibs[i]-1] = false;
//					recoveryStatus.notifyAll();
//				}
				fl.unlock();
			}else{
				conn.rollback();
			}
			
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		BRSetNorifier notifier = new BRSetNorifier(newConn);
		notifier.start();
		
		return success;
	}

	public Integer[] getIBs(Connection conn,long currentTxId){
		Integer[] tempIB=null;
		try{
			Statement getIBs = conn.createStatement();
//			ResultSet rs = getIBs.executeQuery("select ib from transactions_ib where transaction_id=" + currentTxId);
			ResultSet rs = getIBs.executeQuery("select distinct i.ib from log_table l,ibd i where transaction_id ="+ currentTxId +" and i.object_id = l.object_id;");
			ArrayList<Integer> ibs = new ArrayList<Integer>();
			while(rs.next()){
				ibs.add(rs.getInt(1));
			}
			tempIB = ibs.toArray(new Integer[ibs.size()]);
			conn.commit();
		}
		catch(Exception e){
			e.printStackTrace();
			LOG.info(e.toString());
		} 

		return tempIB;
	}


	boolean checkRecoveryStatus(Integer[] ibs){
		boolean go = false;
		for(int i=0;i<ibs.length;i++){
			if(recoveryStatus[ibs[i]-1])
				return true;
		}
		return go;
	}

	boolean checkIBTxStatus(Integer[] ibs){
		boolean go = false;
		for(int i=0;i<ibs.length;i++){
			if(ibTxStatus[ibs[i]-1])
				return true;
		}
		return go;
	}
}