/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
package aims.ooo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;

import aims.framework.ActiveTransaction;
import aims.framework.BoundaryReleaser;
import aims.util.QueriesStatements;
import aims.util.Util;

import com.oltpbenchmark.api.BenchmarkModule;
import com.oltpbenchmark.benchmarks.smallworldbank.procedures.Collect;
import com.oltpbenchmark.benchmarks.smallworldbank.procedures.Distribute;
import com.oltpbenchmark.benchmarks.smallworldbank.procedures.MNTransfer;
import com.oltpbenchmark.util.Query;

public class OutOfOrderExecutor implements Runnable {
	private static final Logger LOG = Logger.getLogger(OutOfOrderExecutor.class);
	BenchmarkModule benchmarkModule;
	Map<Long,Long> txidMap;
	List<String> samples ;
	ActiveTransaction at;
	Boolean[] ibTxStatus;
	//	Integer runningTx;
	AtomicInteger runningTx;
	AtomicBoolean waiting;
	AtomicInteger succCounter;
	Boolean blkdBO;
	Connection boConn,newConn;
	long malPrev;
	ConcurrentMap<Long,Long> BO;
	public OutOfOrderExecutor(ActiveTransaction at, BenchmarkModule benchmarkModule,Map<Long,Long> txidMap, long malPrev,
			List<String> samples,Boolean[] ibTxStatus,AtomicInteger runningTx,AtomicBoolean waiting, AtomicInteger succCounter,Boolean blkdBO,Connection boConn,Connection newConn,ConcurrentMap<Long,Long> BO){
		this.at = at;
		this.benchmarkModule= benchmarkModule;
		this.txidMap = txidMap;
		this.samples = samples;
		this.ibTxStatus = ibTxStatus;
		this.runningTx = runningTx;
		this.waiting = waiting;
		this.succCounter = succCounter;
		this.blkdBO = blkdBO;
		this.boConn = boConn;
		this.malPrev = malPrev;
		this.newConn = newConn;
		this.BO = BO;
	}

	public void run(){
		int retried = -100;
		Timestamp startTimestamp = new Timestamp(System.currentTimeMillis());
		int numberRetried = 0;

		boolean repeat = true;
		boolean successExec = false;
		long txid = -1;


		ArrayList<Query> tx = at.getTx();
		Query q = tx.get(0);
		long currentTxID = tx.get(0).getTxid();
		long[] src = q.getSrc();
		long[] dest = q.getDest();
		double amount = q.getAmount();
		long[] all = Util.mergeArrays(src,dest);
		long[] allOids = new long[all.length];
		long[] srcOids = new long[src.length];
		long[] destOids = new long[dest.length];

		//		synchronized(txidMap){
		//			while(!txidMap.containsKey(malPrev) && malPrev != -1){
		//				try {
		////					txidMap.wait();
		//				} catch (InterruptedException e) {
		//					e.printStackTrace();
		//				}
		//			}
		//		}

		//		printBlockedBO();
		ResultSet rs = null;
		Connection conn = null;
		//		at.start();
		int IB = 0;

		try{
			//			LOG.info(Thread.currentThread().getId() + "  " + currentTxID + " starts");
			conn = benchmarkModule.makeConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(benchmarkModule.getWorkloadConfiguration().getIsolationMode());
			PreparedStatement ps = conn.prepareStatement("Select ib from transactions_ib where transaction_id = " + (currentTxID) + ";");
			rs = ps.executeQuery();
			rs.next();
			IB = rs.getInt(1);
			//


			successExec = false;
			Statement getOids = conn.createStatement();
			int i=0;

			rs = getOids.executeQuery(QueriesStatements.createGetOIDsStatement(src));
			i=0;
			while(rs.next()){
				srcOids[i++] = rs.getLong(1);
			}
			rs.close();

			getOids = conn.createStatement();
			rs = getOids.executeQuery(QueriesStatements.createGetOIDsStatement(dest));
			i=0;
			while(rs.next()){
				destOids[i++] = rs.getLong(1);
			}
			rs.close();
			allOids = (long[]) ArrayUtils.addAll(srcOids, destOids);
			getOids.close();
			conn.commit();

			while(repeat){

				at.start();


				numberRetried++;
				int numberOfBlocked =0;
				int numberOfRepairSet=0;
				//				int numberOfDirtyBoundary=0;

				boolean success = false;
				boolean corBlocked = false;
				//				boolean boBlocked = false;
				//				while(!success){
				try{
					//						Statement checkDirtyBoundary = conn.createStatement();
					//						ResultSet rs3 = checkDirtyBoundary.executeQuery(QueriesStatements.createCheckDirtyBoundary(allOids,currentTxID));
					//						rs3.next();
					//						numberOfDirtyBoundary = rs3.getInt(1);
					//						synchronized(BO){
					boolean clean = false;
					while(!clean){
						clean = true;
						for(long oid:allOids){
							if(BO.containsKey(oid))
								if(BO.get(oid)!=-1){
									clean = false;
									break;
								}						
						}
						if(!clean){
							synchronized(BO){
								for(long oid:allOids){
									BO.get(oid).wait();
								}
							}
							LOG.info(Thread.currentThread().getId() + " looping here ");
						}

					}
					for(long oid:allOids){
						if(BO.containsKey(oid)){
							BO.replace(oid, currentTxID);
						}
					}
					//							LOG.info(currentTxID + "  " + Arrays.toString(allOids));
					//							printBlockedBO();
					//						LOG.info(currentTxID + " Locking " + counter + " boundary objects out of " + allOids.length);
					//						}

					success  =true;
				}catch(Exception e){
					e.printStackTrace();
					LOG.info("Problem in blocking BOs");
				}
				//				}

				synchronized(ibTxStatus){
					while(ibTxStatus[IB-1]){
						if(!at.corBlocked){
							at.setCorBlocked();
							at.startBlocking(System.currentTimeMillis());
						}
						synchronized(waiting){
							if(waiting.get()){
								try{
									runningTx.notifyAll();
								}catch(Exception e){

								}
							}
						}
						//						LOG.info("Waiting for recovery transaction " + currentTxID);
						ibTxStatus.wait();
						//						LOG.info("Maybe out for recovery transaction " + currentTxID);
					}
					synchronized(runningTx){
						runningTx.incrementAndGet();
						//						LOG.info("increment " + currentTxID +" "+ runningTx.get());
					}
				}


				Statement checkBlockedTuples = conn.createStatement();
				rs = checkBlockedTuples.executeQuery(QueriesStatements.createCheckBlockedTuples(srcOids)); 
				rs.next();
				numberOfBlocked = rs.getInt(1);
				rs.close();

				Statement checkRepairTable = conn.createStatement();
				ResultSet rs1 = checkRepairTable.executeQuery(QueriesStatements.createRepairTable(srcOids));
				rs1.next();
				numberOfRepairSet = rs1.getInt(1);



				Statement checkTotalBlockedTuples = conn.createStatement();
				rs = checkTotalBlockedTuples.executeQuery(QueriesStatements.createTotalBlockedTuples()); 
				rs.next();
				int totalBlocked = rs.getInt(1);

				Statement checkTotalRepairTuples = conn.createStatement();
				rs = checkTotalRepairTuples.executeQuery(QueriesStatements.getTotalRepairTuples()); 
				rs.next();
				int totalRepair = rs.getInt(1);
				totalBlocked += totalRepair;

				boolean first = true;
				//				if(numberOfBlocked>0 || numberOfRepairSet > 0 || numberOfDirtyBoundary>0 || (at.boundary && !ba.canExecute(boundaries, currentTxID))){
				//				LOG.info(currentTxID + " drty " + numberOfDirtyBoundary + " blkd " + numberOfBlocked + " rpr " + numberOfRepairSet);
				if(numberOfBlocked>0 || numberOfRepairSet > 0){
					if(!corBlocked){
						//						LOG.info(currentTxID + " have to wait at blocked and repair [" + numberOfBlocked + "," + numberOfRepairSet + "]");
						corBlocked =true;
					}
					if(first){
						at.setCorBlocked();
						at.startBlocking(System.currentTimeMillis());
						at.setTuplesBlocked(numberOfBlocked);
						at.setTuplesToRepair(numberOfRepairSet);
						at.setTotalTuplesBlocked(totalBlocked);
						first = false;
					}
					synchronized(runningTx){
						runningTx.decrementAndGet();
						//						LOG.info("Decrement " + currentTxID);
						//						LOG.info("counter " + runningTx.get());
						synchronized(waiting){
							if(waiting.get())
								runningTx.notifyAll();
						}
					}
					Connection lConn = benchmarkModule.makeConnection();
					BRSetListener lstn = new BRSetListener(lConn,srcOids,currentTxID);
					//					BRSetListener lstn = new BRSetListener(conn,srcOids);
					lstn.start();

					lstn.join();
					lConn.close();
					synchronized(ibTxStatus){
						while(ibTxStatus[IB-1]){
							ibTxStatus.wait();
						}
						synchronized(runningTx){
							runningTx.incrementAndGet();
							//							LOG.info("increment " + currentTxID +" "+ runningTx.get());
						}
					}
				}


				conn.commit();
				if(at.corBlocked)
					at.endBlocking(System.currentTimeMillis());		// end blockage time computation
				//				LOG.info("Executing " + currentTxID);
				switch(q.getType()){
				case 2:{
					break;
				}
				case 3: { 
					txid = new Distribute().isRun(conn, src[0], dest, amount);
					break;
				}
				case 4: {
					txid = new Collect().isRun(conn, src, dest[0], amount);
					break;
				}
				case 5: {
					txid= new MNTransfer().isRun(conn, src, dest,amount);
					break;
				}
				}
				if(txid==-1){
					successExec = false;
				}
				else{
					successExec = true;
				}
				at.end();
				if(successExec){
					Statement addMap = conn.createStatement();
					addMap.executeUpdate("insert into transaction_id_map values ("+ txid +"," + currentTxID+ ", clock_timestamp());");
					conn.commit();
					repeat = false;
					//					LOG.info(currentTxID);
					if(!q.isMalicious())
						releaseBoundaryObject(benchmarkModule,allOids,txid,blkdBO,boConn,currentTxID,BO);
					txidMap.put(q.getTxid(), txid);
					//					conn.close();
				}else{
					//					System.exit(0);
					at.endBlocking(System.currentTimeMillis());
					repeat = true;
					conn.rollback();
					retried = (int) currentTxID;
					LOG.info("retrying " + currentTxID);
					String stmt = "update boundary_objects set on_hold = 0, transaction_id = -1 where transaction_id = " + currentTxID +";";
					ps = conn.prepareStatement(stmt);
					ps.executeUpdate();
					conn.commit();
				}

				synchronized(runningTx){
					runningTx.decrementAndGet();
					//					LOG.info("Decrement " + currentTxID);
					//					LOG.info("counter " + runningTx.get());
					synchronized(waiting){
						//						LOG.info("Waiting now is " + waiting);
						if(waiting.get()){
							runningTx.notifyAll();
							//							LOG.info("All bee notified on runningTx");
						}
					}
				}

				if(retried != -100)
					LOG.info(retried + " done");

				synchronized(succCounter){
					if(successExec){
						succCounter.incrementAndGet();
						succCounter.notify();
					}
				}

				//				LOG.info(currentTxID + " done");
				if(repeat)
					LOG.info("looping here ");
			}




			Timestamp endTimestamp = new Timestamp(System.currentTimeMillis());
			startTimestamp.setHours(startTimestamp.getHours()+4);
			endTimestamp.setHours(endTimestamp.getHours()+4);

			PreparedStatement addTimeInfo = conn.prepareStatement("insert into availability_info values ("+ txid +",'" + startTimestamp +"','" 
					+ endTimestamp+ "', " + at.getRunTime()+"," + at.getTotalRunTime() +","+ at.getBlockageTime() + "," + ((at.boBlocked)? 1:0 ) +","+((at.corBlocked)? 1:0 )
					+ ", (select count( distinct object_id) from log_table where transaction_id="+txid+ "),"
					+at.getTuplesBlocked() + "," +at.getTuplesToRepair()+"," +at.getTotalBlockedTuples()+","+IB+","+numberRetried+");");
			addTimeInfo.executeUpdate();
			//			LOG.info(txid + " retried " + numberRetried);

			String stmt = "insert into touched_boundary_objects select "+currentTxID+ ",object_id from boundary_objects where object_id in (";
			for( i=0;i<allOids.length;i++){
				stmt += allOids[i] +",";
			}
			stmt = stmt.substring(0,stmt.length()-1) + ");";
			ps = conn.prepareStatement(stmt);
			ps.executeUpdate();
			synchronized(txidMap){
				txidMap.put(q.getTxid(), txid);
				txidMap.notifyAll();
			}
			conn.commit();
			conn.close();


			BRSetNorifier notifier = new BRSetNorifier(newConn);
			notifier.start();

		}
		catch(Exception e){
			e.printStackTrace();
		}


	}

	public  void releaseBoundaryObject(BenchmarkModule benchmarkModule, long[] oids,long txid,Boolean blkdBO,Connection boConn,long currentTxID,ConcurrentMap<Long,Long> BO) {
		try{
			//			LOG.info("Holding " + updated + " boundary tuples");
			Thread th = new Thread(new BoundaryReleaser(txid,benchmarkModule,boConn,oids,false,blkdBO,currentTxID,BO));
			th.start();
		}

		catch(Exception e){
			e.printStackTrace();
		}

	}

	//	public void printBlockedBO(){
	//		String bos= "Blocked BO [";
	//		for(Long oid:BO.keySet()){
	//			if(BO.get(oid)!=-1){
	//				bos += oid + ":" + BO.get(oid) + ",";
	//			}
	//		}
	//		LOG.info(bos+"]");
	//	}


	public  boolean containBlocked(Long[] src, List<Long> blockedTuples){
		boolean contain  = false;
		if(blockedTuples.isEmpty())
			return contain;
		for(Long id:src){
			if(blockedTuples.contains(id))
				return true;
		}
		return contain;
	}

	/* (non-Javadoc)
	 * @see java.util.concurrent.Callable#call()
	 */

}