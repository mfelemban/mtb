/**
 * Created by: Muhamad Felemban 
 * Jun 6, 2017
 */
package aims.ooo;

/**
 * @author mfelemban
 *
 */
public class Procedures {
	public static String prepareCollect(long[] src, long dest, double amount){
		String stmt = "select collect('{";
		for(int i=0;i<src.length;i++){
			stmt += src[i];
			if(i<src.length-1)
				stmt += ",";
		}
		stmt += "}'::int[], " + dest + ", " + amount + ");";
//		System.out.println(stmt);
		return stmt;
	}
	
	public static String prepareDistribute(long src, long[] dest, double amount){
		String stmt = "select distribute("+src + ",'{";
		for(int i=0;i<dest.length;i++){
			stmt += dest[i];
			if(i<dest.length-1)
				stmt += ",";
		}
		stmt += "}'::int[], " + amount + ");";
//		System.out.println(stmt);
		return stmt;
	}
	
	public static String prepareMNT(long[] src, long[] dest, double amount){
		String stmt = "select mnt('{";
		for(int i=0;i<src.length;i++){
			stmt += src[i];
			if(i<src.length-1)
				stmt += ",";
		}
		stmt += "}'::int[],'{";
		for(int i=0;i<dest.length;i++){
			stmt += dest[i];
			if(i<dest.length-1)
				stmt += ",";
		}
		stmt += "}'::int[], " + amount + ");";
//		System.out.println(stmt);
		return stmt;
	}
	
	
}
