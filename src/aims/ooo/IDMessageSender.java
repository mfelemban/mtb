/**
 * Created by: Muhamad Felemban 
 * Apr 4, 2017
 */
 package aims.ooo;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import aims.util.FairLock;

import com.oltpbenchmark.api.BenchmarkModule;

public class IDMessageSender implements Runnable {

	private static final Logger LOG = Logger.getLogger(IDMessageSender.class);

	private long txid;
	private Timestamp ts;
	private long delay;
	BenchmarkModule benchmarkModule;
	Map<Long,Long> txidMap;
	Boolean[] ibTxStatus;
	AtomicInteger runningTx;
	AtomicBoolean waiting;
	Boolean blkdBO;
	FairLock fl;
	Connection boConn,newConn;
	long malID;
	AtomicBoolean recRunning;
	ConcurrentMap<Long,Long> BO;

	public IDMessageSender(long txid,BenchmarkModule benchmarkModule,Connection boConn,Connection newConn,
			Boolean [] ibTxStatus,AtomicInteger runningTx,AtomicBoolean waiting,Boolean blkdBO,FairLock fl,
			long malID,AtomicBoolean recRunning,Map<Long,Long> txidMap,ConcurrentMap<Long,Long> BO) throws SQLException {
		super();
		this.malID = malID;
		this.fl = fl;
		this.txid = txid;
		this.delay = benchmarkModule.getWorkloadConfiguration().getXmlConfig().getLong("mddelay");;
		this.benchmarkModule = benchmarkModule;
		this.ibTxStatus = ibTxStatus;
		this.runningTx = runningTx;
		this.waiting = waiting;
		this.blkdBO=blkdBO;
		this.boConn=boConn;
		this.newConn = newConn;
		this.recRunning = recRunning;
		this.txidMap=txidMap;
		this.BO = BO;
	}
//	public IDMessageSender(long txid,BenchmarkModule benchmarkModule,Connection boConn,Connection newConn,
//			Boolean [] ibTxStatus,AtomicInteger runningTx,AtomicBoolean waiting,Boolean blkdBO,FairLock fl,
//			long malID,AtomicBoolean recRunning) throws SQLException {
//		super();
//		this.malID = malID;
//		this.fl = fl;
//		this.txid = txid;
//		this.delay = benchmarkModule.getWorkloadConfiguration().getXmlConfig().getLong("mddelay");;
//		this.benchmarkModule = benchmarkModule;
//		this.ibTxStatus = ibTxStatus;
//		this.runningTx = runningTx;
//		this.waiting = waiting;
//		this.blkdBO=blkdBO;
//		this.boConn=boConn;
//		this.newConn = newConn;
//		this.recRunning = recRunning;
//		
//	}

//	public IDMessageSender(long txid,long delay,BenchmarkModule benchmarkModule) throws SQLException {
//		super();
//		this.txid = txid;
//		this.delay = delay;
//		this.benchmarkModule = benchmarkModule;
//	}




	public void run() {
		try{
			long malTxid = 0;
			synchronized(txidMap){
				while(!txidMap.containsKey(txid)){
					txidMap.wait();
				}
			}
			malTxid = txidMap.get(txid);
			
			Thread.sleep(delay);
//			LOG.info("Malicious tx " + txid + " " + malTxid);
			ts = new Timestamp(System.currentTimeMillis());
			
			ExecutorService pool = Executors.newSingleThreadScheduledExecutor();
//			Callable<Boolean> aims = new OOResponse(benchmarkModule,txid,blkdBO,boConn,malID);
			Callable<Boolean> aims = new OOResponse(benchmarkModule,malTxid,blkdBO,boConn,malID,BO);
			Future<Boolean> resFuture = pool.submit(aims);

			boolean resSucess= resFuture.get();

			while(!resSucess){
//				LOG.info("Re executing response " + txid);
//				aims = new OOResponse(benchmarkModule,txid,blkdBO,boConn,malID);
				aims = new OOResponse(benchmarkModule,malTxid,blkdBO,boConn,malID,BO);
				resFuture = pool.submit(aims);
				resSucess= resFuture.get();				
			}

//			Connection conn = benchmarkModule.makeConnection();
//			conn.setAutoCommit(false);
//			conn.setTransactionIsolation(Connection.TRANSACTION_REPEATABLE_READ);
//			
//			PreparedStatement ps = conn.prepareStatement("insert into IDS values ("+txid+",clock_timestamp()::timestamp + interval '4hours')");
//			ps.executeUpdate();
//			
//			conn.commit();
//			conn.close();

//			aims = new OORecovery2(txid,benchmarkModule,newConn,ts,ibTxStatus,runningTx,waiting,fl,recRunning);
			aims = new OORecovery2(malTxid,benchmarkModule,newConn,ts,ibTxStatus,runningTx,waiting,fl,recRunning);
			Future<Boolean> recFuture = pool.submit(aims);

			boolean recSucess= recFuture.get();
			while(!recSucess){
//				LOG.info("Re executing recovery " + txid);
//				aims = new OORecovery2(txid,benchmarkModule,newConn,ts,ibTxStatus,runningTx,waiting,fl,recRunning);
				aims = new OORecovery2(malTxid,benchmarkModule,newConn,ts,ibTxStatus,runningTx,waiting,fl,recRunning);
				recFuture = pool.submit(aims);
				recSucess= recFuture.get();				
			}
			pool.shutdown();
			
			

		}catch(Exception e){
			e.printStackTrace();
		}

		//        LOG.info(String.format("Transaction # %d is malicious",txid));
	}

}