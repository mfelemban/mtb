/**
 * Created by: Muhamad Felemban 
 * Jun 30, 2017
 */
package aims.ooo;

import java.sql.*;

import org.apache.log4j.Logger;

import aims.util.QueriesStatements;

/**
 * @author mfelemban
 *
 */
public class BRSetListener extends Thread{
	private static final Logger LOG = Logger.getLogger(BRSetListener.class);
	long[] srcOids;
	Connection conn;
	org.postgresql.PGConnection pgconn;
	long currentTxID;
	public BRSetListener(Connection conn,long[] srcOids,long currentTxID) {
		this.conn = conn;
		this.pgconn = (org.postgresql.PGConnection)conn;
		this.currentTxID = currentTxID;
		try{
			Statement stmt = conn.createStatement();
			stmt.execute("LISTEN BRSet");
			stmt.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		this.srcOids = srcOids;

	}

	public void run() {



		try {
			// issue a dummy query to contact the backend
			// and receive any pending notifications.
			int numberOfBlocked =0, numberOfRepairSet=0;
			boolean keepGoing = true;
//			LOG.info("BR listner " + currentTxID + " started");
			while(keepGoing){

				org.postgresql.PGNotification notifications[] = pgconn.getNotifications();
				if (notifications != null) {
					for (int i=0; i<notifications.length; i++) {
					}

					Statement checkBlockedTuples = conn.createStatement();
					ResultSet rs = checkBlockedTuples.executeQuery(QueriesStatements.createCheckBlockedTuples(srcOids)); 
					rs.next();
					numberOfBlocked = rs.getInt(1);

					Statement checkRepairTable = conn.createStatement();
					ResultSet rs1 = checkRepairTable.executeQuery(QueriesStatements.createRepairTable(srcOids));
					rs1.next();
					numberOfRepairSet = rs1.getInt(1);

					if(numberOfBlocked==0 && numberOfRepairSet == 0)
						keepGoing = false;
				}

				Thread.sleep(50);


			}
//			LOG.info("BR listner " + currentTxID + " done");
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		 catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}
}
