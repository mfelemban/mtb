package com.oltpbenchmark.util;

public class Util {
	public static String printArray(long[] a){
		String output = "";
		for(int i=0;i<a.length;i++){
			if(i<a.length-1)
				output += a[i]+":";
			else
				output += a[i];
		}
		return output;	
	}
}
