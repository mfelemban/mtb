package com.oltpbenchmark.benchmarks.smallworldbank;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.oltpbenchmark.WorkloadConfiguration;
import com.oltpbenchmark.api.BenchmarkModule;
import com.oltpbenchmark.api.Procedure.UserAbortException;
import com.oltpbenchmark.api.TransactionTypes;

public class SWBankRepeat implements Thread.UncaughtExceptionHandler{
	
	@SuppressWarnings("unused")
	private final ArrayList<Thread> workerThreads;
	private static final Logger LOG = Logger.getLogger(SWBankRepeat.class);
	@SuppressWarnings("unused")
	public SWBankRepeat(String rptFile, List<BenchmarkModule> benchList) {
		FileInputStream fi;
		this.workerThreads = new ArrayList<Thread>();
		
		try {
			fi = new FileInputStream(rptFile);
			BufferedReader br = new BufferedReader(new InputStreamReader(fi));
			BenchmarkModule bench = benchList.get(0);
			WorkloadConfiguration workConfs = bench.getWorkloadConfiguration();
			TransactionTypes types = workConfs.getTransTypes();
			String line;
			int id = 0;
			while((line=br.readLine())!=null){
				LOG.info("DODO " + line);
				int typeNumber = Integer.parseInt(line.split(",")[0]);
//				switch(typeNumber)s
				RepeatThread rt=  new RepeatThread(bench,id,false);
//				new Thread(rt).start();
				
			}
			
			
			
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UserAbortException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		


		
	}
	@Override
	public void uncaughtException(Thread t, Throwable e) {
		// TODO Auto-generated method stub
		
		 e.printStackTrace();
	        System.exit(-1);
		
	}
}

class RepeatThread implements Runnable
{
    SWBankWorker sw;
    public RepeatThread(BenchmarkModule bench, int id,boolean isAttacker) 
    {
//        this.sw= new SWBankWorker(bench,id,isAttacker);
    }
    @Override
    public void run() 
    {
       try {
//		sw.executeWork(" ");
		sw.tearDown(false);
	} catch (UserAbortException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
        //Do the actual thing
    }
}

