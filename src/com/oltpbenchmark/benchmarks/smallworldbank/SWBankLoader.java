/******************************************************************************
 *  Copyright 2016 by OLTPBenchmark Project  
 *  
 *  Author: Thamir Qadah                                 *
 *                                                                            *
 *  Licensed under the Apache License, Version 2.0 (the "License");           *
 *  you may not use this file except in compliance with the License.          *
 *  You may obtain a copy of the License at                                   *
 *                                                                            *
 *    http://www.apache.org/licenses/LICENSE-2.0                              *
 *                                                                            *
 *  Unless required by applicable law or agreed to in writing, software       *
 *  distributed under the License is distributed on an "AS IS" BASIS,         *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *
 *  See the License for the specific language governing permissions and       *
 *  limitations under the License.                                            *
 ******************************************************************************/

package com.oltpbenchmark.benchmarks.smallworldbank;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.oltpbenchmark.api.BenchmarkModule;
import com.oltpbenchmark.api.Loader;
import com.oltpbenchmark.catalog.Table;
import com.oltpbenchmark.types.DatabaseType;
import com.oltpbenchmark.util.SQLUtil;

public class SWBankLoader extends Loader {

	private static final Logger LOG = Logger.getLogger(SWBankLoader.class);


	private long totaltuples = 0;

	public SWBankLoader(BenchmarkModule benchmark, Connection conn) {
		super(benchmark, conn);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void load() throws SQLException {
		// TODO Auto-generated method stub
		//        int c_id = 1;  
		//        int b_id = 1;

		long a_id = 1;


		//        String c_sql = createInsertSql(SWBankConstants.TABLENAME_COUNTRY);
		//        String b_sql = createInsertSql(SWBankConstants.TABLENAME_BRANCH);
		//        String cust_sql = createInsertSql(SWBankConstants.TABLENAME_CUSTOMER);
//		String a_sql = createInsertSql(SWBankConstants.TABLENAME_ACCOUNT);
		String chk_sql = createInsertSql(SWBankConstants.TABLENAME_CHECKING);
//		String sav_sql = createInsertSql(SWBankConstants.TABLENAME_SAVING);

		//        PreparedStatement c_ps = this.conn.prepareStatement(c_sql);
		//        PreparedStatement b_ps = this.conn.prepareStatement(b_sql);
		//        PreparedStatement cust_ps = this.conn.prepareStatement(cust_sql);
//		PreparedStatement a_ps = this.conn.prepareStatement(a_sql);
		PreparedStatement chk_ps = this.conn.prepareStatement(chk_sql);
//		PreparedStatement sav_ps = this.conn.prepareStatement(sav_sql);


		try {


			int numberOfAccounts = (int) Math.round(this.scaleFactor*SWBankConstants.NUMBER_OF_ACCOUNTS);

			//                LOG.info("b_num = "+b_num);

			for (int j = 0; j < numberOfAccounts; j++) {

				/// create account
				// 1 account per customer for now.
//				a_ps.setLong(1, cust_id);
//				a_ps.setLong(2, a_id);
//
//
//				a_ps.executeUpdate();
//				incrementTotalDataObjects();


				// initialize checking
				chk_ps.setLong(1, a_id);
				chk_ps.setFloat(2, SWBankConstants.INITAL_BALANCE);                        
				chk_ps.executeUpdate();
				incrementTotalDataObjects();

//				// initialize saving
//				sav_ps.setLong(1, a_id);
//				sav_ps.setFloat(2, SWBankConstants.INITAL_BALANCE);
//				sav_ps.executeUpdate();
//				incrementTotalDataObjects();

				a_id++;


			} // end for


			LOG.info("Total tuples = "+totaltuples);

		}  catch (SQLException e) {
			e.printStackTrace();
			throw e.getNextException();
		} 

	}

	private void incrementTotalDataObjects() {
		++totaltuples ;
	}

	/**
	 * @return
	 */
	private String createInsertSql(String tablename) {
		Table catalog_tbl = getTableCatalog(tablename);
		String sql;

		// Load countries
		if (this.getDatabaseType() == DatabaseType.POSTGRES
				|| this.getDatabaseType() == DatabaseType.MONETDB)
			sql = SQLUtil.getInsertSQL(catalog_tbl, false);
		else
			sql = SQLUtil.getInsertSQL(catalog_tbl);
		return sql;
	}

}
