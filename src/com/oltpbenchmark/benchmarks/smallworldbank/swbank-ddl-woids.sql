-- Drop tables
DROP TABLE IF EXISTS SAVING;
DROP TABLE IF EXISTS CHECKING;
DROP TABLE IF EXISTS ACCOUNT;


-- Account table
-- CREATE TABLE ACCOUNT
-- ( cust_id bigint  NOT NULL
-- , a_id bigint     NOT NULL
-- , PRIMARY KEY
--   (
--     a_id
--   )
-- ) with oids;

-- Checking table
CREATE TABLE CHECKING
(
  chk_id 	    bigint     
, balance 	FLOAT not null
) with oids;

-- Saving table
-- CREATE TABLE SAVING
-- (
--   sav_id 	    bigint     NOT NULL REFERENCES ACCOUNT (a_id)
-- , balance 	FLOAT not null
-- ) with oids;