/******************************************************************************
 *  Copyright 2015 by OLTPBenchmark Project                                   *
 *                                                                            *
 *  Licensed under the Apache License, Version 2.0 (the "License");           *
 *  you may not use this file except in compliance with the License.          *
 *  You may obtain a copy of the License at                                   *
 *                                                                            *
 *    http://www.apache.org/licenses/LICENSE-2.0                              *
 *                                                                            *
 *  Unless required by applicable law or agreed to in writing, software       *
 *  distributed under the License is distributed on an "AS IS" BASIS,         *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *
 *  See the License for the specific language governing permissions and       *
 *  limitations under the License.                                            *
 ******************************************************************************/


package com.oltpbenchmark.benchmarks.smallworldbank;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.collections15.map.ListOrderedMap;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.SubnodeConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.tree.xpath.XPathExpressionEngine;
import org.apache.log4j.Logger;

import com.oltpbenchmark.Phase;
import com.oltpbenchmark.TraceReader;
import com.oltpbenchmark.WorkloadConfiguration;
import com.oltpbenchmark.api.BenchmarkModule;
import com.oltpbenchmark.api.TransactionType;
import com.oltpbenchmark.api.TransactionTypes;
import com.oltpbenchmark.types.DatabaseType;
import com.oltpbenchmark.util.ClassUtil;
import com.oltpbenchmark.util.StringUtil;

public class ExecuteWorkloadDTQR {

	private static final Logger LOG = Logger.getLogger(ExecuteWorkloadDTQR.class);

	private static final String SINGLE_LINE = "**********************************************************************************";

	private static final String RATE_DISABLED = "disabled";
	private static final String RATE_UNLIMITED = "unlimited";

	/**
	 * @param args
	 * @throws Exception 
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {
		// Initialize log4j
		String log4jPath = System.getProperty("log4j.configuration");
		System.out.println(log4jPath);
		if (log4jPath != null) {
			org.apache.log4j.PropertyConfigurator.configure(log4jPath);
		} else {
			throw new RuntimeException("Missing log4j.properties file");
		}

		// create the command line parser
		CommandLineParser parser = new PosixParser();
		XMLConfiguration pluginConfig=null;
		try {
			pluginConfig = new XMLConfiguration("config/plugin.xml");
		} catch (ConfigurationException e1) {
			LOG.info("Plugin configuration file config/plugin.xml is missing");
			e1.printStackTrace();
		}
		pluginConfig.setExpressionEngine(new XPathExpressionEngine());
		Options options = new Options();
		options.addOption(
				"b",
				"bench",
				true,
				"[required] Benchmark class. Currently supported: "+ pluginConfig.getList("/plugin//@name"));
		options.addOption(
				"c", 
				"config", 
				true,
				"[required] Workload configuration file");
		options.addOption(
				null,
				"runscript",
				true,
				"Run an SQL script");

		options.addOption("v", "verbose", false, "Display Messages");
		options.addOption("h", "help", false, "Print this help");
		options.addOption("s", "sample", true, "Sampling window");
		options.addOption("im", "interval-monitor", true, "Throughput Monitoring Interval in seconds");
		options.addOption("ss", false, "Verbose Sampling per Transaction");
		options.addOption("o", "output", true, "Output file (default System.out)");
		options.addOption("d", "directory", true, "Base directory for the result files, default is current directory");
		options.addOption("t", "timestamp", false, "Each result file is prepended with a timestamp for the beginning of the experiment");
		options.addOption("ts", "tracescript", true, "Script of transactions to execute");
		// K
		options.addOption("ar", "Arrival files", true, "File contains transactions arrival trace");
		options.addOption("ib", "IB files", true, "File contains the IB assignment");
		options.addOption("hot", "Backup tuples set", true, "File contains the set of duplicated tuples");

		// parse the command line arguments
		CommandLine argsLine = parser.parse(options, args);
		if (argsLine.hasOption("h")) {
			printUsage(options);
			return;
		} else if (argsLine.hasOption("c") == false) {
			LOG.error("Missing Configuration file");
			printUsage(options);
			return;
		} else if (argsLine.hasOption("b") == false) {
			LOG.fatal("Missing Benchmark Class to load");
			printUsage(options);
			return;
		}

	
		String arrivalTraceFile ="";
		if(argsLine.hasOption("ar")){
			arrivalTraceFile  = argsLine.getOptionValue("ar");
		}
		
		String ibAssignmentFile ="";
		if(argsLine.hasOption("ib")){
			ibAssignmentFile  = argsLine.getOptionValue("ib");
		}
		
		String hotspot ="";
		if(argsLine.hasOption("hot")){
			hotspot  = argsLine.getOptionValue("hot");
		}

		// -------------------------------------------------------------------
		// GET PLUGIN LIST
		// -------------------------------------------------------------------

		String targetBenchmarks = argsLine.getOptionValue("b");

		String[] targetList = targetBenchmarks.split(",");
		List<BenchmarkModule> benchList = new ArrayList<BenchmarkModule>();

		// Use this list for filtering of the output
		List<TransactionType> activeTXTypes = new ArrayList<TransactionType>();

		String configFile = argsLine.getOptionValue("c");
		XMLConfiguration xmlConfig = new XMLConfiguration(configFile);
		xmlConfig.setExpressionEngine(new XPathExpressionEngine());

		// Load the configuration for each benchmark
		int lastTxnId = 0;
		for (String plugin : targetList) {
			String pluginTest = "[@bench='" + plugin + "']";

			// ----------------------------------------------------------------
			// BEGIN LOADING WORKLOAD CONFIGURATION
			// ----------------------------------------------------------------

			WorkloadConfiguration wrkld = new WorkloadConfiguration();
			wrkld.setBenchmarkName(plugin);
			wrkld.setXmlConfig(xmlConfig);
			boolean scriptRun = false;
			if (argsLine.hasOption("t")) {
				scriptRun = true;
				String traceFile = argsLine.getOptionValue("t");
				wrkld.setTraceReader(new TraceReader(traceFile));
				if (LOG.isDebugEnabled()) LOG.debug(wrkld.getTraceReader().toString());
			}

			// Pull in database configuration
			wrkld.setDBType(DatabaseType.get(xmlConfig.getString("dbtype")));
			wrkld.setDBDriver(xmlConfig.getString("driver"));
			wrkld.setDBConnection(xmlConfig.getString("DBUrl"));
			wrkld.setDBName(xmlConfig.getString("DBName"));
			wrkld.setDBUsername(xmlConfig.getString("username"));
			wrkld.setDBPassword(xmlConfig.getString("password"));
			wrkld.setDelay(xmlConfig.getLong("delay"));
			int terminals = xmlConfig.getInt("terminals[not(@bench)]", 0);
			terminals = xmlConfig.getInt("terminals" + pluginTest, terminals);
			wrkld.setTerminals(terminals);
			String isolationMode = xmlConfig.getString("isolation[not(@bench)]", "TRANSACTION_SERIALIZABLE");
			wrkld.setIsolationMode(xmlConfig.getString("isolation" + pluginTest, isolationMode));
			wrkld.setScaleFactor(xmlConfig.getDouble("scalefactor", 1.0));
			wrkld.setRecordAbortMessages(xmlConfig.getBoolean("recordabortmessages", false));
			wrkld.setDataDir(xmlConfig.getString("datadir", "."));

			// ----------------------------------------------------------------
			// CREATE BENCHMARK MODULE
			// ----------------------------------------------------------------

			String classname = pluginConfig.getString("/plugin[@name='" + plugin + "']");

			if (classname == null)
				throw new ParseException("Plugin " + plugin + " is undefined in config/plugin.xml");
			BenchmarkModule bench = ClassUtil.newInstance(classname,
					new Object[] { wrkld },
					new Class<?>[] { WorkloadConfiguration.class });
			Map<String, Object> initDebug = new ListOrderedMap<String, Object>();
			initDebug.put("Benchmark", String.format("%s {%s}", plugin.toUpperCase(), classname));
			initDebug.put("Configuration", configFile);
			initDebug.put("Type", wrkld.getDBType());
			initDebug.put("Driver", wrkld.getDBDriver());
			initDebug.put("URL", wrkld.getDBConnection());
			initDebug.put("Isolation", wrkld.getIsolationString());
			initDebug.put("Scale Factor", wrkld.getScaleFactor());
			initDebug.put("IDS Delay", wrkld.getDelay());
			LOG.info(SINGLE_LINE + "\n\n" + StringUtil.formatMaps(initDebug));
			LOG.info(SINGLE_LINE);

			// ----------------------------------------------------------------
			// LOAD TRANSACTION DESCRIPTIONS
			// ----------------------------------------------------------------
			int numTxnTypes = xmlConfig.configurationsAt("transactiontypes" + pluginTest + "/transactiontype").size();
			if (numTxnTypes == 0 && targetList.length == 1) {
				//if it is a single workload run, <transactiontypes /> w/o attribute is used
				pluginTest = "[not(@bench)]";
				numTxnTypes = xmlConfig.configurationsAt("transactiontypes" + pluginTest + "/transactiontype").size();
			}
			wrkld.setNumTxnTypes(numTxnTypes);

			List<TransactionType> ttypes = new ArrayList<TransactionType>();
			ttypes.add(TransactionType.INVALID);
			int txnIdOffset = lastTxnId;
			for (int i = 1; i < wrkld.getNumTxnTypes() + 1; i++) {
				String key = "transactiontypes" + pluginTest + "/transactiontype[" + i + "]";
				String txnName = xmlConfig.getString(key + "/name");

				// Get ID if specified; else increment from last one.
				int txnId = i + 1;
				if (xmlConfig.containsKey(key + "/id")) {
					txnId = xmlConfig.getInt(key + "/id");
				}

				TransactionType tmpType = bench.initTransactionType(txnName, txnId + txnIdOffset);

				// Keep a reference for filtering
				activeTXTypes.add(tmpType);

				// Add a ref for the active TTypes in this benchmark
				ttypes.add(tmpType);
				lastTxnId = i;
			} // FOR

			// Wrap the list of transactions and save them
			TransactionTypes tt = new TransactionTypes(ttypes);
			wrkld.setTransTypes(tt);
			LOG.debug("Using the following transaction types: " + tt);

			// Read in the groupings of transactions (if any) defined for this
			// benchmark
			HashMap<String,List<String>> groupings = new HashMap<String,List<String>>();
			int numGroupings = xmlConfig.configurationsAt("transactiontypes" + pluginTest + "/groupings/grouping").size();
			LOG.debug("Num groupings: " + numGroupings);
			for (int i = 1; i < numGroupings + 1; i++) {
				String key = "transactiontypes" + pluginTest + "/groupings/grouping[" + i + "]";

				// Get the name for the grouping and make sure it's valid.
				String groupingName = xmlConfig.getString(key + "/name").toLowerCase();
				if (!groupingName.matches("^[a-z]\\w*$")) {
					LOG.fatal(String.format("Grouping name \"%s\" is invalid."
							+ " Must begin with a letter and contain only"
							+ " alphanumeric characters.", groupingName));
					System.exit(-1);
				}
				else if (groupingName.equals("all")) {
					LOG.fatal("Grouping name \"all\" is reserved."
							+ " Please pick a different name.");
					System.exit(-1);
				}

				// Get the weights for this grouping and make sure that there
				// is an appropriate number of them.
				List<String> groupingWeights = xmlConfig.getList(key + "/weights");
				if (groupingWeights.size() != numTxnTypes) {
					LOG.fatal(String.format("Grouping \"%s\" has %d weights,"
							+ " but there are %d transactions in this"
							+ " benchmark.", groupingName,
							groupingWeights.size(), numTxnTypes));
					System.exit(-1);
				}

				LOG.debug("Creating grouping with name, weights: " + groupingName + ", " + groupingWeights);
				groupings.put(groupingName, groupingWeights);
			}

			// All benchmarks should also have an "all" grouping that gives
			// even weight to all transactions in the benchmark.
			List<String> weightAll = new ArrayList<String>();
			for (int i = 0; i < numTxnTypes; ++i)
				weightAll.add("1");
			groupings.put("all", weightAll);
			// Add all types of benchmarks
			benchList.add(bench);

			// ----------------------------------------------------------------
			// WORKLOAD CONFIGURATION
			// ----------------------------------------------------------------

			int size = xmlConfig.configurationsAt("/works/work").size();
			for (int i = 1; i < size + 1; i++) {
				SubnodeConfiguration work = xmlConfig.configurationAt("works/work[" + i + "]");
				//muhamad
				List<String> weight_strings;

				// use a workaround if there multiple workloads or single
				// attributed workload
				if (targetList.length > 1 || work.containsKey("weights[@bench]")) {
					String weightKey = work.getString("weights" + pluginTest).toLowerCase();
					if (groupings.containsKey(weightKey))
						weight_strings = groupings.get(weightKey);
					else
						weight_strings = get_weights(plugin, work);



				} else {
					//muhamad 
					String weightKey = work.getString("weights[not(@bench)]").toLowerCase();
					if (groupings.containsKey(weightKey))
						weight_strings = groupings.get(weightKey);
					else
						weight_strings = work.getList("weights[not(@bench)]");

				}
				int rate = 1;
				boolean rateLimited = true;
				boolean disabled = false;
				boolean serial = false;
				boolean timed = false;

				// can be "disabled", "unlimited" or a number
				String rate_string;
				rate_string = work.getString("rate[not(@bench)]", "");
				rate_string = work.getString("rate" + pluginTest, rate_string);
				if (rate_string.equals(RATE_DISABLED)) {
					disabled = true;
				} else if (rate_string.equals(RATE_UNLIMITED)) {
					rateLimited = false;
				} else if (rate_string.isEmpty()) {
					LOG.fatal(String.format("Please specify the rate for phase %d and workload %s", i, plugin));
					System.exit(-1);
				} else {
					try {
						rate = Integer.parseInt(rate_string);
						if (rate < 1) {
							LOG.fatal("Rate limit must be at least 1. Use unlimited or disabled values instead.");
							System.exit(-1);
						}
					} catch (NumberFormatException e) {
						LOG.fatal(String.format("Rate string must be '%s', '%s' or a number", RATE_DISABLED, RATE_UNLIMITED));
						System.exit(-1);
					}
				}



				Phase.Arrival arrival=Phase.Arrival.REGULAR;
				String arrive=work.getString("@arrival","regular");
				if(arrive.toUpperCase().equals("POISSON"))
					arrival=Phase.Arrival.POISSON;

				// We now have the option to run all queries exactly once in
				// a serial (rather than random) order.
				String serial_string;
				serial_string = work.getString("serial", "false");
				if (serial_string.equals("true")) {
					serial = true;
				}
				else if (serial_string.equals("false")) {
					serial = false;
				}
				else {
					LOG.fatal("Serial string should be either 'true' or 'false'.");
					System.exit(-1);
				}

				// We're not actually serial if we're running a script, so make
				// sure to suppress the serial flag in this case.
				serial = serial && (wrkld.getTraceReader() == null);

				// muhamad
				//				numActiveTerminals = work.getInt("active_terminals[not(@bench)]", terminals);
				//				numActiveTerminals = work.getInt("active_terminals" + pluginTest, numActiveTerminals);
				//				numActiveTerminals = work.getList("active_terminals[not(@bench)]", terminals);
				List<String> atList = work.getList("/active_terminals" );
				ArrayList<Integer> tempList = new ArrayList<Integer>();
				for(String range:atList){
					int start,end;
					if(range.contains("-")){
						start = Integer.parseInt(range.split("-")[0]);
						end = Integer.parseInt(range.split("-")[1]);
					}else{
						start = end = Integer.parseInt(range);
					}
					for(int it=start;it<=end;it++)
						tempList.add(it);
				}
				int[] activeTerminals = new int[tempList.size()];
				for(int it=0;it<tempList.size();it++)
					activeTerminals[it] = tempList.get(it);

				// If using serial, we should have only one terminal
				if (serial && activeTerminals.length != 1) {
					LOG.warn("Serial ordering is enabled, so # of active terminals is clamped to 1.");
					//					numActiveTerminals = 1;
				}
				if (activeTerminals.length> terminals) {
					LOG.error(String.format("Configuration error in work %d: " +
							"Number of active terminals is bigger than the total number of terminals",
							i));
					System.exit(-1);
				}

				List<String> matList = work.getList("/active_mterminals" );
				tempList = new ArrayList<Integer>();
				for(String range:matList){
					int start,end;
					if(range.contains("-")){
						start = Integer.parseInt(range.split("-")[0]);
						end = Integer.parseInt(range.split("-")[1]);
					}else{
						start = end = Integer.parseInt(range);
					}
					for(int it=start;it<=end;it++)
						tempList.add(it);
				}
				int[] activeMTerminals = new int[tempList.size()];
				for(int it=0;it<tempList.size();it++)
					activeMTerminals[it] = tempList.get(it);

				// If using serial, we should have only one terminal
				if (serial && activeMTerminals.length != 1) {
					LOG.warn("Serial ordering is enabled, so # of active terminals is clamped to 1.");
					//					numActiveTerminals = 1;
				}
				if (activeMTerminals.length> terminals) {
					LOG.error(String.format("Configuration error in work %d: " +
							"Number of active terminals is bigger than the total number of terminals",
							i));
					System.exit(-1);
				}


				List<String> workType = work.getList("/workers_work");
				ArrayList<ArrayList<Integer>> workerMmbrs = new ArrayList<ArrayList<Integer>>();
				for(String range:workType){
					tempList = new ArrayList<Integer>();
					int start,end;
					if(range.contains("-")){
						start = Integer.parseInt(range.split("-")[0]);
						end = Integer.parseInt(range.split("-")[1]);
						for(int it=start;it<=end;it++)
							tempList.add(it);
					}else if(range.isEmpty()){}
					else{
						for(String w:range.split(":"))
							tempList.add(Integer.parseInt(w));
					}
					workerMmbrs.add(tempList);
				}



				int time = work.getInt("/time", 0);
				timed = (time > 0);
				if (scriptRun) {
					LOG.info("Running a script; ignoring timer, serial, and weight settings.");
				}
				else if (!timed) {
					if (serial)
						LOG.info("Timer disabled for serial run; will execute"
								+ " all queries exactly once.");
					else {
						LOG.fatal("Must provide positive time bound for"
								+ " non-serial executions. Either provide"
								+ " a valid time or enable serial mode.");
						System.exit(-1);
					}
				}
				else if (serial)
					LOG.info("Timer enabled for serial run; will run queries"
							+ " serially in a loop until the timer expires.");
				//				wrkld.addWork(time,
				//						rate,
				//						weight_strings,
				//						rateLimited,
				//						disabled,
				//						serial,
				//						timed,
				//						activeTerminals,
				//						arrival,
				//						malProp);

				wrkld.addWork(time,
						rate,
						weight_strings,
						rateLimited,
						disabled,
						serial,
						timed,
						activeTerminals,
						arrival,
						workerMmbrs);


			} // FOR

			// CHECKING INPUT PHASES
			int j = 0;
			for (Phase p : wrkld.getAllPhases()) {
				j++;
				//muhamad
				if (p.getWeightCount()!= wrkld.getNumTxnTypes()) {
					LOG.fatal(String.format("Configuration files is inconsistent, phase %d contains %d weights but you defined %d transaction types",
							j, p.getWeightCount(), wrkld.getNumTxnTypes()));
					if (p.isSerial()) {
						LOG.fatal("However, note that since this a serial phase, the weights are irrelevant (but still must be included---sorry).");
					}
					System.exit(-1);
				}
			} // FOR

			// Generate the dialect map
			wrkld.init();

			assert (wrkld.getNumTxnTypes() >= 0);
			assert (xmlConfig != null);
		}
		assert(benchList.isEmpty() == false);
		assert(benchList.get(0) != null);

		


		// Execute a Script
		//		if (argsLine.hasOption("runscript")) {
			for (BenchmarkModule benchmark : benchList) {
				String script = argsLine.getOptionValue("ts");
				LOG.info("Running a SQL script: "+script);
				runScript(benchmark, script,arrivalTraceFile,ibAssignmentFile,hotspot);
			}






	}

	/* buggy piece of shit of Java XPath implementation made me do it 
       replaces good old [@bench="{plugin_name}", which doesn't work in Java XPath with lists
	 */
	private static List<String> get_weights(String plugin, SubnodeConfiguration work) {

		List<String> weight_strings = new LinkedList<String>();
		@SuppressWarnings("unchecked")
		List<SubnodeConfiguration> weights = work.configurationsAt("weights");
		boolean weights_started = false;

		for (SubnodeConfiguration weight : weights) {

			// stop if second attributed node encountered
			if (weights_started && weight.getRootNode().getAttributeCount() > 0) {
				break;
			}
			//start adding node values, if node with attribute equal to current plugin encountered
			if (weight.getRootNode().getAttributeCount() > 0 && weight.getRootNode().getAttribute(0).getValue().equals(plugin)) {
				weights_started = true;
			}
			if (weights_started) {
				weight_strings.add(weight.getString(""));
			}

		}
		return weight_strings;
	}

	private static void runScript(BenchmarkModule bench, String script,String arrivalTraceFile,String ib,String hotspot) {
		LOG.debug(String.format("Running %s", script));
		bench.runScript(script,arrivalTraceFile,ib,hotspot,false,false,true);
	}




	private static void printUsage(Options options) {
		HelpFormatter hlpfrmt = new HelpFormatter();
		hlpfrmt.printHelp("oltpbenchmark", options);
	}

	/**
	 * Returns true if the given key is in the CommandLine object and is set to
	 * true.
	 * 
	 * @param argsLine
	 * @param key
	 * @return
	 */
	

}
