package com.oltpbenchmark.benchmarks.smallworldbank;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SWBankUtil {
    

    private static final String getAcctIdMaxSql = "select max(chk_id) from checking";
    private static final String getCustIdMaxSql = "select max(chk_id) from checking";
    
    public static long getCustIdMax(Connection conn) throws SQLException {
        PreparedStatement ps = conn.prepareStatement(getCustIdMaxSql);
        ResultSet rs = ps.executeQuery();
        if (!rs.next()){
            throw new RuntimeException("Could not get customer id max");            
        }
        long res = rs.getLong(1);
        rs.close();
        return res;
    }

    public static long getAcctIdMax(Connection conn) throws SQLException {
        PreparedStatement ps = conn.prepareStatement(getAcctIdMaxSql);
        ResultSet rs = ps.executeQuery();
        if (!rs.next()){
            throw new RuntimeException("Could not get account id max");            
        }
        long res = rs.getLong(1);
        rs.close();
        return res;
    }
}
