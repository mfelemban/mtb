/*
 * Slightly modified version of the com.ibatis.common.jdbc.ScriptRunner class
 * from the iBATIS Apache project. Only removed dependency on Resource class
 * and a constructor 
 */
/*
 *  Copyright 2004 Clinton Begin
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.oltpbenchmark.benchmarks.smallworldbank;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Reader;
import java.net.URL;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import aims.framework.Vacuumer;
import aims.ooo.BRSetNorifier;
import aims.syncooo.ActiveTransaction;
import aims.syncooo.Query;
import aims.util.FairLock;

import com.oltpbenchmark.api.BenchmarkModule;
//import aims.freestyle.Executor;

/**
 * Tool to run database scripts
 * http://pastebin.com/f10584951
 */
public class SWBankScriptRunnerSynco {
	private static final Logger LOG = Logger.getLogger(SWBankScriptRunnerSynco.class);

	private static final String DEFAULT_DELIMITER = ";";

	final private Connection connection;

	private boolean autoCommit;

	final private BenchmarkModule benchmarkModule;
	URL resource;
	private String arrivalTraceFile;
	private String ibAssignment;
	private String hotspot; 

	//	ArrayList<Long> samples = new ArrayList<Long>();
	List<String> samples;

	private String delimiter = DEFAULT_DELIMITER;
	private boolean fullLineDelimiter = false;

	/**
	 * Default constructor
	 */
	public SWBankScriptRunnerSynco(Connection connection, boolean autoCommit,
			boolean stopOnError, BenchmarkModule benchmarkModule,String arrivalTraceFile,String ibAssignment,String hotspot) {
		this.connection = connection;
		//		this.autoCommit = autoCommit;
		this.autoCommit = false;
		this.benchmarkModule = benchmarkModule;
		this.arrivalTraceFile = arrivalTraceFile;
		this.ibAssignment = ibAssignment;
		this.hotspot = hotspot;
	}

	public void setDelimiter(String delimiter, boolean fullLineDelimiter) {
		this.delimiter = delimiter;
		this.fullLineDelimiter = fullLineDelimiter;
	}
	/**
	 * Runs an SQL script (read in using the Reader parameter)
	 * 
	 * @param reader
	 *            - the source of the script
	 */
	public void runScript(URL resource){
		samples = Collections.synchronizedList(new ArrayList<String>());
		this.resource = resource;
		try{
			Reader reader = new InputStreamReader(resource.openStream());
			boolean originalAutoCommit = connection.getAutoCommit();
			try {

				if (originalAutoCommit != this.autoCommit) {
					connection.setAutoCommit(this.autoCommit);
				}
				Utils.emptyDB(benchmarkModule);
			}catch(Exception e){
				System.out.println("Empty DB is had paroblems");
			}
			//					setUpSolution(benchmarkModule,"s1_1.5_s2_1.0_3_efa");
			/*
			 * Change execution style here 
			 */
			runScriptOutOrder(connection,reader,ibAssignment,hotspot);
			connection.setAutoCommit(originalAutoCommit);
		} 
		catch (Exception e) {
			throw new RuntimeException("Error running script.  Cause: " + e, e);
		}

	}

	private ArrayList<ArrayList<Query>> readScript102( Reader reader) throws IOException{
		StringBuffer command = null;
		ArrayList<ArrayList<Query>> transactions = new ArrayList<ArrayList<Query>>();
		LineNumberReader lineReader = new LineNumberReader(reader);
		String line = null;
		//Added

		while ((line = lineReader.readLine()) != null) {
			//			LOG.info(line);
			ArrayList<Query> currTx =new ArrayList<Query>();
			if (LOG.isDebugEnabled()) LOG.debug(line);
			if (command == null) {
				command = new StringBuffer();
			}
			String trimmedLine = line.trim();
			if (trimmedLine.startsWith("--")) {
				continue;
				//					LOG.debug(trimmedLine);
			} else if (trimmedLine.length() < 1
					|| trimmedLine.startsWith("--") || trimmedLine.startsWith("#") || trimmedLine.startsWith("//")) {
				// Do nothing
				continue;
			} else if (!fullLineDelimiter
					&& trimmedLine.endsWith(getDelimiter())
					|| fullLineDelimiter
					&& trimmedLine.equals(getDelimiter())) {
				command.append(line.substring(0, line
						.lastIndexOf(getDelimiter())));
				command.append(" ");
				//					Statement statement = conn.createStatement();
			}
			final String query = new String(command);
			int type = Integer.parseInt(query.split(",")[0]);
			Integer txid = Integer.parseInt(query.split(",")[1]);

			Timestamp ts = Timestamp.valueOf((query.split(",")[query.split(",").length-2]));
			String lastOne = query.split(",")[query.split(",").length-1];
			int mal = Integer.parseInt(lastOne.trim());
			if(type==102){
				currTx.add(new Query(type,txid,ts));
				transactions.add(currTx);
			}else{
				String[] srcIDsString = query.split(",")[2].split(":");
				long[] srcIDs = new long[srcIDsString.length];
				int i=0;
				for(String temp : srcIDsString) srcIDs[i++] = Long.parseLong(temp);
				String[] destIDsString = query.split(",")[3].split(":");
				long[] destIDs = new long[destIDsString.length];
				i=0;
				for(String temp : destIDsString) destIDs[i++] = Long.parseLong(temp);
				double amount = Double.parseDouble(query.split(",")[4]);
				i=0;
				currTx.add(new Query(type,txid,srcIDs,destIDs,amount,ts,mal));
				transactions.add(currTx);
			}
			command = null;
		}


		return transactions;
	}


	private Map<Integer,Set<Integer>> readSolFile(String solFile) throws IOException{
		boolean one = false;
		if(solFile.equals("one"))
			one = true;
		Map<Integer,Set<Integer>> sol = new HashMap<Integer,Set<Integer>>();
		BufferedReader br = new BufferedReader(new FileReader(solFile));
		String line = "";
		while((line = br.readLine()) != null){
			String[] tkns = line.split(",");
			int t = Integer.parseInt(tkns[0]);
			int tx = Integer.parseInt(tkns[2]);
			Set<Integer> txs=null;
			if(!sol.containsKey(t)){
				txs = new HashSet<Integer>();
			}else{
				txs = sol.get(t);
			}
			txs.add(tx);
			if(!one)
				sol.put(t, txs);
		}

		br.close();
		return sol;
	}

	private Map<Integer,Integer> readTxSolFile(String solFile) throws IOException{
		Map<Integer,Integer> txsol = new HashMap<Integer,Integer>();
		BufferedReader br = new BufferedReader(new FileReader(solFile));
		String line = "";
		while((line = br.readLine()) != null){
			String[] tkns = line.split(",");
			int t = Integer.parseInt(tkns[0]);
			int tx = Integer.parseInt(tkns[1]);
			txsol.put(t, tx);
		}

		br.close();
		return txsol;
	}

	private Set<Long> readHotspotFile(String hotspotFile) throws NumberFormatException, IOException{
		BufferedReader br = new BufferedReader(new FileReader(hotspotFile));
		Set<Long> hs = new HashSet<Long>();
		String line = "";
		while((line=br.readLine())!=null){
			hs.add(Long.parseLong(line));
		}
		br.close();
		return hs;
	}
	private void runScriptOutOrder(Connection connect, Reader reader,String ibFile,String hotspot) {
		try{
			ArrayList<ArrayList<Query>> transactions = readScript102(reader);
			String solFile = ibFile;
			LOG.info(solFile);
			String txsolFile = ibFile.replace("sol","txsol");
			LOG.info(txsolFile);

			LOG.info(hotspot);
			Set<Long> hotspots = readHotspotFile(hotspot);
			if(hotspots.size()>0)
				Utils.uploadHotspotTuples(benchmarkModule,hotspots);

			Map<Integer,Set<Integer>> sol = readSolFile(solFile);
			Map<Integer,Integer> txsol = readTxSolFile(txsolFile);
			//			LOG.info(txsol.toString());

			int k = (int) txsol.values().stream().distinct().count();



			LOG.info("Number of IBs " + k);
			LOG.info("Number of transactions to be executed "  + transactions.size());

			Connection newConn = benchmarkModule.makeConnection();
			Connection vacConn = benchmarkModule.makeConnection();


			ConcurrentMap<Long,Integer> BO = getBoundaryObjects(sol);


			Iterator<ArrayList<Query>> iter = transactions.iterator();
			Map<Integer,Integer> txidMap = new ConcurrentHashMap<Integer,Integer>();

			AtomicBoolean idAlert = new AtomicBoolean();

			// used to synchronize recovery transactions among IBs
			Boolean[] ibTxStatus = new Boolean[k];
			AtomicBoolean waiting = new AtomicBoolean();
			AtomicInteger runningTx = new AtomicInteger();
			AtomicBoolean recRunning = new AtomicBoolean();
			FairLock fl = new FairLock();
			for(int i=0;i<k;i++){
				ibTxStatus[i] = false;
			}
			Vacuumer vac = new Vacuumer(vacConn,benchmarkModule);
			Thread vacThread = new Thread(vac);
			vacThread.start();

			BufferedReader br = new BufferedReader(new FileReader(arrivalTraceFile));
			ArrayList<Double> arrivalDelays = new ArrayList<Double>();
			int i=0;
			String line="";
			while((line=br.readLine())!=null){
				arrivalDelays.add(Double.parseDouble(line.split(":")[0]));
			}
			br.close();
			i = 0;

			// More threads reduces the average response time in general due to lock contention on the data table
			ExecutorService pool = Executors.newFixedThreadPool(8);
			ExecutorService malPool = Executors.newFixedThreadPool(16);


			// Execute a thread to periodically notify BRSetListner to avoid deadlocks
			// Happens when rate is too fast
			AtomicBoolean done = new AtomicBoolean();
			done.set(false);
			
			ExecutorService notifier = Executors.newSingleThreadExecutor();
			notifier.execute(new Runnable(){

				@Override
				public void run() {
					// TODO Auto-generated method stub
					while(!done.get()){
						BRSetNorifier notifier = new BRSetNorifier(newConn);
//						LOG.info("Notifying");
						notifier.start();
						try {
							Thread.currentThread().sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				
			});
			
			long startTime = System.currentTimeMillis();
			AtomicInteger succCounter = new AtomicInteger();
			Integer malPrev = -1;
			while(iter.hasNext()){
				final ArrayList<Query> tx = iter.next();
				Query q=null;
				q = tx.get(0);


				long wait = (long) arrivalDelays.get(i++).doubleValue();
				Thread.sleep((long)wait);
				int ib = txsol.get(q.getTxid());
				ActiveTransaction at = new ActiveTransaction(tx,ib,System.currentTimeMillis());
				/*
				 * Executer takes the following thread managements objects
				 * 1- txidmap: maps transactionID to PG transactionsID
				 * 2- ibTXStatus: gives the recovery status for the IB
				 * 3- runningTx: returns the number of actively running transactions, used during the blockein window
				 * 4- waiting: a flag to check if there are recovery transactions waiting for running transaction (to avoid illegla monitoring exception)
				 * 5-   
				 */
				pool.execute(new aims.syncooo.TxExecutor(at, benchmarkModule,txidMap,ibTxStatus,runningTx,waiting,succCounter,newConn,BO,idAlert));

				if(q.isMalicious()){
					malPrev = q.getTxid();
					malPool.execute(new aims.syncooo.IDS(malPrev,benchmarkModule,newConn,ibTxStatus,runningTx,waiting,fl,malPrev,recRunning,txidMap,BO,ib,idAlert));
				}
			}
			pool.shutdown();
			pool.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
			

			malPool.shutdown();
			malPool.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
			
			done.set(true);
			notifier.shutdown();
			notifier.awaitTermination(2, TimeUnit.MINUTES);

			long endTime = System.currentTimeMillis();
			LOG.info("Tx counter : " + succCounter);
			LOG.info("Throughput " + (double) ((double)succCounter.intValue()/((double)(endTime - startTime)/1000.0)));

			vac.terminate();
			vacThread.join();
			long delay = benchmarkModule.getWorkloadConfiguration().getXmlConfig().getLong("delay");

			Thread.sleep((long) (delay*1.5));
			newConn.close();
			vacConn.close();

			LOG.info("Collecting results...");

			String txFile = resource.toString();;
			txFile = txFile.substring(txFile.indexOf("workload"));
			String rawFile = txFile+".raw";
			//			String aveFile = resource.toString();
			//			aveFile = aveFile.substring(aveFile.indexOf("workload"),aveFile.lastIndexOf("."))+".ave";
			String globalSum = txFile+".ginfo";
			String conciseSum = txFile+".info";
			//			String globalHist = txFile+".ghist";
			//			String ibHist = txFile+".ibhist";
			String ibStat = txFile+".ibstat";
			//			String affTimes = txFile+".afti";
			ResultsCollector.printLatencies(samples,rawFile);
			//			printAvailability(pp,samples);
			ResultsCollector.collectGlobalSummary(k,benchmarkModule,globalSum);
			ResultsCollector.collectConciseSummary(k,benchmarkModule,conciseSum);
			ResultsCollector.collectIBStats(k,benchmarkModule,ibStat);
			//			ResultsCollector.collectGlobalHist(benchmarkModule,globalHist);
			//			ResultsCollector.collectIBHist(k,benchmarkModule,ibHist);
			//			ResultsCollector.getAffectedTimes(benchmarkModule,affTimes);
		}catch(Exception e){
			e.printStackTrace();
		}
		LOG.info("****************************************");
		LOG.info("Done");

	}

	public ConcurrentMap<Long,Integer> getBoundaryObjects(Map<Integer,Set<Integer>> sol){
		ConcurrentMap<Long,Integer> BO = new ConcurrentHashMap<Long,Integer>();
		for(Map.Entry<Integer, Set<Integer>> entry:sol.entrySet()){
			if(entry.getValue().size()>1)
				BO.put((long)entry.getKey(),-1);
		}
		return BO;
	}

	public static int nextPoisson(double lambda) {
		double L = Math.exp(-lambda);
		double p = 1.0;
		int k = 0;

		do {
			k++;
			p *= Math.random();
		} while (p > L);

		return k - 1;
	}



	//	public void printLatencies(PrintStream file,ArrayList<Long> latencies){


	private String getDelimiter() {
		return delimiter;
	}





}
