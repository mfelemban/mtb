/*
 * Slightly modified version of the com.ibatis.common.jdbc.ScriptRunner class
 * from the iBATIS Apache project. Only removed dependency on Resource class
 * and a constructor 
 */
/*
 *  Copyright 2004 Clinton Begin
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.oltpbenchmark.benchmarks.smallworldbank;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Reader;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import aims.framework.ActiveTransaction;
import aims.framework.Vacuumer;
import aims.util.FairLock;

import com.oltpbenchmark.api.BenchmarkModule;
import com.oltpbenchmark.util.Query;
import com.oltpbenchmark.util.ScriptRunner;
//import aims.freestyle.Executor;

/**
 * Tool to run database scripts
 * http://pastebin.com/f10584951
 */
public class SWBankScriptRunner {
	private static final Logger LOG = Logger.getLogger(ScriptRunner.class);

	private static final String DEFAULT_DELIMITER = ";";

	final private Connection connection;

	private boolean autoCommit;

	final private BenchmarkModule benchmarkModule;
	URL resource;
	String arrivalTraceFile;

	//	ArrayList<Long> samples = new ArrayList<Long>();
	List<String> samples;

	private String delimiter = DEFAULT_DELIMITER;
	private boolean fullLineDelimiter = false;

	/**
	 * Default constructor
	 */
	public SWBankScriptRunner(Connection connection, boolean autoCommit,
			boolean stopOnError, BenchmarkModule benchmarkModule,String arrivalTraceFile) {
		this.connection = connection;
		//		this.autoCommit = autoCommit;
		this.autoCommit = false;
		this.benchmarkModule = benchmarkModule;
		this.arrivalTraceFile = arrivalTraceFile;
	}

	public void setDelimiter(String delimiter, boolean fullLineDelimiter) {
		this.delimiter = delimiter;
		this.fullLineDelimiter = fullLineDelimiter;
	}
	/**
	 * Runs an SQL script (read in using the Reader parameter)
	 * 
	 * @param reader
	 *            - the source of the script
	 */
	public void runScript(URL resource){
		samples = Collections.synchronizedList(new ArrayList<String>());
		this.resource = resource;
		try{
			Reader reader = new InputStreamReader(resource.openStream());
			boolean originalAutoCommit = connection.getAutoCommit();
			try {

				if (originalAutoCommit != this.autoCommit) {
					connection.setAutoCommit(this.autoCommit);
				}
				Utils.emptyDB(benchmarkModule);
			}catch(Exception e){
				System.out.println("Empty DB is had paroblems");
			}
			//					setUpSolution(benchmarkModule,"s1_1.5_s2_1.0_3_efa");
			/*
			 * Change execution style here 
			 */
			//					runScriptInOrder(connection, reader);
			//				runScriptOutOrder(connection, reader);
			//				runScriptFreeStyle(connection,reader);
			runScriptOutOrder(connection,reader);
			connection.setAutoCommit(originalAutoCommit);
		} 
		catch (Exception e) {
			throw new RuntimeException("Error running script.  Cause: " + e, e);
		}

	}

		private ArrayList<ArrayList<Query>> readScript101( Reader reader) throws IOException{
			StringBuffer command = null;
			ArrayList<ArrayList<Query>> transactions = new ArrayList<ArrayList<Query>>();
			LineNumberReader lineReader = new LineNumberReader(reader);
			String line = null;
			long prevTx = -1;
			//Added
			ArrayList<Query> currTx =null;
			boolean next = true;
			while ((line = lineReader.readLine()) != null) {
				//			LOG.info(line);
				next = true;
				if (LOG.isDebugEnabled()) LOG.debug(line);
				if (command == null) {
					command = new StringBuffer();
				}
				String trimmedLine = line.trim();
				if (trimmedLine.startsWith("--")) {
					continue;
					//					LOG.debug(trimmedLine);
				} else if (trimmedLine.length() < 1
						|| trimmedLine.startsWith("//")) {
					// Do nothing
				} else if (trimmedLine.length() < 1
						|| trimmedLine.startsWith("--")) {
					// Do nothing
					continue;
				} else if (!fullLineDelimiter
						&& trimmedLine.endsWith(getDelimiter())
						|| fullLineDelimiter
						&& trimmedLine.equals(getDelimiter())) {
					command.append(line.substring(0, line
							.lastIndexOf(getDelimiter())));
					command.append(" ");
					//					Statement statement = conn.createStatement();
				}
				final String query = new String(command);
				int type = Integer.parseInt(query.split(",")[0]);
				long txid = Long.parseLong(query.split(",")[1]);
				// Added
				//			if(prevTx!=txid || type==101){
				if(prevTx!=txid){
					if(currTx!=null)
						transactions.add(currTx);
					prevTx = txid;
					currTx = new ArrayList<Query>();
				}
				Timestamp ts = Timestamp.valueOf((query.split(",")[query.split(",").length-1]));
				//			if(type==101 || type==900){
				if(type==900 ){
					currTx.add(new Query(type,txid,null,null,0,ts));
					transactions.add(currTx);
					currTx=null;
					next = false;
				}else{
					String[] srcIDsString = query.split(",")[2].split(":");
					long[] srcIDs = new long[srcIDsString.length];
					int i=0;
					for(String temp : srcIDsString) srcIDs[i++] = Long.parseLong(temp);
					String[] destIDsString = query.split(",")[3].split(":");
					long[] destIDs = new long[destIDsString.length];
					i=0;
					for(String temp : destIDsString) destIDs[i++] = Long.parseLong(temp);
					double amount = Double.parseDouble(query.split(",")[4]);
					i=0;
					currTx.add(new Query(type,txid,srcIDs,destIDs,amount,ts));
				}
				command = null;
			}
			if(next)
				transactions.add(currTx);
			currTx = new ArrayList<Query>();
			currTx.add(new Query(900));
			transactions.add(currTx);
	
			return transactions;
		}

	private ArrayList<ArrayList<Query>> readScript102( Reader reader) throws IOException{
		StringBuffer command = null;
		ArrayList<ArrayList<Query>> transactions = new ArrayList<ArrayList<Query>>();
		LineNumberReader lineReader = new LineNumberReader(reader);
		String line = null;
		//Added

		while ((line = lineReader.readLine()) != null) {
			//			LOG.info(line);
			ArrayList<Query> currTx =new ArrayList<Query>();
			if (LOG.isDebugEnabled()) LOG.debug(line);
			if (command == null) {
				command = new StringBuffer();
			}
			String trimmedLine = line.trim();
			if (trimmedLine.startsWith("--")) {
				continue;
				//					LOG.debug(trimmedLine);
			} else if (trimmedLine.length() < 1
					|| trimmedLine.startsWith("--") || trimmedLine.startsWith("#") || trimmedLine.startsWith("//")) {
				// Do nothing
				continue;
			} else if (!fullLineDelimiter
					&& trimmedLine.endsWith(getDelimiter())
					|| fullLineDelimiter
					&& trimmedLine.equals(getDelimiter())) {
				command.append(line.substring(0, line
						.lastIndexOf(getDelimiter())));
				command.append(" ");
				//					Statement statement = conn.createStatement();
			}
			final String query = new String(command);
			int type = Integer.parseInt(query.split(",")[0]);
			long txid = Long.parseLong(query.split(",")[1]);

			Timestamp ts = Timestamp.valueOf((query.split(",")[query.split(",").length-2]));
			String lastOne = query.split(",")[query.split(",").length-1];
			int mal = Integer.parseInt(lastOne.trim());
			if(type==102){
				currTx.add(new Query(type,txid,ts));
				transactions.add(currTx);
			}else{
				String[] srcIDsString = query.split(",")[2].split(":");
				long[] srcIDs = new long[srcIDsString.length];
				int i=0;
				for(String temp : srcIDsString) srcIDs[i++] = Long.parseLong(temp);
				String[] destIDsString = query.split(",")[3].split(":");
				long[] destIDs = new long[destIDsString.length];
				i=0;
				for(String temp : destIDsString) destIDs[i++] = Long.parseLong(temp);
				double amount = Double.parseDouble(query.split(",")[4]);
				i=0;
				currTx.add(new Query(type,txid,srcIDs,destIDs,amount,ts,mal));
				transactions.add(currTx);
			}
			command = null;
		}


		return transactions;
	}



	public void runScript(Connection conn,Reader reader, int mode, double rate) throws Exception{
		runScriptOutOrder(conn,reader);
	}

	/**
	 * Runs an SQL script (read in using the Reader parameter) using the
	 * connection passed in
	 * 
	 * @param conn
	 *            - the connection to use for the script
	 * @param reader
	 *            - the source of the script
	 * @throws SQLException
	 *             if any SQL errors occur
	 * @throws IOException
	 *             if there is an error reading from the Reader
	 * @throws InterruptedException 
	 */



	private void runScriptOutOrder(Connection connect, Reader reader) {
		try{
			ArrayList<ArrayList<Query>> transactions = readScript102(reader);

			Connection boConn = benchmarkModule.makeConnection();
			Connection newConn = benchmarkModule.makeConnection();
			Connection vacConn = benchmarkModule.makeConnection();

			PreparedStatement ps = boConn.prepareStatement("select count(distinct ib) from transactions_ib;");
			ResultSet rs = ps.executeQuery();
			rs.next();
			int k = rs.getInt(1);
			LOG.info("Number of IBs " + k);
			LOG.info("Number of transactions to be executed "  + transactions.size());
			
			ConcurrentMap<Long,Long> BO = getBoundaryObjects(boConn);
			
			
			
			Iterator<ArrayList<Query>> iter = transactions.iterator();
			Map<Long,Long> txidMap = new ConcurrentHashMap<Long,Long>();


			// used to synchronize recovery transactions among IBs
			Boolean[] ibTxStatus = new Boolean[k];
			AtomicBoolean waiting = new AtomicBoolean();
			AtomicInteger runningTx = new AtomicInteger();
			AtomicBoolean recRunning = new AtomicBoolean();
			FairLock fl = new FairLock();
			for(int i=0;i<k;i++){
				ibTxStatus[i] = false;
			}
			Vacuumer vac = new Vacuumer(vacConn,benchmarkModule);
			Thread vacThread = new Thread(vac);
			vacThread.start();

			BufferedReader br = new BufferedReader(new FileReader(arrivalTraceFile));
			ArrayList<Double> arrivalDelays = new ArrayList<Double>();
			int i=0;
			String line="";
			while((line=br.readLine())!=null){
				arrivalDelays.add(Double.parseDouble(line.split(":")[0]));
			}
			br.close();
			i = 0;

			// More threads reduces the average response time in general due to lock contention on the data table
			ExecutorService pool = Executors.newFixedThreadPool(10);
			ExecutorService malPool = Executors.newFixedThreadPool(20);


			long startTime = System.currentTimeMillis();
			AtomicInteger succCounter = new AtomicInteger();
			Boolean blkdBO = false;
			long prev = -1;
			long malPrev = -1;
			int count = 0;
			while(iter.hasNext()){
				final ArrayList<Query> tx = iter.next();
				Query q=null;
				q = tx.get(0);


				if(q.getType()!=101 || q.getType()!=102){
					long wait = (long) arrivalDelays.get(i++).doubleValue();
//					LOG.info(Thread.currentThread().getId() + " sleeping for " + wait);
					Thread.sleep((long)wait);
				}


//				if(q.getType()==101){		
//					malPrev = q.getTxid();
//					malPool.execute(new OOMaliciousTx( q.getTxid(),k,benchmarkModule,txidMap,prev,boConn,newConn,q,ibTxStatus,runningTx,waiting,blkdBO,fl,recRunning));
//				}else if(q.getType()==102){
//					malPrev = q.getTxid();
//					//					LOG.info("Mal tx" + malPrev);
//					//					malPool.execute(new OOMaliciousTx( q.getTxid(),k,benchmarkModule,txidMap,prev,boConn,newConn,q,ibTxStatus,runningTx,waiting,blkdBO,fl,recRunning));
//					malPool.execute(new aims.ooo.IDMessageSender(malPrev,benchmarkModule,boConn,newConn,ibTxStatus,runningTx,waiting,blkdBO,fl,malPrev,recRunning,txidMap,BO));
//				}else{
//					//					// Change it to have boundary flag
//					ActiveTransaction at = new ActiveTransaction(tx,System.currentTimeMillis());
//					pool.execute(new aims.ooo.OutOfOrderExecutor(at, benchmarkModule,txidMap,malPrev,samples,ibTxStatus,runningTx,waiting,succCounter,blkdBO,boConn,newConn,BO));
//				}
				
				ActiveTransaction at = new ActiveTransaction(tx,System.currentTimeMillis());
				pool.execute(new aims.ooo.OutOfOrderExecutor(at, benchmarkModule,txidMap,malPrev,samples,ibTxStatus,runningTx,waiting,succCounter,blkdBO,boConn,newConn,BO));
				
				if(q.isMalicious()){
					malPrev = q.getTxid();
//					LOG.info(q.getTxid() + " is malicious");
					malPool.execute(new aims.ooo.IDMessageSender(malPrev,benchmarkModule,boConn,newConn,ibTxStatus,runningTx,waiting,blkdBO,fl,malPrev,recRunning,txidMap,BO));
				}
				prev = q.getTxid();
			}
			pool.shutdown();
			pool.awaitTermination(2, TimeUnit.MINUTES);

			malPool.shutdown();
			malPool.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);


			long endTime = System.currentTimeMillis();
			LOG.info("Tx counter : " + succCounter);
			LOG.info("Throughput " + (double) ((double)succCounter.intValue()/((double)(endTime - startTime)/1000.0)));

			vac.terminate();
			vacThread.join();
			long delay = benchmarkModule.getWorkloadConfiguration().getXmlConfig().getLong("mddelay");

			Thread.sleep((long) (delay*1.5));
			boConn.close();
			newConn.close();
			vacConn.close();

			LOG.info("Collecting results...");

			String txFile = resource.toString();;
			txFile = txFile.substring(txFile.indexOf("workload"));
			String rawFile = txFile+".raw";
			//			String aveFile = resource.toString();
			//			aveFile = aveFile.substring(aveFile.indexOf("workload"),aveFile.lastIndexOf("."))+".ave";
			String globalSum = txFile+".info";
			//			String globalHist = txFile+".ghist";
			//			String ibHist = txFile+".ibhist";
			String ibStat = txFile+".ibstat";
			//			String affTimes = txFile+".afti";
			ResultsCollector.printLatencies(samples,rawFile);
			//			printAvailability(pp,samples);
			ResultsCollector.collectGlobalSummary(k,benchmarkModule,globalSum);
			ResultsCollector.collectIBStats(k,benchmarkModule,ibStat);
			//			ResultsCollector.collectGlobalHist(benchmarkModule,globalHist);
			//			ResultsCollector.collectIBHist(k,benchmarkModule,ibHist);
			//			ResultsCollector.getAffectedTimes(benchmarkModule,affTimes);
		}catch(Exception e){
			e.printStackTrace();
		}
		LOG.info("****************************************");
		LOG.info("Done");

	}
	
	public ConcurrentMap<Long,Long> getBoundaryObjects(Connection boConn){
		ConcurrentMap<Long,Long> BO = new ConcurrentHashMap<Long,Long>();
		PreparedStatement ps;
		try {
			ps = boConn.prepareStatement("select * from boundary_objects;");
			ResultSet rs= ps.executeQuery();
			while(rs.next()){
				long oid = rs.getInt(1);
				BO.put(oid, (long)-1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LOG.info(BO.size() + " boundary objects");
		return BO;
	}

	public static int nextPoisson(double lambda) {
		double L = Math.exp(-lambda);
		double p = 1.0;
		int k = 0;

		do {
			k++;
			p *= Math.random();
		} while (p > L);

		return k - 1;
	}



	//	public void printLatencies(PrintStream file,ArrayList<Long> latencies){


	private String getDelimiter() {
		return delimiter;
	}





}
