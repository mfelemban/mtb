/******************************************************************************
 *  Copyright 2016 by OLTPBenchmark Project  
 *  
 *  Author: Thamir Qadah                                 *
 *                                                                            *
 *  Licensed under the Apache License, Version 2.0 (the "License");           *
 *  you may not use this file except in compliance with the License.          *
 *  You may obtain a copy of the License at                                   *
 *                                                                            *
 *    http://www.apache.org/licenses/LICENSE-2.0                              *
 *                                                                            *
 *  Unless required by applicable law or agreed to in writing, software       *
 *  distributed under the License is distributed on an "AS IS" BASIS,         *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *
 *  See the License for the specific language governing permissions and       *
 *  limitations under the License.                                            *
 ******************************************************************************/

package com.oltpbenchmark.benchmarks.smallworldbank;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import com.oltpbenchmark.WorkloadConfiguration;
import com.oltpbenchmark.api.BenchmarkModule;
import com.oltpbenchmark.api.Loader;
import com.oltpbenchmark.api.TransactionType;
import com.oltpbenchmark.api.Worker;
import com.oltpbenchmark.benchmarks.smallworldbank.procedures.Balance;

public class SWBankBenchmark extends BenchmarkModule {

	private static final Logger LOG = Logger.getLogger(SWBankBenchmark.class);
//    Stack<TransactionType> profile;
//	Stack<Long> txidNumbers;
	List<TransactionType> profile;
	List<Timestamp> times;
	List<Long> txidNumbers;
	Timestamp lastTime;
	boolean injct = true;
	
	
	// Muhamad
	public SWBankBenchmark(WorkloadConfiguration workConf) {
		super("swbank", workConf, true);
//        this.profile = new Stack<TransactionType>();
//		this.txidNumbers = new Stack<Long>();
		profile =  Collections.synchronizedList(new ArrayList<TransactionType>());
		txidNumbers =  Collections.synchronizedList(new ArrayList<Long>());
		times = Collections.synchronizedList(new ArrayList<Timestamp>());
		lastTime = new Timestamp(new java.util.Date().getTime());
		// TODO Auto-generated constructor stub        
	}
	
//	  private void addType(TransactionType txnType, Long txid){
//	    	profile.push(txnType);
//	    	txidNumbers.push(txid);
//	    }
//	    
//	    private Stack<TransactionType> getTypeStack(){
//	    	return profile;
//	    }
//	    
//	    private Stack<Long> getTxidStack(){
//	    	return txidNumbers;
//	    }

	
	

	@Override
	protected List<Worker> makeWorkersImpl(boolean verbose) throws IOException {
		LOG.info("Creating workers");
		
		List<Worker> workers = new ArrayList<Worker>();
		for (int i = 0; i < workConf.getTerminals(); ++i) {
			workers.add(new SWBankWorker(this, i));
		}
		return workers;
	}

	@Override
	protected Loader makeLoaderImpl(Connection conn) throws SQLException {
		return new SWBankLoader(this, conn);
	}

	@Override
	protected Package getProcedurePackageImpl() {
		// TODO Auto-generated method stub
		return Balance.class.getPackage();        
	}

}
