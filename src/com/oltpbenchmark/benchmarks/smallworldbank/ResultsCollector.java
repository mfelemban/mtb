/**
 * Created by: Muhamad Felemban 
 * Jun 19, 2017
 */
package com.oltpbenchmark.benchmarks.smallworldbank;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.oltpbenchmark.api.BenchmarkModule;

/**
 * @author mfelemban
 *
 */
public class ResultsCollector {

	public static void printAvailability(List<String> latencies,String statFile) throws FileNotFoundException{
		File p = new File(statFile);
		FileOutputStream fos = new FileOutputStream(p,false);
		PrintWriter file = new PrintWriter(fos);
		file.append("************************\n");
		double run=0;
		double blk = 0;
		double totalAve=0,ave;
		int totalTxNum = 0;
		double globalTotalAve = 0;
		for(String lat:latencies){
			if(Integer.parseInt(lat.split(",")[6])==1){
				file.append(lat+"\n");
				//				txNum[ib-1]++;
				totalTxNum++;
				run = Double.parseDouble(lat.split(",")[3]);
				blk = Double.parseDouble(lat.split(",")[4]);
				ave = blk/run;
				//				ibAve[ib-1] += ave;
				globalTotalAve += ave;
			}
		}
		double totalTx=0;
		//		for(int i=0;i<k;i++){
		if(totalTxNum!=0){
			totalAve += 100-(globalTotalAve/totalTxNum)*100;
			file.append(totalTxNum + "," + (100-(globalTotalAve/totalTxNum)*100)+"\n");
		}
		else{
			totalAve += 100;
			file.append( totalTxNum+",100\n");
		}
		//		}
		//		if(txNum!=0)
		file.append("Total=" + totalTx+"," + totalAve);
		//		else
		//			file.append("Total=" + txNum+"," + 0);
		file.append("\n");

		file.close();
	}

	public static void collectIBStats(int k, BenchmarkModule benchmarkModule, String statFile){
		try{
			File file = new File(statFile);
			FileOutputStream fos = new FileOutputStream(file,false);
			PrintWriter p = new PrintWriter(fos);
			p.write("");

			Connection conn = benchmarkModule.makeConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			p.append("IB\t");

			for(int i=0;i<k;i++){
				p.append((i+1) + "\t");
			}
			p.append("\n");

			PreparedStatement ps = conn.prepareStatement("select ib,count(*) from transaction_id_map tid, transactions_ib tib where tid.wl_txid = tib.transaction_id group by tib.ib order by ib ;");
			ResultSet rs = ps.executeQuery();
			//			int[] txDist = new int[k];
			p.append("Tx\t");
			int i=1;
			while(rs.next()){
				//				txDist[rs.getInt(1)-1] = ;
				if(rs.getInt(1) == i)
					p.append(rs.getInt(2)+"\t");
				else{
					for(int j=0;j<(rs.getInt(1)-i);j++){
						p.append("0\t");
						i++;
					}
					p.append(rs.getInt(2)+"\t");
				}
				i++;
			} 
			p.append("\n");

			double[] temp = new double[k];

			ps = conn.prepareStatement("select ib, count(transaction_id) from malicious_transactions_table group by ib order by ib;");
			rs = ps.executeQuery();
			//			int[] ibTxDist = new int[k];
			i=1;
			p.append("Mtx\t");
			temp = new double[k];
			while(rs.next()){
				if(rs.getInt(1)!=0)
					temp[rs.getInt(1)-1] = rs.getDouble(2);
			}
			for(i=0;i<temp.length;i++)
				p.append(temp[i]+"\t");
			p.append("\n");

			ps = conn.prepareStatement("select ib,count(transaction_id) from corrupted_transactions_table where status = 'affected' group by ib order by ib;");
			rs = ps.executeQuery();			
			p.append("Atx\t");
			i=1;
			temp = new double[k];
			while(rs.next()){
				if(rs.getInt(1)!=0)
					temp[rs.getInt(1)-1] = rs.getDouble(2);
			}
			for(i=0;i<temp.length;i++)
				p.append(temp[i]+"\t");
			p.append("\n");


			ps = conn.prepareStatement("select ib,count(*) from availability_info where waiting_for_blocked =1 group by ib order by ib; ");
			rs = ps.executeQuery();
			//			int[] blkdTxDist = new int[k];
			p.append("Btx\t");
			i=1;

			while(rs.next()){
				if(rs.getInt(1)!=0)
					temp[rs.getInt(1)-1] = rs.getDouble(2);
			}
			for(i=0;i<temp.length;i++)
				p.append(temp[i]+"\t");
			p.append("\n");

			ps = conn.prepareStatement("select ib,count(*) from availability_info where waiting_for_boundary =1 group by ib order by ib; ");
			rs = ps.executeQuery();
			//			int[] blkdTxDist = new int[k];
			p.append("Htx\t");
			i=1;
			temp = new double[k];
			while(rs.next()){
				if(rs.getInt(1)!=0)
					temp[rs.getInt(1)-1] = rs.getDouble(2);
			}
			for(i=0;i<temp.length;i++)
				p.append(temp[i]+"\t");
			p.append("\n");



			ps = conn.prepareStatement("select ib,count(distinct object_id) from log_table l, transactions_ib i, transaction_id_map map where map.pg_transaction_id = l.transaction_id and map.wl_txid = i.transaction_id group by i.ib order by i.ib;");
			rs = ps.executeQuery();
			i=1;
			p.append("ATp\t");
			temp = new double[k];
			while(rs.next()){
				if(rs.getInt(1)!=0)
					temp[rs.getInt(1)-1] = rs.getDouble(2);
			}
			for(i=0;i<temp.length;i++)
				p.append(temp[i]+"\t");
			p.append("\n");

			ps = conn.prepareStatement("select i.ib,count(object_id) from touched_boundary_objects b, transaction_id_map map, transactions_ib i where i.transaction_id = map.wl_txid and map.pg_transaction_id = b.transaction_id group by i.ib order by i.ib;");
			rs = ps.executeQuery();
			i=1;
			p.append("TBO\t");
			temp = new double[k];
			while(rs.next()){
				if(rs.getInt(1)!=0)
					temp[rs.getInt(1)-1] = rs.getDouble(2);
			}
			for(i=0;i<temp.length;i++)
				p.append(temp[i]+"\t");
			p.append("\n");

			ps = conn.prepareStatement("select ib,count(distinct object_id) from log_table l, transactions_ib i, transaction_id_map map where object_id not in (select object_id from boundary_objects) and map.pg_transaction_id = l.transaction_id and map.wl_txid = i.transaction_id group by i.ib order by i.ib;");
			rs = ps.executeQuery();
			i=1;
			p.append("NotBO\t");
			temp = new double[k];
			while(rs.next()){
				if(rs.getInt(1)!=0)
					temp[rs.getInt(1)-1] = rs.getDouble(2);
			}
			for(i=0;i<temp.length;i++)
				p.append(temp[i]+"\t");
			p.append("\n");

			ps = conn.prepareStatement("select ib,count(blocked_tuples) from blocked_tuples_table group by ib order by ib;");
			rs = ps.executeQuery();
			int[] blkd = new int[k];
			while(rs.next()){
				if(rs.getInt(1)!=0)
					blkd[rs.getInt(1)-1] = rs.getInt(2);
			}
			ps = conn.prepareStatement(" select ib,count(distinct object_id) from repair_table t, transactions_ib i, transaction_id_map map where map.pg_transaction_id = t.malicious_transaction and map.wl_txid = i.transaction_id group by ib order by ib ;");
			rs = ps.executeQuery();
			p.append("BLKD\t");
			while(rs.next()){
				blkd[rs.getInt(1)-1] += rs.getInt(2);
			}
			for(i=0;i<k;i++){
				p.append(blkd[i]+"\t");
			}
			p.append("\n");

			ps = conn.prepareStatement("select ib,count(blocked_tuples) from blocked_tuples_table where blocked_tuples in (select distinct object_id from boundary_objects) group by ib order by ib;");
			rs = ps.executeQuery();
			int[] boblkd = new int[k];
			while(rs.next()){
				if(rs.getInt(1)!=0)
					boblkd[rs.getInt(1)-1] = rs.getInt(2);
			}
			ps = conn.prepareStatement(" select ib,count(distinct object_id) from repair_table t, transactions_ib i, transaction_id_map map where map.pg_transaction_id = t.malicious_transaction and map.wl_txid = i.transaction_id and object_id  in (select distinct object_id from boundary_objects) group by ib order by ib ;");
			rs = ps.executeQuery();
			p.append("BOBlkd\t");
			while(rs.next()){
				boblkd[rs.getInt(1)-1] += rs.getInt(2);
			}
			for(i=0;i<k;i++){
				p.append(boblkd[i]+"\t");
			}
			p.append("\n");

			ps = conn.prepareStatement("select ib,avg(extract(epoch from end_recovery_timestamp - start_recovery_timestamp))*1000 from malicious_transactions_table group by ib order by ib;");
			rs = ps.executeQuery();
			i=1;
			p.append("avg Rec Time\t");
			temp = new double[k];
			while(rs.next()){
				if(rs.getInt(1)!=0)
					temp[rs.getInt(1)-1] = rs.getDouble(2);
			}
			for(i=0;i<temp.length;i++)
				p.append(String.format( "%.4f", temp[i] )+"\t");
			p.append("\n");


			ps = conn.prepareStatement("select ib,avg(run_time)::float from availability_info group by ib order by ib;");
			rs = ps.executeQuery();
			i=1;
			p.append("ART\t");
			temp = new double[k];
			while(rs.next()){
				if(rs.getInt(1)!=0)
					temp[rs.getInt(1)-1] = rs.getDouble(2);
			}
			for(i=0;i<temp.length;i++)
				p.append(String.format( "%.4f", temp[i] )+"\t");
			p.append("\n");

			ps = conn.prepareStatement("select ib,avg(blockage_time)::float from availability_info group by ib order by ib;");
			rs = ps.executeQuery();
			i=1;
			p.append("ABT\t");
			temp = new double[k];
			while(rs.next()){
				if(rs.getInt(1)!=0)
					temp[rs.getInt(1)-1] = rs.getDouble(2);
			}
			for(i=0;i<temp.length;i++)
				p.append(String.format( "%.4f", temp[i] )+"\t");
			p.append("\n");

			p.close();
			p.append("\n");
			conn.commit();
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static void printLatencies(List<String> latencies,String statFile) throws FileNotFoundException{
		File p = new File(statFile);
		FileOutputStream fos = new FileOutputStream(p,false);
		PrintWriter file = new PrintWriter(fos);
		file.append("************************\n");
		double txNum = 0;
		double timeSum=0;
		for(String lat:latencies){
			txNum++;
			file.append(lat+"\n");
			timeSum += Double.parseDouble(lat.split(",")[3]);
		}
		if(txNum!=0)
			file.append("Total=" + txNum+"," + (timeSum/txNum));
		else
			file.append("Total=" + txNum+"," + 0);
		file.append("\n");
		file.close();
	}
	//select transaction_id, count(distinct object_id) from log_table where object_id not in (select object_id from boundary_objects) group by transaction_id;

	public static void collectGlobalSummary(int k,BenchmarkModule benchmarkModule, String statFile){
		try{
			File file = new File(statFile);
			FileOutputStream fos = new FileOutputStream(file,false);
			PrintWriter p = new PrintWriter(fos);
			p.write("");
			Connection conn = benchmarkModule.makeConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			/*
			 * 
			ps = conn.prepareStatement("");
			rs = ps.executeQuery();
			rs.next();
			int  = rs.getInt(1);
			p.append(":"+ +"\n");
			 */

			PreparedStatement ps = conn.prepareStatement("select count(distinct pg_transaction_id) from transaction_id_map;");
			ResultSet rs = ps.executeQuery();
			rs.next();
			int numOfTx = rs.getInt(1);
			p.append("Number of Transactions:"+ numOfTx+"\n");

			ps = conn.prepareStatement("select count(distinct transaction_id) from malicious_transactions_table;");
			rs = ps.executeQuery();
			rs.next();
			int numOfMalTx = rs.getInt(1);
			p.append("Number of Malicious transactions:"+ numOfMalTx+"\n");

			ps = conn.prepareStatement("select count(*) from corrupted_transactions_table where status ='affected';");
			rs = ps.executeQuery();
			rs.next();
			int numOfAffTx = rs.getInt(1);
			p.append("Number of Affected transactions:"+ numOfAffTx+"\n");

			ps = conn.prepareStatement("select count(*) from availability_info where waiting_for_blocked=1;");
			rs = ps.executeQuery();
			rs.next();
			int delayedTx = rs.getInt(1);
			p.append("Number of Blocked tx:"+ delayedTx+"\n");
			
			ps = conn.prepareStatement("select count(*) from availability_info where waiting_for_repair=1;");
			rs = ps.executeQuery();
			rs.next();
			int r_halted = rs.getInt(1);
			p.append("Number of R_halted tx:"+ r_halted+"\n");
			
			p.append("Number of halted:" + (delayedTx + r_halted)+"\n");

			ps = conn.prepareStatement("select count(*) from availability_info where waiting_for_boundary=1;");
			rs = ps.executeQuery();
			rs.next();
			int boDelayedTx = rs.getInt(1);
			p.append("Number of BO_halted tx:"+ boDelayedTx+"\n");
			
			p.append("Number of overal halted tx:"+ (boDelayedTx + delayedTx + r_halted)+"\n");

			ps = conn.prepareStatement("select count(distinct pg_transaction_id) from transaction_id_map where wl_txid in (select distinct transaction_id from touched_boundary_objects);");
			rs = ps.executeQuery();
			rs.next();
			int numOfBoTx = rs.getInt(1);
			p.append("Number of Boundary transactions:"+ numOfBoTx+"\n");

			ps = conn.prepareStatement("select count(distinct object_id) from log_table;");
			rs = ps.executeQuery();
			rs.next();
			int numOfAllTpls = rs.getInt(1);
			p.append("Number of all tuples:"+ numOfAllTpls+"\n");

			ps = conn.prepareStatement("select count(object_id) from touched_boundary_objects ;");
			rs = ps.executeQuery();
			rs.next();
			int numTchdBO = rs.getInt(1);
			p.append("Number of Tuoched Boundary tuples:"+ numTchdBO +"\n");

			ps = conn.prepareStatement("select count(distinct object_id) from log_table where object_id not in (select distinct object_id from touched_boundary_objects);");
			rs = ps.executeQuery();
			rs.next();
			int numNonBO = rs.getInt(1);
			p.append("Number of Non-Boundary tuples:"+ numNonBO +"\n");


			ps = conn.prepareStatement("select count(distinct b.blocked_tuples) from blocked_tuples_table b;");
			rs = ps.executeQuery();
			rs.next();
			int numOfBlkdTples = rs.getInt(1);
			ps = conn.prepareStatement("select count(distinct r.object_id) from repair_table r;");
			rs = ps.executeQuery();
			rs.next();
			numOfBlkdTples += rs.getInt(1);
			p.append("Number of Blocked tuples:"+ numOfBlkdTples+"\n");

			ps = conn.prepareStatement("select count(distinct b.blocked_tuples) from blocked_tuples_table b where b.blocked_tuples in (select distinct object_id from boundary_objects);");
			rs = ps.executeQuery();
			rs.next();
			int numOfBOBlkdTples = rs.getInt(1);
			ps = conn.prepareStatement("select count(distinct r.object_id) from repair_table r where object_id in (select distinct object_id from boundary_objects);");
			rs = ps.executeQuery();
			rs.next();
			numOfBOBlkdTples += rs.getInt(1);
			p.append("Number of Boundary Blocked tuples:"+ numOfBOBlkdTples+"\n");

			ps = conn.prepareStatement("select avg(extract(epoch from end_recovery_timestamp - start_recovery_timestamp))*1000 from malicious_transactions_table ;");
			rs = ps.executeQuery();
			rs.next();
			float avgRecoveryTime = rs.getFloat(1);
			p.append("Average recovery time:"+ avgRecoveryTime+"\n");

			ps = conn.prepareStatement("select avg(total_run_time)::float from availability_info ;");
			rs = ps.executeQuery();
			rs.next();
			float avgResponseTime = rs.getFloat(1);
			p.append("Average Response time:"+ avgResponseTime+"\n");
			
			ps = conn.prepareStatement("select avg(blockage_time)::float from availability_info ;");
			rs = ps.executeQuery();
			rs.next();
			float avgBlockageTime = rs.getFloat(1);
			p.append("Average Blockage time:"+ avgBlockageTime+"\n");
			
			ps = conn.prepareStatement("select avg(repair_time)::float from availability_info ;");
			rs = ps.executeQuery();
			rs.next();
			float avgRepairTime = rs.getFloat(1);
			p.append("Average Repair time:"+ avgRepairTime+"\n");
			
			ps = conn.prepareStatement("select avg(boblocked_time)::float from availability_info ;");
			rs = ps.executeQuery();
			rs.next();
			float boBlockedTime = rs.getFloat(1);
			p.append("Average BO_bkd time:"+ boBlockedTime+"\n");

			p.close();

			conn.commit();
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void collectConciseSummary(int k,BenchmarkModule benchmarkModule, String statFile){
		try{
			File file = new File(statFile);
			FileOutputStream fos = new FileOutputStream(file,false);
			PrintWriter p = new PrintWriter(fos);
			p.write("");
			Connection conn = benchmarkModule.makeConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			/*
			 * 
			ps = conn.prepareStatement("");
			rs = ps.executeQuery();
			rs.next();
			int  = rs.getInt(1);
			p.append(":"+ +"\n");
			 */

			PreparedStatement ps = conn.prepareStatement("select count(distinct pg_transaction_id) from transaction_id_map;");
			ResultSet rs = ps.executeQuery();
			rs.next();
			int numOfTx = rs.getInt(1);
			p.append("Number of Transactions:"+ numOfTx+"\n");

			ps = conn.prepareStatement("select count(distinct transaction_id) from malicious_transactions_table;");
			rs = ps.executeQuery();
			rs.next();
			int numOfMalTx = rs.getInt(1);
			p.append("Number of Malicious transactions:"+ numOfMalTx+"\n");

			ps = conn.prepareStatement("select count(*) from corrupted_transactions_table where status ='affected';");
			rs = ps.executeQuery();
			rs.next();
			int numOfAffTx = rs.getInt(1);
			p.append("Number of Affected transactions:"+ numOfAffTx+"\n");

			ps = conn.prepareStatement("select count(*) from availability_info where waiting_for_blocked=1;");
			rs = ps.executeQuery();
			rs.next();
			int delayedTx = rs.getInt(1);
			p.append("Number of Blocked tx:"+ delayedTx+"\n");
			
			ps = conn.prepareStatement("select count(*) from availability_info where waiting_for_repair=1;");
			rs = ps.executeQuery();
			rs.next();
			int r_halted = rs.getInt(1);
			p.append("Number of R_halted tx:"+ r_halted+"\n");
			
			p.append("Number of halted:" + (delayedTx + r_halted)+"\n");

			ps = conn.prepareStatement("select count(*) from availability_info where waiting_for_boundary=1;");
			rs = ps.executeQuery();
			rs.next();
			int boDelayedTx = rs.getInt(1);
			p.append("Number of BO_halted tx:"+ boDelayedTx+"\n");
			
			p.append("Number of overall halted tx:"+ (boDelayedTx + delayedTx + r_halted)+"\n");

			ps = conn.prepareStatement("select avg(extract(epoch from end_recovery_timestamp - start_recovery_timestamp))*1000 from malicious_transactions_table ;");
			rs = ps.executeQuery();
			rs.next();
			float avgRecoveryTime = rs.getFloat(1);
			p.append("Average recovery time:"+ avgRecoveryTime+"\n");

			ps = conn.prepareStatement("select avg(total_run_time)::float from availability_info ;");
			rs = ps.executeQuery();
			rs.next();
			float avgResponseTime = rs.getFloat(1);
			p.append("Average Response time:"+ avgResponseTime+"\n");
			
			ps = conn.prepareStatement("select avg(blockage_time)::float from availability_info ;");
			rs = ps.executeQuery();
			rs.next();
			float avgBlockageTime = rs.getFloat(1);
			p.append("Average Blockage time:"+ avgBlockageTime+"\n");
			
			ps = conn.prepareStatement("select avg(repair_time)::float from availability_info ;");
			rs = ps.executeQuery();
			rs.next();
			float avgRepairTime = rs.getFloat(1);
			p.append("Average Repair time:"+ avgRepairTime+"\n");
			
			ps = conn.prepareStatement("select avg(boblocked_time)::float from availability_info ;");
			rs = ps.executeQuery();
			rs.next();
			float boBlockedTime = rs.getFloat(1);
			p.append("Average BO_bkd time:"+ boBlockedTime+"\n");

			p.close();

			conn.commit();
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	public static void getAffectedTimes(BenchmarkModule benchmarkModule, String statFile){
		try{
			File file = new File(statFile);
			FileOutputStream fos = new FileOutputStream(file,false);
			PrintWriter p = new PrintWriter(fos);
			p.write("");
			Connection conn = benchmarkModule.makeConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			PreparedStatement ps = conn.prepareStatement("select transaction_id, min(time_stamp) from log_table where transaction_id in (select transaction_id from corrupted_transactions_table where status='affected') group by transaction_id;");
			ResultSet rs= ps.executeQuery();
			while(rs.next()){
				p.append(rs.getInt(1) +","+rs.getTime(2));
			}

			p.close();

			conn.commit();
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static void collectGlobalHist(BenchmarkModule benchmarkModule,String histFile){
		//select count(distinct blocked_tuples) from blocked_tuples_table group by malicious_transaction; in 10s
		//select run_time,blockage_time from availability_info; in 500s and 500s
		// select extract(epoch from (end_recovery_timestamp)-(start_recovery_timestamp))*1000 from malicious_transactions_table; in 500s
		try{
			File file = new File(histFile);
			FileOutputStream fos = new FileOutputStream(file,false);
			PrintWriter p = new PrintWriter(fos);
			p.write("");

			Connection conn = benchmarkModule.makeConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			PreparedStatement ps = conn.prepareStatement("select count(distinct blocked_tuples) from blocked_tuples_table group by malicious_transaction;");
			ResultSet rs = ps.executeQuery();
			ArrayList<Double> blkdTplsPerMal = new ArrayList<Double>();
			while(rs.next()){
				blkdTplsPerMal.add(rs.getDouble(1));
			} 
			p.append("#Blocked Tuples per Mal:\n"+ Utils.printArrayList(blkdTplsPerMal) +"\n");



			ps = conn.prepareStatement("select run_time,blockage_time from availability_info ;");
			rs = ps.executeQuery();
			ArrayList<Double> glblRunTime = new ArrayList<Double>();
			ArrayList<Double> glblBlkTime = new ArrayList<Double>();
			while(rs.next()){
				double t1 = rs.getDouble(1);
				double t2 = rs.getDouble(2);
				glblRunTime.add(t1);
				glblBlkTime.add(t2);
			} 


			p.append("#Run time :\n"+ Utils.printArrayList(glblRunTime) +"\n");
			p.append("#Blk time :\n"+ Utils.printArrayList(glblBlkTime) +"\n");





			//			p.append("Transaction to IB Distribution:\n"+ Arrays.toString(txDist)+"\n");
			ps = conn.prepareStatement("select extract(epoch from (end_recovery_timestamp)-(start_recovery_timestamp))*1000 from malicious_transactions_table order by ib;");
			rs = ps.executeQuery();
			ArrayList<Double> glblRecTime = new ArrayList<Double>();
			while(rs.next()){
				double t = rs.getDouble(1);
				glblRecTime.add(t);
			} 
			p.append("#Recovery time :\n"+ Utils.printArrayList(glblRecTime) +"\n");


			//			
			ps = conn.prepareStatement("select count(ib) from temp_tuples_table_aims group by malicious_transaction;");
			rs = ps.executeQuery();
			ArrayList<Double> numIBs = new ArrayList<Double>();
			while(rs.next()){
				double t = rs.getDouble(1);
				numIBs.add(t);
			} 
			p.append("#Number of touched IBs :\n"+ Utils.printArrayList(numIBs) +"\n");

			//			
			ps = conn.prepareStatement("select extract (epoch from recovery_timestamp-added_timestamp)*1000 from blocked_tuples_table;");
			rs = ps.executeQuery();
			ArrayList<Double> tplRec = new ArrayList<Double>();
			while(rs.next()){
				double t = rs.getDouble(1);
				tplRec.add(t);
			} 
			p.append("#Recovery time for tuples :\n"+ Utils.printArrayList(tplRec) +"\n");

			p.close();

			conn.commit();
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	public static void collectIBHist(int k, BenchmarkModule benchmarkModule,String histFile){
		//select count(distinct blocked_tuples) from blocked_tuples_table group by malicious_transaction; in 10s
		//select run_time,blockage_time from availability_info; in 500s and 500s
		// select extract(epoch from (end_recovery_timestamp)-(start_recovery_timestamp))*1000 from malicious_transactions_table; in 500s
		try{
			File file = new File(histFile);
			FileOutputStream fos = new FileOutputStream(file,false);
			PrintWriter p = new PrintWriter(fos);
			p.write("");

			Connection conn = benchmarkModule.makeConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			int ib = 0;


			PreparedStatement ps = conn.prepareStatement("with d as (select b.malicious_transaction m,count(distinct b.blocked_tuples) c from blocked_tuples_table b group by malicious_transaction) select i.ib,d.c from d , transactions_ib i, transaction_id_map map  where i.transaction_id = map.wl_txid and map.pg_transaction_id = d.m;");
			ResultSet rs = ps.executeQuery();
			ArrayList<ArrayList<Double>> blkdTplsPerIB = new ArrayList<ArrayList<Double>>(k);
			for(int i=0;i<k;i++){
				blkdTplsPerIB.add(new ArrayList<Double>());
			}
			while(rs.next()){
				double temp = rs.getDouble(2);
				ib = rs.getInt(1)-1;
				if(ib>=0)
					blkdTplsPerIB.get(ib).add(temp);
			}
			p.append("#Blocked Tuples IB\n");
			for(int i=0;i<k;i++)
				p.append("\t$" + (i+1) + ":"+ Utils.printArrayList(blkdTplsPerIB.get(i))+"\n");


			ps = conn.prepareStatement("select ib,run_time,blockage_time from availability_info order by ib;");
			rs = ps.executeQuery();
			ArrayList<ArrayList<Double>> runTime = new ArrayList<ArrayList<Double>>();
			ArrayList<ArrayList<Double>> blkTime = new ArrayList<ArrayList<Double>>();
			for(int i=0;i<k;i++){
				runTime.add(new ArrayList<Double>());
				blkTime.add(new ArrayList<Double>());
			}
			while(rs.next()){
				double t1 = rs.getDouble(2);
				double t2 = rs.getDouble(3);
				ib = rs.getInt(1)-1;
				if(ib>=0){
					runTime.get(ib).add(t1);
					blkTime.get(ib).add(t2);
				}
			} 
			p.append("#Run time\n");
			for(int i=0;i<k;i++){
				p.append("\t$" + (i+1) + ":"+ Utils.printArrayList(runTime.get(i))+"\n");
			}
			p.append("#Blokage time\n");
			for(int i=0;i<k;i++){
				p.append("\t$" + (i+1) + ":"+ Utils.printArrayList(blkTime.get(i))+"\n");
			}



			ps = conn.prepareStatement("select ib,extract(epoch from (end_recovery_timestamp)-(start_recovery_timestamp))*1000 from malicious_transactions_table order by ib;");
			rs = ps.executeQuery();
			ArrayList<ArrayList<Double>> recTime = new ArrayList<ArrayList<Double>>();
			for(int i=0;i<k;i++){
				recTime.add(new ArrayList<Double>());
			}
			while(rs.next()){
				double t = rs.getDouble(2);
				ib = rs.getInt(1)-1;
				if(ib>=0)
					recTime.get(ib).add(t);
			} 
			p.append("#Recovery time :\n");
			for(int i=0;i<k;i++){
				p.append("\t$" + (i+1) + ":"+ Utils.printArrayList(recTime.get(i))+"\n");
			}

			ps = conn.prepareStatement("select ib,extract (epoch from recovery_timestamp-added_timestamp)*1000 from blocked_tuples_table order by ib;");
			rs = ps.executeQuery();
			ArrayList<ArrayList<Double>> recTimeTPls = new ArrayList<ArrayList<Double>>();
			for(int i=0;i<k;i++){
				recTimeTPls.add(new ArrayList<Double>());
			}
			while(rs.next()){
				double t = rs.getDouble(2);
				ib = rs.getInt(1)-1;
				if(ib>=0)
					recTimeTPls.get(ib).add(t);
			} 
			p.append("#Recovery time per tuple :\n");
			for(int i=0;i<k;i++){
				p.append("\t$" + (i+1) + ":"+ Utils.printArrayList(recTimeTPls.get(i))+"\n");
			}


			//			

			p.close();

			conn.commit();
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
