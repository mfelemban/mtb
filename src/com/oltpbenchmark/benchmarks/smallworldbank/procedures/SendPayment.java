package com.oltpbenchmark.benchmarks.smallworldbank.procedures;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.oltpbenchmark.api.Procedure;
import com.oltpbenchmark.api.SQLStmt;
import com.oltpbenchmark.benchmarks.smallworldbank.SWBankConstants;
import com.oltpbenchmark.util.AIMSLogger;

public class SendPayment extends Procedure {

	private static final Logger LOG = Logger.getLogger(SendPayment.class);

	// Change the getLong functions below
	public final SQLStmt spGetChkBalance = new SQLStmt("Select oid,tableoid,xmin,* from "+SWBankConstants.TABLENAME_CHECKING+" where CHK_ID = ?");
	public String createIncSrcChkBalanceSQL(long src_a_id, long dest_a_id, double amount){
		String stmt = "UPDATE " 
				+ SWBankConstants.TABLENAME_CHECKING 
				+ " SET ";
		//		stmt += "BALANCE = BALANCE - c.column_a"
		//				+ " from ( values " + "(" + (dest_a_id) + "," + (-1*amount) +"),"
		//				+ "(" + (src_a_id) + "," + amount +")";
		stmt += "BALANCE = BALANCE -  c.column_a"
				+ " from ( values " + "(" + (dest_a_id) + "," + (-1*amount) +"),"
				+ "(" + (src_a_id) + "," + amount +")";
		stmt += ") as c(id, column_a) where c.id = chk_id";
		return stmt;


	}




	public boolean isRun(Connection conn, long srcID, long destID, double fraction) {
		boolean success =false;
		double srcBal = 0;
		double txAmount = 0;
		try{

			ResultSet tmprs = null;
			PreparedStatement spGetAcctBal = null;
			PreparedStatement spUpdate = null;
			spGetAcctBal = this.getPreparedStatement(conn, spGetChkBalance);
			//			LOG.info(spGetAcctBal.toString());


			// read src balance

			spGetAcctBal.setLong(1, srcID);
			tmprs = spGetAcctBal.executeQuery();
			//			LOG.info(spGetAcctBal.toString());
			tmprs.next();
			srcBal = tmprs.getDouble(3);
			txAmount = srcBal * fraction;
			//			LOG.info("Previous balance " + srcBal + " fraction " + amount + " txAmoun " + txAmount);
			if(srcBal <=0 || srcBal - txAmount<0){
				LOG.info(tmprs.getLong(2) + " balance " + srcBal + " and sending out " + txAmount);
				//				System.exit(0);
				LOG.info("Rolled back!");
				conn.rollback();
				return false;
			}
			tmprs.close();

			//		LOG.info("Send payment 2");


			SQLStmt update = new SQLStmt(createIncSrcChkBalanceSQL(srcID,destID,txAmount));
			spUpdate = this.getPreparedStatement(conn, update);
			spUpdate.executeUpdate();
			//			LOG.info(spUpdate.toString());

			success= true;
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return success;
	}
	
	public long run(Connection conn, long srcID, long destID, double fraction) {
		long txid = -1;
		boolean success =false;
		double srcBal = 0;
		double txAmount = 0;
		try{

			ResultSet tmprs = null;
			PreparedStatement spGetAcctBal = null;
			PreparedStatement spUpdate = null;
			spGetAcctBal = this.getPreparedStatement(conn, spGetChkBalance);
			//			LOG.info(spGetAcctBal.toString());


			// read src balance

			spGetAcctBal.setLong(1, srcID);
			tmprs = spGetAcctBal.executeQuery();
			//			LOG.info(spGetAcctBal.toString());
			tmprs.next();
			srcBal = tmprs.getDouble(5);
			txAmount = srcBal * fraction;
			//			LOG.info("Previous balance " + srcBal + " fraction " + amount + " txAmoun " + txAmount);
			if(srcBal <=0 || srcBal - txAmount<0){
				LOG.info(tmprs.getLong(4) + " balance " + srcBal + " and sending out " + txAmount);
				//				System.exit(0);
				LOG.info("Rolled back!");
				conn.rollback();
				return -1;
			}
			tmprs.close();

			//		LOG.info("Send payment 2");


			SQLStmt update = new SQLStmt(createIncSrcChkBalanceSQL(srcID,destID,txAmount));
			spUpdate = this.getPreparedStatement(conn, update);
			spUpdate.executeUpdate();
			//			LOG.info(spUpdate.toString());

			success= true;
			txid = AIMSLogger.getTransactionId(conn, this);
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return txid;
	}


}
