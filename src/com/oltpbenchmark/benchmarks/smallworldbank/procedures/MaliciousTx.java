/**
 * Created by: Muhamad Felemban 
 * Jul 2, 2017
 */
package com.oltpbenchmark.benchmarks.smallworldbank.procedures;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import aims.util.QueriesStatements;

import com.oltpbenchmark.api.Procedure;
import com.oltpbenchmark.api.SQLStmt;
import com.oltpbenchmark.benchmarks.smallworldbank.SWBankConstants;

/**
 * @author mfelemban
 *
 */
public class MaliciousTx extends Procedure{
	private static final Logger LOG = Logger.getLogger(Distribute.class);

	public boolean malRun(long malID,Connection conn, long srcID, long[] destIDs,
			double fraction) throws SQLException {
		boolean success = false;
		long txid = 0;
		try{
			PreparedStatement ps = conn.prepareStatement("select txid_current()");
			ResultSet rr = ps.executeQuery();
			rr.next();
			txid = rr.getLong(1);
		}
		catch(Exception e){
			LOG.info("Problem with getting TXID");
		}
		while(!success){
			try {
				SQLStmt spGetCustIdSQL = new SQLStmt(
						malCreateGetSrcGetBalanceSQL(srcID));
				PreparedStatement getCustId = this.getPreparedStatement(conn,
						spGetCustIdSQL);
				ResultSet rs = getCustId.executeQuery();
				//			LOG.info(getCustId.toString());
				double[] txAmounts = new double[destIDs.length];
				rs.next();
				float srcBal = rs.getFloat(2);
				double txAmount = srcBal * fraction;
				if (srcBal <= 0 || srcBal - txAmount < 0) {
					LOG.info(rs.getLong(1) + " balance " + srcBal
							+ " and sending out " + txAmount);
					LOG.info("Rolled back!");
					conn.rollback();
					return false;
				}
				double n = destIDs.length;
				for (int i = 0; i < destIDs.length; i++) {
					txAmounts[i] = txAmount / n;
				}
				
				Map<Long,Double> ids = new TreeMap<Long, Double>();
				double total =0;
				for (int i = 0; i < destIDs.length; i++) {
					total += txAmounts[i];
					ids.put(destIDs[i], txAmounts[i]);
				}
				ids.put(srcID, -1 * total);

				rs.close();
				if (destIDs.length <= 10000) {
					SQLStmt incChkBalanceSQL = new SQLStmt(
							createIncDestChkBalanceSQL(srcID, destIDs, txAmounts));
					PreparedStatement incChkBalance = this.getPreparedStatement(
							conn, incChkBalanceSQL);
					incChkBalance.executeUpdate();
					
					SQLStmt backupStms = new SQLStmt(QueriesStatements.insertIntoBackup(ids));
					PreparedStatement backup = this.getPreparedStatement(conn, backupStms);
					backup.executeUpdate();
					backupStms = new SQLStmt(QueriesStatements.updateBackup(ids,txid));
					backup = this.getPreparedStatement(conn, backupStms);
					backup.executeUpdate();
					
				} else {
					LOG.info("tx greater than 1000");
					System.exit(0);
				}
				success = true;
			} catch (SQLException e) {
				e.printStackTrace();
				LOG.error("Deadlock! on " + malID);
				success = false;
				conn.rollback();
			}
		}
		return success;

	}

	public String malCreateGetSrcGetBalanceSQL(long s_a_id) {
		String stmt = "with temp_list (id, sort_order) as (" + "values ("
				+ s_a_id + ",1)";
		stmt += ") select chk_id,balance from "
				+ SWBankConstants.TABLENAME_CHECKING
				+ " a  join temp_list t on t.id = a.chk_id order by t.sort_order for update";
		return stmt;
	}

	public String createIncDestChkBalanceSQL(long src, long[] destIds,
			double[] amounts) {
		double total = 0;
		Map<Long, Double> ids = new TreeMap<Long, Double>();
		for (int i = 0; i < destIds.length; i++) {
			total += amounts[i];
			ids.put(destIds[i], amounts[i]);
		}
		ids.put(src, -1 * total);

		String stmt = "UPDATE " + SWBankConstants.TABLENAME_CHECKING + " SET ";
		stmt += "BALANCE = BALANCE + c.column_a" + " from ( values ";
		for (Map.Entry<Long, Double> entry : ids.entrySet()) {
			stmt += "(" + entry.getValue() + "," + entry.getKey() + "),";
		}
		stmt = stmt.substring(0, stmt.length() - 1)
				+ ") as c(column_a, id) where c.id = chk_id";
		return stmt;

	}

}
