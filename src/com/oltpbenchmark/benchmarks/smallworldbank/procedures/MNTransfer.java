package com.oltpbenchmark.benchmarks.smallworldbank.procedures;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import aims.util.QueriesStatements;

import com.oltpbenchmark.api.Procedure;
import com.oltpbenchmark.api.SQLStmt;
import com.oltpbenchmark.benchmarks.smallworldbank.SWBankConstants;

public class MNTransfer extends Procedure {
	private static final Logger LOG = Logger.getLogger(MNTransfer.class);


	public Integer isRun(Connection conn, long[] srcIDs ,  long[] destIDs, double fraction) throws SQLException {


		double[] txAmounts = new double[srcIDs.length];
		double[] rxAmounts = new double[destIDs.length];
		double txSum = 0;
//		boolean success = false;
		Integer txid = 0;
		try{
			PreparedStatement ps = conn.prepareStatement("select txid_current()");
			ResultSet rr = ps.executeQuery();
			rr.next();
			txid = rr.getInt(1);
//			LOG.info(txid);
		}catch(Exception e){
			LOG.info("Problem at getting TXID");
		}

		try{
			SQLStmt spGetCustIdSQL = new SQLStmt(createGetSrcGetBalanceSQL(srcIDs));
			PreparedStatement getCustId = this.getPreparedStatement(conn, spGetCustIdSQL);
			ResultSet rs = getCustId.executeQuery();
			int i=0;
			float balance =0;
			while(rs.next() && i<srcIDs.length) {
				balance = rs.getFloat(5);
				txAmounts[i] = fraction *balance;
				txSum += txAmounts[i];
				if(balance<=0 || balance - txAmounts[i]<0){
					LOG.info(rs.getLong(4) + " balance " + balance + " and sending out " + txAmounts[i]);
					LOG.info("Rolled back!");
					System.exit(0);
					conn.rollback();
//					return false;
					return -1;
				}
				i++;
			}
			rs.close();


			// Computing receivers shares 
			double recSize = destIDs.length;
			for(i=0;i<destIDs.length;i++){
				rxAmounts[i] = txSum /recSize;
			}
			
			Map<Long,Double> ids = new TreeMap<Long,Double>();
			for(i=0;i<srcIDs.length;i++){
				ids.put(srcIDs[i], txAmounts[i]);
			}
			for( i=0;i<destIDs.length;i++){
				ids.put(destIDs[i], -1*rxAmounts[i]);
			}

			if(srcIDs.length + destIDs.length<=10000){
				SQLStmt incChkBalanceSQL = new SQLStmt(createIncChkBalanceSQL(ids));
				PreparedStatement incChkBalance = this.getPreparedStatement(conn, incChkBalanceSQL);
				incChkBalance.executeUpdate();
				SQLStmt backupStms = new SQLStmt(QueriesStatements.insertIntoBackup(ids));
				PreparedStatement backup = this.getPreparedStatement(conn, backupStms);
				backup.executeUpdate();
				backupStms = new SQLStmt(QueriesStatements.updateBackup(ids,txid));
				backup = this.getPreparedStatement(conn, backupStms);
				backup.executeUpdate();
			}else{
				LOG.info("Tx is large");
				System.exit(0);
			}
			
			
		} catch(SQLException e){
			txid = -1;
			conn.rollback();
//			LOG.info("redoing it");
		}

		return txid;
	}
	
	

	public String  createGetSrcGetBalanceSQL( long[] srcIDs){
		String stmt = "with temp_list (id, sort_order) as (" + "values ";
		for(int i=0;i<srcIDs.length;i++){
			stmt += "(" + srcIDs[i]   +","+ (i+1) +"),";
		}
		stmt = stmt.substring(0,stmt.length()-1) + ") select a.oid,a.tableoid,a.xmin,a.chk_id,balance from " + SWBankConstants.TABLENAME_CHECKING  +" a  join temp_list t on t.id = a.chk_id order by t.sort_order";
		return stmt;
	}
	
	public String createIncChkBalanceSQL(Map<Long,Double> ids){
		
		String stmt = "UPDATE " 
				+ SWBankConstants.TABLENAME_CHECKING 
				+ " SET ";
		stmt += "BALANCE = BALANCE - c.column_a from ( values";  
		for(Map.Entry<Long,Double> entry: ids.entrySet()){
			stmt += "(" + entry.getValue() + "," + entry.getKey() +"),";
		}
		stmt = stmt.substring(0, stmt.length()-1) +  ") as c(column_a, id) where c.id = chk_id";
		return stmt;


	}

}
