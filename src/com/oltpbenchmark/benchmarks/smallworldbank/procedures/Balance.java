package com.oltpbenchmark.benchmarks.smallworldbank.procedures;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.oltpbenchmark.api.Procedure;
import com.oltpbenchmark.api.SQLStmt;
import com.oltpbenchmark.benchmarks.smallworldbank.SWBankConstants;
import com.oltpbenchmark.util.AIMSLogger;

public class Balance extends Procedure {
    
//    private static final Logger LOG = Logger.getLogger(Balance.class);

    
    public final SQLStmt stmtGetCheckingBalSQL = new SQLStmt(
            "SELECT oid,tableoid,*"
            + "  FROM " + SWBankConstants.TABLENAME_CHECKING 
            + " WHERE CHK_ID = ?");
    
    private PreparedStatement stmtGetCheckingBal = null;
    

    public long run(Connection conn, long a_id) throws SQLException {
        
        // initialize queries
        stmtGetCheckingBal = this.getPreparedStatement(conn, stmtGetCheckingBalSQL);
        
        stmtGetCheckingBal.setLong(1, a_id);
        
        
        // execute queries
        ResultSet rsChk = stmtGetCheckingBal.executeQuery();
        
        if (!rsChk.next()){
            throw new RuntimeException("CHK_ID =" + a_id + " not found! in Checking");
        }
        
        rsChk.close();

        

        return AIMSLogger.getTransactionId(conn, this);
    }

}
