package com.oltpbenchmark.benchmarks.smallworldbank.procedures;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import aims.util.QueriesStatements;

import com.oltpbenchmark.api.Procedure;
import com.oltpbenchmark.api.SQLStmt;
import com.oltpbenchmark.benchmarks.smallworldbank.SWBankConstants;

public class Collect extends Procedure {
	private static final Logger LOG = Logger.getLogger(Collect.class);

	public Integer isRun(Connection conn, long[] srcIDs ,  long destID, double fraction) throws SQLException {
//		boolean success = false;
		Integer txid = 0;
		try{
			PreparedStatement ps = conn.prepareStatement("select txid_current()");
			ResultSet rr = ps.executeQuery();
			rr.next();
			txid = rr.getInt(1);
		}
		catch(Exception e){
			LOG.info("Problem with getting TXID");
		}
		try{
			double[] txAmounts = new double[srcIDs.length];
			SQLStmt spGetCustIdSQL = new SQLStmt(createGetSrcGetBalanceSQL(srcIDs));
			PreparedStatement getCustId = this.getPreparedStatement(conn, spGetCustIdSQL);
			ResultSet rs = getCustId.executeQuery();
			int i=0;
			float balance =0;
			
			while(rs.next() && i < srcIDs.length) {
				txAmounts[i] = fraction *rs.getFloat(5);
				balance =rs.getFloat(5); 
				if(balance<=0 || balance - txAmounts[i]<0){
					LOG.info(rs.getLong(4) + " balance " + balance + " and sending out " + txAmounts[i]);
					LOG.info("Rolled back!");
					conn.rollback();
					return -1;
				}
				i++;
			}
			rs.close();
			
			double total = 0;
			Map<Long,Double> ids = new TreeMap<Long,Double>();
			for(i=0;i<txAmounts.length;i++){
				ids.put(srcIDs[i], txAmounts[i]);
				total += txAmounts[i];
			}
			ids.put(destID, -1*total);
			
			
			
			if(srcIDs.length<=10000){ 
				SQLStmt incChkBalanceSQL = new SQLStmt(createIncSrcChkBalanceSQL(ids));
				PreparedStatement incChkBalance = this.getPreparedStatement(conn, incChkBalanceSQL);
				incChkBalance.executeUpdate();
				

				SQLStmt backupStms = new SQLStmt(QueriesStatements.insertIntoBackup(ids));
				PreparedStatement backup = this.getPreparedStatement(conn, backupStms);
				backup.executeUpdate();
				backupStms = new SQLStmt(QueriesStatements.updateBackup(ids,txid));
				backup = this.getPreparedStatement(conn, backupStms);
				backup.executeUpdate();
				
				
			}else{
				LOG.info("Tx is large");
				System.exit(0);
			}
		}catch(SQLException e){
			conn.rollback();
			txid = -1;
		}
		return txid;

	}
	
	public String  createGetSrcGetBalanceSQL( long[] s_a_ids){
		String stmt = "with temp_list (id, sort_order) as (" + "values ";
		for(int i=0;i<s_a_ids.length;i++){
			stmt += "(" + s_a_ids[i]   +","+ (i+1) +"),";
		}
		stmt = stmt.substring(0,stmt.length()-1) + ") select a.oid,a.tableoid,a.xmin,a.chk_id,balance from " + SWBankConstants.TABLENAME_CHECKING  +" a  join temp_list t on t.id = a.chk_id order by t.sort_order";
		return stmt;
	}
	
	
	
	public String createIncSrcChkBalanceSQL(Map<Long,Double> ids){
		
			
		
		String stmt = "UPDATE " 
				+ SWBankConstants.TABLENAME_CHECKING 
				+ " SET ";
		stmt += "BALANCE = BALANCE - c.column_a"
				+ " from ( values ";
		for(Entry<Long, Double> entry: ids.entrySet()){
			stmt += "(" + entry.getValue() + "," + entry.getKey() +"),";
		}
		stmt = stmt.substring(0, stmt.length()-1) +  ") as c(column_a, id) where c.id = chk_id";
		return stmt;
	}
}
