package com.oltpbenchmark.benchmarks.smallworldbank.procedures;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import aims.util.QueriesStatements;

import com.oltpbenchmark.api.Procedure;
import com.oltpbenchmark.api.SQLStmt;
import com.oltpbenchmark.benchmarks.smallworldbank.SWBankConstants;

public class Distribute extends Procedure  {
	private static final Logger LOG = Logger.getLogger(Distribute.class);

	Connection conn;
	long srcID;
	long[] destIDs;
	double fraction;



	public Integer isRun(Connection conn, long srcID, long[] destIDs,
			double fraction) throws SQLException {
		//		boolean success = false;
		Integer txid = 0;
		try {
			PreparedStatement ps = conn.prepareStatement("select txid_current()");
			ResultSet rr = ps.executeQuery();
			rr.next();
			txid = rr.getInt(1);

		}catch(Exception e){
			LOG.info("Problem with getting TXID");
		}
		
		Map<Long, Double> ids= null;
		try{
			SQLStmt spGetCustIdSQL = new SQLStmt(
					createGetSrcGetBalanceSQL(srcID));
			PreparedStatement getCustId = this.getPreparedStatement(conn,
					spGetCustIdSQL);
			ResultSet rs = getCustId.executeQuery();
			System.out.println(getCustId);
			double[] txAmounts = new double[destIDs.length];
			rs.next();
			float srcBal = rs.getFloat(5);
			double txAmount = srcBal * fraction;
			if (srcBal <= 0 || srcBal - txAmount < 0) {
				LOG.info(rs.getLong(4) + " balance " + srcBal
						+ " and sending out " + txAmount);
				LOG.info("Rolled back!");
				conn.rollback();
				return -1;
			}
			double n = destIDs.length;
			for (int i = 0; i < destIDs.length; i++) {
				txAmounts[i] = txAmount / n;
			}

			rs.close();

			double total = 0;
			ids = new TreeMap<Long, Double>();
			for (int i = 0; i < destIDs.length; i++) {
				total += txAmounts[i];
				ids.put(destIDs[i],-1* txAmounts[i]);
			}
			ids.put(srcID,  total);

		}catch(Exception e){
			e.printStackTrace();
			LOG.info("Stage 1");
			System.exit(0);
		}

		try{

			if (destIDs.length <= 10000) {
				SQLStmt incChkBalanceSQL = new SQLStmt(
						createIncDestChkBalanceSQL(ids));
				PreparedStatement incChkBalance = this.getPreparedStatement(
						conn, incChkBalanceSQL);
				incChkBalance.executeUpdate();
				
				SQLStmt backupStms = new SQLStmt(QueriesStatements.insertIntoBackup(ids));
//				LOG.info(backupStms.toString());
				PreparedStatement backup = this.getPreparedStatement(conn, backupStms);
				backup.executeUpdate();
				backupStms = new SQLStmt(QueriesStatements.updateBackup(ids,txid));
//				LOG.info(backupStms.toString());
				backup = this.getPreparedStatement(conn, backupStms);
				backup.executeUpdate();
				
			} else {
				LOG.info("tx greater than 1000");
				System.exit(0);
			}

//			conn.commit();
			
//			LOG.info(t + " " + ii + " " + uu + " " + bb +"=" + (t+ii+uu+bb) + " co: " + (System.currentTimeMillis() - start));
		} catch (SQLException e) {
//			e.printStackTrace();
			txid = -1;
			conn.rollback();
		}
		return txid;

	}



	public String createGetSrcGetBalanceSQL(long s_a_id) {
		String stmt = "with temp_list (id, sort_order) as (" + "values ("
				+ s_a_id + ",1)";
		stmt += ") select a.oid,a.tableoid,a.xmin,chk_id,balance from "
				+ SWBankConstants.TABLENAME_CHECKING
				+ " a  join temp_list t on t.id = a.chk_id order by t.sort_order";
		return stmt;
	}



	public String createIncDestChkBalanceSQL(Map<Long, Double> ids) {
		String stmt = "UPDATE " + SWBankConstants.TABLENAME_CHECKING + " SET ";
		stmt += "BALANCE = BALANCE - c.column_a" + " from ( values ";
		for (Map.Entry<Long, Double> entry : ids.entrySet()) {
			stmt += "(" + entry.getValue() + "," + entry.getKey() + "),";
		}
		stmt = stmt.substring(0, stmt.length() - 1)
				+ ") as c(column_a, id) where c.id = chk_id";
		return stmt;

	}


}