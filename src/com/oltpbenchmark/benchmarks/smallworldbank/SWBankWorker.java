/******************************************************************************
 *  Copyright 2016 by OLTPBenchmark Project  
 *  
 *  Author: Thamir Qadah                                 *
 *                                                                            *
 *  Licensed under the Apache License, Version 2.0 (the "License");           *
 *  you may not use this file except in compliance with the License.          *
 *  You may obtain a copy of the License at                                   *
 *                                                                            *
 *    http://www.apache.org/licenses/LICENSE-2.0                              *
 *                                                                            *
 *  Unless required by applicable law or agreed to in writing, software       *
 *  distributed under the License is distributed on an "AS IS" BASIS,         *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  *
 *  See the License for the specific language governing permissions and       *
 *  limitations under the License.                                            *
 ******************************************************************************/

package com.oltpbenchmark.benchmarks.smallworldbank;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.configuration.SubnodeConfiguration;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;

import aims.framework.IDMessageSender;

import com.oltpbenchmark.api.BenchmarkModule;
import com.oltpbenchmark.api.Procedure.UserAbortException;
import com.oltpbenchmark.api.TransactionType;
import com.oltpbenchmark.api.Worker;
import com.oltpbenchmark.benchmarks.smallworldbank.procedures.Balance;
import com.oltpbenchmark.benchmarks.smallworldbank.procedures.Collect;
import com.oltpbenchmark.benchmarks.smallworldbank.procedures.Distribute;
import com.oltpbenchmark.benchmarks.smallworldbank.procedures.InvalidTransactionTypeException;
import com.oltpbenchmark.benchmarks.smallworldbank.procedures.MNTransfer;
import com.oltpbenchmark.benchmarks.smallworldbank.procedures.SendPayment;
import com.oltpbenchmark.types.TransactionStatus;
import com.oltpbenchmark.util.AIMSLogger;

public class SWBankWorker extends Worker {

	private static final Logger LOG = Logger.getLogger(SWBankWorker.class);
	//muhamad's changes
	private long custIdMax = -1;
	private long custIdMin = -1;

	private int type = -1;


	private int mddelay = 0; // default detection delay is 5 msec for malicious
	// txns

	private double[] malProp ;
	private int dbSize = 0;

	SWBankBenchmark benchmarkModule;
	Stack<Long> idRange;



	public SWBankWorker(BenchmarkModule benchmarkModule, int id) {
		super(benchmarkModule, id);
		this.benchmarkModule = (SWBankBenchmark) benchmarkModule;

		int noWorkers = benchmarkModule.getWorkloadConfiguration().getTerminals();
		mddelay = benchmarkModule.getWorkloadConfiguration().getXmlConfig().getInt("mddelay");
		dbSize = benchmarkModule.getWorkloadConfiguration().getXmlConfig().getInt("dbsize");
		long _custIdMax = dbSize;
		long custRange = _custIdMax / noWorkers;


		custIdMin = 1;
		custIdMax = _custIdMax;

		ArrayList<Long> rangeToCover = new ArrayList<Long>();
		for(int i=0;i<custRange;i++)
			rangeToCover.add((long)i);
		long seed = System.nanoTime();
		Collections.shuffle(rangeToCover, new Random(seed));
		idRange = new Stack<Long>();
		idRange.addAll(rangeToCover);
		

		LOG.info(String.format("workerId=%d, custIdMin=%d, custIdMax=%d, custRange=%d,  ", id, custIdMin, custIdMax, custRange));


	}

	@SuppressWarnings("unchecked")
	@Override
	protected TransactionStatus executeWork(TransactionType txnType) throws UserAbortException, SQLException {
		
		int[] activeTerminals = benchmarkModule.getWorkloadConfiguration().getWorkloadState().getCurrentPhase().getActiveTerminals();
		List<Double> weights = benchmarkModule.getWorkloadConfiguration().getWorkloadState().getCurrentPhase().getWeights();
		ArrayList<ArrayList<Integer>> workMember = benchmarkModule.getWorkloadConfiguration().getWorkloadState().getCurrentPhase().getWorkerMbmr();
		List<Integer> t = Arrays.asList(ArrayUtils.toObject(activeTerminals));
//				LOG.info("Active terminals " + t.toString());

		
		if(!t.contains(Integer.valueOf(this.getId()))){	
			return TransactionStatus.IGNORED;
		}
		
		

		List<Double> tempWeights = new ArrayList<Double>(weights); 
		Collections.copy(tempWeights, weights);
		int ii=0;

		for(ArrayList<Integer> tempList:workMember){
			if(!tempList.contains(this.getId()))
				tempWeights.set(ii, 0.0);
			ii++;
		}

		if(txnType.getProcedureClass().equals(Balance.class)){
			type = 0;
		}
		else if(txnType.getProcedureClass().equals(SendPayment.class)){
			type = 1;
		}
		else if(txnType.getProcedureClass().equals(Distribute.class)){
			type = 2;
		}
		else if(txnType.getProcedureClass().equals(Collect.class)){
			type = 3;
		}
		else if(txnType.getProcedureClass().equals(MNTransfer.class)){
			type = 4;
		}
		if(tempWeights.get(type)<=0)
			return TransactionStatus.IGNORED;

//		LOG.info("Tx type " + type);
		long txid = -1;
		try {
			if (txnType.getProcedureClass().equals(Balance.class)) {
				Balance proc = getProcedure(Balance.class);
				assert (proc != null);

				//				long acctId = rdg.nextLong(acctIdMin + 1, acctIdMax);
				long acctId = ThreadLocalRandom.current().nextLong(custIdMin ,custIdMax+1);
				txid = proc.run(conn, acctId);

				AIMSLogger.logTransactionSpecs(1, String.format("%d, %d,%d,%s",txid, acctId, this.getId(),";"));
			}else if (txnType.getProcedureClass().equals(SendPayment.class) ) {

				// generate random parameters
				long srcCustId = -1;
				long destCustId;
				if(!idRange.isEmpty())
					destCustId = idRange.pop();
				else
					destCustId = ThreadLocalRandom.current().nextLong(custIdMin,custIdMax+1);
				// make sure that it is not a self transfer
				if(!idRange.isEmpty())
					srcCustId = idRange.pop();
				else
					srcCustId = ThreadLocalRandom.current().nextLong(custIdMin,custIdMax+1);
				
				
				while (destCustId == srcCustId) {
					destCustId = ThreadLocalRandom.current().nextLong(custIdMin,custIdMax+1);
				}
//				LOG.info("Preparing a send payment tx ");
				double balv = (float) ThreadLocalRandom.current().nextDouble(0,0.01);

				SendPayment proc = getProcedure(SendPayment.class);
				assert (proc != null);

				txid = proc.run(conn, srcCustId, destCustId, balv);
//				LOG.info("Worker  "+ this.getId() + "  Tx: " + txid + " SendPayment: " + 1 + "->" +1);
				AIMSLogger.logTransactionSpecs(2, String.format("%d,%d,%d,%3f,%d,%s", txid,srcCustId, destCustId, balv, this.getId(),new Timestamp(System.currentTimeMillis())+";"));
			} else if (txnType.getProcedureClass().equals(Distribute.class)) {

				List<SubnodeConfiguration> sncs = this.getBenchmarkModule().getWorkloadConfiguration().getXmlConfig().configurationsAt("transactiontypes/transactiontype[name='Distribute']");
				if (sncs.size() != 1) {
					throw new RuntimeException("Duplicate transaction types: Distribute");
				}

				assert (sncs.size() == 1);
				// LOG.info("size of sncs:"+sncs.size());

				SubnodeConfiguration snc = sncs.get(0);

				int fanout_n = snc.getInt("fanout_n");
				double fanout_p = snc.getDouble("fanout_p");
				int fanout_val = getBinomial(fanout_n, fanout_p);
				if(fanout_val<2)
					fanout_val = 2;
				//				LOG.info("Worker " + this.getId() + " Number of fan out " + fanout_val);

				long[] accIds = nextDistinctKLongs(custIdMin, custIdMax, fanout_val + 1);
				long[] destIds = Arrays.copyOfRange(accIds, 1, accIds.length);

				double srcFrac = (float) ThreadLocalRandom.current().nextDouble(0,0.01);
				long srcId = -1;
				//				if(last==null){
				//					last = destIds;
				//					srcId = accIds[0];
				//				}else{
				//					srcId = last[rdg.nextInt(0, last.length-1)];
				//					last = destIds;
				//				}
				srcId = accIds[0];

				Distribute proc = getProcedure(Distribute.class);

//				txid = proc.run(conn, srcId, destIds, srcFrac);

				StringBuilder sb1 = new StringBuilder();
				StringBuilder sb2 = new StringBuilder();
				for (int i = 0; i < destIds.length; i++) {
					if (i != 0) {
						sb1.append(':');
					}
					sb1.append(destIds[i]);
				}
				sb2.append(String.format("%3f", srcFrac));
				if(txid!=-1){
//					LOG.info("Worker  "+ this.getId() + "  Tx: " + txid + " Distribute: " + 1 + "->" +fanout_val);
					AIMSLogger.logTransactionSpecs(3, String.format("%d,%d,%s,%s,%d,%s", txid,srcId, sb1.toString(), sb2.toString(), this.getId(),new Timestamp(System.currentTimeMillis())+";"));
				}
			} else if (txnType.getProcedureClass().equals(Collect.class)) {

				List<SubnodeConfiguration> sncs = this.getBenchmarkModule().getWorkloadConfiguration().getXmlConfig().configurationsAt("transactiontypes/transactiontype[name='Collect']");

				if (sncs.size() != 1) {
					throw new RuntimeException("Duplicate transaction types: Collect");
				}
				assert (sncs.size() == 1);
				SubnodeConfiguration snc = sncs.get(0);

				int fanin_n = snc.getInt("fanin_n");
				double fanin_p = snc.getDouble("fanin_p");
				int fanin_val = getBinomial(fanin_n, fanin_p);
				if(fanin_val<2)
					fanin_val = 2;

				long[] accIds = nextDistinctKLongs(custIdMin, custIdMax, fanin_val + 1);
				//				if(last!=null){
				//					for(int i=1;i<last.length && i< accIds.length;i++)
				//						accIds[i] = last[i];
				//				}
				long[] srcIds = Arrays.copyOfRange(accIds, 1, accIds.length);
				long destId = accIds[0];
				//				double[] src_vals = nextKDoubles(0, 1, 100, fanin_val);
				//				double[] src_vals = nextKDoubles(0, 0.1, 1, fanin_val);
				double srcFrac = (float) ThreadLocalRandom.current().nextDouble(0,(double)1/fanin_val);
				Collect proc = getProcedure(Collect.class);
//				txid = proc.run(conn, srcIds, destId, srcFrac);

				StringBuilder sb1 = new StringBuilder();
				StringBuilder sb2 = new StringBuilder();
				for (int i = 0; i < srcIds.length; i++) {
					if (i != 0) {
						sb1.append(':');
					}
					sb1.append(srcIds[i]);

				}
				sb2.append(String.format("%3f", srcFrac));
				if(txid!=-1){
//					LOG.info("Worker  "+ this.getId() + " Tx: " + txid + " Collect: " + fanin_val + "->" + 1);
					AIMSLogger.logTransactionSpecs(4, String.format("%d,%s,%d,%s,%d,%s", txid,sb1.toString(), accIds[0], sb2.toString(), this.getId(),new Timestamp(System.currentTimeMillis())+";"));
				}

			} else if (txnType.getProcedureClass().equals(MNTransfer.class)) {
				List<SubnodeConfiguration> sncs = null;
				try{
					sncs = this.getBenchmarkModule().getWorkloadConfiguration().getXmlConfig().configurationsAt("transactiontypes/transactiontype[name='MNTransfer']");
					if (sncs.size() != 1) {
						throw new RuntimeException("Duplicate transaction types: MNTrasnfer");
					}
				}catch(Exception e){
					LOG.info("MNTransfere transaction information doesn't exist in Config file");
				}


				assert (sncs.size() == 1);
				SubnodeConfiguration snc = sncs.get(0);

				int fanin_n = snc.getInt("fanin_n");
				double fanin_p = snc.getDouble("fanin_p");

				int fanout_n = snc.getInt("fanout_n");
				double fanout_p = snc.getDouble("fanout_p");

				//				int fanin_val = rdg.nextBinomial(fanin_n, fanin_p);
				int fanin_val = getBinomial(fanin_n, fanin_p);
				if(fanin_val<2)
					fanin_val = 2;

				//				int fanout_val = rdg.nextBinomial(fanout_n, fanout_p);
				int fanout_val = getBinomial(fanout_n, fanout_p);
				if(fanout_val<2)
					fanout_val = 2;

				//				LOG.info("F/in " + fanin_val + " f/out " + fanout_val);

				long[] accIds = nextDistinctKLongs(custIdMin, custIdMax, fanin_val + fanout_val);
				long[] srcIds = Arrays.copyOfRange(accIds, 0, fanin_val);
				long[] destIds = Arrays.copyOfRange(accIds, fanin_val, accIds.length);
				//				double[] fractions= nextKDoubles(0, (double)1/fanout_val, 1, fanin_val + fanout_val);
				double srcFrac = (float) ThreadLocalRandom.current().nextDouble(0,(double)1/fanin_val);
				//				LOG.info("Fractions size " + fractions.length);
				MNTransfer proc = getProcedure(MNTransfer.class);

//				txid = proc.run(conn, srcIds, destIds, srcFrac);

				StringBuilder sb1 = new StringBuilder();
				StringBuilder sb2 = new StringBuilder();
				StringBuilder sb3 = new StringBuilder();
				for (int i = 0; i < srcIds.length; i++) {
					if (i != 0) {
						sb1.append(':');
					}
					sb1.append(srcIds[i]);

				}
				sb2.append(String.format("%3f", srcFrac));
				for (int i = 0; i < destIds.length; i++) {
					if (i != 0) {
						sb3.append(':');
					}
					sb3.append(destIds[i]);
				}
				if(txid!=-1){
//					LOG.info("Worker  "+ this.getId() + " Tx: " + txid + " MNTransfer: " + fanin_val + "->" + fanout_val + " frac " + srcFrac);
					AIMSLogger.logTransactionSpecs(5, String.format("%d,%s,%s,%s,%d,%s", txid,sb1.toString(), sb3.toString(), sb2.toString(), this.getId(),new Timestamp(System.currentTimeMillis())+";"));
				}

			} 

			else {

				throw new InvalidTransactionTypeException("Invalid transaction type: " + txnType.getProcedureClass().getName());

			}


		} catch (InvalidTransactionTypeException e) {
			//			LOG.info("Error! 1");
			e.printStackTrace();
			return TransactionStatus.RETRY_DIFFERENT;

		} catch (RuntimeException e) {
			//			LOG.info("Error! 2");
			conn.rollback();
			// LOG.info("Unknown transaction type");
			e.printStackTrace();
			return TransactionStatus.RETRY;
		}

		conn.commit();
		//		LOG.info("Worker " + this.getId() + " committed and free");


//		malProp = this.getMalProp();
		double genProp = ThreadLocalRandom.current().nextDouble();
//		if(txid!=-1 && mt.contains(Integer.valueOf(this.getId()))&&genProp<malProp[type]){
		if(txid!=-1 && genProp<malProp[type]){
			LOG.info("Malicious disclaimer " + txid + " at " + this.getId());
			Connection resConnection ;
			Connection recConnection ;
			try {
				resConnection = this.benchmarkModule.makeConnection();
				resConnection.setAutoCommit(false);
				resConnection.setTransactionIsolation(this.wrkld.getIsolationMode());

				recConnection = this.benchmarkModule.makeConnection();
				recConnection.setAutoCommit(false);
				recConnection.setTransactionIsolation(this.wrkld.getIsolationMode());
			} catch (SQLException ex) {
				throw new RuntimeException("Failed to connect to database", ex);
			}
			//				exservice.schedule(new IDMessageSender(conn,txid,mddelay), mddelay, TimeUnit.MILLISECONDS);
			try{
				new Thread(new IDMessageSender(txid,mddelay,benchmarkModule)).start();
				//					LOG.info("Back here!!" + txid);
			}catch(SQLException e){
				LOG.info("Hahaha ");
				e.printStackTrace();
			}

		}
		benchmarkModule.getWorkloadConfiguration().getWorkloadState().incSuccesfulTx();
		//		LOG.info("Succ tx " + benchmarkModule.getWorkloadConfiguration().getWorkloadState().getSuccesfulTx());
		return TransactionStatus.SUCCESS;

	}

	// returns k distinct long values
	private long[] nextDistinctKLongs(long min, long max, int k) {
		long[] res;
		Set<Long> distLongs = new HashSet<Long>();
		
		while(!idRange.isEmpty() && distLongs.size()<k){
			distLongs.add(idRange.pop());
		}
		

		while(distLongs.size()<k){
			//			LOG.info("Can't find distinct!!");
			distLongs.add(ThreadLocalRandom.current().nextLong(min, max+1));
		}
		ArrayList<Long> temp = new ArrayList<Long>(distLongs);
		res = ArrayUtils.toPrimitive(temp.toArray(new Long[k]));
		return res ;
	}

	/*
	 * (non-Javadoc)
	 * @see com.oltpbenchmark.api.Worker#tearDown(boolean)
	 */
	@Override
	public void tearDown(boolean error) {
		super.tearDown(error);

		// gracefully shutdown executor service
		// make sure all IDS are delivered.

		//		exservice.shutdown();

	}

	public static int getBinomial(int n, double p) {
		// Sometimes p =1, to avoid -inf log
		final double epsilon = Double.MIN_VALUE;
		double log_q = Math.log(1.0 - p + epsilon);
		int x = 0;
		double sum = 0;
		for(;;) {
			sum += Math.log(ThreadLocalRandom.current().nextDouble()) / (n - x);
			if(sum < log_q) {
				return x;
			}
			x++;
		}
	}
}
