/**
 * Created by: Muhamad Felemban 
 * Jun 19, 2017
 */
package com.oltpbenchmark.benchmarks.smallworldbank;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

import org.apache.log4j.Logger;

import com.oltpbenchmark.api.BenchmarkModule;

/**
 * @author mfelemban
 *
 */
public class Utils {
	private static final Logger LOG = Logger.getLogger(Utils.class);
	
	public static String printArrayList(ArrayList<Double> a){
		String out ="";
		for(Double d:a){
			out += d + ",";
		}
		return out;
	}
	
	public static double getNextExp(double lambda) {
		Random rand = new Random();
		return  Math.log(1-rand.nextDouble())/(-lambda);
	}
	
	public static String printArray(long[] a){
		String output = "";
		for(int i=0;i<a.length;i++){
			if(i<a.length-1)
				output += a[i]+":";
			else
				output += a[i];
		}
		return output;	
	}
	
	
	// 
	public static void uploadHotspotTuples(BenchmarkModule benchmarkModule, Set<Long> hotspots){
		try{
			Connection conn = benchmarkModule.makeConnection();
			conn.setAutoCommit(true);
			conn.setTransactionIsolation(benchmarkModule.getWorkloadConfiguration().getIsolationMode());
			String selectChecking = "Select null, null, oid, chk_id, balance from checking where chk_id in (";
			for(Long t:hotspots)
				selectChecking += t + ",";
			selectChecking = selectChecking.substring(0,selectChecking.length()-1) + ")";
//			LOG.info(selectChecking);
			PreparedStatement ps = conn.prepareStatement("insert into hotspot_backup (" + selectChecking + ")");
//			LOG.info(ps.toString());
			ps.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	

	
	public static void emptyDB(BenchmarkModule benchmarkModule){
		String[] tables = {"log_table","temp_log_table","corrupted_transactions_table","malicious_transactions_table","blocked_tuples_table","blocked_transactions_table","checking_backup","temp_tuples_table_AIMS","blocked_tuples_table_aims","blocked_tuples_status","benign_transaction_table","checking_backup","redo_transactions_table","temp_alpha_checker","temp_transactions_type","transactions_commit_time","transaction_id_map","avail_status","repair_table","availability_info","touched_boundary_objects"};
//		String[] tables = {"log_table"};
		//		String[] tables = {"log_table","temp_log_table","corrupted_transactions_table","malicious_transactions_table","blocked_transactions_table","checking_backup","temp_tuples_table_AIMS","blocked_tuples_table_aims","blocked_tuples_status","benign_transaction_table","checking_backup","redo_transactions_table","temp_alpha_checker","temp_transactions_type","transactions_commit_time","transaction_id_map","avail_status","repair_table","availability_info","touched_boundary_objects"};
		try {
			Connection conn = benchmarkModule.makeConnection();
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(benchmarkModule.getWorkloadConfiguration().getIsolationMode());
			PreparedStatement ps = conn.prepareStatement("update checking set balance = 10000");
			int count = ps.executeUpdate();
			LOG.info("Updated " + count + " in Checking");
//			ps= conn.prepareStatement("update boundary_objects set on_hold = 0");
//			count = ps.executeUpdate();
			ps = conn.prepareStatement("delete from hotspot_backup");
			ps.executeUpdate();
			LOG.info("Updated " + count + " in Bonudary objects");
			for(String table: tables){
				ps = conn.prepareStatement("delete from " + table);
				count = ps.executeUpdate();
				LOG.info("Deleted " + count + " from " + table);
			}

			conn.commit();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	
	
}
